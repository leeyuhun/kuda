package com.escrow.kuda.model;

import lombok.*;

@Data
public class E_remittance_info {
    
    private String eri_account_number;
    private String eri_account_holder;
    private long eri_amount;
    private String eri_state;
    private String bank_code;
    private String esl_code;
    private int seller_code;
    private String car_owner_ph;


}

