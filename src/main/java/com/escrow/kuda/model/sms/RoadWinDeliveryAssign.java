package com.escrow.kuda.model.sms;

import lombok.Data;

@Data
public class RoadWinDeliveryAssign {
    private String sellerName;
    private String sellerPh;
    private String reqName;
    private String reqPh;
    private String carOwnerPh;
    private long csmPrice;
    private long eslPrice;
    private String startAddress;
    private String endAddress;
    private String productName;
    private String orderNo;
    private String deliverName;
    private String deliverPh;

}
