package com.escrow.kuda.model.sms;

import lombok.Data;

@Data
public class RefundInfo {
    private String productName;
    private String reqPh;
    private String sellerPh;
    private String ownerPh;
    private long eslPrice;
    private String orderNo;

}
