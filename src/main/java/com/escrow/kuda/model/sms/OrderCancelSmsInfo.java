package com.escrow.kuda.model.sms;

import lombok.Data;

@Data
public class OrderCancelSmsInfo {
    private String sellerName;
    private String sellerPh;
    private String reqName;
    private String reqPh;
    private long eslPrice;
    private String productName;
    private String orderNo;
}
