package com.escrow.kuda.model.sms;

import lombok.Data;

@Data
public class RoadwinDeliveryStart {
    private String sellerName;
    private String sellerPh;
    private String reqName;
    private String reqPh;
    private String carOwnerPh;
    private String startAddress;
    private String endAddress;
    private String deliverName;
    private String deliverPh;
    private String productName;
    private String serviceViewLink;
    private String orderNo;


}
