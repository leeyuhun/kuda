package com.escrow.kuda.model.sms;

import lombok.Data;

@Data
public class RoadWinOrderModify {

    private String sellerName;
    private String sellerPh;
    private String reqName;
    private String reqPh;

    private String startAddress;
    private String endAddress;
    private String productName;
    private String orderNo;
    private long eslPrice;
    private long csmPricePre;
    private long csmPriceNext;
}
