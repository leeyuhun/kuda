package com.escrow.kuda.model.excel;

import lombok.Data;

@Data
public class EsRemitBank {

    private String bankCode;
    private String bankNumber;
    private long amount;
    private String bankHolder;
    private String memo;
}
