package com.escrow.kuda.model.infobank;


import jdk.nashorn.internal.objects.annotations.Constructor;
import lombok.*;

@Getter
@Setter
@Builder
//@Constructor
public class MessageData {
    private String title; //메시지 제목 (최대 길이40byte) 메시지 제목은 UTF-8과 URL 인코딩 필요
    private String from; //발신번호 (최대 길이16byte) ex) 한국 발송 시 01029652189 (국가코드 미포함)
    private String text; //메시지 내용
    private String fileKey; //MMS 발송 시 파일 키
    private String destinations; //수신번호 그룹
    private String to; //수신번호 (국제표준) ex) 한국 발송 시 821029652189
    private String replaceWord1; //치환문구1
    private String replaceWord2; //치환문구2
    private String replaceWord3; //치환문구3
    private String replaceWord4; //치환문구4
    private String replaceWord5; //치환문구5
    private String ref; //여분 필드, 고객이 설정할 수 있는 필드 값(최대 길이 20byte, 영/숫자만 가능)
    private int ttl; //메시지 유효 시간(초 단위)
    private String paymentCode; //정산용 부서코드 (최대 길이 20byte)
    private String clientSubId; //Sender ID , 메시지 서명 을 복수로 지정하기 위한 구분자 (최대 길이 20byte)


//    public MessageData(String title, String from, String text, String fileKey, String destinations, String to, String replaceWord1, String replaceWord2, String replaceWord3, String replaceWord4, String replaceWord5, String ref, int ttl, String paymentCode, String clientSubId) {
//        this.title = title;
//        this.from = from;
//        this.text = text;
//        this.fileKey = fileKey;
//        this.destinations = destinations;
//        this.to = to;
//        this.replaceWord1 = replaceWord1;
//        this.replaceWord2 = replaceWord2;
//        this.replaceWord3 = replaceWord3;
//        this.replaceWord4 = replaceWord4;
//        this.replaceWord5 = replaceWord5;
//        this.ref = ref;
//        this.ttl = ttl;
//        this.paymentCode = paymentCode;
//        this.clientSubId = clientSubId;
//    }
}
