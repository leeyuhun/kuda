package com.escrow.kuda.model.infobank;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class TokenIssuanceVo {

    private String accessToken;
    private String schema;

}
