package com.escrow.kuda.model.infobank;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class MessageSubmitResult {

    private String ref;
    private String toCount;
    private String groupId;
    private String[] destinations;



//    public class destinations{
//
//        private String messageId;
//        private String to;
//        private String status;
//        private String errorText;
//
//    }
}
