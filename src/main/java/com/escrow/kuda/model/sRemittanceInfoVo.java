package com.escrow.kuda.model;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class sRemittanceInfoVo {

    private String ri_account_number ;
    private String ri_account_holder;
    private String bank_name;
    private String bank_code;

}
