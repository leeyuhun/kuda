package com.escrow.kuda.model;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class SearchAdd {

    private String productNum;
    private String productName;
    private String Amt;
    private String essUserCode;
}
