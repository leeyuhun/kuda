package com.escrow.kuda.model.band.bandlist;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class BandList {


    private int result_code;
    private result_data result_data;


}



