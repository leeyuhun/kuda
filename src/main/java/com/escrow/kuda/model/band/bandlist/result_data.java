package com.escrow.kuda.model.band.bandlist;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

/*
* 밴드 목록 리스트
* 밴드명과 식별키만 추출
*
* */


@Getter
@Setter
@ToString
@NoArgsConstructor
public class result_data {
    private List<bands> bands;

}


