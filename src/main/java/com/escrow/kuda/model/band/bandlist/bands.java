package com.escrow.kuda.model.band.bandlist;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class bands {
    private String name;
    private String band_key;
    private String cover;
    private int member_count;

}
