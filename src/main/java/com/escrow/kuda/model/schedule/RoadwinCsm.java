package com.escrow.kuda.model.schedule;

import lombok.Data;

@Data
public class RoadwinCsm {

    private String sellerName;
    private String sellerPh;
    private String reqName;
    private String reqPh;
    private String orderNo;
    private String productName;
    private String startAddress;
    private String endAddress;
    private Long eslPrice;
    private Long csmPrice;
    private String scheduleTime;

}
