package com.escrow.kuda.model;

import lombok.Data;

@Data
public class GetAddress {
    private String address;
    private String addressDetail;
}
