package com.escrow.kuda.model;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class Escrow_list {

    private String esl_code;
    private long esl_payment_amount;
    private long esl_all_amount;
    private long esl_fee_amount;
    private String esr_code;
    private String ess_code;
    private String esl_request_date;
    private String esl_request_time;
    private String esl_end_date;
    private String esl_end_time;
    private String esl_bank_code;
    private String esl_bank_name;
    private String esl_bank_number;
    private String esl_exp_date;
    private String esl_exp_time;
    private String esl_auth_date;
    private String esl_auth_time;
    private String esl_product_name;
    private String esl_address;
    private String esl_address_detail;
}
