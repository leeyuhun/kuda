package com.escrow.kuda.model;


import com.escrow.kuda.Role;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString

public class Seller {



    private Role role;

    private int seller_code;
    private String seller_default_fee;
    private String login_id;
    private String ps_code;
    private String area_code;
    private String seller_name;
    private String seller_email;
    private String seller_picture;
    private String login_grade;
    private String seller_oauth_key;
    private String seller_ph;
    private String seller_state;
    private String seller_company;
    private String seller_address;
    private String seller_address_detail;


//    @Builder
//    public Seller(String seller_name, String seller_email, String seller_picture, String login_grade) {
//        this.seller_name = seller_name;
//        this.seller_email = seller_email;
//        this.seller_picture = seller_picture;
//        this.login_grade = login_grade;
//    }


    public Seller update(String seller_name , String seller_picture){
        this.seller_name = seller_name;
        this.seller_picture = seller_picture;

        return this;
    }

    public String getRoleKey(){
        return this.role.getKey();
    }
}
