package com.escrow.kuda.model.camel;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class SellerUpdate {
    private int sellerCode;
    private String company;
    private String empNum;
    private String ph;
    private String bankCode;
    private String bankNum;
    private String bankHolder;
    private String address;
    private String addressDetail;

}
