package com.escrow.kuda.model.camel;


import lombok.Data;

@Data
public class EscrowRefundInfoMap {
    private String sellerPh;
    private String eslPrice;
}
