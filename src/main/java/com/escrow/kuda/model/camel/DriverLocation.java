package com.escrow.kuda.model.camel;

import lombok.Data;

@Data
public class DriverLocation {

    private double lat;
    private double lon;
    private String productName;
    private String sellerName;
    private String sellerPh;
    private String driverHp;
    private String driverName;
    private String startAddress;
    private String startAddressDetail;
    private String endAddress;
    private String endAddressDetail;
    private String rwId;
    private String csmStatus;


    private String resultCode;
    private String resultMsg;
}
