package com.escrow.kuda.model.camel;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class UpRemainInfo {

    private int userCode;
    private String phoneNum;
    private String departName;
    private String departNum;
    private String bankHolder;
    private String bankNumber;
    private String bankCode;
    private String address;
    private String addressDetail;
    private String sellerName;


}
