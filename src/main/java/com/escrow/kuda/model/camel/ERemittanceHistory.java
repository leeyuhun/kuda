package com.escrow.kuda.model.camel;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class ERemittanceHistory {

    private String bankCode;
    private String bankNumber;
    private String bankName;
    private String bankHolder;
    private String eslCode;
}
