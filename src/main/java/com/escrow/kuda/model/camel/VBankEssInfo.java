package com.escrow.kuda.model.camel;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class VBankEssInfo {

    private String essName;

    private String essPh;

}
