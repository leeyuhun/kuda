package com.escrow.kuda.model.camel;

import com.escrow.kuda.model.Bank;
import lombok.Data;

import java.util.List;

@Data
public class UserRefund {
    private String OrderNo;
    private String productName;
    private String eslPrice;
    private String sellerName;
    private String sellerPh;
    private List<Bank> bankList;
    private String bankHolder;
    private String bankName;
    private String bankNum;
    private String bankCode;

}
