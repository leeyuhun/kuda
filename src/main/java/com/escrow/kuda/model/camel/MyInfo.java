package com.escrow.kuda.model.camel;

import lombok.Data;

@Data
public class MyInfo {

    private String myHolder;
    private String myPh;
    private String myAddress;
    private String myAddressDetail;

}
