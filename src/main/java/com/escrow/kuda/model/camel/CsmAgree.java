package com.escrow.kuda.model.camel;

import lombok.Data;

@Data
public class CsmAgree {


    private String esrName;
    private String esrPh;
    private String esrCompany;
    private String essName;
    private String essPh;
    private String startAddress;
    private String startAddressDetail;
    private String endAddress;
    private String endAddressDetail;
    private String productName;
    private String price;



}
