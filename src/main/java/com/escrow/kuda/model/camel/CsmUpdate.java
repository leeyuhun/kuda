package com.escrow.kuda.model.camel;

import lombok.Data;

@Data
public class CsmUpdate {
    private String startAddress;
    private String endAddress;
    private String csmPrice;
    private String reqName;
    private String reqPh;
    private String sellerName;
    private String sellerPh;
    private String productName;
    private String priceType;
    private String rwId;
}
