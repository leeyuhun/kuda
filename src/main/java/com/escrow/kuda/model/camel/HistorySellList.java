package com.escrow.kuda.model.camel;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class HistorySellList {

    private String productName;
    private String userName;
    private String userPh;
    private long amount;
    private String endDate;
    private String code;

}
