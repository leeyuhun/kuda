package com.escrow.kuda.model.camel;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class ProfileUpdate {

    private int usercode;
    private String company;
    private String empNum;
    private String ph;
    private String bankCode;
    private String bankHolder;
    private String bankNum;
    private String bankName;
    private String address;
    private String addressDetail;
}
