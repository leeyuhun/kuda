package com.escrow.kuda.model.camel;

import lombok.Data;

@Data
public class Consignment {

    private String orderCode;
    private String memo;

    private String requesterName;
    private String requesterPh;
    private String requesterAddress1;
    private String requesterAddress2;
    private long srCode;

    private String sellerName;
    private String sellerPh;
    private String sellerAddress1;
    private String sellerAddress2;
    private String sellerCompany;
    private long ssCode;

    private long price;

    private String productName1;
    private String productName2;


}

