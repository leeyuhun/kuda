package com.escrow.kuda.model.camel;


import lombok.*;

import java.io.Serializable;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor

public class EscrowProcess implements Serializable {

    private String productNum;
    private String productName;
    private String allAmountStr;
    private String esrCode; //요청자Table pk
    private String essCode; //판매자Table pk
    private long allAmount;
    private long payAmount;
    private long feeAmount;
    private String requestDate;
    private String requestTime;
    private String productNumName;
    private String esl_code;

    private int esrUserCode; //요청자
    private int essUserCode; //판매자

}
