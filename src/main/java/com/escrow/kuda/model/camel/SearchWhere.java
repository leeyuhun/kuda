package com.escrow.kuda.model.camel;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class SearchWhere {

    private String searchDiv;
    private String searchName;
    private int userCode;
}
