package com.escrow.kuda.model.camel;


import lombok.Data;

@Data
public class TrnscRgstr {

    private String code;
    private String essPh;
    private String essName;
    private String productName;
    private String productNum;
    private String essCompany;
    private long price;


    /*탁송*/
    private String startAddress;
    private String startAddressDetail;
    private String endAddress;
    private String endAddressDetail;
    private String csmState;
    private String deliName;
    private String deliPh;
    private long DeliveryCost;

    private String eslStatus;


}
