package com.escrow.kuda.model.camel;

import lombok.Data;

@Data
public class Delivery {
    private String productName;
    private String productNum;
    private String esrPh;
    private String esrName;
    private String esrCompany;
    private String address;
    private String addressDetail;

    private String bankCode;
    private String account;
    private String holder;
}
