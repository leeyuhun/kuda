package com.escrow.kuda.model.camel;

import lombok.Data;

@Data
public class EscrowInsert {

    private String productNum;
    private String productName;
    private String allAmountStr;
    private String esrCode; //요청자Table pk
    private String essCode; //판매자Table pk
    private long allAmount;
    private long payAmount;
    private long feeAmount;
    private String requestDate;
    private String requestTime;
    private String productNumName;
    private String eslCode;
    private int essUserCode;
    private int esrUserCode;
    private String bankHolder;


    /*위탁배송*/
    private String address1;
    private String addressDetail1;
    private String start_LON;
    private String start_LAT;

    private String address2;
    private String addressDetail2;
    private String end_LON;
    private String end_LAT;

    private String price_TYPE;

    private String rwID;
    private String deliveryCost;

}
