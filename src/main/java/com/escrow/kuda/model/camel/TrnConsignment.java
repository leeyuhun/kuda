package com.escrow.kuda.model.camel;


import lombok.Data;

@Data
public class TrnConsignment {

    private long price;
    private String startAddress;
    private String startAddressDetail;
    private String state;
    private String deliName;
    private String deliPh;

}
