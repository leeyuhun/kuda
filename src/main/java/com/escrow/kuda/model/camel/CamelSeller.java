package com.escrow.kuda.model.camel;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class CamelSeller {

    private int usercode;
    private String ph;
    private String email;
    private String name;
    private String company;
}
