package com.escrow.kuda.model.camel;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class StateUpdate {
    private String orderNo; // esl_code 애스크로 주문번호
    private String state; //바꿀상태
    private int userCode; //유저코드 체크 (상태에 따라 체크컬럼 변경)
    private String check; //1:esr_code  2: ess_code
}
