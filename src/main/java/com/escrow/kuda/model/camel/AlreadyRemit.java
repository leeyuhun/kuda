package com.escrow.kuda.model.camel;

import lombok.Data;

@Data
public class AlreadyRemit {

    private String holder;
    private String bankName;
    private String account;

}
