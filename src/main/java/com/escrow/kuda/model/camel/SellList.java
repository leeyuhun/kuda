package com.escrow.kuda.model.camel;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class SellList {

    private String productName;
    private String userName;
    private String userPh;
    private long amount;
    private String requestDate;
    private String code;
    private String state;

}
