package com.escrow.kuda.model.payment;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/*
* nicepay 실결제 승인결과
* */
@Getter
@Setter
@ToString
@NoArgsConstructor
public class NicePayResult {

    /*공통*/
    private String ResultCode; // 3001 : 신용카드 성공코드
    private String ResultMsg; // 결과메시지 (euc-kr)
    private String Amt; // 금액 예)1000원인 경우 -> 000000001000
    private String MID; // 상점 ID 예) nictest00m
    private String Moid; // 상점주문번호
    private String BuyerEmail; // 메일주소 예) test@abc.com
    private String BuyerTel; // 구매자 연락처
    private String BuyerName; // 구매자명
    private String GoodsName; // 상품명
    private String TID; // 거래ID 예)nictest00m01011104191651325596
    private String AuthCode; // 승인 번호 (신용카드, 계좌이체, 휴대폰)
    private String AuthDate; // YYMMDDHHMMSS, 승인 날짜
    private String PayMethod; // CARD : 신용카드
    /*공통종료*/




    /*카드*/
    private String CardCode; // 결제 카드사 코드(결과 코드 참조)
    private String CardName; // 결제 카드사 이름 예) 비씨
    private String CardNo; // 카드번호 예) 53611234****1234
    private String CardQuota; // 할부개월 예) 00(일시불) 03(3개월)
    private String CardInterest; // 0:미적용, 1:적용 (상점분담 무이자 적용여부)
    private String AcquCardCode; // 매입카드사코드 예) 06
    private String AcquCardName; // 매입카드사명 예) 신한
    private String CardCl; // 0:신용, 1:체크 (카드 구분)
    private String CcPartCl; // 0:불가능 1:가능, 부분취소 가능 여부
    private String ClickpayCl; // 간편결제구분 6:SKPAY   8:SAMSUNGPAY   15:PAYCO   16:KAKAOPAY
    private String PointAppAmt; // 포인트 승인금액 예)1000원인 경우 -> 000000001000
    /*카드종료*/



    /*가상계좌*/
    private String VbankBankCode; //결제은행코드(은행 코드 참조)
    private String VbankBankName; //결제은행명 (euc-kr)
    private String VbankNum; //가상계좌번호
    private String VbankExpDate; //가상계좌 입금만료일(yyyyMMdd)
    private String VbankExpTime; //가상계좌 입금만료시간(HHmmss)
    /*가상계좌종료*/


}
