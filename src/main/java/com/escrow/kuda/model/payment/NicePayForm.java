package com.escrow.kuda.model.payment;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class NicePayForm {



    private String PayMethod;
    private String GoodsName; //결제상품명
    private String Amt; //결제상품금액
    private String MID; //상점아이디
    private String moid; //주문번호
    private String buyerEmail; //이메일
    private String BuyerName; //구매자명
    private String BuyerTel; //구매자연락처
    private String ReturnURL; //returnURL (Mobile)
    private String VbankExpDate; //가상계좌 종료일시


    /*변경불가*/
    private String EdiDate; //yyyyMMddHHmmSSS
    private String SignData; //암호화전문


    private int essUserCode;
    private int esrUserCode;
}
