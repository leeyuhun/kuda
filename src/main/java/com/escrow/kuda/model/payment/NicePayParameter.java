package com.escrow.kuda.model.payment;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;


/*
* nicepay 인증결과
*
* */
@Getter
@Setter
@ToString
@NoArgsConstructor
public class NicePayParameter {
    private String AuthResultCode; //인증 결과 코드, 0000 : 성공 (이외 실패)
    private String AuthResultMsg; //인증 결과 메시지
    private String AuthToken; //인증 토큰
    private String PayMethod; //CARD : 신용카드  BANK : 계좌이체   VBANK : 가상계좌   CELLPHONE : 휴대폰결제
    private String MID; // 상점 아이디
    private String Moid; // 상품 주문번호
    private String Amt; // 금액
    private String ReqReserved; // 가맹점 여분 필드
    private String TxTid; // 거래 ID
    private String NextAppURL; //승인 요청 URL 최종 결제를 위한 승인 요청할 URL 입니다. 인증결과를 포함하여, NextAppURL로 POST 처리 합니다.
    private String NetCancelURL; //  망취소 요청 URL
    private String BuyerName;
    private String BuyerTel;


    /*
    * 자체
    * */
    private String orderNo; //승인주문서 주문번호
    private String payNo; //PG 주문번호(결제승인pk)

    private int essUserCode;
    private int esrUserCode;

}
