package com.escrow.kuda.model.payment;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.web.bind.annotation.GetMapping;


@Getter
@Setter
@ToString
@NoArgsConstructor
public class Vbank {

    private String ediDate;
    private String hashString;

}