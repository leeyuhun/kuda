package com.escrow.kuda.model.payment;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/*나이스페이스 가상계좌 입금 통보 */
@Getter
@Setter
@ToString
@NoArgsConstructor
public class NicePayVBankResult {

    private String PayMethod;           //지불수단
    private String MID;                 //상점ID
    private String MallUserID;          //회원사 ID
    private String Amt;                 //금액
    private String name;                //구매자명
    private String GoodsName;           //상품명
    private String TID;                 //거래번호
    private String MOID;                //주문번호
    private String AuthDate;            //입금일시 (yyMMddHHmmss)
    private String ResultCode;          //결과코드 ('4110' 경우 입금통보)
    private String ResultMsg;           //결과메시지
    private String VbankNum;            //가상계좌번호
    private String FnCd;                //가상계좌 은행코드
    private String VbankName;           //가상계좌 은행명
    private String VbankInputName;      //입금자 명
    private String CancelDate;          //취소일시

    private String RcptTID;             //현금영수증 거래번호
    private String RcptType;            //현금 영수증 구분(0:미발행, 1:소득공제용, 2:지출증빙용)
    private String RcptAuthCode;        //현금영수증 승인번호

    /*자체*/
    private String DepositDate;         //입금일
    private String DepositTime;         //입금시
}
