package com.escrow.kuda.model.payment;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class GetAuth {

    private String allAmountStr;
    private long amount;
    private int essUserCode;
    private String productNum;
    private String productName;
    private String addressFull;

}
