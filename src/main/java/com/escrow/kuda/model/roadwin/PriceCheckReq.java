package com.escrow.kuda.model.roadwin;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PriceCheckReq {

    private String start_ADDR;
    private Double start_LAT;
    private Double start_LON;
    private String end_ADDR;
    private Double end_LAT;
    private Double end_LON;

    /*
    * 요청값값
   * */
    private String request_URL;
    private String result_CODE;
    private String result_MSG;
    private String distance;
    private int price;
    private String rw_ID;
    private String rDate;
    private String rTime;

    private String esl_code;


    @Override
    public String toString() {
        return "{" +
                "\"START_ADDR\":\"" + start_ADDR + '\"' +
                ", \"START_LAT\":" + start_LAT +
                ", \"START_LON\":" + start_LON +
                ", \"END_ADDR\":\"" + end_ADDR + '\"' +
                ", \"END_LAT\":" + end_LAT +
                ", \"END_LON\":" + end_LON +
                "}";
    }
}
