package com.escrow.kuda.model.roadwin;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class StatusChange {


    private String rw_ID;
    private String status_CODE;
    private String cancel_REASON;
    private String service_VIEW_LINK;


    @Override
    public String toString() {
        return "{" +
                "\"RW_ID\"=\"" + rw_ID + "\"" +
                ", \"STATUS_CODE\"=\"" + status_CODE + "\"" +
                ", \"CANCEL_REASON\"=\"" + cancel_REASON + "\"" +
                ", \"SERVICE_VIEW_LINK\"=\"" + service_VIEW_LINK + "\"" +
                '}';
    }



}
