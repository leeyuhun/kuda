package com.escrow.kuda.model.roadwin;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ResultMsg {

    private String RESULT_CODE;
    private String RESULT_MSG;


    @Override
    public String toString() {
        return "{" +
                "\"RESULT_CODE\":\"" + RESULT_CODE + '\"' +
                ", \"RESULT_MSG\":\"" + RESULT_MSG + '\"' +
                '}';
    }
}
