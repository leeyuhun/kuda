package com.escrow.kuda.model.roadwin;

import lombok.Data;

@Data
public class ModifyReq {
    private int csmPrice;
    private String rwId;
    private String orderNo;
}
