package com.escrow.kuda.model.roadwin;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BookingReq {

    private String rw_ID;
    private String trans_ID;
    private String order_TYPE;
    private String comp_CODE;

    private String price_TYPE;
    private int price;

    private String start_ADDR;
    private String start_ADDR_DETAIL;
    private Double start_LAT;
    private Double start_LON;
    private String end_ADDR;
    private String end_ADDR_DETAIL;
    private Double end_LAT;
    private Double end_LON;


    private String customer_NAME;
    private String customer_PH;

    private String car_NO;
    private String car_TYPE;


    private String orderNo;
    private String addressDetail;
    private String rDate;
    private String rTime;

    private String insertURL;


    private String result_CODE;
    private String result_MSG;
    private String MEMO;



    @Override
    public String toString() {
        return "{" +
                "\"RW_ID\":\"" + rw_ID + '\"' +
                ", \"TRANS_ID\":\"" + trans_ID + '\"' +
                ", \"ORDER_TYPE\":\"" + order_TYPE + '\"' +
                ", \"COMP_CODE\":\"" + comp_CODE + '\"' +
                ", \"PRICE_TYPE\":\"" + price_TYPE + '\"' +
                ", \"PRICE\":" + price +
                ", \"START_ADDR\":\"" + start_ADDR + '\"' +
                ", \"START_ADDR_DETAIL\":\"" + start_ADDR_DETAIL + '\"' +
                ", \"START_LAT\":" + start_LAT +
                ", \"START_LON\":" + start_LON +
                ", \"END_ADDR\":\"" + end_ADDR + '\"' +
                ", \"END_ADDR_DETAIL\":\"" + end_ADDR_DETAIL + '\"' +
                ", \"END_LAT\":" + end_LAT +
                ", \"END_LON\":" + end_LON +
                ", \"CUSTOMER_NAME\":\"" + customer_NAME + '\"' +
                ", \"CUSTOMER_PH\":\"" + customer_PH + '\"' +
                ", \"CAR_NO\":\"" + car_NO + '\"' +
                ", \"CAR_TYPE\":\"" + car_TYPE + '\"' +
                ", \"MEMO\":\"" + "출발지 연락처:"+MEMO +'\"' +
                '}';
    }
}
