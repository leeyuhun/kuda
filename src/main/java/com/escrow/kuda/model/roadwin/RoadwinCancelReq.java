package com.escrow.kuda.model.roadwin;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RoadwinCancelReq {

    private String rw_ID;
    private String cancel_REASON;
    private String result_CODE;
    private String result_MSG;
    private String orderNo;


    @Override
    public String toString() {
        return "{" +
                "\"RW_ID\":\"" + rw_ID + '\"' +
                ", \"CANCEL_REASON\":\"" + cancel_REASON + '\"' +
                '}';
    }
}
