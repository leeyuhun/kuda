package com.escrow.kuda.model.roadwin;

import lombok.Data;

@Data
public class WorkerLocationReq {

    private String rw_ID;
    private String driver_NAME;
    private String driver_HP;

    private String result_CODE;
    private String result_MSG;
    private double lat;
    private double lon;



}
