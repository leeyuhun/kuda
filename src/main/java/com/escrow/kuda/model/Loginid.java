package com.escrow.kuda.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class Loginid {
    private String login_id;
    private String login_grade;
    private String login_pass;
    private String login_regdate;
    private String login_name;

}
