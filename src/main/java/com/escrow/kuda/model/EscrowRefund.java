package com.escrow.kuda.model;

import lombok.Data;

@Data
public class EscrowRefund {
    private long erfCode;
    private String bankHolder;
    private String bankCode;
    private String bankName;
    private String bankNum;
    private String orderNo;
    private long eslPrice;

}
