package com.escrow.kuda.model.kakao.address;

import lombok.Data;

import java.util.List;

@Data
public class KaKaoMap {

    List<GetLonLat> documents;

}
