package com.escrow.kuda.model.kakao.address;

import lombok.Data;

@Data
public class GetLonLat {
    private String x;
    private String y;

}
