package com.escrow.kuda.model.admin;

import lombok.Data;

@Data
public class EscrowListWhere {

    private String startDate;
    private String lastDate;
    private String state;
    private String searchWhere;
    private String searchText;
}
