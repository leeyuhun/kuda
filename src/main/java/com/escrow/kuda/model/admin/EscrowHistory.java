package com.escrow.kuda.model.admin;


import lombok.Data;

@Data
public class EscrowHistory {

    private String requestDate;
    private String requestTime;
    private String endDate;
    private String endTime;
    private String productName;
    private String srSellerCode;
    private String srSellerName;
    private String srSellerPh;
    private String ssSellerCode;
    private String ssSellerName;
    private String ssSellerPh;
    private long allAmount;
    private long paymentAmount;
    private long feeAmount;
    private String code;

}
