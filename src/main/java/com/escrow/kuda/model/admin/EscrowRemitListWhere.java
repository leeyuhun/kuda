package com.escrow.kuda.model.admin;

import lombok.Data;

@Data
public class EscrowRemitListWhere {

    private String[] checkBoxValues;
}
