package com.escrow.kuda.model.admin;

import lombok.Data;

@Data
public class EEndEslUpdate {

    private String orderNo;
    private String date;
    private String time;

}
