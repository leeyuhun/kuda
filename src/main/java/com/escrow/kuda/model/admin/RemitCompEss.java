package com.escrow.kuda.model.admin;

import lombok.Data;

@Data
public class RemitCompEss {

    private String essName;
    private String essPh;
}
