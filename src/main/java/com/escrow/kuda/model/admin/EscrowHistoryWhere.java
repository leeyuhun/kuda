package com.escrow.kuda.model.admin;

import lombok.Data;

@Data
public class EscrowHistoryWhere {
    private String where;
}
