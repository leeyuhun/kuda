package com.escrow.kuda.model.admin;

import lombok.Data;

@Data
public class EscrowList {

    private String requestDate;
    private String requestTime;
    private String productName;
    private String srSellerCode;
    private String srSellerName;
    private String srSellerPh;
    private String ssSellerCode;
    private String ssSellerName;
    private String ssSellerPh;
    private String state;
    private long eslAmount;
    private long csmAmount;
    private String csmState;
    private String code;
    private String driverInfo;

}
