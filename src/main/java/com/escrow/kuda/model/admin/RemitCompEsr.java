package com.escrow.kuda.model.admin;


import lombok.Data;

@Data
public class RemitCompEsr {


    private String esrName;
    private String esrPh;
    private String productName;
    private long allMoney;
    private long paymentMoney;
    private String startAddress;
    private String endAddress;

    private String essName;
    private String essPh;

    private String orderNo;

    private String carOwnerPh;
    private String deliveryCost;
}
