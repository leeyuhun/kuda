package com.escrow.kuda.model.admin;

import lombok.Data;

@Data
public class UserList {

    private String code;
    private String regDate;
    private String loginId;
    private String name;
    private String ph;
    private String state;
    private String company;

}
