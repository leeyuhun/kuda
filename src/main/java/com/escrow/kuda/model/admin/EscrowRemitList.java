package com.escrow.kuda.model.admin;

import lombok.Data;

@Data
public class EscrowRemitList {

    private String code;
    private String requestDate;
    private String requestTime;
    private long pAmount;
    private long aAmount;
    private String product;
    private String rName;
    private String sName;
    private String bankName;
    private String bankHolder;
    private String bankNumber;
    private long eriAmount;
}
