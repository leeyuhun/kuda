package com.escrow.kuda.model.admin;

import lombok.Data;

@Data
public class EscrowListDetail {
    private String elCode;
    private String ssCode;
    private String ssName;
    private String ssCompany;
    private String ssPh;
    private String srCode;
    private String srName;
    private String srCompany;
    private String srPh;
    private String productName;
    private long allAmount;
    private String startAddress;
    private String startAddressDetail;
    private String endAddress;
    private String endAddressDetail;
    private String requestDate;
    private String requestTime;
    private String accountHolder;
    private String bankNumber;
    private String bankName;
    private String bankInfo;
}
