package com.escrow.kuda.model.admin;

import lombok.Data;

@Data
public class EscrowValueCheck {
    private String code;
    private String eslCount;
}
