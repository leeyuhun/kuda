package com.escrow.kuda.model.admin;

import lombok.Data;

@Data
public class ExistsEscrowList {

    private String amount;
    private String bankHolder;

}
