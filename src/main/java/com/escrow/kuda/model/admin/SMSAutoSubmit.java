package com.escrow.kuda.model.admin;

import lombok.Data;

@Data
public class SMSAutoSubmit {

    private String data;
    private String from;
    private String to;
    private String message;

}
