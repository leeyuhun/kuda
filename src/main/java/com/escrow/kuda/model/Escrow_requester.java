package com.escrow.kuda.model;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class Escrow_requester {
    private String esr_code;
    private int seller_code;
}
