package com.escrow.kuda.model.util;

import lombok.Data;

@Data
public class DeliveryCompInfo {

    private String esrName;
    private String esrPh;
    private String productName;
    private long allMoney;
    private long paymentMoney;
    private String essName;
    private String essPh;

    private String orderNo;


    private String calDate;

    private String account;
    private String holder;
    private String bankName;

    private String carOwnerPh;

}
