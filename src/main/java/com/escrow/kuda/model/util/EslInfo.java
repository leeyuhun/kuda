package com.escrow.kuda.model.util;

import lombok.Data;

@Data
public class EslInfo {

    private String productName;
    private long pAmount;
    private long aAmount;
    private long fAmount;
    private String eSellerPh;
    private String eRequesterPh;
    private String aNumber;
    private String aHolder;
    private String bankName;
    private String code;
    private String carOwnerPh;

}
