package com.escrow.kuda.model.util;

import lombok.Data;

@Data
public class RemitEsl {

    private String productName;
    private long pAmount;
    private String orderNo;
    private String bankName;
    private String calStr;
    private String eSellerPh;
    private String eSellerName;
    private String eRequesterPh;
    private String eRequesterName;
    private String carOwnerPh;
}
