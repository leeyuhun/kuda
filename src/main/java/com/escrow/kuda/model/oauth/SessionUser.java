package com.escrow.kuda.model.oauth;

import com.escrow.kuda.model.Seller;
import lombok.Getter;
import lombok.ToString;

import java.io.Serializable;

@Getter
@ToString
public class SessionUser implements Serializable {

    private static final long serialVersionUID =1L;

    private String name;
    private String email;
    private String picture;
    private int userCode;
    private String userState;
    private String userPh;
    private String userGrade;
//    private String oauthKey;

//    public SessionUser(String name, String email, String picture, int userCode, String userState, String userPh, String oauthKey) {
//        this.name = name;
//        this.email = email;
//        this.picture = picture;
//        this.userCode = userCode;
//        this.userState = userState;
//        this.userPh = userPh;
//        this.oauthKey = oauthKey;
//    }


    public SessionUser(Seller seller) {
        this.name = seller.getSeller_name();
        this.email = seller.getSeller_email();
        this.picture = seller.getSeller_picture();
        this.userCode = seller.getSeller_code();
        this.userState = seller.getSeller_state();
        this.userPh = seller.getSeller_ph();
        this.userGrade = seller.getLogin_grade();
    }


}
