package com.escrow.kuda.schedule;

import com.escrow.kuda.service.RoadwinServiceImpl;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;


@Component
@Log4j
@RequiredArgsConstructor
public class RoadwinSchedule {

    private final RoadwinServiceImpl roadwinService;



    //1분마다 실행
    @Scheduled(cron = "0/60 * * * * *")
    public void testSchedule() {

        roadwinService.csmSchedule();

    }

}
