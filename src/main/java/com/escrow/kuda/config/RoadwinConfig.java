package com.escrow.kuda.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class RoadwinConfig {

    public static String baseURL;
    @Value("${roadwin.base.url}")
    public void setBaseURL(String baseUrl) {
        RoadwinConfig.baseURL = baseUrl;
    }


    public static String apiKey;
    @Value("${roadwin.api.key}")
    public void setApiKey(String apiKey) {
        RoadwinConfig.apiKey = apiKey;
    }
}
