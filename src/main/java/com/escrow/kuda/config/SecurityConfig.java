package com.escrow.kuda.config;

import com.escrow.kuda.Role;
import com.escrow.kuda.service.UserOAuthService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.CorsUtils;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

@Configuration
@EnableWebSecurity
@RequiredArgsConstructor
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private UserOAuthService userOAuthService;

    @Override
    protected void configure(HttpSecurity http) throws Exception {



//        http.csrf().ignoringAntMatchers("/rwin/**")
//                .csrfTokenRepository(CookieCsrfTokenRepository.withHttpOnlyFalse());
        http
                .cors()

//                .configurationSource(corsConfigurationSource())

                .and()
//                .csrf().ignoringAntMatchers("/rwin/**","/v2/api-docs", "/swagger*/**", "/webjars/**","/swagger-ui.html")
//                    .csrfTokenRepository(CookieCsrfTokenRepository.withHttpOnlyFalse())
//                .and()
//                    .headers().frameOptions().disable()
//                .and()
                .csrf().disable()
                    .authorizeRequests()
                    .requestMatchers(CorsUtils::isPreFlightRequest).permitAll()
                        .antMatchers("/adm/**").hasRole(Role.ADMIN.name())
                        .antMatchers("/user/**").hasAnyRole(Role.SELLER.name() , Role.ADMIN.name())
                        .antMatchers("/", "/static/**" , "/resources/static/**" ,"/assets/**","/static/admin_assets/**","/rwin/**","/bakcha/**"
                                ,"/v2/api-docs", "/configuration/**","/swagger-resources" ,"/swagger*/**", "/webjars/**","/swagger-ui.html","/login/**" , "/api/**").permitAll()
                        .anyRequest().authenticated()

                .and()
                    .logout()
                        .logoutUrl("/logOut")
                        .invalidateHttpSession(true)
                        .logoutSuccessUrl("/")
                        .clearAuthentication(true)
                        .permitAll()

                .and()
                    .oauth2Login()
                        .userInfoEndpoint()
                        .userService(userOAuthService);

    }

    @Override
    public void configure(WebSecurity webSecurity) throws Exception {
        webSecurity.ignoring().antMatchers(
                "/v2/api-docs",
                "/configuration/ui",
                "/swagger-resources/**",
                "/configuration/security",
                "/swagger-ui.html",
                "/webjars/**");
    }

    @Bean
    public CorsConfigurationSource corsConfigurationSource() {

        CorsConfiguration configuration = new CorsConfiguration();

//        configuration.setAllowedOriginPatterns(Arrays.asList("*"));
//        configuration.setAllowedOrigins(Arrays.asList("*"));
//        configuration.setAllowedMethods(Arrays.asList("*"));
//        configuration.setAllowedHeaders(Arrays.asList("*"));
//        configuration.setAllowCredentials(true);
        configuration.addAllowedHeader("*");
        configuration.addAllowedMethod("*");
        configuration.addAllowedOriginPattern("*");
        configuration.setAllowCredentials(true);
        configuration.setMaxAge(3600L);

        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", configuration);

        return source;
    }


}