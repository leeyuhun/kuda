package com.escrow.kuda.config;

import com.escrow.kuda.interceptor.LoginStateInterceptor;
import com.escrow.kuda.oauth.LoginUserArgumentResolver;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.List;

@RequiredArgsConstructor
@Configuration
public class WebConfig implements WebMvcConfigurer {

    @Autowired
    private LoginUserArgumentResolver loginUserArgumentResolver;


    @Autowired
    private LoginStateInterceptor loginStateInterceptor;

    @Override
    public void addArgumentResolvers(List<HandlerMethodArgumentResolver> argumentResolvers) {
        argumentResolvers.add(loginUserArgumentResolver);
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(loginStateInterceptor)
                .excludePathPatterns("/")
                .excludePathPatterns("/index")
                .excludePathPatterns("/static/**")
                .excludePathPatterns("/resources/static/**")
                .excludePathPatterns("/assets")
                .excludePathPatterns("/admin_assets")
                .excludePathPatterns("/kudaAccount")
                .excludePathPatterns("/registration")
                .excludePathPatterns("/upRemainInfo")
                .excludePathPatterns("/rwin/statusChange")
                .excludePathPatterns("/bakcha/**")
                .excludePathPatterns("/swagger-ui.html")
                .excludePathPatterns("/v2/api-docs")
                .excludePathPatterns("/swagger*/**")
                .excludePathPatterns("/swagger-resources")
                .excludePathPatterns("/webjars/**")
                .excludePathPatterns("/configuration/**")
                .excludePathPatterns("/login/**")
                .excludePathPatterns("/api/**")
                .addPathPatterns("/adm/**")
                .addPathPatterns("/user/**");
    }



//    @Bean
//    public CorsConfigurationSource corsConfigurationSource() {
//        CorsConfiguration corsConfiguration = new CorsConfiguration();
//        corsConfiguration.addAllowedOrigin("*");
//        corsConfiguration.addAllowedHeader("*");
//        corsConfiguration.addAllowedMethod("*");
//        corsConfiguration.setAllowCredentials(true);
//        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
//        source.registerCorsConfiguration("/**", corsConfiguration);
//        return source;
//    }


//    @Override
//    public void addCorsMappings(CorsRegistry registry) {
//        registry.addMapping("/**").allowedMethods("GET", "POST", "PUT", "DELETE")
//                .allowedOrigins("*");
//    }


    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**").allowedOrigins("*")
                .allowedMethods(HttpMethod.POST.name() , HttpMethod.GET.name() , HttpMethod.PUT.name() ,HttpMethod.DELETE.name() ,HttpMethod.OPTIONS.name() ,HttpMethod.PATCH.name())
                .allowCredentials(false).maxAge(3600);

    }




}




