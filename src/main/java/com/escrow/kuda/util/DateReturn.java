package com.escrow.kuda.util;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.concurrent.ConcurrentHashMap;

public class DateReturn {

    /*
     * 정산예정일 계산기
     * */
    public String scheduleDate(String yymmdd , int cycle){

        int year = Integer.parseInt( yymmdd.substring(0,4) );
        int month = Integer.parseInt( yymmdd.substring(4,6) );
        int date = Integer.parseInt( yymmdd.substring(6,8) );


        ArrayList<String> holiDay = new ArrayList<>();

        holiDay.add("2021-08-16");
        holiDay.add("2021-09-20");
        holiDay.add("2021-09-21");
        holiDay.add("2021-09-22");
        holiDay.add("2021-10-04");
        holiDay.add("2021-10-11");
        holiDay.add("2021-12-27");


        LocalDate ld = LocalDate.of(year , month , date);

        int preSum = 0;//총 늘어날 일수


        for(int i=1; i<=cycle; i++){


            if(holiDay.contains(ld.plusDays(preSum).toString())){ //휴일
                preSum +=1;
                --i ;
            }else if((ld.plusDays(preSum).getDayOfWeek().toString().equals("SATURDAY"))){ //토요일
                preSum +=2;
                --i ;
            }else if((ld.plusDays(preSum).getDayOfWeek().toString().equals("SUNDAY"))){ //일요일
                preSum +=1;
                --i ;
            }else{ //cleaer
                preSum +=1;

            }

        }

        return String.format(ld.plusDays(preSum).toString(), "yyyy-MM-dd") ;
    }



    /*
    * 현재날짜 휴일인지 아닌지 boolean
    * 휴일 false
    * 평일 true
    * */

    public boolean holyOrNot(String yymmdd){

        boolean returnVal;

        int year = Integer.parseInt( yymmdd.substring(0,4) );
        int month = Integer.parseInt( yymmdd.substring(4,6) );
        int date = Integer.parseInt( yymmdd.substring(6,8) );


        ArrayList<String> holiDay = new ArrayList<>();

        holiDay.add("2021-08-16");
        holiDay.add("2021-09-20");
        holiDay.add("2021-09-21");
        holiDay.add("2021-09-22");
        holiDay.add("2021-10-04");
        holiDay.add("2021-10-11");
        holiDay.add("2021-12-27");


        LocalDate ld = LocalDate.of(year , month , date);


        if(holiDay.contains(ld.toString())  ||
                (ld.getDayOfWeek().toString().equals("SATURDAY")) ||
                (ld.getDayOfWeek().toString().equals("SUNDAY"))
        ){ //휴일 or 토요일 or 일요일
            returnVal = false;

        }else{
            returnVal = true;
        }

        return returnVal;

    }

    public String escrowScheduleDate(String yymmdd , int cycle){

        int year = Integer.parseInt( yymmdd.substring(0,4) );
        int month = Integer.parseInt( yymmdd.substring(4,6) );
        int date = Integer.parseInt( yymmdd.substring(6,8) );


        ArrayList<String> holiDay = new ArrayList<>();


        holiDay.add("2021-08-16");
        holiDay.add("2021-09-20");
        holiDay.add("2021-09-21");
        holiDay.add("2021-09-22");
        holiDay.add("2021-10-04");
        holiDay.add("2021-10-11");
        holiDay.add("2021-12-27");


        LocalDate ld = LocalDate.of(year , month , date);

        int preSum = 0;//총 늘어날 일수


        for(int i=1; i<=cycle; i++){


            if(holiDay.contains(ld.plusDays(preSum).toString())){ //휴일
                preSum +=1;
                --i ;
            }else if((ld.plusDays(preSum).getDayOfWeek().toString().equals("SATURDAY"))){ //토요일
                preSum +=2;
                --i ;
            }else if((ld.plusDays(preSum).getDayOfWeek().toString().equals("SUNDAY"))){ //일요일
                preSum +=1;
                --i ;
            }else{ //cleaer
                preSum +=1;

            }

        }

        return String.format(ld.plusDays(preSum).toString(), "yyyy-MM-dd") ;

    }



    /*현재시간리턴 */
    public String preTimeNotHyphen(){
        return String.format(LocalTime.now().toString(), "yyyyMMdd");
    }


    /*
     * 오늘날짜 리턴
     * */

    public String preDate(){
        return String.format(LocalDate.now().toString(), "yyyy-MM-dd");
    }

    /*
    * 현재시각 + 10분 & 10 ~ 18 여부 리턴
    * 주말 일 시 다음날 송금
    * */
    public String escrowCalDatetime(){

        String returnDateTime = "";
        LocalDate ldNow =  LocalDate.now();

        DayOfWeek nowDay = ldNow.getDayOfWeek();


        LocalTime time10 = LocalTime.parse("10:00:00.000");
        LocalTime time12 = LocalTime.parse("12:00:00.000");
        LocalTime time14 = LocalTime.parse("14:00:00.000");
        LocalTime time16 = LocalTime.parse("16:00:00.000");
        LocalTime time18 = LocalTime.parse("18:00:00.000");
        LocalTime timeNow = LocalTime.now().plusMinutes(10);
        String dateNowStr = ldNow.format(DateTimeFormatter.ofPattern("yy-MM-dd"));


        if(timeNow.isAfter(time18)){ //18 : 00 이후 (다음날 정산)
            DayOfWeek tomorrowDay = ldNow.plusDays(1).getDayOfWeek(); //내일 요일

            String time = " 10:00";

            if(tomorrowDay.getValue() == 6 || tomorrowDay.getValue() == 7){ //다음날이 주말 -> 18:00 송금
                time = " 18:00";
            }

            returnDateTime = ldNow.plusDays(1).format(DateTimeFormatter.ISO_DATE) + time ;

        }else{ //18 :00 이전

            if(nowDay.getValue() == 6 || nowDay.getValue() == 7){ // 현재 주말 -> 18:00

                    returnDateTime = dateNowStr + " 18:00";

            }else{ //평일현재

                    if(timeNow.isBefore(time10)){
                        returnDateTime = dateNowStr + " 10:00";
                    }else if(timeNow.isAfter(time10) && timeNow.isBefore(time12)){
                        returnDateTime = dateNowStr + " 12:00";
                    }else if(timeNow.isAfter(time12) && timeNow.isBefore(time14)){
                        returnDateTime = dateNowStr + " 14:00";
                    }else if(timeNow.isAfter(time14) && timeNow.isBefore(time16)){
                        returnDateTime = dateNowStr + " 16:00";
                    }else if(timeNow.isAfter(time16) && timeNow.isBefore(time18)){
                        returnDateTime = dateNowStr + " 18:00";
                    }

            }
        }
        return returnDateTime;
    }


    public String preDateNotHy(){
        return LocalDate.now().format(DateTimeFormatter.BASIC_ISO_DATE);
    }

    /*
     * 현재시각 리턴
     * */
    public String preTime(){
        return LocalTime.now().format(DateTimeFormatter.ofPattern("HH:mm:ss") );
    }


    public ConcurrentHashMap<String ,String> dateFormat(String yymmddhhmmss){
        ConcurrentHashMap<String, String> returnHash = new ConcurrentHashMap<>();

        String date = yymmddhhmmss.substring(0,6);
        String time = yymmddhhmmss.substring(6,12);

        returnHash.put("date" , date.substring(0,2) +"-"+ date.substring(2,4) +"-" +date.substring(4,6));
        returnHash.put("time" , time.substring(0,2) +":"+ time.substring(2,4) +":"+ time.substring(4,6));


        return returnHash;


    }



}


