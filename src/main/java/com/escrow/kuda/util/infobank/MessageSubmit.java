package com.escrow.kuda.util.infobank;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;

@Configuration
public class MessageSubmit {
    @Value("${spring.profiles.active}") private String activeProfile;


    public String submitMSG(String ph , String content){
        String msgReturn ="";

        if(!activeProfile.equals("real")) return "ok";

        if(ph.length() >= 10) {


            ph = "82" +ph.substring(1);

            TokenIssuance ti = new TokenIssuance();
            HttpHeaders headers = new HttpHeaders();
            JSONObject jsonObject = new JSONObject();
            JSONObject destinationsJO = new JSONObject(); //destinations jsonArray 담을 object
            JSONArray jsonArray = new JSONArray();

            try {
                jsonObject.put("title" , "전중연 안전거래");
                jsonObject.put("from" , "16604840");
//                jsonObject.put("from" , "16604840");
                jsonObject.put("text" , content);
                jsonObject.put("ttl" , 86400);

                jsonArray.put(destinationsJO.put("to",ph));
                jsonObject.put("destinations" , jsonArray);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            String jsonString = jsonObject.toString();

            HttpEntity<?> entity = new HttpEntity<>(jsonString , headers);

            RestTemplate restTemplate = new RestTemplate( );

            System.out.println("ti.getToken().getSchema() : " + ti.getToken().getSchema());
            System.out.println("ti.getToken().getAccessToken() : " + ti.getToken().getAccessToken());
            headers.set("Authorization" ,ti.getToken().getSchema() + " " + ti.getToken().getAccessToken() );
            headers.set("Accept" , "application/json;");
            headers.set("Content-Type" , "application/json;");

            ResponseEntity<HashMap> responseEntity = restTemplate.exchange(
                    "https://sms.supersms.co:7020/sms/v3/multiple-destinations",
                    HttpMethod.POST,
                    entity,
                    HashMap.class
            );

            System.out.println( responseEntity.getBody() );

            msgReturn = "ok";

        }

        return msgReturn;
    }
}
