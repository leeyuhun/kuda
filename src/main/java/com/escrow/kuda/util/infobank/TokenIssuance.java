package com.escrow.kuda.util.infobank;

import com.escrow.kuda.model.infobank.TokenIssuanceVo;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;

public class TokenIssuance {


    /*인포뱅크 문자 수신 accessToken & schema 발급*/
    public TokenIssuanceVo getToken(){

        HttpComponentsClientHttpRequestFactory factory = new HttpComponentsClientHttpRequestFactory();

        factory.setConnectTimeout(60);
        factory.setReadTimeout(60);

        HttpHeaders headers = new HttpHeaders();
        HttpEntity<?> entity = new HttpEntity<>(headers);

        RestTemplate restTemplate = new RestTemplate( );

//        headers.set("X-IB-Client-Id" , "chatoss1122_http");
        headers.set("X-IB-Client-Id" , "chatoss1122_rest");
//        headers.set("X-IB-Client-Passwd" , "Z79105JUR5310EX6227A");
        headers.set("X-IB-Client-Passwd" , "651010LU6IU410GLU689");
        headers.set("Accept" , "application/json");


        ResponseEntity<Map> responseEntity = restTemplate.exchange(
                "https://auth.supersms.co:7000/auth/v3/token",
                HttpMethod.POST,
                entity,
                Map.class
        );

        HashMap hashMap = (HashMap) responseEntity.getBody();

        TokenIssuanceVo vo = new TokenIssuanceVo();

        vo.setAccessToken( hashMap.get("accessToken").toString() );
        vo.setSchema( hashMap.get("schema").toString() );

        return vo;

    }

}
