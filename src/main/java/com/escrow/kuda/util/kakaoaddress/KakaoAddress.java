package com.escrow.kuda.util.kakaoaddress;

import com.escrow.kuda.model.kakao.address.KaKaoMap;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;


public class KakaoAddress {



    public ResponseEntity<KaKaoMap> xyReturn(String query){

        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();

        HttpEntity<?> entity = new HttpEntity<>(headers);

        String url  = "https://dapi.kakao.com/v2/local/search/address.json?query=" + query;
        String authorization = "KakaoAK f01ccebc2db491ef783cbedeb3689f8c";
        String contentType = "application/x-www-form-urlencoded";


        headers.set("Content-Type",contentType);
        headers.set("Authorization" ,authorization);

        ResponseEntity<KaKaoMap> responseEntity = restTemplate.exchange(
                url,
                HttpMethod.POST,
                entity,
                KaKaoMap.class
        );


//        return Objects.requireNonNull(responseEntity.getBody() , "KakaoAPI return null").getDocuments().get(0);
        return responseEntity;
    }


}
