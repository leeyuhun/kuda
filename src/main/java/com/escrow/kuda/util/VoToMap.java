package com.escrow.kuda.util;

import org.apache.commons.lang.StringUtils;

import java.beans.BeanInfo;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

public class VoToMap {

    /**
     * vo를 map형식으로 변환해서 반환
     * @param vo VO
     * @return
     * @throws Exception
     */
    public static Map<String, Object> domainToMap(Object vo) throws Exception {
        return domainToMapWithExcept(vo, null);
    }

    /**
     * 특정 변수를 제외해서 vo를 map형식으로 변환해서 반환.
     * @param vo VO
     * @param arrExceptList 제외할 property 명 리스트
     * @return
     * @throws Exception
     */
    public static Map<String, Object> domainToMapWithExcept(Object vo, String[] arrExceptList) throws Exception {
        Map<String, Object> result = new HashMap<String, Object>();
        BeanInfo info = Introspector.getBeanInfo(vo.getClass());
        for (PropertyDescriptor pd : info.getPropertyDescriptors()) {
            Method reader = pd.getReadMethod();
            if (reader != null) {
                if(arrExceptList != null && arrExceptList.length > 0 && isContain(arrExceptList, pd.getName())) continue;
                result.put(pd.getName(), reader.invoke(vo));
            }
        }
        return result;
    }


    public static Boolean isContain(String[] arrList, String name) {
        for (String arr : arrList) {
            if (StringUtils.contains(arr, name))
                return true;
        }
        return false;
    }




    /*
    * null체크 후 제거
    * */
    public static Map<String, Object> domainToMapWithExceptNull(Object vo) throws Exception {
        Map<String, Object> result = new HashMap<String, Object>();
        BeanInfo info = Introspector.getBeanInfo(vo.getClass());
        for (PropertyDescriptor pd : info.getPropertyDescriptors()) {
            Method reader = pd.getReadMethod();
            if (reader != null) {

                if(reader.invoke(vo) != null) //null check
                    result.put(pd.getName(), reader.invoke(vo));
            }
        }

        result.remove("class");
        return result;
    }

}
