package com.escrow.kuda.util.band;


/*
* 토큰발급후 리스트 조회
* */

import com.escrow.kuda.model.band.bandlist.BandList;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class Band {

    @Value("${spring.profiles.active}") private String activeProfile;
    /*
     * 사용자 밴드리스트 전중연 포함여부
     * 포함:N 미포함:C
     * */
    public String bandListGrepKuda(String accessToken) {

        String returnMsg = "C";

        RestTemplate restTemplate = new RestTemplate();

        String bandKey = "AABfOzd37DwJbsqjN-47DTx-"; //전중연 밴드키


        BandList responseEntity = restTemplate.getForObject(
                "https://openapi.band.us/v2.1/bands?access_token=" + accessToken,
                BandList.class
        );

//        System.out.println(responseEntity.getResult_data());

        /*밴드키 확인*/
        if (responseEntity != null && responseEntity.getResult_data().getBands().size() >= 1) {

            for(int i = 0; i < responseEntity.getResult_data().getBands().size(); i++){
                if (responseEntity.getResult_data().getBands().get(i).getBand_key().equals(bandKey)) {
                    returnMsg = "N";
                    break;
                }
            }

        }

//        return "N";
        if(activeProfile.equals("local")) returnMsg = "N";
        return returnMsg;
    }
}
