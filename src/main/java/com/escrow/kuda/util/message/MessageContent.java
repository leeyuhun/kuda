package com.escrow.kuda.util.message;


import com.escrow.kuda.model.E_remittance_info;
import com.escrow.kuda.model.EscrowRefund;
import com.escrow.kuda.model.admin.RemitCompEsr;
import com.escrow.kuda.model.schedule.RoadwinCsm;
import com.escrow.kuda.model.sms.*;
import com.escrow.kuda.model.util.DeliveryCompInfo;
import com.escrow.kuda.model.util.EslInfo;
import com.escrow.kuda.model.util.RemitEsl;
import org.springframework.context.annotation.Configuration;

import java.text.DecimalFormat;
import java.time.LocalTime;
import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;

@Configuration
public class MessageContent {

    public String toMoneyFormat(long price){

        DecimalFormat df = new DecimalFormat("###,###");

        return df.format(price);
    }

    /*
    * To.구매자
    * 주문 생성 알림
    * */
    public String messageContent1_requester(String name , String holder , long price){

        return
                name+"님 안전거래 입금계좌 안내입니다.\n" +
                        "\n" +
                        "■입금정보\n" +
                        "거래금액 : "+toMoneyFormat(price)+"원\n" +
                        "계좌 : 신한은행 100-035-645669 \n" +
                        "예금주 : (주)차토스\n"+
                        "\n" +
                        "■안내사항\n" +
                        "입금자명을 반드시 '"+holder+"'으로 입금 바랍니다.\n" +
                        "거래금액 입금 확인 시, 차량 위탁배송이 판매자의 동의 후에 진행됩니다.\n" +
                        "\n" +
                        "■고객센터\n" +
                        "(주)차토스 에스크로 운영팀 : 1660-4840";
    }


    /*
     * To.구매자
     * 입금 완료 알림
     * */
    public String messageContent2_requester(RemitCompEsr esr ){


        return 
                esr.getEsrName()+"님 안전거래 계좌이체가 확인 되었습니다.\n" +
                "\n"+
                "■거래정보\n" +
                "상품명 : "+esr.getProductName()+"\n" +
                "입금금액 : "+toMoneyFormat(esr.getAllMoney())+"원\n" +
                "\n"+
                "■배송정보\n" +
                "출발지 주소 : "+esr.getStartAddress() + "\n" +
                "도착지 주소 : "+esr.getEndAddress() + "\n" +
                "배송요청금액 : "+toMoneyFormat(Long.parseLong(esr.getDeliveryCost()))+ "원\n" +
                "\n"+
                "■안내사항\n" +
                "판매자 "+esr.getEssName()+"("+esr.getEssPh()+")님의 차량 위탁배송 동의 후에 차량배송이 진행됩니다.\n\n" +
                "\n"+
                "■고객센터\n" +
                "(주)차토스 에스크로 운영팀 : 1660-4840";
    }

    /*
    * To.판매자
    * 입금 완료 알림 lms
    * */
    public String messageContent2_seller(RemitCompEsr esr){

        return 
                esr.getEsrName()+"님 안전거래 계좌이체가 확인 되었습니다.\n" +
                "\n"+
                "■거래정보\n" +
                "상품명 : "+esr.getProductName()+"\n" +
                "입금금액 : "+toMoneyFormat(esr.getPaymentMoney())+"원\n" +
                "\n"+
                "■배송정보\n" +
                "출발지 주소 : "+esr.getStartAddress() + "\n" +
                "도착지 주소 : "+esr.getEndAddress() + "\n" +
                "\n"+
                "■안내사항\n" +
                "아래의 URL에서 위탁배송 동의 버튼을 눌러주세요. 동의 후 자동으로 위탁배송신청이 완료됩니다.\n" +
                "http://kuda.chatoss.com/user/csmAgree?orderNo=" + esr.getOrderNo() +"\n"+
                "\n"+
                "■고객센터\n" +
                "(주)차토스 에스크로 운영팀 : 1660-4840";
    }




    public String csmAgreeY_remit_seller(CsmAgreeInfo csmAgreeInfo){
        return csmAgreeInfo.getSellerName()+"님 계좌입력 안내입니다.\n" +
                "\n"+
                "■거래정보\n" +
                "상품명 : "+csmAgreeInfo.getProductName()+"\n" +
                "입금금액 : "+toMoneyFormat(csmAgreeInfo.getEslPrice())+"원\n" +
                "구매자 : "+ csmAgreeInfo.getReqName()+"(" +csmAgreeInfo.getReqPh()+")"+"\n" +
                "\n"+
                "■안내사항\n" +
                "아래의 URL에서 계좌정보를 입력해주세요. 구매자가 차량을 인도받으면 입력한 계좌로 송금됩니다.\n" +
                "http://kuda.chatoss.com/user/remittance?orderNo=" + csmAgreeInfo.getOrderNo() +"\n"+
                "\n"+
                "■고객센터\n" +
                "(주)차토스 에스크로 운영팀 : 1660-4840";
    }


    public String csmAgreeY_requester(CsmAgreeInfo csmAgreeInfo){

        LocalTime lt = LocalTime.now();

        boolean ltAfter = lt.isAfter(LocalTime.of(17, 00));

        String msg = "";

        if(ltAfter){
            msg = "17시 이후의 위탁배송 요청 건은 익일 오전 9시 처리로 전환됩니다.\n"+
                    "익일 9시 이후에도 처리가 안될 시 배송업체에 문의 바랍니다.";
        }else {
            msg = "위탁배송 동의가 완료되었습니다. 위의 정보로 배송 요청합니다.\n" +
                    "배송에 대한 문의 사항은 배송업체에 문의 바랍니다.\n";
        }
        
        return "위탁배송 동의가 완료되었습니다.\n" +
                "\n" +
                "■거래정보\n" +
                "상품명 : "+csmAgreeInfo.getProductName()+"\n" +
                "판매자 : "+csmAgreeInfo.getSellerName()+"(" +csmAgreeInfo.getSellerPh()+")"+"\n" +
                "\n"+
                "■배송정보\n" +
                "배송금액 : "+toMoneyFormat(csmAgreeInfo.getCsmPrice())+"원\n" +
                "출발지 주소 : "+csmAgreeInfo.getStartAddress() +"\n" +
                "도착지 주소 : "+csmAgreeInfo.getEndAddress()+"\n" +
                "\n" +
                "■안내사항\n" +
                msg +
                "※배송기사 배정 후 취소 시 위약 수수료가 부과될 수 있습니다."+
                "\n" +
                "■배송업체 유선번호\n" +
                "고객센터 : 1599-2811";

    }

    public String csmAgreeY_seller(CsmAgreeInfo csmAgreeInfo){

        LocalTime lt = LocalTime.now();

        boolean ltAfter = lt.isAfter(LocalTime.of(17, 00));

        String msg = "";

        if(ltAfter){
            msg = "17시 이후의 위탁배송 요청 건은 익일 오전 9시 처리로 전환됩니다.\n"+
                    "익일 9시 이후에도 처리가 안될 시 배송업체에 문의 바랍니다.";
        }else {
            msg = "위탁배송 동의가 완료되었습니다. 위의 정보로 배송 요청합니다.\n" +
                    "배송에 대한 문의 사항은 배송업체에 문의 바랍니다.\n";
        }


        return "위탁배송 동의가 완료되었습니다.\n" +
                "\n" +
                "■거래정보\n" +
                "상품명 : "+csmAgreeInfo.getProductName()+"\n" +
                "입금금액 : "+toMoneyFormat(csmAgreeInfo.getEslPrice())+"원\n" +
                "\n"+
                "■배송정보\n" +
                "구매자 : "+ csmAgreeInfo.getReqName()+"(" +csmAgreeInfo.getReqPh()+")"+"\n" +
                "출발지 주소 : "+csmAgreeInfo.getStartAddress() +"\n" +
                "도착지 주소 : "+csmAgreeInfo.getEndAddress()+"\n" +
                "\n" +
                "■안내사항\n" +
                msg +
                "\n" +
                "■배송업체 유선번호\n" +
                "고객센터 : 1599-2811";
    }



    /*
    *
    * */

    /*
    *To.구매자
    * 차량배송 시작알림 lms
    * */
    public String messageContent3_requester(DeliveryCompInfo esr){

        return 
                "차량배송이 시작되었습니다.\n" +
                "\n" +
                "■배송정보\n" +
                "상품명 : "+esr.getProductName()+"\n" +
                "\n" +
                "■판매자정보\n" +
                "이름 : "+esr.getEssName()+"\n" +
                "핸드폰번호 : "+esr.getEssPh()+"\n" +
                "\n" +
                "■거래진행\n" +
                "차량을 인도 받으신 후 아래의 URL을 통해 거래완료 버튼을 눌러주세요.\n" +
                "http://kuda.chatoss.com/user/purchaseSale\n" +
                "\n" +
                "■고객센터\n" +
                "(주)차토스 에스크로 운영팀 : 1660-4840";

    }


    /*
    * To.판매자
    * 판매자계좌 송금안내
    * */
    public String messageContent4_seller(DeliveryCompInfo deliveryCompInfo){

        return 
            "구매자가 차량확인을 완료했습니다.\n" +
            "\n" +
            "■차량정보\n" +
            "상품명 : "+deliveryCompInfo.getProductName()+"\n" +
            "금액 : "+toMoneyFormat(deliveryCompInfo.getPaymentMoney())+"원\n" +
            "\n" +
            "■계좌정보\n" +
            "은행명 : "+deliveryCompInfo.getBankName()+"\n" +
            "계좌번호 : "+deliveryCompInfo.getAccount()+"\n" +
            "예금주 : "+deliveryCompInfo.getHolder()+"\n" +
                    "\n" +
            "■안내사항\n" +
                    deliveryCompInfo.getCalDate()+"까지 위의 계좌로 입금 완료됩니다.\n" +
                    "해당 계좌정보에 문제가 있을 시, 고객센터로 연락 바랍니다.\n" +
                    "\n" +
            "■고객센터\n" +
            "(주)차토스 에스크로 운영팀 : 1660-4840";

    }


    public String messageContent4_seller_owner(DeliveryCompInfo deliveryCompInfo){

        return
                "구매자가 차량확인을 완료했습니다.\n" +
                        "\n" +
                        "■차량정보\n" +
                        "상품명 : "+deliveryCompInfo.getProductName()+"\n" +
                        "금액 : "+toMoneyFormat(deliveryCompInfo.getPaymentMoney())+"원\n" +
                        "\n" +
                        "■계좌정보\n" +
                        "은행명 : "+deliveryCompInfo.getBankName()+"\n" +
                        "계좌번호 : "+deliveryCompInfo.getAccount()+"\n" +
                        "예금주 : "+deliveryCompInfo.getHolder()+"\n" +
                        "\n" +
                        "■안내사항\n" +
                        deliveryCompInfo.getCalDate()+"까지 위의 계좌로 입금 완료됩니다.\n" +
                        "해당 계좌정보에 문제가 있을 시, 고객센터로 연락 바랍니다.\n" +
                        "\n" +
                        "■고객센터\n" +
                        "(주)차토스 에스크로 운영팀 : 1660-4840";

    }


    public String messageContent4_requester(ConcurrentHashMap<String,Object> map){

        return
                "차량대금 지급안내\n" +
                        "\n" +
                        "■차량정보\n" +
                        "상품명 : "+map.get("productName")+"\n" +
                        "금액 : "+toMoneyFormat((Long) map.get("amount"))+"원\n" +
                        "\n" +
                        "■안내사항\n" +
                        map.get("calDate")+"까지 판매자에게 입금 완료됩니다.\n" +
                        "해당 거래에 문제가 있을 시, 고객센터로 연락 바랍니다.\n" +
                        "\n" +
                        "■고객센터\n" +
                        "(주)차토스 에스크로 운영팀 : 1660-4840";

    }


    /*
     * To.판매자
     * 판매자에게 차량대금 입금 안내 lms
     * 계좌정보 입력 완료했을시
     * */
    public String messageContent5_seller(E_remittance_info eInfo , RemitEsl remitEsl){

        return 
                "계좌정보 입력완료\n" +
                "\n" +
                "■차량정보\n" +
                "상품명 : "+remitEsl.getProductName()+"\n" +
                "금액 : "+toMoneyFormat(remitEsl.getPAmount())+"원\n" +
                "\n" +
                "■계좌정보\n" +
                "은행명 : "+remitEsl.getBankName()+"\n" +
                "계좌번호 : "+eInfo.getEri_account_number()+"\n" +
                "예금주 : "+eInfo.getEri_account_holder()+"\n" +
                "\n" +
                "■안내사항\n"+
                "구매자가 차량 수령후 위의 계좌정보로 송금 됩니다. \n"+
                "\n" +
                "■주의 사항\n" +
                "계좌 정보를 반드시 확인해 주세요. 오 입력으로 인한 금융거래 사고는 차토스에서 책임지지 않습니다.\n" +
                "계좌 정보에 문제가 있을 시 아래의 고객센터로 연락 바랍니다.\n"+
                "\n" +
                "■고객센터\n" +
                "(주)차토스 에스크로 운영팀 : 1660-4840";

    }


    public String messageContent5_seller_Owner(E_remittance_info eInfo , RemitEsl remitEsl){

        return
                "구매자 송금 확인 및 계좌정보 입력완료\n" +
                        "\n" +
                        "■차량정보\n" +
                        "상품명 : "+remitEsl.getProductName()+"\n" +
                        "금액 : "+toMoneyFormat(remitEsl.getPAmount())+"원\n" +
                        "\n" +
                        "■계좌정보\n" +
                        "은행명 : "+remitEsl.getBankName()+"\n" +
                        "계좌번호 : "+eInfo.getEri_account_number()+"\n" +
                        "예금주 : "+eInfo.getEri_account_holder()+"\n" +
                        "\n"+
                        "■판매자정보\n" +
                        "이름 : "+remitEsl.getESellerName()+"\n" +
                        "연락처 : "+remitEsl.getESellerPh()+"\n" +
                        "\n" +
                        "■안내사항\n"+
                        "구매자가 차량 수령후 위의 계좌정보로 송금 됩니다. \n"+
                        "\n" +
                        "■주의 사항\n" +
                        "계좌 정보를 반드시 확인해 주세요. 오 입력으로 인한 금융거래 사고는 차토스에서 책임지지 않습니다.\n" +
                        "계좌 정보에 문제가 있을 시 판매자에게 연락 바랍니다.\n"+
                        "\n" +
                        "■고객센터\n" +
                        "(주)차토스 에스크로 운영팀 : 1660-4840";

    }

    /*
     * To.구매자
     * 구매자에게 판매자에게 차량대금 입금 안내 lms
     * */




    /*
    * To.요청자
    * 거래종료메세지
    * */
    public String messageContent6_requester(EslInfo eslInfo){

        return
                "안전거래 종료 안내\n" +
                        "\n" +
                        "■차량정보\n" +
                        "상품명 : "+eslInfo.getProductName()+"\n" +
                        "지급액 : "+toMoneyFormat(eslInfo.getPAmount())+"원\n" +
                        "\n" +
                        "판매자에게 차량대금 정산처리되었습니다.\n" +
                        "이용해 주셔서 감사합니다.\n" +
                        "\n" +
                        "■고객센터\n" +
                        "(주)차토스 에스크로 운영팀 : 1660-4840";

    }

    /*
     * To.판매자
     * 거래종료메세지
     * */
    public String messageContent6_seller(EslInfo eslInfo){

        return
               "안전거래 종료 안내\n" +
                       "\n" +
                       "■정산정보\n" +
                       "은행 : "+eslInfo.getBankName()+"\n" +
                       "계좌번호 : "+eslInfo.getANumber()+"\n" +
                       "예금주 : "+eslInfo.getAHolder()+"\n" +
                       "금액 : "+toMoneyFormat(eslInfo.getPAmount())+"원\n" +
                       "\n" +
                       "위의 계좌로 차량대금 정산처리되었습니다.\n" +
                       "이용해 주셔서 감사합니다.\n" +
                       "\n" +
                       "■고객센터\n" +
                       "(주)차토스 에스크로 운영팀 : 1660-4840 ";

    }



    /*
    * to.초대 한사람
    * 초대자 가입완료 후 거래 페이지 url 전송
    * */
    public String messageContentInvite_seller(){

        return "회원님이 초대한 가입자가 전중연 안전거래 서비스 가입을 완료했습니다.\n" +
        "아래의 URL을 통해 거래를 진행해 주세요.\n" +
        "http://kuda.chatoss.com\n" +
        "\n"+
        "■고객센터\n" +
        "(주)차토스 에스크로 운영팀 : 1660-4840";
    }


    /*
    * to.초대 받은사람
    * 초대메세지
    * */
    public String messageContentInvite_requester(String userName){

        return 
                userName+" 님이 안전거래 초대 URL을 보냈습니다.\n" +
                "아래의 URL을 통해 가입을 진행해 주세요.\n" +
                "http://kuda.chatoss.com\n" +
                "\n" +
                "■고객센터\n" +
                "(주)차토스 에스크로 운영팀 : 1660-4840";
    }


    public String adminMessage_Overlap(HashMap map){

        return "송금 처리 중복\n" +
                "■주문번호 : " + map.get("eslCode") + "\n"+
                "■예금주명 : " + map.get("bankHolder") + "\n"+
                "■금액 : " + toMoneyFormat((Long)map.get("amount")) + "원\n";
//                "■처리URL : http://kuda.chatoss.com/adm/escrowList/" + map.get("eslCode");

    }


    public String adminMessage_NotExists(HashMap<String, Object> esValueChk) {
        return "송금 처리 에러발생-해당 정보 찾을수 없음 \n" +
                "■예금주명 : " + esValueChk.get("bankHolder") + "\n"+
                "■금액 : " + esValueChk.get("amount") + "\n" ;
//                "■처리URL : http://kuda.chatoss.com/adm/escrowList/" + esValueChk.get("amount")
    }


    public String roadWin_delivery_assign_requester(RoadWinDeliveryAssign assign){

        return "위탁배송 기사배정 안내\n" +
                "\n" +
                "■위탁배송 정보\n" +
                "상품정보 : "+assign.getProductName()+"\n"+
                "출발지 : "+assign.getStartAddress()+"\n" +
                "도착지 : "+assign.getEndAddress()+"\n" +
                "배송요금 : "+toMoneyFormat( assign.getCsmPrice())+"원\n" +
                "배송기사 : "+assign.getDeliverName()+"("+assign.getDeliverPh()+")"+"\n" +
                "\n" +
                "배송기사 배정이 완료되었습니다. 해당 배송에 문의사항 있을시, 아래의 고객센터로 연락 바랍니다.\n" +
//                "기사의 위치는 아래의 URL의 통해 확인할수 있습니다.\n" +
//                "\n" +
//                "■ 배송기사 위치\n" +
                "\n" +
                "■배송업체 유선번호\n" +
                "고객센터 : 1599-2811";
    }

    public String roadWin_delivery_assign_seller(RoadWinDeliveryAssign assign){

        return "위탁배송 기사배정 안내\n" +
                "\n" +
                "■위탁배송 정보\n" +
                "상품정보 : "+assign.getProductName()+"\n"+
                "출발지 : "+assign.getStartAddress()+"\n" +
                "도착지 : "+assign.getEndAddress()+"\n" +
                "배송기사 : "+assign.getDeliverName()+"("+assign.getDeliverPh()+")"+"\n" +
                "\n" +
                "배송기사 배정이 완료되었습니다. 해당 배송에 문의사항 있을시, 아래의 고객센터로 연락 바랍니다.\n" +
//                "기사의 위치는 아래의 URL의 통해 확인할수 있습니다.\n" +
//                "\n" +
//                "■ 배송기사 위치\n" +
                "\n" +
                "■배송업체 유선번호\n" +
                "고객센터 : 1599-2811";
    }


    public String roadWin_delivery_start_requester(RoadwinDeliveryStart start){
        return "위탁배송 출발 안내\n" +
                "\n" +
                "■위탁배송 정보\n" +
                "상품정보 : "+start.getProductName()+"\n"+
                "출발지 : "+start.getStartAddress()+"\n" +
                "도착지 : "+start.getEndAddress()+"\n" +
                "배송기사 : "+start.getDeliverName()+"("+start.getDeliverPh()+")"+"\n" +
                "차량 확인 : " + start.getServiceViewLink() +"\n"+
                "\n" +
                "배송기사가 출발합니다. 해당 배송에 문의사항 있을시, 아래의 고객센터로 연락 바랍니다.\n" +
                "기사의 위치는 아래의 URL의 통해 지속적으로 확인할수 있습니다.\n" +
                "\n" +
                "■배송기사 위치\n" +
                "URL : http://kuda.chatoss.com/user/driverLocation?orderNo="+ start.getOrderNo()  +"\n"+
                "\n" +
                "■배송업체 유선번호\n" +
                "고객센터 : 1599-2811";
    }


    public String roadWin_delivery_start_seller(RoadwinDeliveryStart start){
        return "위탁배송 출발 안내\n" +
                "\n" +
                "■위탁배송 정보\n" +
                "상품정보 : "+start.getProductName()+"\n"+
                "출발지 : "+start.getStartAddress()+"\n" +
                "도착지 : "+start.getEndAddress()+"\n" +
                "배송기사 : "+start.getDeliverName()+"("+start.getDeliverPh()+")"+"\n" +
                "차량 확인 : " + start.getServiceViewLink() +"\n"+
                "\n" +
                "배송기사가 출발합니다. 해당 배송에 문의사항 있을시, 아래의 고객센터로 연락 바랍니다.\n" +
                "기사의 위치는 아래의 URL의 통해 지속적으로 확인할수 있습니다.\n" +
                "\n" +
                "■배송기사 위치\n" +
                "URL : http://kuda.chatoss.com/user/driverLocation?orderNo="+ start.getOrderNo()  +"\n"+
                "\n" +
                "■배송업체 유선번호\n" +
                "고객센터 : 1599-2811";
    }

    public String roadWin_delivery_start_owner(RoadwinDeliveryStart start){
        return "위탁배송 출발 안내\n" +
                "\n" +
                "■배송 정보\n" +
                "상품정보 : "+start.getProductName()+"\n"+
                "구매자 : "+start.getReqName() +"("+start.getReqPh()+")\n"+
                "판매자 : "+start.getSellerName() +"("+start.getSellerPh()+")\n"+
                "출발지 : "+start.getStartAddress()+"\n" +
                "도착지 : "+start.getEndAddress()+"\n" +
                "배송기사 : "+start.getDeliverName()+"("+start.getDeliverPh()+")"+"\n" +
                "차량 확인 : " +start.getServiceViewLink() +"\n"+
                "\n" +
                "■안내 사항\n" +
                "배송기사가 출발합니다. 해당 배송에 문의사항 있을시, 아래의 고객센터로 연락 바랍니다.\n" +
                "기사의 위치는 아래의 URL의 통해 지속적으로 확인할수 있습니다.\n" +
                "\n" +
                "■배송기사 위치\n" +
                "URL : http://kuda.chatoss.com/user/driverLocation?orderNo="+ start.getOrderNo()  +"\n"+
                "\n" +
                "■배송업체 유선번호\n" +
                "고객센터 : 1599-2811";
    }


    public String roadWin_delivery_end_requester(RoadwinDeliveryEnd end){
        return "위탁배송 완료 안내\n" +
                "\n" +
                "■상품 정보\n" +
                "차량정보 : "+end.getProductName()+"\n"+
                "차량금액 : "+toMoneyFormat( end.getEslPrice())+"원\n"+
                "\n"+
                "■위탁배송 정보\n" +
                "출발지 : "+end.getStartAddress()+"\n" +
                "도착지 : "+end.getEndAddress()+"\n" +
                "배송기사 : "+end.getDeliverName()+"("+end.getDeliverPh()+")"+"\n" +
                "\n" +
                "■안내사항\n" +
                "차량배송이 완료되었습니다. 차량 확인후 아래의 URL을 통해 거래 완료버튼을 누를 시, 판매자에게 차량금액이 송금됩니다.\n"+
                "URL : kuda.chatoss.com/user/purchaseSale\n"+
                "\n" +
                "■고객센터\n" +
                "(주)차토스 에스크로 운영팀 : 1660-4840";
    }


    public String roadWin_delivery_end_seller(RoadwinDeliveryEnd end){
        return "위탁배송 완료 안내\n" +
                "\n" +
                "■상품 정보\n" +
                "차량정보 : "+end.getProductName()+"\n"+
                "차량금액 : "+toMoneyFormat( end.getEslPrice())+"원\n"+
                "\n" +
                "■위탁배송 정보\n" +
                "출발지 : "+end.getStartAddress()+"\n" +
                "도착지 : "+end.getEndAddress()+"\n" +
                "배송기사 : "+end.getDeliverName()+"("+end.getDeliverPh()+")"+"\n" +
                "\n" +
                "■안내사항\n" +
                "차량배송이 완료되었습니다. 구매자가 차량 확인 후 송금이 진행됩니다.\n"+
                "\n" +
                "■고객센터\n" +
                "(주)차토스 에스크로 운영팀 : 1660-4840";
    }


    public String roadWin_delivery_end_owner(RoadwinDeliveryEnd end){
        return "위탁배송 완료 안내\n" +
                "\n" +
                "■상품 정보\n" +
                "차량정보 : "+end.getProductName()+"\n"+
                "차량금액 : "+toMoneyFormat( end.getEslPrice())+"원\n"+
                "\n" +
                "■위탁배송 정보\n" +
                "출발지 : "+end.getStartAddress()+"\n" +
                "도착지 : "+end.getEndAddress()+"\n" +
                "배송기사 : "+end.getDeliverName()+"("+end.getDeliverPh()+")"+"\n" +
                "\n" +
                "■안내사항\n" +
                "차량배송이 완료되었습니다. 구매자가 차량 확인 후 송금이 진행됩니다.\n"+
                "\n" +
                "■고객센터\n" +
                "(주)차토스 에스크로 운영팀 : 1660-4840";
    }


    public String roadWin_modify_order_requester(RoadWinOrderModify modify){
        return "배송 금액 변경 완료 안내\n" +
                "\n" +
                "■상품 정보\n" +
                "차량정보 : "+modify.getProductName()+"\n"+
                "차량금액 : "+toMoneyFormat( modify.getEslPrice())+"원\n"+
                "\n" +
                "■위탁배송 정보\n" +
                "출발지 : "+modify.getStartAddress()+"\n" +
                "도착지 : "+modify.getEndAddress()+"\n" +
                "변경 전 금액 : "+ toMoneyFormat( modify.getCsmPricePre())+"원\n" +
                "변경 후 금액 : "+ toMoneyFormat( modify.getCsmPriceNext())+"원\n" +
                "\n" +
                "■안내사항\n" +
                "차량 배송 금액이 변경되었습니다. 문의사항이 있을 시 아래의 고객센터로 연락 바랍니다.\n"+
                "\n" +
                "■고객센터\n" +
                "(주)차토스 에스크로 운영팀 : 1660-4840";
    }

    public String roadWin_notAssign_requester(RoadwinCsm csm){
        return "배송기사 미배정 안내\n" +
                "\n" +
                "■상품 정보\n" +
                "차량정보 : "+csm.getProductName()+"\n"+
                "차량금액 : "+toMoneyFormat( csm.getEslPrice())+"원\n"+
                "\n" +
                "■위탁배송 정보\n" +
                "출발지 : "+csm.getStartAddress()+"\n" +
                "도착지 : "+csm.getEndAddress()+"\n" +
                "배송금액 : "+ toMoneyFormat( csm.getCsmPrice())+"원\n" +

                "\n" +
                "■안내사항\n" +
                "\n위탁배송신청 이후 "+csm.getScheduleTime()+"분 경과되었습니다. 더 빠른 배송을 원할 시, 아래의 URL을 통해 배송 금액을 수정해 주세요."+
                "URL : http://kuda.chatoss.com/user/csmUpdate?orderNo="+csm.getOrderNo() +"\n"+
                "\n" +
                "■고객센터\n" +
                "(주)차토스 에스크로 운영팀 : 1660-4840";
    }

    public String orderCancel_seller(OrderCancelSmsInfo orderCancelSmsInfo) {
        return "안전거래 취소 안내\n" +
                "\n" +
                "■상품 정보\n" +
                "차량정보 : "+orderCancelSmsInfo.getProductName()+"\n"+
                "차량금액 : "+toMoneyFormat( orderCancelSmsInfo.getEslPrice())+"원\n"+
                "\n" +
                "해당 거래가 취소 되었습니다"+
                "\n" +
                "■고객센터\n" +
                "(주)차토스 에스크로 운영팀 : 1660-4840";
    }


    public String refundInfo_requester(RefundInfo refundInfo){
        return "환불계좌 입력 안내\n" +
                "\n" +
                "■상품 정보\n" +

                "차량정보 : "+refundInfo.getProductName()+"\n"+
                "차량금액 : "+toMoneyFormat( refundInfo.getEslPrice())+"원\n"+

                "■환불 정보 입력\n" +
                "URL : http://kuda.chatoss.com/user/refund?orderNo="+refundInfo.getOrderNo()+ "\n"+

                "\n" +
                "해당 거래가 취소되었습니다. 위의 링크를 통해 환불계좌를 입력해 주세요."+
                "\n" +
                "■고객센터\n" +
                "(주)차토스 에스크로 운영팀 : 1660-4840";

    }

    public String refundInfo_seller(RefundInfo refundInfo){
        return "안전거래 취소 안내\n" +
                "\n" +
                "■상품 정보\n" +
                "차량정보 : "+refundInfo.getProductName()+"\n"+
                "차량금액 : "+toMoneyFormat( refundInfo.getEslPrice())+"원\n"+

                "■안내사항\n" +
                "구매자의 요청으로 해당 거래가 취소되었습니다."+
                "\n" +
                "■고객센터\n" +
                "(주)차토스 에스크로 운영팀 : 1660-4840";

    }

    public String refundInfo_owner(RefundInfo refundInfo){
        return "안전거래 취소 안내\n" +
                "\n" +
                "■상품 정보\n" +
                "차량정보 : "+refundInfo.getProductName()+"\n"+
                "차량금액 : "+toMoneyFormat( refundInfo.getEslPrice())+"원\n"+

                "■안내사항\n" +
                "구매자의 요청으로 해당 거래가 취소되었습니다."+
                "\n" +
                "■고객센터\n" +
                "(주)차토스 에스크로 운영팀 : 1660-4840";

    }

    public String refundComplete_requester(EscrowRefund escrowRefund) {

        return "환불계좌 등록 완료 안내\n" +
                "\n" +
                "■환불계좌 정보\n" +
                "은행명 : "+escrowRefund.getBankName()+"\n"+
                "계좌 : "+escrowRefund.getBankNum()+"\n"+
                "예금주 : "+escrowRefund.getBankHolder()+"\n"+
                "환불금액 : "+toMoneyFormat( escrowRefund.getEslPrice())+"원\n"+

                "\n" +
                "■고객센터\n" +
                "(주)차토스 에스크로 운영팀 : 1660-4840";


    }
}
