package com.escrow.kuda.util;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/*
     * 테이블코드(PK) 생성후 리턴
     * 형식 : yyyyMMddhhmmssSSS-랜덤숫자5개
     * */
    public class CodeReturn {

        public String numberReturn(){

            LocalDateTime ldt = LocalDateTime.now();
            String randomStr = "";

            for(int i=0 ; i<=5; i++){
                randomStr += (int)((Math.random()*10000)%10) +"";
            }
            return LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMddhhmmssSSS")) + "-" + randomStr;

        }

}
