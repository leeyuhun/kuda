package com.escrow.kuda.util.nicepay;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Iterator;
import java.util.concurrent.ConcurrentHashMap;

public class NicePayUtil {

    public String getyyyyMMddHHmmss(){


        return LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss"));

    }


    //JSON String -> HashMap 변환
    public ConcurrentHashMap jsonStringToHashMap(String str) throws Exception{
        ConcurrentHashMap dataMap = new ConcurrentHashMap();
        JSONParser parser = new JSONParser();
        try{
            Object obj = parser.parse(str);
            JSONObject jsonObject = (JSONObject)obj;

            Iterator<String> keyStr = jsonObject.keySet().iterator();
            while(keyStr.hasNext()){
                String key = keyStr.next();
                Object value = jsonObject.get(key);

                dataMap.put(key, value);
            }
        }catch(Exception e){

        }
        return dataMap;
    }

}
