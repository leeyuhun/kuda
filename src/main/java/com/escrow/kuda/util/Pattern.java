package com.escrow.kuda.util;

public class Pattern {

    public String phPattern(String ph){

        String reg = "(\\d{3})(\\d{3,4})(\\d{4})";

        return ph.replaceAll(reg, "$1-$2-$3");


    }

}
