package com.escrow.kuda.util.roadwin;

import com.escrow.kuda.config.RoadwinConfig;
import com.escrow.kuda.model.roadwin.BookingReq;
import com.escrow.kuda.model.roadwin.PriceCheckReq;
import com.escrow.kuda.model.roadwin.RoadwinCancelReq;
import com.escrow.kuda.util.SecurityKey;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.json.simple.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;



@Slf4j
public class Roadwin {





    /*
    * 예상가격체크
    * */
    @SneakyThrows
    public static ConcurrentHashMap priceCheck(PriceCheckReq priceCheckReq){
        HttpComponentsClientHttpRequestFactory factory = new HttpComponentsClientHttpRequestFactory();
        factory.setConnectTimeout(10*1000);
        factory.setReadTimeout(10*1000);
        String url  = RoadwinConfig.baseURL+"/service/priceCheck";



        JSONObject jsonObject = new JSONObject();

        jsonObject.put("data" , SecurityKey.encodeKey( priceCheckReq.toString().toUpperCase(Locale.ROOT) ) );

        RestTemplate restTemplate = new RestTemplate(factory);

        HttpHeaders headers = new HttpHeaders();

        HttpEntity<?> entity = new HttpEntity<>(jsonObject , headers );

        headers.set("apiKey",RoadwinConfig.apiKey);
        headers.set("Content-Type","application/json");
        headers.set("Accept","application/json;charset=UTF-8");

        ResponseEntity<Map> responseEntity = restTemplate.exchange(
                url,
                HttpMethod.POST,
                entity,
                Map.class
        );

        HashMap hashMap = (HashMap) responseEntity.getBody();


        return new ObjectMapper().readValue( SecurityKey.decodeKey((String) hashMap.get("data")), ConcurrentHashMap.class);

    }





    /*
    * 주문하기
    * */
    @SneakyThrows
    public static ConcurrentHashMap booking(BookingReq param){
        HttpComponentsClientHttpRequestFactory factory = new HttpComponentsClientHttpRequestFactory();
        factory.setConnectTimeout(10*1000);
        factory.setReadTimeout(10*1000);

        RestTemplate restTemplate = new RestTemplate(factory);

        String url  = RoadwinConfig.baseURL+"/service/booking";


        JSONObject jsonObject = new JSONObject();


        String jsonString = param.toString();

        log.info(jsonString);
        log.info(SecurityKey.encodeKey( param.toString().toUpperCase(Locale.ROOT) ));

        jsonObject.put("data" , SecurityKey.encodeKey( param.toString().toUpperCase(Locale.ROOT) ) );


        log.info(jsonObject.toString());

        HttpHeaders headers = new HttpHeaders();

        HttpEntity<?> entity = new HttpEntity<>(jsonObject , headers );

        headers.set("apiKey",RoadwinConfig.apiKey);
        headers.set("Content-Type","application/json");
        headers.set("Accept","application/json;charset=UTF-8");
        ResponseEntity<Map> responseEntity = null;

        responseEntity = restTemplate.exchange(
                url,
                HttpMethod.POST,
                entity,
                Map.class
        );



        HashMap hashMap = (HashMap) responseEntity.getBody();

        System.out.println("hashMap = " + hashMap);

        return new ObjectMapper().readValue( SecurityKey.decodeKey((String) hashMap.get("data")), ConcurrentHashMap.class);
    }





    /*
     * 주문취소
     * */
    @SneakyThrows
    public static ConcurrentHashMap cancel(RoadwinCancelReq roadwinCancelReq) {
        HttpComponentsClientHttpRequestFactory factory = new HttpComponentsClientHttpRequestFactory();
        factory.setConnectTimeout(10*1000);
        factory.setReadTimeout(10*1000);
        RestTemplate restTemplate = new RestTemplate(factory);

        String url  = RoadwinConfig.baseURL+"/service/cancel";

        JSONObject jsonObject = new JSONObject();

        String jsonString = roadwinCancelReq.toString();

        log.info(jsonString);
        log.info(SecurityKey.encodeKey( roadwinCancelReq.toString().toUpperCase(Locale.ROOT) ));

        jsonObject.put("data" , SecurityKey.encodeKey( roadwinCancelReq.toString().toUpperCase(Locale.ROOT) ) );


        log.info(jsonObject.toString());

        HttpHeaders headers = new HttpHeaders();

        HttpEntity<?> entity = new HttpEntity<>(jsonObject , headers );

        headers.set("apiKey",RoadwinConfig.apiKey);
        headers.set("Content-Type","application/json");
        headers.set("Accept","application/json;charset=UTF-8");

        ResponseEntity<Map> responseEntity = restTemplate.exchange(
                url,
                HttpMethod.POST,
                entity,
                Map.class
        );

        HashMap hashMap = (HashMap) responseEntity.getBody();

        System.out.println(hashMap);

        return new ObjectMapper().readValue( SecurityKey.decodeKey((String) hashMap.get("data")), ConcurrentHashMap.class);
    }

    /*
    * 수정
    * */
    @SneakyThrows
    public static ConcurrentHashMap modifyOrder(HashMap map) {
        HttpComponentsClientHttpRequestFactory factory = new HttpComponentsClientHttpRequestFactory();
        factory.setConnectTimeout(10*1000);
        factory.setReadTimeout(10*1000);
        RestTemplate restTemplate = new RestTemplate(factory);

        String url = RoadwinConfig.baseURL+ "/service/modifyOrder";


        JSONObject jsonObject = new JSONObject(map);


        String jsonString = jsonObject.toJSONString();

        log.info(SecurityKey.encodeKey( jsonString.toUpperCase(Locale.ROOT) ));

        jsonObject.put("data" , SecurityKey.encodeKey( jsonString.toUpperCase(Locale.ROOT) ) );


        log.info(jsonObject.toString());

        HttpHeaders headers = new HttpHeaders();

        HttpEntity<?> entity = new HttpEntity<>(jsonObject , headers );

        headers.set("apiKey",RoadwinConfig.apiKey);
        headers.set("Content-Type","application/json");
        headers.set("Accept","application/json;charset=UTF-8");

        ResponseEntity<Map> responseEntity = restTemplate.exchange(
                url,
                HttpMethod.POST,
                entity,
                Map.class
        );

        HashMap hashMap = (HashMap) responseEntity.getBody();

        System.out.println(hashMap);

        return new ObjectMapper().readValue( SecurityKey.decodeKey((String) hashMap.get("data")), ConcurrentHashMap.class);


    }






    /*
     * 기사 위치
     * */
    @SneakyThrows
    public static ConcurrentHashMap workerLocation(ConcurrentHashMap map) {
        HttpComponentsClientHttpRequestFactory factory = new HttpComponentsClientHttpRequestFactory();
        factory.setConnectTimeout(10*1000);
        factory.setReadTimeout(10*1000);
        RestTemplate restTemplate = new RestTemplate(factory);

        String url  = RoadwinConfig.baseURL+"/service/workerLocation";


        JSONObject obj = new JSONObject(map);

        String jsonString = obj.toJSONString();

        log.info(SecurityKey.encodeKey( jsonString.toUpperCase(Locale.ROOT) ));

        obj.put("data" , SecurityKey.encodeKey( jsonString.toUpperCase(Locale.ROOT) ) );


        log.info(obj.toString());

        HttpHeaders headers = new HttpHeaders();

        HttpEntity<?> entity = new HttpEntity<>(obj , headers );

        headers.set("apiKey",RoadwinConfig.apiKey);
        headers.set("Content-Type","application/json");
        headers.set("Accept","application/json;charset=UTF-8");

        ResponseEntity<Map> responseEntity = restTemplate.exchange(
                url,
                HttpMethod.POST,
                entity,
                Map.class
        );

        HashMap hashMap = (HashMap) responseEntity.getBody();

        System.out.println(hashMap);

        return new ObjectMapper().readValue( SecurityKey.decodeKey((String) hashMap.get("data")), ConcurrentHashMap.class);

    }



    /*
    * {"data":"암호화"}
    * */
    @SneakyThrows
    public String returnJsonString(String str){
        return "{\"data\":\""+ SecurityKey.encodeKey(str)+"\"}";
    }




}
