package com.escrow.kuda.controller;

import com.escrow.kuda.Role;
import com.escrow.kuda.model.camel.*;
import com.escrow.kuda.model.oauth.SessionUser;
import com.escrow.kuda.model.payment.NicePayParameter;
import com.escrow.kuda.oauth.LoginUser;
import com.escrow.kuda.service.SellerServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;


@Controller
public class SellerController {


    private Logger logger = LoggerFactory.getLogger(SellerController.class);

    @Autowired
    private SellerServiceImpl sellerService;

    /*
     * 로그인 Form
     * */

    @RequestMapping(value = {"/index", "/"}, method = RequestMethod.GET)
    public String sellerLoginForm(@LoginUser SessionUser user, @RequestParam @Nullable String inviteCode, HttpServletRequest request) {


        HttpSession httpSession = request.getSession();

        if (inviteCode != null) {
            httpSession.setAttribute("inviteCode", inviteCode);
        }

        String returnURL = "";
        if (user != null) {

            if (user.getUserState() != null) {

                if (user.getUserGrade().equals(Role.SELLER.getKey())) {
                    //                if(user.getUserState().equals("Y")){
                    returnURL = "redirect:/user/purchaseSale";
                    //                }
                    //                else if(user.getUserState().equals("N")){ //N
                    //                    returnURL = "redirect:/registration";
                    //                }else {
                    //                    returnURL = "redirect:/kudaAccount";
                    //                }

                } else if (user.getUserGrade().equals(Role.ADMIN.getKey())) {
                    returnURL = "redirect:/adm/escrowList";
                } else {
                    logger.info(user.getUserGrade());
                }
            }

        } else {
            returnURL = "/index";
        }

        return returnURL;
    }





    /*회원가입 나머지 정보 입력*/
    @PostMapping(value = "/upRemainInfo")
    public String updateSellerRemainInfo(UpRemainInfo upRemainInfo, @LoginUser SessionUser sessionUser, HttpServletRequest request,
                                         RedirectAttributes attributes) {

        HttpSession httpSession = request.getSession();

        upRemainInfo.setUserCode(sessionUser.getUserCode());

        String returnStr = sellerService.updateSellerRemainInfo(upRemainInfo);

        attributes.addAttribute("upRemainMsg", "회원가입이 완료 되었습니다.<br>다시로그인을 진행해 주세요.");

        if (returnStr.equals("ok") && httpSession.getAttribute("inviteCode") != null) {
            String invitePH = sellerService.selectSellerPH((String) httpSession.getAttribute("inviteCode"));
            logger.info(invitePH + "초대완료 메세지발신");
        }

        return "redirect:/logOut";
    }


    /*회원정보 수정*/
    @PostMapping(value = "/user/updateSeller")
    public String updateSeller(SellerUpdate sellerUpdate) {

        String returnURL = "";

        String returnStr = sellerService.updateSeller(sellerUpdate);

        if (returnStr.equals("ok")) {
            returnURL = "/profile";
        } else {
            returnURL = "redirect:/user/purchaseSale";
        }

        return returnURL;
    }


    /*회원가입*/
    @GetMapping(value = "/registration")
    public String registration(Model model, @LoginUser SessionUser user) {

        if (user == null) {
            return "/index";
        }

        String returnURL = "";

//        if(user != null) {
//            if (user.getUserState() != null && user.getUserState().equals("Y")) {
//                returnURL = "redirect:/purchaseSale";
//            } else {
//                returnURL = "/account/registration";
//                model.addAttribute("bankList", sellerService.bankList());
//            }
//        }else{
//            return "/index";
//        }


        model.addAttribute("bankList", sellerService.bankList());

        returnURL = "/account/registration";

        return returnURL;
    }


    /*MainPage*/
    @GetMapping(value = "/user/purchaseSale")
    public String purchaseSale(Model model, @LoginUser SessionUser user) {

        List<BuyList> buyLists = sellerService.buyLists(user.getUserCode());
        List<SellList> sellLists = sellerService.sellLists(user.getUserCode());

        model.addAttribute("buyLists", buyLists); //차량구매 리스트
        model.addAttribute("buyCount", buyLists.size()); //차량구매 리스트 갯수
        model.addAttribute("sellLists", sellLists); //차량판매 리스트
        model.addAttribute("sellCount", sellLists.size()); //차량판매 리스트 갯수

//        if(user.getUserGrade().equals(Role.ADMIN.getKey())){
//            return "redirect:/adm/escrowList";
//        }else{
        return "/purchaseSale";
//        }


    }


    /*딜러검색*/
    @GetMapping(value = "/user/search")
    public String search() {
        return "/search";
    }


    /*애스크로 거래 완료내역*/
    @GetMapping(value = "/user/history")
    public String history(Model model, @LoginUser SessionUser user) {

        List<HistoryBuyList> historyBuyLists = sellerService.historyBuyLists(user.getUserCode());
        List<HistorySellList> historySellLists = sellerService.historySellLists(user.getUserCode());

        model.addAttribute("buyLists", historyBuyLists); //차량구매 리스트
        model.addAttribute("buyCount", historyBuyLists.size()); //차량구매 리스트 갯수
        model.addAttribute("sellLists", historySellLists); //차량판매 리스트
        model.addAttribute("sellCount", historySellLists.size()); //차량판매 리스트 갯수

        return "/history";
    }

    /*프로필수정*/
    @GetMapping(value = "/user/profile")
    public String profile(@LoginUser SessionUser user, Model model) {

        model.addAttribute("profileList", sellerService.selectProfileInfo(user.getUserCode()));
        model.addAttribute("bankList", sellerService.bankList());

        return "/profile";
    }


    @PostMapping(value = "/user/updateProfile")
    public String updateProfile(SellerUpdate sellerUpdate, @LoginUser SessionUser user) {

        sellerUpdate.setSellerCode(user.getUserCode());
        String updateProfile = sellerService.updateSeller(sellerUpdate);

        logger.info("updateProfile : " + updateProfile);
        return "redirect:/user/purchaseSale";
    }

    /*계좌입력*/
    @GetMapping(value = "/user/remittance")
    public String remittance(@RequestParam String orderNo, @LoginUser SessionUser user, Model model , RedirectAttributes attributes) {

        String chk = sellerService.rmtAccessCheck(user.getUserCode() , orderNo);



        if (chk.equals("N")) {//본인 거래건이 아님
            attributes.addAttribute("remitMsg" , "n");
            return "redirect:/user/purchaseSale";
        }else if(chk.equals("N2")){//이미 송금건 등록 되어있음
//            attributes.addAttribute("remitMsg" , "n2");
            return "redirect:/user/alreadyRemit?orderNo=" +orderNo;
        }



        model.addAttribute("banklist", sellerService.bankList());
        model.addAttribute("history", sellerService.eRemittanceHistory(user.getUserCode()));
        model.addAttribute("myAccount", sellerService.selectProfileInfo(user.getUserCode()));
        return "/remittance";
    }

    /*이미 등록된 계좌*/
    @GetMapping(value = "/user/alreadyRemit")
    public String alreadyRemit(@RequestParam String orderNo , Model model){

        model.addAttribute("data" , sellerService.alreadyRemit(orderNo));

        return "/alreadyRemit";
    }


    /*정보기입*/
    @GetMapping(value = "/user/searchAdd")
    public String searchAdd(@LoginUser SessionUser user, @RequestParam int code, Model model) {

//        List<Escrow_list> el = sellerService.selectAddressHistory(user.getUserCode());
        model.addAttribute("sellerInfo", sellerService.sellerInfo(code));

        model.addAttribute("myInfo" , sellerService.myInfo(user.getUserCode()));

//        model.addAttribute("addressList", el);
//
//        model.addAttribute("addressListCount", el.size());

//        model.addAttribute("myAddress",sellerService.getMyAddress(user.getUserCode()));
//        model.addAttribute("vBank" , sellerService.vBankAuth());

        return "/searchAdd";
    }

    @PostMapping(value = "/user/escrowInsert")
    public String escrowInsert(EscrowInsert escrowInsert, @LoginUser SessionUser user, RedirectAttributes attributes) {

        escrowInsert.setEsrUserCode(user.getUserCode());

        ConcurrentHashMap<String, String> hashMap = sellerService.escrowProcess(escrowInsert);

        String returnStr = hashMap.get("returnStr");

        attributes.addAttribute("orderNo", hashMap.get("orderNo"));
        attributes.addAttribute("escrowInsertMsg", returnStr);
        return "redirect:/user/trnscRgstr";
    }

    /*애스크로 거래생성결과*/
    @GetMapping(value = "/user/searchAddResult")
    public String searchAddResult(Model model, @RequestParam String orderNo) {

        model.addAttribute("result", sellerService.searchAddResult(orderNo));
        return "/user/searchAddResult";
    }

    /*애스크로 가상계좌 발급*/
    @PostMapping(value = "/user/npVBankReturnURL")
    public String npVBankReturnURL(NicePayParameter nicePayParameter, @LoginUser SessionUser user) {

        nicePayParameter.setEsrUserCode(user.getUserCode());
        String returnStr = sellerService.npVBankReturnURL(nicePayParameter);

        return "redirect:/user/purchaseSale";

    }

    @GetMapping(value = "/kudaAccount")
    public String kudaAccount() {
        return "/kudaAccount";
    }

    @GetMapping(value = "/user/invite")
    public String invite() {

        return "/invite";
    }

    @PostMapping(value = "/user/inviteMessage")
    public String inviteMessage(@RequestParam @Nullable String phone, @RequestParam @Nullable String name
            , RedirectAttributes attributes, @LoginUser SessionUser user) {
        String rMsg = "";

        if (phone != null && name != null) {
            if (phone.length() < 10 || name.length() < 2) {
                rMsg = "이름 2자 이상, 휴대전화번호 10자 이상 입력해 주세요";
                attributes.addAttribute("inviteMsg", rMsg);

                return "redirect:/user/invite";
            }
        }


        rMsg = sellerService.inviteMessage(phone, name, user.getUserCode(), user.getName());
        attributes.addAttribute("inviteMsg", rMsg);


        return "redirect:/user/search";
    }


    /*
     * 거래진행 확인창 state = 0
     * */
    @GetMapping(value = "/user/trnscRgstr")
    public String trnscRgstr(@RequestParam String orderNo, @LoginUser SessionUser user, Model model) {

        model.addAttribute("list", sellerService.trnscRgstr(orderNo));

        return "/trnscRgstr";

    }

    /*
     * 요청자 거래완료 state 2 -> 3
     * */
    @GetMapping("/user/escrowCompleteProc")
    public String escrowCompleteProc(@RequestParam String orderNo, @RequestParam String productName, @LoginUser SessionUser user, RedirectAttributes attributes) {

        attributes.addAttribute("escrowCompleteProcMsg", sellerService.escrowCompleteProc(orderNo, user.getUserCode()));
        attributes.addAttribute("productNameMsg", productName);

        return "redirect:/user/purchaseSale";
    }


    @GetMapping("/user/deliveryCancel")
    public String deliveryCancel(String orderNo){

        String msg = sellerService.deliveryCancel(orderNo);

        return "";
    }


    @GetMapping("/user/csmAgree")
    public String csmAgree(String orderNo , Model model){

        model.addAttribute("data" , sellerService.csmAgree(orderNo));

        return "/csmAgree";
    }


    @GetMapping("/user/csmUpdate")
    public String csmUpdate(String orderNo , Model model){

        model.addAttribute("data" , sellerService.csmUpdate(orderNo));

        return "/csmUpdate";
    }


    @GetMapping("/user/driverLocation")
    public String driverLocation(String orderNo ,Model model){

        model.addAttribute("data",sellerService.driverLocation(orderNo));

        return "/driverLocation";


    }



    @GetMapping("/user/refund")
    public String refund(String orderNo ,Model model){

        model.addAttribute("data",sellerService.userRefund(orderNo));

        return "/refund";


    }




}
