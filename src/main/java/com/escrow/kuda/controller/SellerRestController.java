package com.escrow.kuda.controller;


import com.escrow.kuda.model.E_remittance_info;
import com.escrow.kuda.model.EscrowRefund;
import com.escrow.kuda.model.camel.SearchWhere;
import com.escrow.kuda.model.oauth.SessionUser;
import com.escrow.kuda.model.payment.GetAuth;
import com.escrow.kuda.model.payment.NicePayVBankResult;
import com.escrow.kuda.oauth.LoginUser;
import com.escrow.kuda.service.SellerServiceImpl;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.concurrent.ConcurrentHashMap;

@RestController
public class SellerRestController {


    @Autowired
    private SellerServiceImpl sellerService;


    /*찾기*/
    @GetMapping("/user/searchSellerList")
    public ConcurrentHashMap<String , Object> searchSellerList(SearchWhere search , @LoginUser SessionUser user){

        ConcurrentHashMap<String, Object> searchHash = new ConcurrentHashMap<>();

        search.setUserCode(user.getUserCode());

        searchHash.put("searchList" , sellerService.searchSellerList(search));

        return searchHash;
    }



    @PostMapping(value="/getAuthHash")
    public ConcurrentHashMap<String ,Object> getAuthHash(@RequestBody GetAuth getAuth){

        ConcurrentHashMap<String ,Object> returnHash = new ConcurrentHashMap<>();
        System.out.println("authAmount : " + getAuth.getAllAmountStr());
        returnHash.put("vBank" , sellerService.vBankAuth(getAuth.getAllAmountStr()));

        return returnHash;
    }


    /*가상계좌 입금 통보*/
    @PostMapping(value = "/nicepay/nicePayNoti")
    public String nicePayNoti(NicePayVBankResult nicePayVBankResult){

        return sellerService.nicePayNoti(nicePayVBankResult);
    }

    @GetMapping("/user/getStatus")
    public JSONObject getStatus(String orderNo){

        return sellerService.getStatus(orderNo);
    }


    @PostMapping(value="user/api/eRemitUpdate")
    public JSONObject eRemitUpdate(@RequestBody E_remittance_info eRemittanceInfo ,@LoginUser SessionUser user){

        return sellerService.eRemitUpdate(eRemittanceInfo , user);

    }

    @GetMapping("/user/api/csmAgreeY")
    public JSONObject csmAgreeY(@RequestParam String orderNo){
        return sellerService.csmAgreeY(orderNo);
    }


    @GetMapping("/user/api/orderCancel")
    public JSONObject orderCancel(@RequestParam String orderNo){

        return sellerService.orderCancel(orderNo);
    }


    @PostMapping("/user/api/escrowRefundInsert")
    public ConcurrentHashMap<String,String> escrowRefundInsert(@RequestBody EscrowRefund escrowRefund){

        System.out.println("escrowRefund = " + escrowRefund);
        
        return sellerService.escrowRefundInsert(escrowRefund);
    }





}
