package com.escrow.kuda.controller;

import com.escrow.kuda.model.admin.ExistsEscrowList;
import com.escrow.kuda.model.admin.SMSAutoSubmit;
import com.escrow.kuda.service.ApiServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ApiController {

    @Autowired
    ApiServiceImpl apiService;

    /*
     * 애스크로 문자 자동전달
     * */
    @PostMapping(value="/api/escrowSmsAutoSubmit")
    public void escrowSmsAutoSubmit(@RequestBody SMSAutoSubmit smsAutoSubmit){

        apiService.escrowSmsAutoSubmit(smsAutoSubmit);
    }


    /*
     * 애스크로 생성 제약조건
     * 거래상태(esl_state) 가 입금전(0) 이고 같은 금액 같은 예금주가 있을 시, 거래 생성불가
     * */
    @GetMapping(value="/api/existsEscrowList")
    public String existsEscrowList(ExistsEscrowList eel){
        return apiService.existsEscrowList(eel);
    }

}
