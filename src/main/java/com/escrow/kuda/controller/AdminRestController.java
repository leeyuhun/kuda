package com.escrow.kuda.controller;

import com.escrow.kuda.service.AdminServiceImpl;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.ConcurrentHashMap;

@RestController
public class AdminRestController {
    private Logger logger = LoggerFactory.getLogger(SellerController.class);

    @Autowired
    private AdminServiceImpl adminService;


    /*송금완료 process
    * 판매자(ess) / 요청자(esr) 문자처리
    * esl_state => 1
    * */
    @GetMapping("/adm/rComplete")
    public String remittanceComplete(String orderNo){
        return adminService.remittanceComplete(orderNo);
    }


    @GetMapping(value="/adm/escrowEnd")
    public String escrowEnd(String orderNo){
        return adminService.escrowEnd(orderNo);
    }

    /*
    * escrowList detail View
    * */
    @GetMapping(value = "/adm/escrowListDetail")
    public ConcurrentHashMap<String, Object>  escrowListDetail(String orderNo){

        ConcurrentHashMap<String , Object> map = new ConcurrentHashMap<>();

        map.put("detail" , adminService.escrowListDetail(orderNo));

        return map;

    }


    /*정산실행 페이지 입금처리*/
    @PostMapping("/adm/depositCompete")
    public JSONObject depositCompete(@RequestBody String codeList){

        ConcurrentHashMap<String, String> map = new ConcurrentHashMap<>();

        map.put("msg" , adminService.depositCompete(codeList));

        return new JSONObject(map);
    }


    @GetMapping("/adm/getStatus")
    public JSONObject getStatus(String orderNo){

        return adminService.getStatus(orderNo);
    }





}


