package com.escrow.kuda.controller;

import com.escrow.kuda.model.admin.*;
import com.escrow.kuda.service.AdminServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;


@Controller
public class AdminController {


    @Autowired
    private AdminServiceImpl adminService;


    /*
    * 애스크로 현재 거래리스트
    * */
    @GetMapping(value = "/adm/escrowList")
    public String escrowList(EscrowListWhere where , Model model){

        List<EscrowList> escrowList = adminService.escrowList(where);

        model.addAttribute("escrowList" , escrowList);

        return "/admin/escrow_list";

    }


    /*
    * 애스크로 판매자 리스트
    * */
    @GetMapping(value = "/adm/userList")
    public String userList(UserListWhere where , Model model){

        model.addAttribute("userList" , adminService.userList(where));

        return "/admin/user_list";
    }





    /*
    * 애스크로 완료내역
    * */
    @GetMapping(value = "/adm/escrowHistory")
    public String escrowHistory(EscrowHistoryWhere escrowHistoryWhere , Model model){

        model.addAttribute("escrowHistory" , adminService.escrowHistory(escrowHistoryWhere));

        return "/admin/escrow_history";
    }


    /*
     * 에스크로 정산리스트작성 (정산딜러 걸러내기)
     * */
    @GetMapping(value = "/adm/eRemitList")
    public String eRemitList(EscrowRemitListWhere erw , Model model){

        model.addAttribute("list" , adminService.eRemitList(erw));
        return "/admin/escrow_remit_list";
    }

    /*
     * 에스크로 정산실행페이지 (정산딜러 걸러내기)
     * */
    @GetMapping(value = "/adm/eRemitActionList")
    public String eRemitAction(){

        return "";
    }






}
