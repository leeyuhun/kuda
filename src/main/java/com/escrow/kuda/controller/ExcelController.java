package com.escrow.kuda.controller;

import com.escrow.kuda.model.excel.EsRemitBank;
import com.escrow.kuda.service.ExcelServiceImpl;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Controller
public class ExcelController {

    @Autowired
    private ExcelServiceImpl excelService;



    /*
    * 기업은행 정산 excel Download
    * */
    @GetMapping(value="/excel/eRemitBankExcel")
    public void eRemitBankExcel(@RequestParam(value = "codeString") String codeString , Model model, HttpServletResponse response
    ) throws IOException {

        XSSFWorkbook wb = new XSSFWorkbook();
        XSSFSheet sheet = wb.createSheet("첫번째 시트");
        XSSFRow row = null;
        XSSFCell cell = null;
        int rowNum = 0;


        // Header
        row = sheet.createRow(rowNum++);
        cell = row.createCell(0);
        cell.setCellValue("입금은행");
        cell = row.createCell(1);
        cell.setCellValue("입금계좌");
        cell = row.createCell(2);
        cell.setCellValue("고객관리성명");
        cell = row.createCell(3);
        cell.setCellValue("입금액");
        cell = row.createCell(4);
        cell.setCellValue("출금통장표시내용");
        cell = row.createCell(5);
        cell.setCellValue("입금통장표시내용");

        String[] codeArr =codeString.split(",");

        List<String> codeList = new ArrayList<>();

        Collections.addAll(codeList, codeArr);

        List<EsRemitBank> esRemitBanks = excelService.eRemitBankExcel(codeList);

        // Body
        for (int i=0; i < esRemitBanks.size(); i++) {

            row = sheet.createRow(rowNum++);
            cell = row.createCell(0);
            cell.setCellValue(esRemitBanks.get(i).getBankCode());
            cell = row.createCell(1);
            cell.setCellValue(esRemitBanks.get(i).getBankNumber());
            cell = row.createCell(2);
            cell.setCellValue(esRemitBanks.get(i).getBankHolder());
            cell = row.createCell(3);
            cell.setCellValue(esRemitBanks.get(i).getAmount());
            cell = row.createCell(4);
            if(esRemitBanks.get(i).getBankHolder().length()>7){
                cell.setCellValue(esRemitBanks.get(i).getBankHolder().substring(0,7));
            }else{
                cell.setCellValue(esRemitBanks.get(i).getBankHolder());
            }

            cell = row.createCell(5);
            cell.setCellValue("전중연안전거래");

        }

        String fileName= LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMddhhmmss")) +"_대량이체.xlsx";
        String outputFileName = new String(fileName.getBytes("KSC5601"), "8859_1");
        // 컨텐츠 타입과 파일명 지정
        response.setContentType("Application/Msexcel");
//        response.setHeader("Content-Disposition", "attachment;filename=example.xls");
        response.setHeader("Content-Disposition", "attachment;filename="+ outputFileName);


//        System.out.println("in ?");

        // Excel File Output

//        wb.write(response.getOutputStream());
//        wb.close();


        OutputStream fileOut  = response.getOutputStream();
        wb.write(fileOut);
        fileOut.close();
//        wb.close();

        response.getOutputStream().flush();
        response.getOutputStream().close();


//        return "/excel/escrow_remittance";
    }

}
