package com.escrow.kuda.controller;


import com.escrow.kuda.model.roadwin.ModifyReq;
import com.escrow.kuda.service.RoadwinServiceImpl;
import com.escrow.kuda.util.SecurityKey;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.json.simple.JSONObject;
import org.springframework.web.bind.annotation.*;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.servlet.http.HttpServletRequest;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@RestController
@Slf4j
@RequiredArgsConstructor
public class RoadwinRestController {

    private final RoadwinServiceImpl roadwinService;


    ObjectMapper mapper = new ObjectMapper();

    @PostMapping(value="/rwin/priceCheck")
    public ConcurrentHashMap priceCheck(@RequestBody Map<String, Object> param , HttpServletRequest http ) throws InvalidAlgorithmParameterException, UnsupportedEncodingException, NoSuchPaddingException, IllegalBlockSizeException, NoSuchAlgorithmException, BadPaddingException, InvalidKeyException, JsonProcessingException {


        System.out.println("param = " + param);

        return roadwinService.priceCheck(param);
    }


    /*@PostMapping("/rwin/booking")
    public HashMap booking(@RequestBody BookingReq param , HttpServletRequest http ) throws InvalidAlgorithmParameterException, UnsupportedEncodingException, NoSuchPaddingException, IllegalBlockSizeException, NoSuchAlgorithmException, BadPaddingException, InvalidKeyException, JsonProcessingException {

        param.setInsertURL(http.getRemoteHost());

        int returnInt =  roadwinService.booking(param);

        HashMap rMap = new HashMap();

        if (returnInt == 1) {
            rMap.put("msg", "ok");
        } else {
            rMap.put("msg", "fail");
        }

        log.info("rMap : " + rMap);

        return rMap;
    }*/


    @GetMapping("/rwin/cancel")
    public ConcurrentHashMap<String , String> roadwinCancel(String orderNo, HttpServletRequest http){
        log.info("orderNo : " + orderNo);

        return roadwinService.roadwinCancel(orderNo);

    }


    @SneakyThrows
    @CrossOrigin("*")
    @PostMapping(value = "/rwin/statusChange") //
    public String  statusChange(@RequestBody HashMap<String, Object> param){
        ConcurrentHashMap<String ,String> map = mapper.readValue(SecurityKey.decodeKey(param.get("data").toString()) , ConcurrentHashMap.class);

        return roadwinService.statusChange(map);
    }

    @SneakyThrows
    @CrossOrigin("*")
    @PostMapping(value = "/rwin/assign" )  //, @RequestHeader(value="Authorization") String apiKey
    public String  assign(@RequestBody HashMap<String, Object> param){
        System.out.println("param = " + param);
        ConcurrentHashMap<String ,Object> map = mapper.readValue(SecurityKey.decodeKey(param.get("data").toString()).toUpperCase(Locale.ROOT) , ConcurrentHashMap.class);

        return roadwinService.assign(map);
    }


    @SneakyThrows
    @CrossOrigin("*")
    @PostMapping(value="rwin/modifyOrder")
    public JSONObject modifyOrder(@RequestBody ModifyReq param){

        return new JSONObject( roadwinService.modifyOrder(param));
    }

}


