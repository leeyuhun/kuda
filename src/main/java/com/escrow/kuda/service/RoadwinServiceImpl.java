package com.escrow.kuda.service;


import com.escrow.kuda.mapper.RoadwinMapper;
import com.escrow.kuda.model.kakao.address.GetLonLat;
import com.escrow.kuda.model.kakao.address.KaKaoMap;
import com.escrow.kuda.model.roadwin.ModifyReq;
import com.escrow.kuda.model.roadwin.PriceCheckReq;
import com.escrow.kuda.model.roadwin.RoadwinCancelReq;
import com.escrow.kuda.model.schedule.RoadwinCsm;
import com.escrow.kuda.model.sms.*;
import com.escrow.kuda.util.DateReturn;
import com.escrow.kuda.util.infobank.MessageSubmit;
import com.escrow.kuda.util.kakaoaddress.KakaoAddress;
import com.escrow.kuda.util.message.MessageContent;
import com.escrow.kuda.util.roadwin.Roadwin;
import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.json.simple.JSONObject;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

@Service
@Slf4j
@RequiredArgsConstructor
public class RoadwinServiceImpl {

    private final RoadwinMapper roadwinMapper;

    private final MessageContent messageContent;
    private final MessageSubmit messageSubmit;


    /*
     * 가격체크
     * */
    public ConcurrentHashMap priceCheck(Map<String, Object> param) throws InvalidAlgorithmParameterException, UnsupportedEncodingException, NoSuchPaddingException, IllegalBlockSizeException, NoSuchAlgorithmException, BadPaddingException, InvalidKeyException, JsonProcessingException {

        PriceCheckReq priceCheckReq = new PriceCheckReq();

//        String destination = roadwinMapper.destination((String) param.get("orderNo"));

        String startingPoint = (String) param.get("address1");
        String endingPoint = (String) param.get("address2");


//        JSONObject jsonObject = new JSONObject();
//        JSONObject jsonObject1 = new JSONObject();

        KakaoAddress kakaoAddress = new KakaoAddress();

        //도착지 위도&경도&주소
        ResponseEntity<KaKaoMap> documentsEnd = kakaoAddress.xyReturn(endingPoint);

        List<GetLonLat> list = Objects.requireNonNull(documentsEnd.getBody()).getDocuments();

        priceCheckReq.setEnd_LON(Double.valueOf(list.get(0).getX()));
        priceCheckReq.setEnd_LAT(Double.valueOf(list.get(0).getY()));
        priceCheckReq.setEnd_ADDR(endingPoint);

        //출발지 위도&경도&주소
        ResponseEntity<KaKaoMap> documentsStarting = kakaoAddress.xyReturn(startingPoint);

        List<GetLonLat> startList = Objects.requireNonNull(documentsStarting.getBody()).getDocuments();

        priceCheckReq.setStart_LON(Double.valueOf(startList.get(0).getX()));
        priceCheckReq.setStart_LAT(Double.valueOf(startList.get(0).getY()));
        priceCheckReq.setStart_ADDR(startingPoint);

        ConcurrentHashMap map = Roadwin.priceCheck(priceCheckReq);

        /*DateReturn dr = new DateReturn();

        priceCheckReq.setResult_CODE((String) map.get("RESULT_CODE"));
        priceCheckReq.setResult_MSG((String) map.get("RESULT_MSG"));
        priceCheckReq.setDistance( "" + map.get("DISTANCE"));
        priceCheckReq.setPrice((int) map.get("PRICE"));
        priceCheckReq.setRw_ID((String) map.get("RW_ID"));
        priceCheckReq.setRDate(dr.preDate() );
        priceCheckReq.setRTime(dr.preTime() );
        priceCheckReq.setRequest_URL( (String)map.get("requestUrl"));
        priceCheckReq.setEsl_code((String) param.get("orderNo"));

        int res = roadwinMapper.insertConsignmentHistory(priceCheckReq);*/

        map.put("priceCheckVo", priceCheckReq);

        return map;
    }


    /*
     * 주문하기
     * */
/*    public int booking(BookingReq param) throws InvalidAlgorithmParameterException, UnsupportedEncodingException, NoSuchPaddingException, IllegalBlockSizeException, NoSuchAlgorithmException, BadPaddingException, InvalidKeyException, JsonProcessingException {

        PriceCheckReq checkVo = roadwinMapper.getStartEnd(param.getRw_ID());

        param.setStart_ADDR(checkVo.getStart_ADDR());
        param.setStart_ADDR_DETAIL(checkVo.getStart_ADDR());
        param.setStart_LAT(checkVo.getStart_LAT());
        param.setStart_LON(checkVo.getStart_LON());
        param.setEnd_ADDR(checkVo.getEnd_ADDR());
        param.setEnd_ADDR_DETAIL(checkVo.getEnd_ADDR());
        param.setEnd_LAT(checkVo.getEnd_LAT());
        param.setEnd_LON(checkVo.getEnd_LON());


        param.setComp_CODE("1279"); //고정 : RW_고유업체번호
        param.setOrder_TYPE("T"); //고정 : 탁송


        param.setTrans_ID(param.getOrderNo());


        ConcurrentHashMap eInfo = roadwinMapper.selectEscrowInfo(param.getOrderNo());

        param.setCustomer_NAME((String) eInfo.get("name"));
        param.setCustomer_PH((String) eInfo.get("ph"));
        param.setCar_NO((String) eInfo.get("carNo"));
        param.setCar_TYPE((String) eInfo.get("carType"));

        ConcurrentHashMap returnMap = Roadwin.booking(param);


        String resultCode = returnMap.get("RESULT_CODE").toString();
        param.setResult_CODE(resultCode);


        param.setResult_MSG((String) returnMap.get("RESULT_MSG"));

        DateReturn dr= new DateReturn();

        param.setRDate(dr.preDate());
        param.setRTime(dr.preTime());


        int result = 0;

        result = roadwinMapper.insertConsignment(param);


        return result;
    }*/

    /*
     * 주문취소
     * */
    @SneakyThrows
    public ConcurrentHashMap<String, String> roadwinCancel(String orderNo) {

//        int returnInt = roadwinMapper.roadwinCancel

        DateReturn dr = new DateReturn();

        String returnRWID = roadwinMapper.returnRWID(orderNo);

        RoadwinCancelReq roadwinCancelReq = new RoadwinCancelReq();

        roadwinCancelReq.setRw_ID(returnRWID);
        roadwinCancelReq.setCancel_REASON("고객 직접 취소");

        ConcurrentHashMap hashMap = Roadwin.cancel(roadwinCancelReq);

        String resultCode = (String) hashMap.get("RESULT_CODE");
        String resultMsg = (String) hashMap.get("RESULT_MSG");

        ConcurrentHashMap<String, String> insertHash = new ConcurrentHashMap<String, String>();

        insertHash.put("cDate", dr.preDate());
        insertHash.put("cTime", dr.preTime());
        insertHash.put("rw_ID", returnRWID);
        insertHash.put("cancel_REASON", roadwinCancelReq.getCancel_REASON());

        int updateStateResult = 0;
        int updateEscrowListResult = 0;

        /*
         * 취소 성공시 DB 테이블에 반영 consignment & escrow list
         * */
        if (resultCode.equals("0000")) {

            updateStateResult = roadwinMapper.rwinCancelConsignment(insertHash);
            updateEscrowListResult = roadwinMapper.rwinCancelEscrowList(orderNo);
            hashMap.put("insertResult", updateStateResult + "");
        }


        return hashMap;
    }


    /*
     * 상태변경
     * A:배정 / J:출발지도착 / S:출발 / E:도착 / D:완료 / C:취소
     * */
    @SneakyThrows
    @Transactional
    public String statusChange(ConcurrentHashMap<String, String> map) {

        log.info("statusChange.map : " + map);
        String SERVICE_VIEW_LINK = "";
        if (map.get("SERVICE_VIEW_LINK") != null && !map.get("SERVICE_VIEW_LINK").equals("")) {
            SERVICE_VIEW_LINK = map.get("SERVICE_VIEW_LINK");
            map.put("SERVICE_VIEW_LINK", SERVICE_VIEW_LINK);
        }

        int updateResult = 0;


        HashMap<String, String> resMap = new HashMap<String, String>();

        if (!SERVICE_VIEW_LINK.equals("")) {
            updateResult = roadwinMapper.statusChangeAndLink(map);
        } else {
            updateResult = roadwinMapper.statusChange(map);
        }


        String status = map.get("STATUS_CODE");

        if (status.equals("S")) {//출발
            RoadwinDeliveryStart roadwinDeliveryStart = roadwinMapper.roadwinDeliveryStartInfo(map.get("TRANS_ID"));
            roadwinDeliveryStart.setServiceViewLink(SERVICE_VIEW_LINK);

            if (roadwinDeliveryStart.getCarOwnerPh() != null && !roadwinDeliveryStart.getCarOwnerPh().equals("")) {
                messageSubmit.submitMSG(roadwinDeliveryStart.getCarOwnerPh(), messageContent.roadWin_delivery_start_owner(roadwinDeliveryStart));
            }

            messageSubmit.submitMSG(roadwinDeliveryStart.getReqPh(), messageContent.roadWin_delivery_start_requester(roadwinDeliveryStart));
            messageSubmit.submitMSG(roadwinDeliveryStart.getSellerPh(), messageContent.roadWin_delivery_start_seller(roadwinDeliveryStart));

            int updateEslDeliveryStart = roadwinMapper.updateEslDeliveryStart(roadwinDeliveryStart.getOrderNo()); //거래상태 : 4로 변경(배송중)
        } else if (status.equals("D")) {//완료
            RoadwinDeliveryEnd roadwinDeliveryEnd = roadwinMapper.roadwinDeliveryEndInfo(map.get("TRANS_ID"));

            if (roadwinDeliveryEnd.getCarOwnerPh() != null && !roadwinDeliveryEnd.getCarOwnerPh().equals("")) {
                messageSubmit.submitMSG(roadwinDeliveryEnd.getCarOwnerPh(), messageContent.roadWin_delivery_end_owner(roadwinDeliveryEnd));
            }

            messageSubmit.submitMSG(roadwinDeliveryEnd.getReqPh(), messageContent.roadWin_delivery_end_requester(roadwinDeliveryEnd));
            messageSubmit.submitMSG(roadwinDeliveryEnd.getSellerPh(), messageContent.roadWin_delivery_end_seller(roadwinDeliveryEnd));

            int updateEslDeliveryEnd = roadwinMapper.updateEslDeliveryEnd(roadwinDeliveryEnd.getOrderNo()); //거래상태 : 5로 변경(배송완료)

        }else if(status.equals("C")){ //취소
            RefundInfo refundInfo = roadwinMapper.refundInfo(map.get("TRANS_ID"));
            refundInfo.setOrderNo(map.get("TRANS_ID"));

            if (refundInfo.getOwnerPh() != null && !refundInfo.getOwnerPh().equals("")) {
                messageSubmit.submitMSG(refundInfo.getOwnerPh(), messageContent.refundInfo_owner(refundInfo));
            }

            messageSubmit.submitMSG(refundInfo.getSellerPh(), messageContent.refundInfo_seller(refundInfo));
            messageSubmit.submitMSG(refundInfo.getReqPh(), messageContent.refundInfo_requester(refundInfo));

            int updateRefundInfo = roadwinMapper.updateRefund(map.get("TRANS_ID"));

        }


        if (updateResult >= 1) {

            resMap.put("RESULT_CODE", "0000");
            resMap.put("RESULT_MSG", "SUCCESS");

        } else {

            resMap.put("RESULT_CODE", "4000");
            resMap.put("RESULT_MSG", "잘못된 요청입니다");

            log.info("assign : " + map.toString());
        }

        log.info("updateResult : " + updateResult);

        Roadwin roadwin = new Roadwin();

        JSONObject obj = new JSONObject(resMap);

        log.info(obj.toJSONString());

        return roadwin.returnJsonString(obj.toJSONString());
    }


    /*
     * 기사배차 성공
     * */
    @SneakyThrows
    public String assign(ConcurrentHashMap<String, Object> map) {


        HashMap<String, String> resMap = new HashMap<>();

        int updateResult = roadwinMapper.updateAssign(map);

        int aa = roadwinMapper.updateAssignEsl((String) map.get("TRANS_ID"));

        RoadWinDeliveryAssign roadWinDeliveryAssign = roadwinMapper.getCsmInfo((String) map.get("TRANS_ID"));
        roadWinDeliveryAssign.setDeliverName((String) map.get("DRIVER_NAME"));
        roadWinDeliveryAssign.setDeliverPh((String) map.get("DRIVER_HP"));
        roadWinDeliveryAssign.setCsmPrice( Integer.parseInt(String.valueOf(map.get("PRICE"))));



        log.info("차토스 기사배차 : " +map);

        if(updateResult >= 1 ){
            messageSubmit.submitMSG(roadWinDeliveryAssign.getSellerPh(),messageContent.roadWin_delivery_assign_seller(roadWinDeliveryAssign));
            messageSubmit.submitMSG(roadWinDeliveryAssign.getReqPh(),messageContent.roadWin_delivery_assign_requester(roadWinDeliveryAssign));
            resMap.put("RESULT_CODE" , "0000");
            resMap.put("RESULT_MSG" , "SUCCESS");

        }else{

            resMap.put("RESULT_CODE" , "4000");
            resMap.put("RESULT_MSG" , "잘못된 요청입니다");

            log.info("assign : " + map);
        }


        Roadwin roadwin = new Roadwin();

        JSONObject obj = new JSONObject(resMap);

        return roadwin.returnJsonString(obj.toJSONString());

    }


    /*
    * 수정
    * */
    public ConcurrentHashMap modifyOrder(ModifyReq req) {

        System.out.println(req);

        HashMap hashMap= new HashMap();
        hashMap.put("TRANS_ID" , req.getOrderNo());
        hashMap.put("PRICE" , req.getCsmPrice());

        ConcurrentHashMap returnMap =  Roadwin.modifyOrder(hashMap);

        if(returnMap.get("RESULT_CODE") != null && returnMap.get("RESULT_CODE").equals("0000")){
            RoadWinOrderModify roadWinOrderModify = roadwinMapper.roadWinOrderModifyInfo(req.getRwId());
            roadWinOrderModify.setCsmPriceNext(req.getCsmPrice());
            int updateCsmPrice = roadwinMapper.updateCsmPrice(req);

            messageSubmit.submitMSG(roadWinOrderModify.getReqPh() , messageContent.roadWin_modify_order_requester(roadWinOrderModify));
        }

        return returnMap;

    }

    /*
    *
    * */
    public void csmSchedule() {

        List<RoadwinCsm> list = roadwinMapper.csmSchedule();
        if(list != null && list.size()>0){
            List<String> eslCodeList = new ArrayList<>();
            for (RoadwinCsm roadwinCsm : list) {
                messageSubmit.submitMSG(roadwinCsm.getReqPh(), messageContent.roadWin_notAssign_requester(roadwinCsm));
                eslCodeList.add(roadwinCsm.getOrderNo());
            }
            log.info("eslCodeList batch = " + eslCodeList);
            int i = roadwinMapper.csmScheduleUpdate(eslCodeList);
        }

    }
}
