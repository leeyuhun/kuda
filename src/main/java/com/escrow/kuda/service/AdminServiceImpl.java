package com.escrow.kuda.service;

import com.escrow.kuda.mapper.AdminMapper;
import com.escrow.kuda.model.admin.*;
import com.escrow.kuda.model.util.EslInfo;
import com.escrow.kuda.util.DateReturn;
import com.escrow.kuda.util.infobank.MessageSubmit;
import com.escrow.kuda.util.message.MessageContent;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

@Service
public class AdminServiceImpl {

    @Autowired
    private AdminMapper adminMapper;

    @Autowired
    MessageSubmit messageSubmit;

    @Autowired
    MessageContent messageContent;

    private Logger logger = LoggerFactory.getLogger(AdminServiceImpl.class);


    /*송금완료 process
     * 판매자(ess) / 요청자(esr) 문자처리
     * esl_state 0 => 1
     * */
    public String remittanceComplete(String orderNo) {

        String returnMsg = "";


        RemitCompEsr remitCompEsr = adminMapper.remitCompEsrInfo(orderNo);

        RemitCompEss remitCompEss = adminMapper.remitCompEssInfo(orderNo);

        remitCompEsr.setEssName(remitCompEss.getEssName());
        remitCompEsr.setEssPh(remitCompEss.getEssPh());
        remitCompEsr.setOrderNo(orderNo);


        /*요청자 문자*/
        String esrMsg = messageSubmit.submitMSG(remitCompEsr.getEsrPh() , messageContent.messageContent2_requester(remitCompEsr));
        /*판매자 문자*/
        String essMsg = messageSubmit.submitMSG(remitCompEss.getEssPh() , messageContent.messageContent2_seller(remitCompEsr));





        int eslStateUpdate = adminMapper.eslStateUpdate(orderNo , "0", "1");

        if(eslStateUpdate == 1 && esrMsg.equals("ok") && essMsg.equals("ok")){
            returnMsg = "ok";
        }

        return returnMsg;
    }


    /*
    * 애스크로 진행리스트
    * */
    public List<EscrowList> escrowList(EscrowListWhere where) {

        DateReturn dr = new DateReturn();
        List<EscrowList> escrowList =  adminMapper.escrowList(where);
        
        String startDate = where.getStartDate() == null ? "" : where.getStartDate();
        String lastDate = where.getLastDate() == null ? "" : where.getLastDate();
        String state = where.getLastDate() == null ? "" : where.getLastDate();
        String searchWhere = where.getSearchWhere() == null ? "" : where.getSearchWhere();
        String searchText = where.getSearchText() == null ? "" : where.getSearchText();

        String sqlWhere = "";


        if(startDate.equals("")){
            startDate = dr.preDate();
        }

        if(lastDate.equals("")){
            lastDate = dr.preDate();
        }

        if(!state.equals("")){
            state = where.getState();
        }

        if(searchWhere.equals("")){

        }else{


        }

        return escrowList ;
    }


    /*
    * 애스크로 완료리스트
    * */

    public List<EscrowHistory> escrowHistory(EscrowHistoryWhere escrowHistoryWhere) {

        return adminMapper.escrowHistory(escrowHistoryWhere);
    }

    /*
    * 유저리스트
    * */

    public List<UserList> userList(UserListWhere where) {


        List<UserList> userLists = adminMapper.userList(where);

        return adminMapper.userList(where);
    }


    /*
    * 애스크로 완료거래처리(어드민 송금완료)
    * */
    public String escrowEnd(String orderNo) {
        String returnMsg = "not";

        DateReturn dr = new DateReturn();

        EEndEslUpdate eslUpdate = new EEndEslUpdate();

        eslUpdate.setOrderNo(orderNo);
        eslUpdate.setDate(dr.preDate());
        eslUpdate.setTime(dr.preTime());


        int escrowResult = adminMapper.eEndEslUpdate(eslUpdate);

//        int escrowResult = adminMapper.eslStateUpdate(orderNo , "4" , "5");



        if(escrowResult >= 1){


            EslInfo eslInfo = adminMapper.eslInfo(orderNo);
            messageSubmit.submitMSG(eslInfo.getESellerPh() , messageContent.messageContent6_seller(eslInfo) );
            messageSubmit.submitMSG(eslInfo.getERequesterPh() , messageContent.messageContent6_requester(eslInfo) );

            if(eslInfo.getCarOwnerPh() != null && !eslInfo.getCarOwnerPh().equals("")){
                messageSubmit.submitMSG(eslInfo.getESellerPh(), messageContent.messageContent6_seller(eslInfo));
            }

            int eRemitStateUpdate = adminMapper.eRemitStateUpdate(orderNo);

            if(eRemitStateUpdate >= 1) returnMsg = "ok";

        }


        return returnMsg;
    }

    /*
    *  escrowList detail View
    * */
    public EscrowListDetail escrowListDetail(String orderNo) {

        EscrowListDetail ed = adminMapper.escrowListDetail(orderNo);

        if(ed.getBankName() != null && ed.getBankName().equals("")){
            ed.setBankInfo("N");
        }

        return ed;
    }



    /*
     * 에스크로 정산리스트작성 (정산딜러 걸러내기)
     * */
    public List<EscrowRemitList> eRemitList(EscrowRemitListWhere erw) {
        return adminMapper.eRemitList(erw);
    }


    public String depositCompete(String codeList) {

        String returnMsg = "not";
        String[] codeArr = codeList.split(",");
        ArrayList<String> lists = new ArrayList<>();

        Collections.addAll(lists,codeArr);

        int eslRes = adminMapper.eslListUpdate(lists);

        if(eslRes >= 1){


            List<EslInfo> eslListInfo = adminMapper.eslListInfo(lists);

            for (EslInfo eslInfo : eslListInfo) {
                messageSubmit.submitMSG(eslInfo.getESellerPh(), messageContent.messageContent6_seller(eslInfo));
                messageSubmit.submitMSG(eslInfo.getERequesterPh(), messageContent.messageContent6_requester(eslInfo));

                if(eslInfo.getCarOwnerPh() != null && !eslInfo.getCarOwnerPh().equals("")){
                    messageSubmit.submitMSG(eslInfo.getESellerPh(), messageContent.messageContent6_seller(eslInfo));
                }
            }

            int eRemitStateListUpdate = adminMapper.eRemitStateListUpdate(lists);

            if(eRemitStateListUpdate >= 1) returnMsg = "ok";

        }

        return returnMsg;
    }


    public JSONObject getStatus(String orderNo) {

        ConcurrentHashMap<String,String> map = new ConcurrentHashMap<>();

        map.put("status" , adminMapper.getStatus(orderNo));
        return new JSONObject(map);
    }
}
