package com.escrow.kuda.service;

import com.escrow.kuda.mapper.SellerMapper;
import com.escrow.kuda.model.*;
import com.escrow.kuda.model.camel.*;
import com.escrow.kuda.model.oauth.SessionUser;
import com.escrow.kuda.model.payment.*;
import com.escrow.kuda.model.roadwin.BookingReq;
import com.escrow.kuda.model.sms.CsmAgreeInfo;
import com.escrow.kuda.model.sms.OrderCancelSmsInfo;
import com.escrow.kuda.model.util.DeliveryCompInfo;
import com.escrow.kuda.model.util.RemitEsl;
import com.escrow.kuda.util.CodeReturn;
import com.escrow.kuda.util.DateReturn;
import com.escrow.kuda.util.MaskReturn;
import com.escrow.kuda.util.infobank.MessageSubmit;
import com.escrow.kuda.util.message.MessageContent;
import com.escrow.kuda.util.nicepay.ConnectToServer;
import com.escrow.kuda.util.nicepay.DataEncrypt;
import com.escrow.kuda.util.nicepay.NicePayUtil;
import com.escrow.kuda.util.roadwin.Roadwin;
import org.json.simple.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

@Service
public class SellerServiceImpl{

    @Autowired
    private SellerMapper sellerMapper;

    @Autowired
    MessageSubmit messageSubmit;

    @Autowired
    MessageContent messageContent;

    private Logger logger = LoggerFactory.getLogger(SellerServiceImpl.class);



    /*
     * 판매자 지정계좌 가져오기
     * */
    public List<sRemittanceInfoVo> sRemittanceInfo(int sellerCode) {
        return sellerMapper.sRemittanceInfo(sellerCode);
    }


    public List<CamelSeller> searchSellerList(SearchWhere search) {
        return sellerMapper.searchSellerList(search);
    }

    /*
    * 판매자 나머지 정보 기입
    * */
    public String updateSellerRemainInfo(UpRemainInfo upRemainInfo) {




        String rVal = "not";
        int updateResult = sellerMapper.updateSellerRemainInfo(upRemainInfo);
        int insertSRemittanceInfo = sellerMapper.insertSRemittanceInfo(upRemainInfo);



        if(updateResult >=1 && insertSRemittanceInfo >= 1){

            rVal = "ok" ;
        }
        return rVal;
    }


    /*
    * 에스크로 계상계좌X 버젼
    * */
    @Transactional(rollbackFor = Exception.class)
    public ConcurrentHashMap<String,String> escrowProcess(EscrowInsert ep) {


        System.out.println(ep.toString());

        ep.setAllAmount(Long.parseLong(ep.getAllAmountStr().replaceAll(",","")) );
        ep.setDeliveryCost(ep.getDeliveryCost().replaceAll(",",""));


        DateReturn dr= new DateReturn();
        CodeReturn cr = new CodeReturn();
        String returnStr = "not";

        String eslCode = cr.numberReturn();
        String esrCode = cr.numberReturn(); //escrow_requester(요청자) table pk
        String essCode = cr.numberReturn(); //escrow_seller(판매자) table pk

        //금액 comma 제거
        long paymentAmount = Long.parseLong( ep.getAllAmountStr().replaceAll("," , "") );

        //프로모션 3개월간 수수료 0원
        long feeAmount = 0;

        /*거래대금이 10,000,000 이상일시 수수료 0.1*/
//        if (paymentAmount >= 10000000) {
//            feeAmount = paymentAmount / 1000 ;
//        }

        long allAmount = paymentAmount + feeAmount;

        ep.setEslCode(eslCode);
        ep.setFeeAmount(feeAmount); //수수료
        ep.setPayAmount(paymentAmount); //정산금액
        ep.setAllAmount(allAmount); //총거래금액
        ep.setRequestDate(dr.preDate());
        ep.setRequestTime(dr.preTime());
        ep.setEssCode(essCode);
        ep.setEsrCode(esrCode);

        ep.setProductNumName( ep.getProductName() + "-" + ep.getProductNum() );


        int insertEscrowList =  sellerMapper.insertEscrowList(ep);


        //애스크로 판매자 Table

        Escrow_seller es = new Escrow_seller();
        es.setSeller_code(ep.getEssUserCode());
        es.setEss_code(essCode);
        int insertEscrowSeller = sellerMapper.insertEscrowSeller(es);

        //애스크로 구매자 Table

        Escrow_requester er = new Escrow_requester();
        er.setSeller_code(ep.getEsrUserCode());
        er.setEsr_code(esrCode);
        int insertEscrowRequester = sellerMapper.insertEscrowRequester(er);

        if(insertEscrowList >=1 && insertEscrowRequester >= 1 && insertEscrowSeller >= 1){
            returnStr = "ok";

            int insertEsr = sellerMapper.insertEscrowRemit(eslCode);
            int insertConsignment = sellerMapper.insertConsignment(ep);

            Seller sellerInfo = sellerMapper.getEsrUserPh(ep.getEsrUserCode());
            messageSubmit.submitMSG(sellerInfo.getSeller_ph() , messageContent.messageContent1_requester(sellerInfo.getSeller_name() , ep.getBankHolder(), allAmount));

        }

        ConcurrentHashMap<String,String> returnHash = new ConcurrentHashMap<>();

        returnHash.put("escrowProcessMsg", returnStr);
        returnHash.put("orderNo", eslCode);


        return returnHash;

    }


    /*
    * 에스크로 거래등록 완료창
    * */
    public TrnscRgstr trnscRgstr(String orderNo) {

        TrnscRgstr trnscRgstr =  sellerMapper.trnscRgstr(orderNo);
        String pNumName = trnscRgstr.getProductName(); // 차량번호 & 차량명 차종 분리
        String[] nnSplit =  pNumName.split("-");

        trnscRgstr.setProductName(nnSplit[0]);
        trnscRgstr.setProductNum(nnSplit[1]);

//        TrnConsignment trnConsignment = new TrnConsignment();
//        if(trnscRgstr.getDeliveryIsuse() != null && trnscRgstr.getDeliveryIsuse().equals("Y")){

//            TrnConsignment trnConsignment =  sellerMapper.trnConsignment(orderNo);

//            trnscRgstr.setTrnConsignment(trnConsignment);

//        }


        return trnscRgstr;
    }


    /*
     * 요청자 거래완료 state 2 -> 3
    * */
    public String escrowCompleteProc(String orderNo, int userCode) {

        String returnMsg = "";
        StateUpdate su = new StateUpdate();

        su.setCheck("1");
        su.setState("6");
        su.setOrderNo(orderNo);
        su.setUserCode(userCode);

        String calTime = new DateReturn().escrowCalDatetime();

        int result = sellerMapper.stateUpdate(su);

        if(result >= 1) returnMsg = "ok";

        DeliveryCompInfo deliveryCompInfo  = sellerMapper.deliveryCompInfo(orderNo);

        deliveryCompInfo.setOrderNo(orderNo);
        deliveryCompInfo.setCalDate(calTime);

        ConcurrentHashMap<String, Object> map = new ConcurrentHashMap<>();

        map.put("productName" , deliveryCompInfo.getProductName() );
        map.put("amount" , deliveryCompInfo.getPaymentMoney());
        map.put("calDate" , calTime);

        messageSubmit.submitMSG(deliveryCompInfo.getEssPh()  , messageContent.messageContent4_seller(deliveryCompInfo) );

        if(deliveryCompInfo.getCarOwnerPh() != null && !deliveryCompInfo.getCarOwnerPh().equals("") ){
            messageSubmit.submitMSG(deliveryCompInfo.getCarOwnerPh()  , messageContent.messageContent4_seller_owner(deliveryCompInfo) );
        }

        messageSubmit.submitMSG(deliveryCompInfo.getEsrPh() , messageContent.messageContent4_requester(map) );

//        if(deliveryCompInfo.getAccount() == null || deliveryCompInfo.getHolder().equals("")){
//
//            messageSubmit.submitMSG(deliveryCompInfo.getEssPh() , messageContent.messageContent4_seller_no(deliveryCompInfo) );
//        }

        return returnMsg;
    }



    /*
    * bankCode로 bankName 가져오기
    * */
    public String getBankName(String bank_code) {
        return sellerMapper.getBankName(bank_code);
    }

    public long getEscrowPrice(String esl_code) {
        return sellerMapper.getEscrowPrice(esl_code);
    }




    //유저 정보수정
    public String updateSeller(SellerUpdate sellerUpdate) {

        String returnStr= "";
        int updateSellerResult = sellerMapper.updateSeller(sellerUpdate);

        int sRemittanceResult = sellerMapper.updateSellerRemittance(sellerUpdate);

        if(updateSellerResult + sRemittanceResult >= 2) returnStr= "ok";

        return returnStr;
    }



    /*
    * 은행계좌 (select all)
    * */
    public List<Bank> bankList() {
        return sellerMapper.bankList();
    }


    /*seller Info*/
    public Seller sellerInfo(int code) {
        return sellerMapper.sellerInfo(code);
    }


    /*에스크로 구매리스트*/
    public List<BuyList> buyLists(int userCode) {
        return sellerMapper.buyLists(userCode);
    }

    /*에스크로 판매리스트*/
    public List<SellList> sellLists(int userCode) {
        return sellerMapper.sellerLists(userCode);
    }

    /*프로필 수정 Select*/
    public ProfileUpdate selectProfileInfo(int userCode) {
        return sellerMapper.selectProfileInfo(userCode);
    }



    /*애스크로 구매완료내역*/
    public List<HistoryBuyList> historyBuyLists(int userCode) {
        return sellerMapper.historyBuyLists(userCode);
    }

    /*애스크로 판매완료내역*/
    public List<HistorySellList> historySellLists(int userCode) {
        return sellerMapper.historySellLists(userCode);
    }

    /*가상계좌 인증토큰 */
    public Vbank vBankAuth(String amount) {

        NicePayUtil ncu         = new NicePayUtil();
        DataEncrypt sha256Enc 	= new DataEncrypt();
        String ediDate 			= ncu.getyyyyMMddHHmmss();

        Vbank vbank             = new Vbank();

        String merchantKey 		= "q8X3l1HxvSUuMKyfF/C/tXp7lLUC1GUX/dmUoUv6K6SBMjS7zGUy+RhFZGlq5/ffKjt8HKdASEKUIehjsEKN9A==";
        String merchantID       = "cidauto01m";
        String price            = amount.replace("," , ""); //콤마제거
        String hashString 		= sha256Enc.encrypt(ediDate + merchantID + price + merchantKey); // 현재시간 + PG가맹점코드 + 가격 + 상점키

        vbank.setHashString(hashString);
        vbank.setEdiDate(ediDate);

        return vbank;
    }

    public NicePayForm nicePayFormLoad(SearchAdd searchAdd) {

        NicePayUtil ncu         = new NicePayUtil();
        DataEncrypt sha256Enc 	= new DataEncrypt();
        String ediDate 			= ncu.getyyyyMMddHHmmss();
        NicePayForm  nicePayForm = new NicePayForm();

        DateReturn dr = new DateReturn();


        Vbank vbank             = new Vbank();

//        String merchantKey 		= "EYzu8jGGMfqaDEp76gSckuvnaHHu+bC4opsSN6lHv3b2lurNYkVXrZ7Z1AoqQnXI3eLuaUFyoRNC6FkrzVjceg==";
//        String merchantID       = "nicepay00m";
        String merchantKey 		= "q8X3l1HxvSUuMKyfF/C/tXp7lLUC1GUX/dmUoUv6K6SBMjS7zGUy+RhFZGlq5/ffKjt8HKdASEKUIehjsEKN9A==";
        String merchantID       = "cidauto01m";
        String price            = searchAdd.getAmt().replace("," , ""); //콤마제거
        String hashString 		= sha256Enc.encrypt(ediDate + merchantID + price + merchantKey); // 현재시간 + PG가맹점코드 + 가격 + 상점키


        nicePayForm.setSignData(hashString);
        nicePayForm.setEdiDate(ediDate);


        return nicePayForm;
    }



    public NicePayForm escrowVbankProcess(GetAuth getAuth) {

        NicePayUtil ncu         = new NicePayUtil();
        DataEncrypt sha256Enc 	= new DataEncrypt();
        String ediDate 			= ncu.getyyyyMMddHHmmss();
        NicePayForm  nicePayForm = new NicePayForm();

        DateReturn dr = new DateReturn();
        CodeReturn cr = new CodeReturn();


        Vbank vbank             = new Vbank();

        String merchantKey 		= "q8X3l1HxvSUuMKyfF/C/tXp7lLUC1GUX/dmUoUv6K6SBMjS7zGUy+RhFZGlq5/ffKjt8HKdASEKUIehjsEKN9A==";
        String merchantID       = "cidauto01m";
        String price            = getAuth.getAllAmountStr().replaceAll("," ,""); //콤마제거
        String hashString 		= sha256Enc.encrypt(ediDate + merchantID + price + merchantKey); // 현재시간 + PG가맹점코드 + 가격 + 상점키
        String orderNo          = cr.numberReturn();

        nicePayForm.setGoodsName(getAuth.getProductName() + "-" + getAuth.getProductNum() );
        nicePayForm.setAmt( price );
        nicePayForm.setMID( merchantID );
        nicePayForm.setMoid( orderNo );
        nicePayForm.setSignData(hashString);
        nicePayForm.setEdiDate(ediDate);

        return nicePayForm;
    }


    /*NICEPAY 결과 */
    public String npVBankReturnURL(NicePayParameter nicePayParameter) {

        String resultJsonStr = "";

        String ResultCode 	= ""; String ResultMsg 	= ""; String PayMethod 	= "";
        String GoodsName 	= "";  String TID 		= "";

        String authResultCode 	= nicePayParameter.getAuthResultCode(); 	// 인증결과 : 0000(성공)
        String authResultMsg 	= nicePayParameter.getAuthResultMsg(); 	// 인증결과 메시지
        String nextAppURL 		= nicePayParameter.getNextAppURL(); 		// 승인 요청 URL
        String txTid 			= nicePayParameter.getTxTid(); 			// 거래 ID
        String authToken 		= nicePayParameter.getAuthToken(); 		// 인증 TOKEN
        String payMethod 		= nicePayParameter.getPayMethod(); 		// 결제수단
        String mid 				= nicePayParameter.getMID(); 				// 상점 아이디
        String moid 			= nicePayParameter.getMoid(); 			// 상점 주문번호
        String amt 				= nicePayParameter.getAmt(); 				// 결제 금액
        String reqReserved 		= nicePayParameter.getReqReserved(); 		// 상점 예약필드
        String netCancelURL 	= nicePayParameter.getNetCancelURL(); 	// 망취소 요청 URL
        int essUserCode 	    = nicePayParameter.getEssUserCode(); 	// 망취소 요청 URL
        int esrUserCode 	    = nicePayParameter.getEsrUserCode(); 	// 망취소 요청 URL

        String returnStr = "";
        if(authResultCode.equals("0000")){

            NicePayUtil npu = new NicePayUtil();
            ConnectToServer cts = new ConnectToServer();



            /*
             ****************************************************************************************
             * <해쉬암호화> (수정하지 마세요)
             * SHA-256 해쉬암호화는 거래 위변조를 막기위한 방법입니다.
             ****************************************************************************************
             */

            DataEncrypt sha256Enc 	= new DataEncrypt();
            String merchantKey 		= "q8X3l1HxvSUuMKyfF/C/tXp7lLUC1GUX/dmUoUv6K6SBMjS7zGUy+RhFZGlq5/ffKjt8HKdASEKUIehjsEKN9A=="; // 상점키
            String ediDate			= npu.getyyyyMMddHHmmss();
            String signData 		= sha256Enc.encrypt(authToken + mid + amt + ediDate + merchantKey);

            /*
             ****************************************************************************************
             * <승인 요청>
             * 승인에 필요한 데이터 생성 후 server to server 통신을 통해 승인 처리 합니다.
             ****************************************************************************************
             */


            StringBuffer requestData = new StringBuffer();
            requestData.append("TID=").append(txTid).append("&");
            requestData.append("AuthToken=").append(authToken).append("&");
            requestData.append("MID=").append(mid).append("&");
            requestData.append("Amt=").append(amt).append("&");
            requestData.append("EdiDate=").append(ediDate).append("&");
            requestData.append("CharSet=").append("utf-8").append("&");
            requestData.append("SignData=").append(signData);


            try {
                resultJsonStr = cts.connectToServer(requestData.toString(), nextAppURL);
            } catch (Exception e) {
                e.printStackTrace();
            }


            ConcurrentHashMap resultData = new ConcurrentHashMap();
            boolean paySuccess = false;
            if("9999".equals(resultJsonStr)){
                /*
                 *************************************************************************************
                 * <망취소 요청>
                 * 승인 통신중에 Exception 발생시 망취소 처리를 권고합니다.
                 *************************************************************************************
                 */
                StringBuffer netCancelData = new StringBuffer();
                requestData.append("&").append("NetCancel=").append("1");
                String cancelResultJsonStr = null;

                try {
                    cancelResultJsonStr = cts.connectToServer(requestData.toString(), netCancelURL);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                ConcurrentHashMap cancelResultData = null;

                try {
                    cancelResultData = npu.jsonStringToHashMap(cancelResultJsonStr);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                ResultCode = (String)cancelResultData.get("ResultCode");
                ResultMsg = (String)cancelResultData.get("ResultMsg");

                logger.info("망취소발생 \r\n " + ResultCode  + "\r\n"+ ResultMsg) ;
            }else{

                try {
                    resultData = npu.jsonStringToHashMap(resultJsonStr);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                ResultCode 	= (String)resultData.get("ResultCode");	// 결과코드 (정상 결과코드:3001)


                if(ResultCode.equals("4100")){ //가상계좌 발급성공


                    System.out.println("발급성공");

                    String bankCode =  (String)resultData.get("VbankBankCode");
                    String bankName =  (String)resultData.get("VbankBankName");
                    String bankNum =  (String)resultData.get("VbankNum");
                    String expDate =  (String)resultData.get("VbankExpDate");
                    String expTime =  (String)resultData.get("VbankExpTime");
                    String resultAmt =  (String)resultData.get("Amt");
                    String buyerName =  (String)resultData.get("BuyerName");
                    String buyerTel =  (String)resultData.get("BuyerTel");
                    String goodsName =  (String)resultData.get("GoodsName");
                    String authDate = (String)resultData.get("AuthDate");
                    String resultMoid = (String)resultData.get("Moid");


                    returnStr = resultMoid; //esl_code return ;




                    CodeReturn cr = new CodeReturn();
                    String esrCode = cr.numberReturn(); //escrow_requester(요청자) table pk
                    String essCode = cr.numberReturn(); //escrow_seller(판매자) table pk



                    //금액 comma 제거
                    long allAmount = Long.parseLong(resultAmt);

                    //프로모션 3개월간 수수료 0원
                    long feeAmount = 0;

                    /*거래대금이 10,000,000 이상일시 수수료 0.1*/
            //        if (paymentAmount >= 10000000) {
            //            feeAmount = paymentAmount / 1000 ;
            //        }


                    System.out.println("essUserCode : " + reqReserved);
                    System.out.println("esrUserCode : " + esrUserCode);
                    //애스크로 판매자 Table

                    Escrow_seller es = new Escrow_seller();
                    es.setSeller_code( Integer.parseInt(reqReserved) );
                    es.setEss_code(essCode);
                    int insertEscrowSeller = sellerMapper.insertEscrowSeller(es);

                    //애스크로 구매자 Table

                    Escrow_requester er = new Escrow_requester();
                    er.setSeller_code(nicePayParameter.getEsrUserCode());
                    er.setEsr_code(esrCode);
                    int insertEscrowRequester = sellerMapper.insertEscrowRequester(er);

                    Escrow_list escrowList = new Escrow_list();
                    DateReturn dr= new DateReturn();



                    ConcurrentHashMap<String , String> authDateHashMap = dr.dateFormat(authDate);

                    /*escrow_list table insert info*/
                    escrowList.setEsl_code(resultMoid);
                    escrowList.setEsl_all_amount(allAmount);
                    escrowList.setEsl_fee_amount(feeAmount);
                    escrowList.setEsl_payment_amount(allAmount);
                    escrowList.setEsr_code(esrCode);
                    escrowList.setEss_code(essCode);
                    escrowList.setEsl_request_date(dr.preDate());
                    escrowList.setEsl_request_time(dr.preTime());
                    escrowList.setEsl_product_name(goodsName);
                    escrowList.setEsl_bank_code(bankCode);
                    escrowList.setEsl_bank_name(bankName);
                    escrowList.setEsl_bank_number(bankNum);
                    escrowList.setEsl_exp_date(expDate);
                    escrowList.setEsl_exp_time(expTime);
                    escrowList.setEsl_auth_date(authDateHashMap.get("date"));
                    escrowList.setEsl_auth_time(authDateHashMap.get("time"));



                    int insertEscrowList = sellerMapper.insertEscrow(escrowList);


                    if(insertEscrowList + insertEscrowSeller + insertEscrowRequester >=3){
                        System.out.println("insert success");
                    }else{
                        System.out.println("not insert");
                    }
                }else{ //실패

                    System.out.println("실패");


                }


                /*
                 *************************************************************************************
                 * <결제 성공 여부 확인>
                 *************************************************************************************
                 */
                if(PayMethod != null){
                    if(PayMethod.equals("CARD")){
                        if(ResultCode.equals("3001")) paySuccess = true; // 신용카드(정상 결과코드:3001)
                    }else if(PayMethod.equals("BANK")){
                        if(ResultCode.equals("4000")) paySuccess = true; // 계좌이체(정상 결과코드:4000)
                    }else if(PayMethod.equals("CELLPHONE")){
                        if(ResultCode.equals("A000")) paySuccess = true; // 휴대폰(정상 결과코드:A000)
                    }else if(PayMethod.equals("VBANK")){
                        if(ResultCode.equals("4100")) paySuccess = true; // 가상계좌(정상 결과코드:4100)
                    }else if(PayMethod.equals("SSG_BANK")){
                        if(ResultCode.equals("0000")) paySuccess = true; // SSG은행계좌(정상 결과코드:0000)
                    }else if(PayMethod.equals("CMS_BANK")){
                        if(ResultCode.equals("0000")) paySuccess = true; // 계좌간편결제(정상 결과코드:0000)
                    }
                }
            }
        }else{
            ResultCode 	= authResultCode;
            ResultMsg 	= authResultMsg;
        }


        return returnStr;
    }

    /*애스크로 거래생성 결과*/
    public Escrow_list searchAddResult(String orderNo) {
        return sellerMapper.searchAddResult(orderNo);
    }

    /*송금히스토리*/
    public List<ERemittanceHistory> eRemittanceHistory(int userCode) {
        return sellerMapper.eRemittanceHistory(userCode);
    }



    /*nicePay 가상계좌 승인 노티*/
    public String nicePayNoti(NicePayVBankResult nicePayVBankResult) {

        /*String file_path = "/usr/local/jboss/jboss-as/server/pay/log/nice_vacct_noti_result.log";


        File file = new File(file_path);
        try {
            file.createNewFile();

            FileWriter fw = new FileWriter(file_path, true);

            fw.write("************************************************\r\n");
            fw.write("PayMethod     : " + nicePayVBankResult.getPayMethod() + "\r\n");
            fw.write("MID           : " + nicePayVBankResult.getMID() + "\r\n");
            fw.write("MallUserID    : "+ nicePayVBankResult.getMallUserID() + "\r\n");
            fw.write("Amt           : " + nicePayVBankResult.getAmt() + "\r\n");
            fw.write("name          : " +  nicePayVBankResult.getName() + "\r\n");
            fw.write("GoodsName     : " + nicePayVBankResult.getGoodsName() + "\r\n");
            fw.write("TID           : "+ nicePayVBankResult.getTID() + "\r\n");
            fw.write("MOID          : "+ nicePayVBankResult.getMOID() + "\r\n");
            fw.write("AuthDate      : "+ nicePayVBankResult.getAuthDate() + "\r\n");
            fw.write("ResultCode    : "+ nicePayVBankResult.getResultCode() + "\r\n");
            fw.write("ResultMsg     : "+ nicePayVBankResult.getResultMsg() + "\r\n");
            fw.write("VbankNum      : "+ nicePayVBankResult.getVbankNum() + "\r\n");
            fw.write("FnCd          : "+ nicePayVBankResult.getFnCd() + "\r\n");
            fw.write("VbankName     : "+ nicePayVBankResult.getVbankName() + "\r\n");
            fw.write("VbankInputName : "+ nicePayVBankResult.getVbankInputName() + "\r\n");
            fw.write("RcptTID       : "+ nicePayVBankResult.getRcptTID() + "\r\n");
            fw.write("RcptType      : "+ nicePayVBankResult.getRcptType() + "\r\n");
            fw.write("RcptAuthCode  : "+ nicePayVBankResult.getRcptAuthCode() + "\r\n");
            fw.write("CancelDate    : "+ nicePayVBankResult.getCancelDate() + "\r\n");
            fw.write("************************************************\r\n");

            fw.close();
        } catch (Exception e) {
            e.printStackTrace();
        }*/

        String returnStr = "FAIL";

        /*
        * 1. 양측 문자 보낸다.
        * 2. 거래상태 (esl_state) = 1로 변경
        * 3. OK 메세지 보내기
        * */

        DateReturn dateReturn = new DateReturn();
        ConcurrentHashMap<String , String> dateHash = dateReturn.dateFormat(nicePayVBankResult.getAuthDate());

        nicePayVBankResult.setDepositDate(dateHash.get("date"));
        nicePayVBankResult.setDepositTime(dateHash.get("time"));


        int esl_state = sellerMapper.nicepayNoti( nicePayVBankResult ); //주문번호


        /*요청자 & 판매자 정보 가져오기 */
        VBankEsrInfo vBankEsrInfo = sellerMapper.vBankEsrInfo(nicePayVBankResult.getMOID());
        VBankEssInfo vBankEssInfo = sellerMapper.vBankEssInfo(nicePayVBankResult.getMOID());


        messageSubmit.submitMSG(vBankEsrInfo.getEsrPh(),"입금완료 .. MSG 문구 미정");
        messageSubmit.submitMSG(vBankEssInfo.getEssPh(),"입금완료 .. MSG 문구 미정");

        return returnStr;

    }

    /*판매자 핸드폰번호 가져오기 && 문자보내기*/
    public String selectSellerPH(String inviteCode) {

        int sellerCode = Integer.parseInt(inviteCode);

        String ph = sellerMapper.selectSellerPH(sellerCode);

        messageSubmit.submitMSG(ph,messageContent.messageContentInvite_seller()); //초대받은자에게 문자

        return sellerMapper.selectSellerPH(sellerCode);

    }



    /*판매자 문자보내기*/
    public String inviteMessage(String phone, String name,int userCode , String userName) {

        String returnMSG = "잘못된 접근입니다.";

        String returnStr  = sellerMapper.inviteMessage(phone);
        MaskReturn masking = new MaskReturn();

        if(returnStr.equals("1")){

            returnMSG = "exist" ;
        }else if(returnStr.equals("0")){

                messageSubmit.submitMSG(phone ,messageContent.messageContentInvite_requester( masking.nameReturn(userName) ));

                returnMSG = "ok";
        }


        return returnMSG;
    }


    /*거래 상태 변경 && 문자처리 (state=2 or3) */
    public String stateUpdate(StateUpdate stateUpdate) {

        String returnMsg = "not";

        String state = stateUpdate.getState();

        if(state.equals("3")){ //esr_code 검수
            stateUpdate.setCheck("1");


        }else if(state.equals("2") || state.equals("4")){ //ess_code 검수
            stateUpdate.setCheck("2");
        }

        int result =  sellerMapper.stateUpdate(stateUpdate);

        if(result >= 1){

            returnMsg ="ok";
        }

        return returnMsg;
    }


    /*
     * 애스크로 송금계좌
     * 거래완료됨에 따른 판매자에게 송금할 계좌정보
     * */

    public JSONObject eRemitUpdate(E_remittance_info eRemittanceInfo , SessionUser user) {
        long paymentAmount = getEscrowPrice(eRemittanceInfo.getEsl_code());
        eRemittanceInfo.setEri_amount(paymentAmount);
        eRemittanceInfo.setSeller_code(user.getUserCode());


        ConcurrentHashMap<String,Object> map = new ConcurrentHashMap<>();
        int result = sellerMapper.eRemitUpdate(eRemittanceInfo);



        DateReturn dateReturn = new DateReturn();
        String returnMsg ="FAIL";


        if(result >= 1){

            returnMsg="OK";
//            deliveryProc(eRemittanceInfo.getEsl_code(),user.getUserCode());


            RemitEsl remitEsl = sellerMapper.productInfo(eRemittanceInfo.getEsl_code());
            String bankName = getBankName(eRemittanceInfo.getBank_code());
            remitEsl.setBankName(bankName);
//            remitEsl.setCalStr( dateReturn.escrowCalDatetime() );

            messageSubmit.submitMSG(remitEsl.getESellerPh() , messageContent.messageContent5_seller(eRemittanceInfo , remitEsl) );

            if(eRemittanceInfo.getCar_owner_ph() != null){
                HashMap<String,Object> modifyMap = new HashMap<>();
                modifyMap.put("TRANS_ID" , eRemittanceInfo.getEsl_code());
                modifyMap.put("MEMO" ,"출발지 연락처 변경 : " +eRemittanceInfo.getCar_owner_ph());

                Roadwin.modifyOrder(modifyMap);

                int res = sellerMapper.updateCarOwnerPh(eRemittanceInfo.getEsl_code() , eRemittanceInfo.getCar_owner_ph());
                remitEsl.setCarOwnerPh(eRemittanceInfo.getCar_owner_ph());
                messageSubmit.submitMSG(remitEsl.getCarOwnerPh() , messageContent.messageContent5_seller_Owner(eRemittanceInfo , remitEsl) );
            }


        }

        map.put("msg" , returnMsg);


        return new JSONObject(map);

    }




    /*
     *
     * */

    public String deliveryCancel(String orderNo) {

        return sellerMapper.deliveryCancel(orderNo);
    }

    public GetAddress getMyAddress(int code) {

        return sellerMapper.getMyAddress(code);
    }

    public String rmtAccessCheck(int userCode , String orderNo) {
        ConcurrentHashMap<String,Object> reMap = new ConcurrentHashMap<>();

        reMap.put("userCode" , userCode);
        reMap.put("orderNo" , orderNo);

        /*본인 거래건이 아닐시*/
        String exists = sellerMapper.existEscRemit(reMap);
        if(exists.equals("0")){/*본인 진행 거래건이 아님*/
            return "N";
        }

        /*이미 진행된 거래건일시*/
        String existRemitInfo =sellerMapper.existEscRemitInfo(orderNo);
        if(existRemitInfo.equals("1")){ /*이미송금건 등록이 되어있음*/
            return "N2";
        }
        return "Y";
    }

    public JSONObject getStatus(String orderNo) {
        ConcurrentHashMap<String,String> map = new ConcurrentHashMap<>();
        map.put("status" , sellerMapper.getStatus(orderNo));
        return new JSONObject(map);
    }

    public MyInfo myInfo(int userCode) {
        return sellerMapper.myInfo(userCode);
    }

    public CsmAgree csmAgree(String orderNo) {
        return sellerMapper.csmAgree(orderNo);
    }

    /*위탁배송 요청동의 및 문자 보내기*/
    public JSONObject csmAgreeY(String orderNo) {
        ConcurrentHashMap<String , Object> map = new ConcurrentHashMap<>();

        String bookingExists = sellerMapper.bookingExists(orderNo);

        if (bookingExists.equals("1")){
            map.put("resultCode" , "3134");
            map.put("resultMSG" , "이미 동의가 완료된 주문입니다.");
        }


        BookingReq bookingReq = sellerMapper.bookingReq(orderNo);

        bookingReq.setTrans_ID(orderNo);

        bookingReq.setComp_CODE("1279"); //고정 : RW_고유업체번호
        bookingReq.setOrder_TYPE("T"); //고정 : 탁송

        ConcurrentHashMap returnMap = Roadwin.booking(bookingReq);

        System.out.println("returnMap = " + returnMap);

        String resultCode = returnMap.get("RESULT_CODE").toString();
        String resultMSG = returnMap.get("RESULT_MSG").toString();

        if(resultCode.equals("0000")){
            String roadwinOrderNo = returnMap.get("ORDER_NO").toString();
            int updateConsignment = sellerMapper.updateConsignment(orderNo,roadwinOrderNo);
            int updateEscrowList = sellerMapper.updateEscrowList(orderNo,roadwinOrderNo);

            CsmAgreeInfo csmAgreeSmsInfo = sellerMapper.csmAgreeSmsInfo(orderNo);
            csmAgreeSmsInfo.setOrderNo(orderNo);

            messageSubmit.submitMSG(csmAgreeSmsInfo.getSellerPh() , messageContent.csmAgreeY_seller(csmAgreeSmsInfo) ); //판매자 구매동의 완료알림
            messageSubmit.submitMSG(csmAgreeSmsInfo.getSellerPh() , messageContent.csmAgreeY_remit_seller(csmAgreeSmsInfo) ); //계좌입금알림

            messageSubmit.submitMSG(csmAgreeSmsInfo.getReqPh() , messageContent.csmAgreeY_requester(csmAgreeSmsInfo) ); //구매자 구매동의 완료알
        }


//        map.put("RESULT_CODE" , resultCode);
//        map.put("RESULT_MSG" , resultMSG);
//        System.out.println("new JSONObject(map) = " + new JSONObject(map));

        return new JSONObject(returnMap);
    }


    public CsmUpdate csmUpdate(String orderNo) {
        return sellerMapper.csmUpdateInfo(orderNo);
    }

    public DriverLocation driverLocation(String orderNo) {
        DriverLocation driverLocation = sellerMapper.driverLocation(orderNo);

        ConcurrentHashMap driverMap = new ConcurrentHashMap();
        driverMap.put("TRANS_ID" , orderNo);
        driverMap.put("DRIVER_NAME" , driverLocation.getDriverName());
        driverMap.put("DRIVER_HP" , driverLocation.getDriverHp());


        ConcurrentHashMap map =  Roadwin.workerLocation(driverMap);

        if(map.get("RESULT_CODE").equals("0000")){
            driverLocation.setLon(Double.parseDouble((String) map.get("LON")) );
            driverLocation.setLat(Double.parseDouble((String) map.get("LAT")) );
        }

        driverLocation.setResultCode((String) map.get("RESULT_CODE"));
        driverLocation.setResultMsg((String) map.get("RESULT_MSG"));




        return driverLocation;
    }

    public JSONObject orderCancel(String orderNo) {
        ConcurrentHashMap<String, Object> returnMap = new ConcurrentHashMap<>();


        //위탁배송 안잡혔을때만 가능
        String orderGetStatus = sellerMapper.orderGetStatus(orderNo);

        if(orderGetStatus.equals("0") || orderGetStatus.equals("1") || orderGetStatus.equals("2") || orderGetStatus.equals("3")){
            int cancelOrderStatus = sellerMapper.cancelOrderStatus(orderNo);

            if(cancelOrderStatus >= 1){

                OrderCancelSmsInfo orderCancelSmsInfo = sellerMapper.orderCancelSmsInfo(orderNo);
                //주문취소 문자
                messageSubmit.submitMSG(orderCancelSmsInfo.getSellerPh() , messageContent.orderCancel_seller(orderCancelSmsInfo) );
                messageSubmit.submitMSG(orderCancelSmsInfo.getReqPh() , messageContent.orderCancel_seller(orderCancelSmsInfo) );
                returnMap.put("resultCd" , "0000");
                returnMap.put("resultMsg" , "해당 거래가 취소 되었습니다.");
            }else{
                returnMap.put("resultCd" , "9999");
                returnMap.put("resultMsg" , "알수없는 오류.");
            }

        }else{
            returnMap.put("resultCd" , "9999");
            returnMap.put("resultMsg" , "배송이 이미 진행중인 거래건은 취소할수 없습니다.\n 탁송 취소문의 : 1599-2811");
        }



        return new JSONObject(returnMap);

    }

    public AlreadyRemit alreadyRemit(String orderNo) {

        return sellerMapper.alreadyRemit(orderNo);
    }

    public UserRefund userRefund(String orderNo) {
        UserRefund userRefund = sellerMapper.userRefund(orderNo);
        userRefund.setOrderNo(orderNo);
        userRefund.setBankList(sellerMapper.bankList());

        return userRefund;
    }

    public ConcurrentHashMap<String, String> escrowRefundInsert(EscrowRefund escrowRefund) {

        ConcurrentHashMap<String,String> map = new ConcurrentHashMap<>();

        int escrowRefundInsert = sellerMapper.escrowRefundInsert(escrowRefund);

        if (escrowRefundInsert >=1){
            map.put("msg" , "ok");


            EscrowRefundInfoMap infoMap = sellerMapper.escrowRefundInfoMap(escrowRefund.getOrderNo());
            escrowRefund.setEslPrice(Long.parseLong(infoMap.getEslPrice()));
            escrowRefund.setBankName(sellerMapper.getBankName(escrowRefund.getBankCode()));


            messageSubmit.submitMSG(infoMap.getSellerPh() , messageContent.refundComplete_requester(escrowRefund) );
        }else{
            map.put("msg" , "fail");
        }

        return map;
    }
}













