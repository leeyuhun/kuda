package com.escrow.kuda.service;

import com.escrow.kuda.mapper.ExcelMapper;
import com.escrow.kuda.model.excel.EsRemitBank;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ExcelServiceImpl {

    @Autowired
    ExcelMapper excelMapper;

    public List<EsRemitBank> eRemitBankExcel(List<String> codeList) {

        String returnStr = "";



        return excelMapper.eRemitBankExcel(codeList);
    }
}




