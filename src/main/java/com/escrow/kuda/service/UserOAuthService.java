package com.escrow.kuda.service;

import com.escrow.kuda.Role;
import com.escrow.kuda.mapper.SellerMapper;
import com.escrow.kuda.model.Loginid;
import com.escrow.kuda.model.Seller;
import com.escrow.kuda.model.oauth.OAuthAttributes;
import com.escrow.kuda.model.oauth.SessionUser;
import com.escrow.kuda.util.DateReturn;
import com.escrow.kuda.util.RandomStr;
import com.escrow.kuda.util.band.Band;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.oauth2.client.userinfo.DefaultOAuth2UserService;
import org.springframework.security.oauth2.client.userinfo.OAuth2UserRequest;
import org.springframework.security.oauth2.client.userinfo.OAuth2UserService;
import org.springframework.security.oauth2.core.OAuth2AccessToken;
import org.springframework.security.oauth2.core.OAuth2AuthenticationException;
import org.springframework.security.oauth2.core.user.DefaultOAuth2User;
import org.springframework.security.oauth2.core.user.OAuth2User;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSession;
import java.util.Collections;
import java.util.concurrent.ConcurrentHashMap;


@Service
public class UserOAuthService implements OAuth2UserService<OAuth2UserRequest , OAuth2User> {

    @Autowired
    private SellerMapper sellerMapper;

    @Autowired
    private HttpSession httpSession;

    @Autowired
    private Band band;

    private Logger logger = LoggerFactory.getLogger(UserOAuthService.class);

    @Override
    public OAuth2User loadUser(OAuth2UserRequest userRequest) throws OAuth2AuthenticationException {
        OAuth2UserService delegate = new DefaultOAuth2UserService();
        OAuth2User oAuth2User = delegate.loadUser(userRequest);

        String registrationId = userRequest.getClientRegistration().getRegistrationId();
        String userNameAttributeKey = userRequest.getClientRegistration()
                .getProviderDetails().getUserInfoEndpoint().getUserNameAttributeName();

        OAuthAttributes attributes = OAuthAttributes.
                of(registrationId, userNameAttributeKey, oAuth2User.getAttributes());

        OAuth2AccessToken accessToken = userRequest.getAccessToken();

        String bandState = band.bandListGrepKuda(accessToken.getTokenValue()); //전중연가입여부 N: 가입 C: 미가입

        DateReturn dateReturn = new DateReturn();
        RandomStr randomStr = new RandomStr();

        /*Login Table set*/
        Loginid loginid = new Loginid();
        loginid.setLogin_id(attributes.getEmail());
        loginid.setLogin_regdate(dateReturn.preDate());
        loginid.setLogin_pass( randomStr.getRandomStr(10) );
        loginid.setLogin_grade(Role.SELLER.getKey());

        String paramState = "";
        int updateSellerState =0;

        /*Seller Table set*/
        Seller seller = new Seller();

        seller.setLogin_id(attributes.getEmail());
        seller.setSeller_email(attributes.getEmail());
        seller.setSeller_name(attributes.getName());
        seller.setSeller_picture(attributes.getPicture());
        seller.setSeller_oauth_key(attributes.getNameAttributeKey());


        String returnVal = sellerMapper.sellerEmailExists( attributes.getEmail() );
        if(returnVal.equals("0")){
            try {
                seller.setSeller_state(bandState);
                int insertSellerVal = sellerMapper.insertSeller(seller);
                int insertLoginVal = sellerMapper.insertLogin(loginid);


            }catch (DataAccessException e){
                logger.error("UserOAuthService DataAccessException : " , e);
            }catch (Exception e){
                logger.error("UserOAuthService Exception : " , e);
            }

        }else{

                String state = sellerMapper.selectUserState( attributes.getEmail() );

                if(bandState.equals("C")){
                    paramState = "C";
                    seller.setSeller_state("C");

                }else if(bandState.equals("N")){
                    if(state.equals("Y")) {
                        paramState = "Y";
                        seller.setSeller_state("Y");
                    }else{
                        paramState = "N";
                        seller.setSeller_state("N");
                    }

                }

                ConcurrentHashMap<String , String> parameter = new ConcurrentHashMap<>();
                parameter.put("email" , attributes.getEmail());
                parameter.put("state" , paramState);

            updateSellerState = sellerMapper.updateSellerState(parameter);
        }


        seller = sellerMapper.sessionUserSelect(attributes.getEmail() );


        if(!paramState.equals("Y")){

            ConcurrentHashMap<String , String> parameter = new ConcurrentHashMap<>();
            parameter.put("email" , attributes.getEmail());
            parameter.put("state" , paramState);

            updateSellerState = sellerMapper.updateSellerState(parameter);
        }

        httpSession.setAttribute("user", new SessionUser(seller));



        if(seller.getLogin_grade().equals("ROLE_ADMIN")){
            return new DefaultOAuth2User(
                    Collections.singleton(new SimpleGrantedAuthority(Role.ADMIN.getKey())),
                    attributes.getAttributes(),
                    attributes.getNameAttributeKey());
        }else{
            return new DefaultOAuth2User(
                    Collections.singleton(new SimpleGrantedAuthority(Role.SELLER.getKey())),
                    attributes.getAttributes(),
                    attributes.getNameAttributeKey());
        }

    }

}
