package com.escrow.kuda.service;

import com.escrow.kuda.mapper.AdminMapper;
import com.escrow.kuda.mapper.ApiMapper;
import com.escrow.kuda.mapper.RoadwinMapper;
import com.escrow.kuda.model.admin.ExistsEscrowList;
import com.escrow.kuda.model.admin.RemitCompEsr;
import com.escrow.kuda.model.admin.RemitCompEss;
import com.escrow.kuda.model.admin.SMSAutoSubmit;
import com.escrow.kuda.util.infobank.MessageSubmit;
import com.escrow.kuda.util.message.MessageContent;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.HashMap;

@Service
@RequiredArgsConstructor
public class ApiServiceImpl{


    private final ApiMapper apiMapper;
    private final RoadwinMapper roadwinMapper;
    private final MessageSubmit messageSubmit;
    private final MessageContent messageContent;
    private final AdminMapper adminMapper;

    /*
     * 애스크로 문자 자동전달
     * */
    public void escrowSmsAutoSubmit(SMSAutoSubmit smsAutoSubmit) {
        /*
         * 바로 처리할수 없는경우 ..
         * 1.금액 && 예금주 중복 또는 없음
         * */

        //message 금액 & 예금주 추출

        //금액 && 예금주 중복 또는 없을경우 어드민에게 문자 보냄

        //아닐시 처리

        //위탁배송 신청자 일시 & 아닐시 처리

        /*
         *
         * */

        if(smsAutoSubmit.getFrom().equals("15778000")){ //신한은행 번호 알림번호

            System.out.println("smsAutoSubmit.getMessage() = " + smsAutoSubmit.getMessage());
            String message = smsAutoSubmit.getMessage();

            String[] messageArr = message.split("\n");

            for (String s : messageArr) {
                System.out.println("messageArr : " + s);
            }

            String amount = messageArr[3].replaceAll(",","").trim() ;
//            String accountHolder = messageArr[5].trim() ;


            if(amount.startsWith("입금")){
                String amountVal = amount.trim().substring(2);
                long lnAmount = Long.parseLong(amountVal.trim());
                String accountHolderVal = messageArr[5].trim() ;

                System.out.println("amountVal = " + amountVal);
                System.out.println("amountVal = " + accountHolderVal);

                HashMap esValueChk = new HashMap<>();

                esValueChk.put("amount" , lnAmount);
                esValueChk.put("accountHolder" , accountHolderVal);

                HashMap esValueChkParam = apiMapper.escrowValueCheck(esValueChk);

                String eslCode = (String) esValueChkParam.get("eslCode");
                long eslCount = (long) esValueChkParam.get("eslCount");

                System.out.println("eslCount = " + eslCount);
                System.out.println("eslCode = " + eslCode);

                if(!eslCode.equals("isNull") && eslCount==1) {//바로 처리할수 있는 경우

                    RemitCompEsr remitCompEsr = adminMapper.remitCompEsrInfo(eslCode);
                    RemitCompEss remitCompEss = adminMapper.remitCompEssInfo(eslCode);

                    remitCompEsr.setEssName(remitCompEss.getEssName());
                    remitCompEsr.setEssPh(remitCompEss.getEssPh());
                    remitCompEsr.setOrderNo(eslCode);

                    /*요청자 문자*/
                    String esrMsg = messageSubmit.submitMSG(remitCompEsr.getEsrPh() , messageContent.messageContent2_requester(remitCompEsr));
                    /*판매자 문자*/
                    String essMsg = messageSubmit.submitMSG(remitCompEss.getEssPh() , messageContent.messageContent2_seller(remitCompEsr));

                    apiMapper.escrowSmsAutoSubmit((String) esValueChkParam.get("eslCode"));

                }else{ //처리할수 없는경우
                    int msgSubmit = 0;

                    //어드민에게 문자보냄.

                    //중복일경우 count >= 1
                    //예금주&금액이 안맞을경우 eslCode = isNull
                    //둘다? x 경우의수 없음


                    if(eslCode.equals("isNull") ){

                        HashMap map = new HashMap<>();

                        map.put("bankHolder" , accountHolderVal);
                        map.put("amount" , lnAmount);

                        String esrMsg = messageSubmit.submitMSG("01084338502" , messageContent.adminMessage_NotExists(map));
                        msgSubmit = 1;

                    }

                    if(eslCount !=0 && msgSubmit==0){
                        HashMap eslInfoMap = apiMapper.getEslInfo((String) esValueChkParam.get("eslCode"));
                        String esrMsg = messageSubmit.submitMSG("01084338502" , messageContent.adminMessage_Overlap(eslInfoMap));

                    }


                }

                //위탁배송 신청자 유무. (신청자 일시 바로 처리)
                /*
                 *
                 * */

            /*HashMap csMap = apiMapper.getCsmStatus(eslCode);

            String csmStatus = (String) csMap.get("csStatus");
            long csmCount = (long) csMap.get("csCount");


            System.out.println("csmCount = " + csmCount);
            System.out.println("csmStatus = " + csmStatus);
            System.out.println("eslCode = " + eslCode);

//            if(csmStatus !=null) {
                if (csmStatus.equals("Y") && csmCount == 0) {

                    int result = 0;

                    BookingReq bookingReq = roadwinMapper.getBookingInfo(eslCode);


                    bookingReq.setComp_CODE("1279"); //고정 : RW_고유업체번호
                    bookingReq.setOrder_TYPE("T"); //고정 : 탁송


                    ConcurrentHashMap returnMap = Roadwin.booking(bookingReq);


                    String resultCode = returnMap.get("RESULT_CODE").toString();
                    bookingReq.setResult_CODE(resultCode);

                    bookingReq.setResult_MSG((String) returnMap.get("RESULT_MSG"));

                    DateReturn dr = new DateReturn();

                    bookingReq.setRDate(dr.preDate());
                    bookingReq.setRTime(dr.preTime());

                    result = roadwinMapper.insertConsignment(bookingReq);

                    if (result >= 1) {
                        roadwinMapper.eDeliveryIsuseUpdate(bookingReq.getOrderNo()); //주문서 esl_consignment_isuse ='Y'
                    }

                } else if (csmCount >= 1) {
                    System.out.println("위탁배송이 이미 신청 완료된 거래건.");
                }*/
            }



            }

//        }

    }

    /*
     * 애스크로 거래생성 제약조건
     * */
    public String existsEscrowList(ExistsEscrowList eel) {
        eel.setAmount(eel.getAmount().replaceAll(",",""));
        return apiMapper.existsEscrowList(eel);
    }

}
