package com.escrow.kuda;


import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor

public enum Role {


    ADMIN("ROLE_ADMIN", "관리자"),
    AGENT("ROLE_AGENT", "에이전트"),
    AGENCY("ROLE_AGENCY", "대리점"),
    SELLER("ROLE_SELLER", "판매자"),
    CUSTOMER("ROLE_CUSTOMER", "고객");

    private final String key;
    private final String title;

}
