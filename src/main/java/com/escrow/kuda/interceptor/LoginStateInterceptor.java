package com.escrow.kuda.interceptor;


import com.escrow.kuda.model.oauth.SessionUser;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@Component
public class LoginStateInterceptor extends HandlerInterceptorAdapter {

    @Override
    public boolean preHandle(HttpServletRequest request,
                             HttpServletResponse response, Object handler) throws Exception {



        HttpSession session = request.getSession();
        SessionUser sessionUser = (SessionUser) session.getAttribute("user");


        if(sessionUser != null) {


            String userState = sessionUser.getUserState();


            if ("N".equals(userState)) {
                response.sendRedirect("/registration");
                return false;
            } else if ("C".equals(userState)) {
                response.sendRedirect("/kudaAccount");
                return false;
            }else{
                return true;
            }

        }


        //else
        response.sendRedirect(request.getContextPath() + "/");
        return false;
    }

    @Override
    public void postHandle(HttpServletRequest request,
                           HttpServletResponse response, Object handler,
                           ModelAndView modelAndView) throws Exception {

        super.postHandle(request, response, handler, modelAndView);
    }

    @Override
    public void afterCompletion(HttpServletRequest request,
                                HttpServletResponse response, Object handler, Exception ex)
            throws Exception {

        super.afterCompletion(request, response, handler, ex);
    }



}
