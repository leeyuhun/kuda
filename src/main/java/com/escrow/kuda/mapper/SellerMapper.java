package com.escrow.kuda.mapper;


import com.escrow.kuda.model.*;
import com.escrow.kuda.model.camel.*;
import com.escrow.kuda.model.payment.NicePayVBankResult;
import com.escrow.kuda.model.roadwin.BookingReq;
import com.escrow.kuda.model.sms.CsmAgreeInfo;
import com.escrow.kuda.model.sms.OrderCancelSmsInfo;
import com.escrow.kuda.model.util.DeliveryCompInfo;
import com.escrow.kuda.model.util.RemitEsl;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

@Mapper
public interface SellerMapper {

    List<sRemittanceInfoVo> sRemittanceInfo(int sellerCode); //판매자 지정계좌 가져오기

    List<Bank> bankList(); //은행리스트

    int insertSeller(Seller seller); //회원가입 insert seller table

    Seller sessionUserSelect(String seller_email); //세션에 저장할 user info selelct

    String sellerEmailExists(String email); //아이디중복확인

    int insertLogin(Loginid loginid); //회원가입 insert login_id table

    List<CamelSeller> searchSellerList(SearchWhere search); //회원검색 List

    int updateSellerRemainInfo(UpRemainInfo upRemainInfo); //회원가입 나머지 입력

    int insertSRemittanceInfo(UpRemainInfo upRemainInfo); //판매자 기본 송금정보

    int insertEscrowList(EscrowInsert escrowInsert); //에스크로 거래생성 (가상계좌X)

    int insertEscrowRequester(Escrow_requester er); //에스크로 요청자생성

    int insertEscrowSeller(Escrow_seller es); //에스크로 판매자생성

    int updateSeller(SellerUpdate sellerUpdate); //회원 정보 수정

    int updateSellerRemittance(SellerUpdate sellerUpdate); //송금정보입력

    Seller sellerInfo(int code); //판매자 정보

    List<BuyList> buyLists(int userCode); //유저 에스크로 구매리스트

    List<SellList> sellerLists(int userCode); //유저 애스크로 판매리스트

    ProfileUpdate selectProfileInfo(int userCode); //프로필 수정 SELECT

    List<HistoryBuyList> historyBuyLists(int userCode); //애스크로 구매 내역

    List<HistorySellList> historySellLists(int userCode); //애스크로 판매 내역

    int insertEscrow(Escrow_list escrowList); // 애스크로 거래생성

    Escrow_list searchAddResult(String orderNo); //애스크로 거래생성 결과

    List<ERemittanceHistory> eRemittanceHistory(int userCode); //송금정보 히스토리

    int nicepayNoti(NicePayVBankResult nicePayVBankResult); //가상계좌 입금 수신

    String selectSellerPH(int sellerCode); //딜러 핸드폰번호 ph (1개)

    String inviteMessage(String phone); //핸드폰번호 가입된번호인지 확인

    int stateUpdate(StateUpdate stateUpdate); //거래상태 변경

    String getProdName(String orderNo); //상품명 가져오기


    VBankEsrInfo vBankEsrInfo(String moid); //요청자 정보 가져오기

    VBankEssInfo vBankEssInfo(String moid); //판매자 정보 가져오기


    String selectUserState(String email);

    int updateSellerState(ConcurrentHashMap<String , String> param);

    TrnscRgstr trnscRgstr(String orderNo);




    int eRemitUpdate(E_remittance_info eRemittanceInfo); //판매자(ess) 애스크로 송금정보 입력

    String getBankName(String bank_code); //bankCode 로 bankName 가져오기

    long getEscrowPrice(String esl_code); //정산금액 가져오기

    Seller getEsrUserPh(int esrCode); //esrUser 핸드폰번호


    DeliveryCompInfo deliveryCompInfo(String orderNo); //차량배송 info 요청자(esr)



    RemitEsl productInfo(String esl_code); //상품정보 가져오기 (상품명 &정산금액)


    String deliveryCancel(String orderNo);

    TrnConsignment trnConsignment(String orderNo);

    GetAddress getMyAddress(int code);

    String existEscRemit(ConcurrentHashMap<String, Object> reMap);

    String existEscRemitInfo(String orderNo);

    String getStatus(String orderNo);

    ConcurrentHashMap<String, String> getEssInfo(String orderNo);

    int updateCarOwnerPh(String eslCode, String ph);


    int insertEscrowRemit(String eslCode);

    MyInfo myInfo(int userCode);

    int insertConsignment(EscrowInsert ep);

    CsmAgree csmAgree(String orderNo);

    BookingReq bookingReq(String orderNo);

    int updateConsignment(String orderNo ,String  roadwinOrderNo);

    int updateEscrowList(String orderNo, String roadwinOrderNo);

    CsmAgreeInfo csmAgreeSmsInfo(String orderNo);


    CsmUpdate csmUpdateInfo(String orderNo);

    DriverLocation driverLocation(String orderNo);

    String orderGetStatus(String orderNo);

    int cancelOrderStatus(String orderNo);

    OrderCancelSmsInfo orderCancelSmsInfo(String orderNo);

    String bookingExists(String orderNo);

    AlreadyRemit alreadyRemit(String orderNo);

    UserRefund userRefund(String orderNo);

    int escrowRefundInsert(EscrowRefund escrowRefund);

    EscrowRefundInfoMap escrowRefundInfoMap(String orderNo);
}

