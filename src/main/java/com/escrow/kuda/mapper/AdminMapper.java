package com.escrow.kuda.mapper;

import com.escrow.kuda.model.admin.*;
import com.escrow.kuda.model.util.EslInfo;
import org.apache.ibatis.annotations.Mapper;

import java.util.ArrayList;
import java.util.List;

@Mapper
public interface AdminMapper {

    RemitCompEsr remitCompEsrInfo(String orderNo); //송금완료 process 문자처리 요청자 정보

    RemitCompEss remitCompEssInfo(String orderNo); //송금완료 process 문자처리 판매자 정보

    int eslStateUpdate(String orderNo ,String preState , String updateState); // esl State update (거래상태)

    List<EscrowList> escrowList(EscrowListWhere where); //애스크로 진행리스트

    List<EscrowHistory> escrowHistory(EscrowHistoryWhere escrowHistoryWhere); //애스크로 완료리스트

    List<UserList> userList(UserListWhere where); // 유저리스트


    EslInfo eslInfo(String orderNo);

    int eEndEslUpdate(EEndEslUpdate eslUpdate);

    int eRemitStateUpdate(String orderNo);

    EscrowListDetail escrowListDetail(String orderNo); //escrowList detail View

    List<EscrowRemitList> eRemitList(EscrowRemitListWhere erw); //에스크로 정산리스트작성 (정산딜러 걸러내기)


    int eslListUpdate(ArrayList<String> lists);
    
    List<EslInfo> eslListInfo(ArrayList<String> lists);

    int eRemitStateListUpdate(ArrayList<String> lists);

    String getStatus(String orderNo);
}
