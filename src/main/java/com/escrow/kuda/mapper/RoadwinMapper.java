package com.escrow.kuda.mapper;

import com.escrow.kuda.model.roadwin.BookingReq;
import com.escrow.kuda.model.roadwin.ModifyReq;
import com.escrow.kuda.model.schedule.RoadwinCsm;
import com.escrow.kuda.model.sms.*;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

@Mapper
public interface RoadwinMapper {



    int statusChange(ConcurrentHashMap<String ,String> map);

    String destination(String orderNo);

    ConcurrentHashMap selectEscrowInfo(String orderNo);

    int insertConsignment(BookingReq param);

    String returnRWID(String orderNo);

    int rwinCancelConsignment(ConcurrentHashMap<String, String> insertHash);

    int statusChangeAndLink(ConcurrentHashMap<String, String> map);

    int updateAssign(ConcurrentHashMap<String , Object> map);

//    int modifyOrder(ConcurrentHashMap<String, String> map);

//    PriceCheckReq getStartEnd(String rw_id);



    RoadWinDeliveryAssign getCsmInfo(String transID);

    RoadwinDeliveryStart roadwinDeliveryStartInfo(String transID);

    int rwinCancelEscrowList(String orderNo);

    RoadwinDeliveryEnd roadwinDeliveryEndInfo(String transID);

    int updateEslDeliveryEnd(String orderNo);

    int updateEslDeliveryStart(String orderNo);

    int updateCsmPrice(ModifyReq req);

    RoadWinOrderModify roadWinOrderModifyInfo(String rwId);

    int updateAssignEsl(String transID);

    List<RoadwinCsm> csmSchedule();

    int csmScheduleUpdate(List<String> list);

    RefundInfo refundInfo(String transId);

    int updateRefund(String transId);
}
