package com.escrow.kuda.mapper;

import com.escrow.kuda.model.admin.ExistsEscrowList;
import org.apache.ibatis.annotations.Mapper;

import java.util.HashMap;

@Mapper
public interface ApiMapper {

    void escrowSmsAutoSubmit(String eslCode); // 애스크로 은행입금 문자 가져와서 자동처리

    String existsEscrowList(ExistsEscrowList eel); //애스크로 거래생성 제약조건

    HashMap escrowValueCheck(HashMap esValueChk); // 금액 && 예금주 중복 또는 없음 select


    HashMap getEslInfo(String eslCode);

    HashMap getCsmStatus(String eslCode);
}
