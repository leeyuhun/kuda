package com.escrow.kuda.mapper;

import com.escrow.kuda.model.excel.EsRemitBank;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface ExcelMapper {

    List<EsRemitBank> eRemitBankExcel(List<String> codeList);
}
