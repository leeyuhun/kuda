<%--
  Created by IntelliJ IDEA.
  User: Lee
  Date: 2021-01-25 오후 5:47
  Explanaion : 
--%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@include file="/WEB-INF/jsp/static/customer/header.jsp" %>


<script type="text/javascript">



    function pgPaymentLoad(el){

        $('#form_orderNo').val($(el).closest('.pg-payment').find('.hidden-orderCode').html());

        $('#form_payNo').val($(el).closest('.pg-payment').find('.hidden-payCode').html());

        $('#form_pgCode').val($(el).closest('.pg-payment').find('.hidden-pgCode').html());



        // alert($('#form_orderNo').val());
        // alert($('#form_payNo').val());
        // alert($('#form_pgCode').val());

        $('#nicePayHiddenForm').attr("action" , "nicePayForm");
        $('#nicePayHiddenForm').attr("method" , "get");
        $('#nicePayHiddenForm').submit();


    }












</script>

<style>
    .hidden-info{display: none}

</style>

<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="main-content">

    <div class="page-content">
        <div class="container-fluid">

            <!-- page title -->
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box d-flex align-items-center justify-content-between">
                        <h4 class="mb-0 font-size-24">Payment</h4>

<%--                        <div class="page-title-right">--%>
<%--                            <ol class="breadcrumb m-0">--%>
<%--                                <li class="breadcrumb-item"><a href="javascript: void(0);">Utility</a></li>--%>
<%--                                <li class="breadcrumb-item active">Starter Page</li>--%>
<%--                            </ol>--%>
<%--                        </div>--%>

                    </div>
                </div>
            </div>
            <!-- page title -->
            <div class="col-xl-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title"></h4>

                        <div class="row">
                            <c:forEach items="${list}" var="item">
                                <div class="col-lg-4 pg-payment">
                                    <div class="border p-3 rounded mt-4">
                                        <div class="d-flex align-items-center mb-3">

                                            <div class="avatar-xs mr-3">

                                                                <span class="avatar-title rounded-circle bg-soft-warning text-warning font-size-18">
                                                                    <i class="bx bx-credit-card"></i>
                                                                </span>
                                            </div>
                                            <h5 class="font-size-18 mb-0">

                                                <c:choose>
                                                    <c:when test="${item.yp_payment_result_code eq 'X'}">
                                                        <c:out value="결제전"/>
                                                    </c:when>
                                                    <c:otherwise>
                                                        <c:out value="${item.yp_card_name }"/>
                                                    </c:otherwise>
                                                </c:choose>
                                            </h5>
                                        </div>

                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="text-muted mt-3">
                                                    <h4><c:out value=" ${item.yp_amount}" /></h4>
                                                    <h5> <c:out value="${item.yp_card_number}"/> </h5>
                                                    <h5> <c:out value="${item.yp_buyer_name}"/> </h5>
                                                    <div class="hidden-info hidden-amount"><c:out value="${item.yp_amount}" /></div>
                                                    <div class="hidden-info hidden-product"><c:out value="${item.yr_product_name}" /></div>
                                                    <div class="hidden-info hidden-psCode"><c:out value="${item.ps_code}" /></div>
                                                    <div class="hidden-info hidden-orderCode" ><c:out value="${item.yr_code}" /></div>
                                                    <div class="hidden-info hidden-payCode" ><c:out value="${item.yp_code}" /></div>
                                                    <div class="hidden-info hidden-pgCode" ><c:out value="${item.pg_code}" /></div>
                                                    <div class="hidden-info hidden-name"><c:out value="${item.yr_buyer_name}" /></div>
                                                    <div class="hidden-info hidden-ph"><c:out value="${item.yr_buyer_ph}" /></div>

                                                </div>
                                            </div>
                                            <div class="col-lg-6 align-self-end">
                                            <c:choose>
                                                <c:when test="${item.yp_payment_result_code eq 'X'}">
                                                    <button class="btn btn-primary btn-lg btn-block waves-effect waves-light mb-1" onclick="pgPaymentLoad(this)" >결제</button>
                                                </c:when>
                                                <c:otherwise>
                                                    <h3>결제가 완료되었습니다.</h3>
                                                </c:otherwise>
                                            </c:choose>


                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </c:forEach>


                        </div>

                    </div>
                </div>

                <%--<div class="card">
                    <div class="card-body">
                        <h4 class="card-title mb-4">결제실패내역</h4>

                        <div class="table-responsive">
                            <table class="table table-nowrap table-centered mb-0">
                                <thead>
                                <tr>
                                    <th scope="col">결제시도일</th>
                                    <th scope="col">카드정보</th>
                                    <th scope="col">메세지</th>
                                    <th scope="col">금액</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <th scope="row">
                                        <div class="d-flex align-items-center">
                                            <div class="avatar-xs mr-3">
                                                <span class="avatar-title rounded-circle bg-soft-warning text-warning font-size-18">
                                                    <i class="mdi mdi-bitcoin"></i>
                                                </span>
                                            </div>
                                            <span>BTC</span>
                                        </div>
                                    </th>
                                    <td>
                                        <div class="text-muted">
                                            $ 7525.47
                                        </div>
                                    </td>
                                    <td>
                                        <h5 class="font-size-14 mb-1">1.2601</h5>
                                        <div class="text-muted">$6225.74</div>
                                    </td>
                                    <td>
                                        <h5 class="font-size-14 mb-1">0.1512</h5>
                                        <div class="text-muted">$742.32</div>
                                    </td>

                                </tr>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>--%>

        </div> <!-- container-fluid -->
    </div>
    <!-- End Page-content -->


<%@include file="/WEB-INF/jsp/static/customer/footer.jsp" %>
        <form name="payForm" method="get" action="nicePayForm" accept-charset="euc-kr" id="nicePayHiddenForm" class="nicePayHiddenForm">

                <input type="hidden" name="orderNo"         id="form_orderNo"     value="">
                <input type="hidden" name="payNo"           id="form_payNo"     value="">       <!--결제상품명-->
                <input type="hidden" name="pgCode"          id="form_pgCode"     value="">       <!--결제상품명-->

        </form>


<%--<%!--%>
<%--public synchronized String getyyyyMMddHHmmss(){--%>
<%--	SimpleDateFormat yyyyMMddHHmmss = new SimpleDateFormat("yyyyMMddHHmmss");--%>

<%--	return yyyyMMddHHmmss.format(new Date());--%>
<%--}--%>
<%--// SHA-256 형식으로 암호화--%>
<%--public class DataEncrypt{--%>
<%--	MessageDigest md;--%>
<%--	String strSRCData = "";--%>
<%--	String strENCData = "";--%>
<%--	String strOUTData = "";--%>

<%--	public DataEncrypt(){ }--%>
<%--	public String encrypt(String strData){--%>
<%--		String passACL = null;--%>
<%--		MessageDigest md = null;--%>
<%--		try{--%>
<%--			md = MessageDigest.getInstance("SHA-256");--%>
<%--			md.reset();--%>
<%--			md.update(strData.getBytes());--%>
<%--			byte[] raw = md.digest();--%>
<%--			passACL = encodeHex(raw);--%>
<%--		}catch(Exception e){--%>
<%--			System.out.print("암호화 에러" + e.toString());--%>
<%--		}--%>
<%--		return passACL;--%>
<%--	}--%>

<%--	public String encodeHex(byte [] b){--%>
<%--		char [] c = Hex.encodeHex(b);--%>
<%--		return new String(c);--%>
<%--	}--%>

<%--}--%>
<%--%>--%>

