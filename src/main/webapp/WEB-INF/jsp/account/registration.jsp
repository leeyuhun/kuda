<%--
  Writer: Lee
  Date: 2021-04-09 오후 5:12
  Explanaion : 회원가입창
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<%@include file="/WEB-INF/jsp/user_static/headerImport.jsp" %>
<script async src="${pageContext.request.contextPath}/static/assets/js/registration.js"></script>

<body id="Login">
<div class="container">
        <section class="pd-lr25">
            <p class="fs22 bold lh145 mr-b30">회원가입시 필요한 정보를<br>입력해 주세요.</p>
            <form action="<c:url value="/upRemainInfo"/>" method="POST" class="form" name="frm" id="registrationForm">
                    <ul>
                        <li>
                            <div class="input-title">상사명</div>
                            <input type="text" placeholder="상사명 입력" id="departName" name="departName"
                                   onchange="regValueChk();" onkeyup="regValueChk();">
                            <div class="feedback-msg departName"></div>
                            <!-- 피드백 메세지 ex) 상사명을 입력해주세요! -->
                        </li>
                        <li>
                            <div class="input-title">이름</div>
                            <input type="text" placeholder="상사명 입력" id="sellerName"  name="sellerName"
                                   onchange="regValueChk();" onkeyup="regValueChk();">
                            <div class="feedback-msg sellerName"></div>
                            <!-- 피드백 메세지 ex) 상사명을 입력해주세요! -->
                        </li>
                        <li>
                            <div class="input-title">종사원 번호</div>
                            <label for="departNum"></label>
                            <input type="number" pattern="\d*" placeholder="종사원 번호 입력" id="departNum" name="departNum"
                                                 onchange="chkNum(this, 'departNum'); regValueChk();" onkeyup="chkNum(this, 'departNum'); regValueChk();">
                            <div class="feedback-msg departNum"></div>
                        </li>
                        <li>
                            <div class="input-title">휴대전화번호</div>
                            <label for="phoneNum"></label>
                            <input type="number" pattern="\d*" class="numberOnly inputPhone" placeholder="-없이 숫자만 입력" id="phoneNum" name="phoneNum"
                                                                 onchange="chkNum(this, 'phoneNum'); regValueChk();" onkeyup="chkNum(this, 'phoneNum'); regValueChk();">
                            <div class="feedback-msg phoneNum"></div>
                        </li>
                        <li>
                            <div class="input-title">계좌번호</div>
                            <div class="clear flex relative">
                                <div class="selectbox" onclick="selectboxClick(this)">
                                    <span>은행 선택</span>
                                </div>
                                <div class="select-list-box">
                                    <ul class="select-list">
                                        <c:forEach items="${bankList}" var="bank" varStatus="status">
                                            <li data-value="${bank.bank_code}" class="select-list-li" onclick="regValueChk();" >${bank.bank_name}</li>
                                        </c:forEach>
                                    </ul>
                                </div>
                                <input type="number" pattern="\d*"  class="numberOnly " placeholder="계좌번호 -없이 숫자만 입력" name="bankNumber"
                                       onchange="chkNum(this, 'bankNumber'); regValueChk();" onkeyup="chkNum(this, 'bankNumber'); regValueChk();">
                            </div>
                            <div class="feedback-msg bankNumber"></div>
                        </li>
                        <li>
                            <div class="input-title">예금주명</div>
                            <input type="text"  placeholder="성명 입력"  name="bankHolder"
                                   onchange=" regValueChk();" onkeyup=" regValueChk();">
                            <div class="feedback-msg bankHolder"></div>
                        </li>
                        <li>
                            <div class="input-title">주소</div>
<%--                            <div class="address-input-box relative">--%>
                                <input type="text" placeholder="주소 검색" readonly  id="address" class="check-input"
                                       onchange="regValueChk();" onkeyup="regValueChk();" value="" name="address" onclick="execPostCode();">
<%--                            </div>--%>
                            <div class="feedback-msg address"></div>
                        </li>
                        <li>
                            <div class="input-title">상세 주소</div>
                            <input type="text" placeholder="상세주소 입력"  id="addressDetail" class="check-input" onchange="regValueChk();" onkeyup="regValueChk();" name="addressDetail">
                            <div class="feedback-msg addressDetail"></div>
                        </li>
                    </ul>


                <input hidden="hidden" name="bankCode" id="bankCode" class="check-input bankCode" value="000">

            </form>
        </section>
        <section class="btn-wrap" style="margin-top: 10%; margin-bottom: 15px; position: static;">
            <%--            <button class="btn-blue type disabled reg-btn" type="submit"  disabled>가입하기</button>--%>
            <button class="bgColor-blue fs16 color-white mr-t12 reg-btn" id="reg-btn"  disabled>가입하기</button>
            <!-- 모든 정보 입력하지 않았을 경우(유효성 검사) disabled class 추가 하거나 disabled 속성 추가-->
        </section>




</div>
</body>
</html>
