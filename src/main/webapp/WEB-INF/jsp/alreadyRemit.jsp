<%--
  Writer: Lee
  Date: 2021-04-09 오후 5:19
  Explanaion : 
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<%@include file="/WEB-INF/jsp/user_static/headerImport.jsp" %>



<body id="trnscRgstr">
<!-- 공통 -->
<header class="header-nav">
    <img src="${pageContext.request.contextPath}/static/assets/img/icons/close.png" alt="nav" class="icon" onclick="locationH('/user/purchaseSale');">
    <span class="nav-title">정산 계좌정보</span>
</header>
<!-- 공통 -->
<div class="container pd-t32 pd-lr25">
    <p class="fs22 bold lh145">거래가 완료 된 후<br>입금 받으실 계좌정보 입니다.</p>
    <form id="remittanceForm" name="remittanceForm" class="form" autocomplete="off">
    <section class="writeAccount mr-t30">

        <ul>
            <li>
                <div class="input-title">계좌정보</div>
                <div class="clear flex relative">
<%--                    <div class="selectbox" >--%>
<%--                        <span>${data.bankName}</span>--%>
<%--                    </div>--%>
<%--                                        <div class="select-list-box">--%>
<%--                                            <ul class="select-list">--%>
<%--                                                <c:forEach items="${banklist}" var="bank">--%>
<%--                                                    <li data-value="${bank.bank_code}" class="select-list-li" onclick="remittanceValueChk();">${bank.bank_name}</li>--%>
<%--                                                </c:forEach>--%>
<%--                                            </ul>--%>
<%--                                        </div>--%>
                    <input type="text" pattern="\d*" placeholder="계좌번호 입력 -없이" name="eri_account_number" id="aNumber" value=" ${data.bankName} ${data.account}" readonly >
                </div>
                <div class="feedback-msg eri_account_number"></div>
            </li>
            <li>
                <div class="input-title">예금주명</div>
                <input type="text" name="eri_account_holder" id="aHolder" value="${data.holder}" readonly>

            </li>



        </ul>


    </section>
    </form>


    <section class="btn-wrap pd0">
        <p class="fs14 color-blue bold">위의 계좌 정보로 정보가 입력되었습니다.
            <br>계좌 변경을 원할 시 고객센터로 연락 주세요.</p>
        <button class="bgColor-blue fs16 color-white" id="" onclick="locationH('/user/purchaseSale');">거래목록으로</button>
    </section>

</div>
</body>
</html>
