<%--
  Writer: Lee
  Date: 2021-04-09 오후 4:24
  Explanaion : kuda loginPage
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html lang="ko">

<%@include file="/WEB-INF/jsp/user_static/headerImport.jsp" %>
<script async src="${pageContext.request.contextPath}/static/assets/js/index.js"></script>

<body id="Main">
<div class="container">
  <img src="${pageContext.request.contextPath}/static/assets/img/icons/kuda-logo.png" alt="Kuda" class="logo">
  <section class="login-before">
    <section class="main-text">
      <p>
        차량 거래를<br>
        조금 더<br>
        <span class="color-blue bold">쉽고 안전하게</span>
      </p>
    </section>
    <section class="btn-wrap">
      <p class="fs14">네이버 밴드를 통해 로그인 및 가입이 가능합니다.</p>
      <button class="bgColor-green fs16 color-white mr-t12" onclick="locationH('/user/purchaseSale')">
        <img src="${pageContext.request.contextPath}/static/assets/img/icons/band-logo.png" alt="네이버 밴드 로그인" class="band-logo">네이버 밴드 로그인
      </button>
    </section>
  </section>
</div>
</body>


</html>
