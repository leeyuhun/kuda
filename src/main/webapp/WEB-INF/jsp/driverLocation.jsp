<%--
  Created by IntelliJ IDEA.
  User: Lee
  Date: 2021-09-24
  Time: 오후 4:06
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="lon" value="${data.lon}" />
<c:set var="lat" value="${data.lat}" />
<html>
<%@include file="/WEB-INF/jsp/user_static/headerImport.jsp" %>

<script>
    var msg =

</script>

<script type="text/javascript" src="//dapi.kakao.com/v2/maps/sdk.js?appkey=5182ea53e79689cbb8e2185c598d170a&libraries=services"></script>
<html>
<%--<%@include file="/WEB-INF/jsp/user_static/headerImport.jsp" %>--%>

<head>
    <meta charset="utf-8"/>
    <title>기사 현위치</title>
    <style>
        html, body {width:100%;height:100%;margin:0;padding:0;}
        .map_wrap {position:relative;overflow:hidden;width:100%;height:40%;}
        .radius_border{border:1px solid #919191;border-radius:5px;}
        .custom_typecontrol {position:absolute;top:10px;right:10px;overflow:hidden;width:130px;height:30px;margin:0;padding:0;z-index:1;font-size:12px;font-family:'Malgun Gothic', '맑은 고딕', sans-serif;}
        .custom_typecontrol span {display:block;width:65px;height:30px;float:left;text-align:center;line-height:30px;cursor:pointer;}
        .custom_typecontrol .btn {background:#fff;background:linear-gradient(#fff,  #e6e6e6);}
        .custom_typecontrol .btn:hover {background:#f5f5f5;background:linear-gradient(#f5f5f5,#e3e3e3);}
        .custom_typecontrol .btn:active {background:#e6e6e6;background:linear-gradient(#e6e6e6, #fff);}
        .custom_typecontrol .selected_btn {color:#fff;background:#425470;background:linear-gradient(#425470, #5b6d8a);}
        .custom_typecontrol .selected_btn:hover {color:#fff;}
        .custom_zoomcontrol {position:absolute;top:50px;right:10px;width:36px;height:80px;overflow:hidden;z-index:1;background-color:#f5f5f5;}
        .custom_zoomcontrol span {display:block;width:36px;height:40px;text-align:center;cursor:pointer;}
        .custom_zoomcontrol span img {width:15px;height:15px;padding:12px 0;border:none;}
        .custom_zoomcontrol span:first-child{border-bottom:1px solid #bfbfbf;}
    </style>

</head>
<body id="trnscRgstr">
<!-- 공통 -->
<header class="header-nav">
    <img src="${pageContext.request.contextPath}/static/assets/img/icons/back.png" alt="nav" class="icon" onclick="locationH('/user/purchaseSale');">
    <span class="nav-title">기사 현위치</span>
</header>
<!-- 공통 -->
<div class="container pd-t32 pd-lr25">

    <div class="map_wrap">
        <div id="map" style="width:100%;height:100%;position:relative;overflow:hidden;"></div>
        <!-- 지도타입 컨트롤 div 입니다 -->
        <div class="custom_typecontrol radius_border">
            <span id="btnRoadmap" class="selected_btn" onclick="setMapType('roadmap')">지도</span>
            <span id="btnSkyview" class="btn" onclick="setMapType('skyview')">스카이뷰</span>
        </div>
        <!-- 지도 확대, 축소 컨트롤 div 입니다 -->
        <div class="custom_zoomcontrol radius_border">
            <span onclick="zoomIn()"><img src="https://t1.daumcdn.net/localimg/localimages/07/mapapidoc/ico_plus.png" alt="확대"></span>
            <span onclick="zoomOut()"><img src="https://t1.daumcdn.net/localimg/localimages/07/mapapidoc/ico_minus.png" alt="축소"></span>
        </div>
    </div>
    <b class="block bold color-gray fs13 mr-t30">거래 정보</b>
    <ul class="transaction-list mr-t16">
        <li>
            <span class="block color-gray">차량정보</span>
            <span class="block">${data.productName}</span>
        </li>
        <li>
            <span class="block color-gray">판매자명</span>
            <span class="block">${data.sellerName} (${data.sellerPh})</span>
        </li>
        <li>
            <span class="block color-gray">배송기사</span>
            <span class="block">${data.driverName} (${data.driverHp})</span>
        </li>

        <li>
            <span class="block color-gray">출발지</span>
            <span class="block">${data.startAddress} (${data.startAddressDetail})</span>
        </li>
        <li>
            <span class="block color-gray">도착지</span>
            <span class="block">${data.endAddress} (${data.endAddressDetail})</span>
        </li>
        <li>
            <span class="block color-gray">배송상태</span>
            <span class="block">
                <c:choose>
                    <c:when test="${data.csmStatus eq 'A'}">기사배정완료</c:when>
                    <c:when test="${data.csmStatus eq 'J'}">출발지도착</c:when>
                    <c:when test="${data.csmStatus eq 'S'}">배송진행중</c:when>
                    <c:when test="${data.csmStatus eq 'E'}">배송도착</c:when>
                    <c:when test="${data.csmStatus eq 'D'}">배송완료</c:when>
                    <c:when test="${data.csmStatus eq 'C'}">배송취소</c:when>
                </c:choose>
                
            </span>
        </li>
        
        <li>
            <span class="block color-gray">배송업체</span>
            <span class="block">로드윈 고객센터</span>
        </li>
    </ul>
    <section class="btn-wrap pd0">
        <button class="bgColor-blue fs16 color-white" onclick="locationH('/user/purchaseSale');" >거래내역</button>
    </section>
</div>


</body>



<script>
    var container = document.getElementById('map');
    var options = {
        center: new kakao.maps.LatLng(${lat}, ${lon}),
        level: 2
    };

    var map = new kakao.maps.Map(container, options);
    // 마커가 표시될 위치입니다
    var markerPosition  = new kakao.maps.LatLng(${lat}, ${lon});

    // 마커를 생성합니다
    var marker = new kakao.maps.Marker({
        position: markerPosition
    });

    // 마커가 지도 위에 표시되도록 설정합니다
    marker.setMap(map);
    // 지도타입 컨트롤의 지도 또는 스카이뷰 버튼을 클릭하면 호출되어 지도타입을 바꾸는 함수입니다
    function setMapType(maptype) {
        var roadmapControl = document.getElementById('btnRoadmap');
        var skyviewControl = document.getElementById('btnSkyview');
        if (maptype === 'roadmap') {
            map.setMapTypeId(kakao.maps.MapTypeId.ROADMAP);
            roadmapControl.className = 'selected_btn';
            skyviewControl.className = 'btn';
        } else {
            map.setMapTypeId(kakao.maps.MapTypeId.HYBRID);
            skyviewControl.className = 'selected_btn';
            roadmapControl.className = 'btn';
        }
    }

    // 지도 확대, 축소 컨트롤에서 확대 버튼을 누르면 호출되어 지도를 확대하는 함수입니다
    function zoomIn() {
        map.setLevel(map.getLevel() - 1);
    }

    // 지도 확대, 축소 컨트롤에서 축소 버튼을 누르면 호출되어 지도를 확대하는 함수입니다
    function zoomOut() {
        map.setLevel(map.getLevel() + 1);
    }
</script>
</html>