<%--
  Writer: Lee
  Date: 2021-04-09 오후 5:19
  Explanaion : 
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<html>
<%@include file="/WEB-INF/jsp/user_static/headerImport.jsp" %>
<script async src="${pageContext.request.contextPath}/static/assets/js/csmUpdate.js"></script>


<style>
    input:read-only{
        color : #8C8C8C;
    }

</style>
<%--<script>
    function  addressListBtn(){

        $('input:radio[name="myAddress"]').eq(0).attr("checked" , "checked");

        addressSetting('<c:out value="${myAddress.address}"/>' , '<c:out value="${myAddress.addressDetail}"/>');

    }

</script>--%>


<body id="searchAdd">
<!-- 공통 -->
<header class="header-nav">
    <img src="${pageContext.request.contextPath}/static/assets/img/icons/close.png" alt="nav" class="icon" onclick="locationH('/user/purchaseSale');">
    <span class="nav-title">배송금액 수정</span>
</header>
<!-- 공통 -->
<div class="container pd-t32 pd-lr25 pd-b134">
    <form  class="form mr-t20" id="searchAddForm" autocomplete="off">
        <ul>
            <li>
                <div class="input-title">차량정보</div>
                <input type="text"  name="productName" value="${data.productName}" readonly >
                <div class="feedback-msg"></div>
            </li>

            <li>
                <div class="input-title">판매자</div>
                <input type="text"  name="sellerInfo" value="${data.sellerName} / ${data.sellerPh}" readonly>
                <div class="feedback-msg"></div>
            </li>
        </ul>
        <ul class="address-add mr-t20">
            <li>
                <div class="input-title">출발지 주소</div>
                <div class="address-input-box relative">
                    <input type="text" readonly  id="address1" value="${data.startAddress}" name="address1">
                </div>
                <div class="feedback-msg"></div>
            </li>
            <li>
                <div class="input-title">도착지 주소</div>
                <div class="address-input-box relative">
                    <input type="text"  readonly  id="address2" value="${data.endAddress}" name="address2">

                </div>
                <div class="feedback-msg"></div>
            </li>
            <li class="price relative">
                <div class="input-title">요청금액</div>
                <input class="price-input price-input-1000" type="text" pattern="\d*" placeholder="숫자만 입력"  onchange="chkNum(this, 'deliveryCost'); searchAddValueChk(); " onkeyup="chkNum(this, 'deliveryCost'); searchAddValueChk(); priceComma(this);" name="deliveryCost" id="deliveryCost" value="<fmt:formatNumber value="${data.csmPrice }" pattern="#,###" />" >
                <div class="feedback-msg deliveryCost"></div>


            </li>
        <input type="hidden" name="price_TYPE" id="price_TYPE" value="CASH">
        </ul>

        <input type="hidden" name="rwID" id="rwID" value="${data.rwId}">
        <input type="hidden" name="orderNo" id="orderNo" value="${param.orderNo}">

        <br><br><br>
        <section class="btn-wrap btn-two flex" style="margin-top: 10%; position: static; width: 100%">
            <button class="btn-white" type="button" onclick="locationH('/user/purchaseSale')">거래내역</button>
            <button class="btn-blue disabled add-btn" type="button" id="searchFormSubmit" onclick="this.disabled=true">요청금액 수정</button>
            <!-- 모든 정보 입력하지 않았을 경우(유효성 검사) disabled class 추가 하거나 disabled 속성 추가-->
        </section>
    </form>
</div>


</body>
</html>
