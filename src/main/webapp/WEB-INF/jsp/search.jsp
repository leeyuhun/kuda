<%--
  Writer: Lee
  Date: 2021-04-09 오후 5:19
  Explanaion : 
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<%@include file="/WEB-INF/jsp/user_static/headerImport.jsp" %>
<script async src="${pageContext.request.contextPath}/static/assets/js/search.js"></script>

<body id="search">
<!-- 공통 -->


<header class="header-nav">
    <img src="${pageContext.request.contextPath}/static/assets/img/icons/close.png" alt="nav" class="icon" onclick="locationH('/user/purchaseSale');">
    <span class="nav-title">판매자 검색</span>
</header>

<!-- 공통 -->
<div class="container pd-t20 pd-lr25 ">
    <form action="" method="" id="userSearchList" autocomplete="off">
        <input type="text" placeholder="이름 또는 핸드폰번호 뒤 4자리" class="search" id="userSearch">
    </form>
    <ul class="list mr-t39">
        <div class="search-container">
            <li>
                검색어를 입력해 주세요.
            </li>
        </div>
    </ul>
</div>
</body>
</html>
