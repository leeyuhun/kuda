<%--
  Writer: Lee
  Date: 2021-04-09 오후 5:18
  Explanaion : 
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<%@include file="/WEB-INF/jsp/user_static/headerImport.jsp" %>

<%--<script async src="${pageContext.request.contextPath}/static/assets/js/validation.js"></script>--%>
<script async src="${pageContext.request.contextPath}/static/assets/js/profile.js"></script>
<body id="profile">
<!-- 공통 -->
<header class="header-nav">
    <img src="${pageContext.request.contextPath}/static/assets/img/icons/back.png" alt="nav" class="icon" onclick="locationH('/user/purchaseSale')">
    <span class="nav-title">회원정보 변경</span>


</header>
<!-- 공통 -->
<div class="container pd-t28 pd-lr25">
    <form action="${pageContext.request.contextPath}/user/updateProfile" method="POST" class="form profile-form" name="profileForm" id="profileForm" autocomplete="off">
        <ul>
            <li>
                <div class="input-title">상사명</div>
                <input type="text" placeholder="상사명 입력" onchange="profileValueChk();" onkeyup="profileValueChk();" value="${profileList.company}"  name="company">
                <div class="feedback-msg company"></div>
                <!-- 피드백 메세지 ex) 상사명을 입력해주세요! -->
            </li>
            <li>
                <div class="input-title">종사원 번호</div>
                <input type="number" pattern="\d*" placeholder="-없이 숫자만 입력" onchange="profileValueChk(); chkNum(this, 'empNum');" onkeyup="profileValueChk(); chkNum(this, 'empNum');" value="${profileList.empNum}" name="empNum">
                <div class="feedback-msg empNum"></div>
            </li>
            <li>
                <div class="input-title">휴대전화번호</div>
                <input type="number" pattern="\d*" placeholder="-없이 숫자만 입력" onchange="profileValueChk(); chkNum(this, 'ph');" onkeyup="profileValueChk(); chkNum(this, 'ph');" value="${profileList.ph}" name="ph" class="inputPhone">
                <div class="feedback-msg ph"></div>
            </li>
            <li>
                <div class="input-title">계좌번호</div>
                <div class="clear flex relative">
                    <div class="selectbox" onclick="selectboxClick(this)" >
                        <span class="up-selectbox-span" style="color: #34373c; font-weight: bold;">${profileList.bankName}</span>
                    </div>
                    <div class="select-list-box">
                        <ul class="select-list">
                            <c:set var="profileBankCode" value="${profileList.bankCode}"/>
                            <c:forEach items="${bankList}" var="bank" varStatus="status">
                                <c:choose>

                                    <c:when test="${bank.bank_code eq profileBankCode}">
                                        <li data-value="${bank.bank_code}" class="select-list-li select-list-li-selected">${bank.bank_name}</li>
                                    </c:when>
                                    <c:otherwise>
                                        <li data-value="${bank.bank_code}" class="select-list-li" onclick="profileValueChk();" >${bank.bank_name}</li>
                                    </c:otherwise>
                                </c:choose>
                            </c:forEach>
                        </ul>
                    </div>
                    <input type="number" pattern="\d*" placeholder="계좌번호 입력" onchange="profileValueChk(); chkNum(this, 'bankNum');" onkeyup="profileValueChk(); chkNum(this, 'bankNum');" value="${profileList.bankNum}" name="bankNum">
                </div>
                <div class="feedback-msg bankNum"></div>
            </li>
            <div class="input-title">예금주명</div>
                <input type="text" placeholder="한글 성명 입력" onchange="profileValueChk(); chkKo(this, 'bankHolder');" onkeyup="profileValueChk(); chkKo(this, 'bankHolder');" value="${profileList.bankHolder}" name="bankHolder">
            <div class="feedback-msg bankHolder"></div>

            <li>
                <div class="input-title">주소</div>
                <div class="address-input-box relative">
                    <input type="text" placeholder="주소 검색" readonly  id="address" onchange="profileValueChk();" onkeyup="profileValueChk();"  name="address" onclick="execPostCode();" value="${profileList.address}">
                </div>
                <div class="feedback-msg"></div>
            </li>
            <li>
                <div class="input-title">상세 주소</div>
                    <input type="text" placeholder="상세주소 입력"  id="addressDetail" onchange="profileValueChk();" onkeyup="profileValueChk();" name="addressDetail" value="${profileList.addressDetail}">
                <div class="feedback-msg"></div>
            </li>

        </ul>
        <input type="text" name="bankCode" id="bankCode" onchange="profileValueChk();" onkeyup="profileValueChk();" class="check-input bankCode updateInput display-none" value="${profileList.bankCode}" name="bankCode">

    <section class="btn-wrap btn-two flex" style="margin-top: 10%; position: static; width: 100%">
        <button class="btn-white" onclick="locationH('/user/purchaseSale')" type="button">취소</button>
        <button class="btn-blue disabled pf-btn"  disabled="disabled" id="profile-form-submit" type="submit">적용</button>
    </section>
    </form>
</div>
</body>
</html>
