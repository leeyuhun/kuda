<%--
  Created by IntelliJ IDEA.
  User: Lee
  Date: 2021-01-25 오후 5:47
  Explanaion : 
--%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@include file="/WEB-INF/jsp/static/customer/header.jsp" %>

<%


%>

<%--<%--%>


<%--    /*--%>
<%--     *******************************************************--%>
<%--     * <결제요청 파라미터>--%>
<%--     * 결제시 Form 에 보내는 결제요청 파라미터입니다.--%>
<%--     * 샘플페이지에서는 기본(필수) 파라미터만 예시되어 있으며,--%>
<%--     * 추가 가능한 옵션 파라미터는 연동메뉴얼을 참고하세요.--%>
<%--     *******************************************************--%>
<%--     */--%>

<%--    String merchantKey 		= "EYzu8jGGMfqaDEp76gSckuvnaHHu+bC4opsSN6lHv3b2lurNYkVXrZ7Z1AoqQnXI3eLuaUFyoRNC6FkrzVjceg=="; // 상점키--%>
<%--    String merchantID 		= ""; 				// 상점아이디--%>
<%--    String goodsName 		= "나이스페이"; 					// 결제상품명--%>
<%--    String price 			= ""; 						// 결제상품금액--%>
<%--    String buyerName 		= "나이스"; 						// 구매자명--%>
<%--    String buyerTel 		= "01000000000"; 				// 구매자연락처--%>
<%--    String buyerEmail 		= "happy@day.co.kr"; 			// 구매자메일주소--%>
<%--    String moid 			= "mnoid1234567890"; 			// 상품주문번호--%>
<%--    String returnURL 		= "http://localhost:8080/nicepay3.0_utf-8/payResult_utf.jsp"; // 결과페이지(절대경로) - 모바일 결제창 전용--%>


<%--    /*--%>
<%--     *******************************************************--%>
<%--     * <해쉬암호화> (수정하지 마세요)--%>
<%--     * SHA-256 해쉬암호화는 거래 위변조를 막기위한 방법입니다.--%>
<%--     *******************************************************--%>
<%--     */--%>



<%--    NicePayUtil ncu = new NicePayUtil();--%>
<%--    DataEncrypt sha256Enc 	= new DataEncrypt();--%>
<%--    String ediDate 			= ncu.getyyyyMMddHHmmss();--%>
<%--    String hashString 		= sha256Enc.encrypt(ediDate + merchantID + price + merchantKey); // PG가맹점코드 + 가격 + 상점키--%>



<%--%>--%>

<script src="https://web.nicepay.co.kr/v3/webstd/js/nicepay-3.0.js" type="text/javascript"></script>
<script type="text/javascript">
    //결제창 최초 요청시 실행됩니다.
    function nicepayStart(){
        if(checkPlatform(window.navigator.userAgent) == "mobile"){//모바일 결제창 진입
            document.payForm.action = "https://web.nicepay.co.kr/v3/v3Payment.jsp";
            document.payForm.acceptCharset="euc-kr";
            document.payForm.submit();
        }else{//PC 결제창 진입
            goPay(document.payForm);
        }
    }

    //[PC 결제창 전용]결제 최종 요청시 실행됩니다. <<'nicepaySubmit()' 이름 수정 불가능>>
    function nicepaySubmit(){
        document.payForm.submit();
    }

    //[PC 결제창 전용]결제창 종료 함수 <<'nicepayClose()' 이름 수정 불가능>>
    function nicepayClose(){
        alert("결제가 취소 되었습니다");
    }

    //pc, mobile 구분(가이드를 위한 샘플 함수입니다.)
    function checkPlatform(ua) {
        if(ua === undefined) {
            ua = window.navigator.userAgent;
        }

        ua = ua.toLowerCase();
        var platform = {};
        var matched = {};
        var userPlatform = "pc";
        var platform_match = /(ipad)/.exec(ua) || /(ipod)/.exec(ua)
            || /(windows phone)/.exec(ua) || /(iphone)/.exec(ua)
            || /(kindle)/.exec(ua) || /(silk)/.exec(ua) || /(android)/.exec(ua)
            || /(win)/.exec(ua) || /(mac)/.exec(ua) || /(linux)/.exec(ua)
            || /(cros)/.exec(ua) || /(playbook)/.exec(ua)
            || /(bb)/.exec(ua) || /(blackberry)/.exec(ua)
            || [];

        matched.platform = platform_match[0] || "";

        if(matched.platform) {
            platform[matched.platform] = true;
        }


        if(platform.android || platform.bb || platform.blackberry
            || platform.ipad || platform.iphone
            || platform.ipod || platform.kindle
            || platform.playbook || platform.silk
            || platform["windows phone"]) {
            userPlatform = "mobile";
        }

        if(platform.cros || platform.mac || platform.linux || platform.win) {
            userPlatform = "pc";
        }

        return userPlatform;
    }

    /*
    * pgPaymentLoad
    *
    * */
    function pgPaymentLoad(el){

        // var niceAmt = $('#nice_Amt').val();
        // niceAmt =   $(el).closest('.pg-payment').find('.hidden-amount').html();


        $('#nice_Amt').val($(el).closest('.pg-payment').find('.hidden-amount').html());

        // var niceGoodsName = $('#nice_GoodsName').val();
        // niceGoodsName =   $(el).closest('.pg-payment').find('.hidden-product').html();

        $('#nice_GoodsName').val($(el).closest('.pg-payment').find('.hidden-product').html());


        // var niceMID = $('#nice_MID').val();
        // niceMID =   $(el).closest('.pg-payment').find('.hidden-psCode').html();

        $('#nice_MID').val($(el).closest('.pg-payment').find('.hidden-psCode').html());

        // var niceMoid = $('#nice_Moid').val();
        // niceMoid =   $(el).closest('.pg-payment').find('.hidden-code').html();

        $('#nice_Moid').val($(el).closest('.pg-payment').find('.hidden-code').html());

        // var niceBuyerName = $('#nice_BuyerName').val();
        // niceBuyerName =   $(el).closest('.pg-payment').find('.hidden-name').html();

        $('#nice_BuyerName').val($(el).closest('.pg-payment').find('.hidden-name').html());

        // var niceBuyerTel = $('#nice_BuyerTel').val();
        // niceBuyerTel =   $(el).closest('.pg-payment').find('.hidden-ph').html();

        $('#nice_BuyerTel').val($(el).closest('.pg-payment').find('.hidden-ph').html());






        nicepayStart();
    }






</script>

<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="main-content">

    <div class="page-content">
        <div class="container-fluid">

            <!-- page title -->
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box d-flex align-items-center justify-content-between">
                        <h4 class="mb-0 font-size-24">Payment</h4>

                        <%--                        <div class="page-title-right">--%>
                        <%--                            <ol class="breadcrumb m-0">--%>
                        <%--                                <li class="breadcrumb-item"><a href="javascript: void(0);">Utility</a></li>--%>
                        <%--                                <li class="breadcrumb-item active">Starter Page</li>--%>
                        <%--                            </ol>--%>
                        <%--                        </div>--%>

                    </div>
                </div>
            </div>
            <!-- page title -->
            <div class="col-xl-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title"></h4>

                        <div class="row">
                                <div class="col-lg-4 pg-payment">
                                    <div class="border p-3 rounded mt-4">
                                        <div class="d-flex align-items-center mb-3">

                                            <h5 class="font-size-14 mb-0">Chatoss 약관동의</h5>
                                        </div>

                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="text-muted mt-3">
                                                    <p>**** **** **** ****</p>
                                                    <h4><c:out value="${list.yp_amount}" /></h4>

                                                    <div class="hidden-info hidden-amount"><c:out value="${list.yp_amount}" /></div>
                                                    <div class="hidden-info hidden-product"><c:out value="${list.yr_product_name}" /></div>
                                                    <div class="hidden-info hidden-psCode"><c:out value="${list.ps_code}" /></div>
                                                    <div class="hidden-info hidden-code" ><c:out value="${list.yr_code}" /></div>
                                                    <div class="hidden-info hidden-name"><c:out value="${list.yr_buyer_name}" /></div>
                                                    <div class="hidden-info hidden-ph"><c:out value="${list.yr_buyer_ph}" /></div>

                                                </div>
                                            </div>

                                            <div class="col-lg-6 align-self-end">
                                                <div class="float-right mt-3">

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>


                        </div>
                        <form name="payForm" method="post" action="nicePayForm" accept-charset="euc-kr" id="nicePayHiddenForm" class="nicePayHiddenForm">

                            <input type="hidden" name="PayMethod"       id="nice_PayMethod"     value="">
                            <input type="hidden" name="GoodsName"       id="nice_GoodsName"     value="${list.yr_product_name}">       <!--결제상품명-->
                            <input type="hidden" name="Amt"             id="nice_Amt"           value="${list.yp_amount}">                 <!--결제상품금액-->
                            <input type="hidden" name="MID"             id="nice_MID"           value="${list.ps_code}">            <!--상점아이디-->
                            <input type="hidden" name="Moid"            id="nice_Moid"          value="${list.yp_code}"> <!--상품주문번호-->
                            <input type="hidden" name="BuyerEmail"      id="nice_BuyerEmail"    value="">
                            <input type="hidden" name="BuyerName"       id="nice_BuyerName"     value="${list.yr_buyer_name}">       <!--구매자명-->
                            <input type="hidden" name="BuyerTel"        id="nice_BuyerTel"      value="${list.yr_buyer_ph}">         <!--구매자연락처-->
                            <input type="hidden" name="ReturnURL"       id="nice_ReturnURL"     value="http://localhost:28088/payment?orderNo=${list.yr_code}">
                            <input type="hidden" name="VbankExpDate"    id="nice_VbankExpDate"  value="">
                            <input type="hidden" name="orderNo"         id="orderNo"  value="${param.orderNo}">
                            <input type="hidden" name="payNo"           id="payNo"  value="${param.payNo}">


                            <!-- 옵션 -->
                            <input type="hidden" name="GoodsCl" value="1"/>						<!-- 상품구분(실물(1),컨텐츠(0)) -->
                            <input type="hidden" name="TransType" value="0"/>					<!-- 일반(0)/에스크로(1) -->
                            <input type="hidden" name="CharSet" value="utf-8"/>					<!-- 응답 파라미터 인코딩 방식 -->
                            <input type="hidden" name="ReqReserved" value=""/>					<!-- 상점 예약필드 -->



                            <!-- 변경 불가능 -->
                            <input type="hidden" name="EdiDate" value="${list.ediDate}"/>			<!-- 전문 생성일시 -->
                            <input type="hidden" name="SignData" value="${list.hashString}"/>	<!-- 해쉬값 -->


                            <br>
                            <br>

                            <button class="btn btn-primary btn-lg btn-block waves-effect waves-light mb-1" onclick="nicepayStart()">결제하기</button>
                        </form>
                    </div>
                </div>






        <%@include file="/WEB-INF/jsp/static/customer/footer.jsp" %>




