<%--
  Writer: Lee
  Date: 2021-04-09 오후 5:19
  Explanaion : 
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<%@include file="/WEB-INF/jsp/user_static/headerImport.jsp" %>
<script async src="${pageContext.request.contextPath}/static/assets/js/remittance.js"></script>
<body id="remittance">
<!-- 공통 -->
<header class="header-nav">
    <img src="${pageContext.request.contextPath}/static/assets/img/icons/close.png" alt="nav" class="icon" onclick="locationH('/user/purchaseSale')">
    <span class="nav-title">입금 정보</span>
</header>
<!-- 공통 -->
<div class="container pd-t32 pd-lr25">
    <p class="fs22 bold lh145">거래가 완료된 후<br>입금 받으실 계좌를 입력해 주세요.</p>
    <ul class="remittanceSelect-list flex mr-t30">
        <li style="width: 110px" class="active list" onclick="navActive(this); remittanceValueChk(); fadeIn('.myAccount', '.writeAccount'); remittanceSetting('${myAccount.bankCode}#$${myAccount.bankHolder}#$${myAccount.bankNum}');">내계좌 송금</li>
        <li style="width: 110px" onclick="navActive(this); remittanceValueChk(); fadeIn('.writeAccount', '.myAccount'); remittanceInit();">차주계좌로 송금</li>
    </ul>
    <form id="remittanceForm" name="remittanceForm" class="form" autocomplete="off">
    <section class="myAccount mr-t30">

            <b class="color-gray bold fs13">내 계좌</b>
            <ul class="mr-t16">
                <li>
                    <label class="radio-chk flex">
                        <input type="radio" class="remittanceList" name="myAccount" value="" checked onclick="remittanceSetting('${myAccount.bankCode}#$${myAccount.bankHolder}#$${myAccount.bankNum}');">
                        <i class="radio-icon"></i>
                        <div class="info mr-l20">
                            <span class="name bold">${myAccount.bankHolder}</span>
                            <span class="block color-gray fs14 mr-t8">${myAccount.bankName} / ${myAccount.bankNum}</span>
                        </div>
                    </label>
                </li>
            </ul>
            <%--<b class="color-gray bold fs13 mr-t36 block">최근 이용</b>
            <ul class="mr-t16">
            <c:forEach items="${history}" var="history">
                <li>
                    <label class="radio-chk flex">
                        <input type="radio" class="remittanceList" name="myAccount" onclick="remittanceSetting('${history.bankCode}#$${history.bankHolder}#$${history.bankNumber}');" >
                        <i class="radio-icon"></i>
                        <div class="info mr-l20">
                            <span class="name bold">${history.bankHolder}</span>
                            <span class="block color-gray fs14 mr-t8">${history.bankName} / ${history.bankNumber}</span>
                        </div>
                    </label>
                </li>
            </c:forEach>
            </ul>--%>
    </section>
    <section class="writeAccount mr-t30">

            <ul>
                <li>
                    <div class="input-title">계좌번호</div>
                    <div class="clear flex relative">
                        <div class="selectbox" onclick="selectboxClick(this)">
                            <span>은행 선택</span>
                        </div>
                        <div class="select-list-box">
                            <ul class="select-list">
                                <c:forEach items="${banklist}" var="bank">
                                    <li data-value="${bank.bank_code}" class="select-list-li" onclick="remittanceValueChk();">${bank.bank_name}</li>
                                </c:forEach>
                            </ul>
                        </div>
                        <input type="number" pattern="\d*" placeholder="계좌번호 입력 -없이" name="eri_account_number" id="aNumber" value="${myAccount.bankNum}" onkeyup="chkNum(this, 'eri_account_number'); remittanceValueChk();" >
                    </div>
                    <div class="feedback-msg eri_account_number"></div>
                </li>
                <li>
                    <div class="input-title">예금주명(차주명)</div>
                    <input type="text" placeholder="예금주명(차주명) 입력" name="eri_account_holder" id="aHolder" value="${myAccount.bankHolder}"  onchange=" remittanceValueChk();" onkeyup=" remittanceValueChk();">
                    <div class="feedback-msg eri_account_holder"></div>
                </li>
                <li>
                    <div class="input-title">차주 전화번호</div>
                    <input type="number" pattern="\d*" class="numberOnly check-input inputPhone"  placeholder="-없이 입력" name="car_owner_ph" id="car_owner_ph"   onchange="chkNum(this, 'car_owner_ph'); chkPhoneLength(this); remittanceValueChk();" onkeyup="chkNum(this, 'car_owner_ph'); chkPhoneLength(this); remittanceValueChk();">
                    <div class="feedback-msg car_owner_ph"></div>
                </li>


            </ul>
        <input type="hidden" name="bank_code" class="bankCode" value="${myAccount.bankCode}" id="bankCode" onchange="remittanceValueChk();"  onkeyup="chkNum(this, 'bank_code');">
        <input type="hidden" name="esl_code"  value="${param.orderNo}">

    </section>
<%--        <ul>--%>
<%--            <li>--%>
<%--                <div class="input-title" style="">계좌 정보를 반드시 확인해 주세요. <br>오 입력으로 인한 금융거래 사고는 차 토스에서 책임지지 않습니다.</div>--%>
<%--            </li>--%>
<%--        </ul>--%>
    <section class="btn-wrap pd0">
        <p class="fs14 color-blue bold">※계좌 정보를 반드시 확인해 주세요. <br>오 입력으로 인한 금융거래 사고는 차 토스에서 책임지지 않습니다.</p>
        <button type="button" class="bgColor-blue fs16 color-white mr-t12 remittanceBtn"  onclick="this.disabled=true" >계좌등록완료</button>
<%--        <button class="btn-blue disabled remittanceBtn" type="submit">계좌 선택 완료</button>--%>
    </section>
    </form>

</div>
</body>
</html>
