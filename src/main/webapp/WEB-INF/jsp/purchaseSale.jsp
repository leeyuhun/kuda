<%--
  Writer: Lee
  Date: 2021-04-09 오후 5:18
  Explanaion : 
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<html>
<%@include file="/WEB-INF/jsp/user_static/headerImport.jsp" %>
<script async src="${pageContext.request.contextPath}/static/assets/js/purchaseSale.js"></script>
<body id="purchaseSale">
<!-- 공통 header -->
<header class="header-nav">
    <img src="${pageContext.request.contextPath}/static/assets/img/icons/menu.png" alt="nav" class="icon" onclick="openNav()">
</header>
<!-- 공통 header --> <%--1670 8055--%>
<div class="container pd-t28 pd-lr25">
    <ul class="nav-list clear">
        <li class="active" onclick="navActive(this); purchaseSaleView('purchase');">구매<span class="count">${buyCount}</span></li>
        <li onclick="navActive(this); purchaseSaleView('sale')">판매<span class="count">${sellCount}</span></li>
    </ul>

    <!-- 구매 View -->
    <section class="purchase-view">
        <section class="add mr-t30">
            <button onclick="locationH('/user/search')">+ 거래 시작하기</button>
        </section>

        <c:choose>
            <c:when test="${buyCount >= 1}">
                <!-- 거래내역 있을 경우! -->
                <section class="transaction-list">
                    <c:forEach items="${buyLists}" var="buy">
                        <ul class="mr-t24">
                            <li>
                                <ul class="clear">
                                    <li>
                                        <span class="color-gray">차량명</span><!--
                                                --><span>${buy.productName}<span class="date">${buy.requestDate}</span></span>
                                    </li>
                                    <li>
                                        <span class="color-gray">판매자</span><!--
                                                --><span>${buy.userName} (${buy.userPh})</span>
                                    </li>
                                    <li class="mr-b0">
                                        <span class="color-gray">금액</span><!--
                                                --><span class="bold"><fmt:formatNumber value="${buy.amount }" pattern="#,###" /> 원</span>
                                    </li>
                                    <c:choose>
                                        <c:when  test="${buy.state eq '0'}">
                                            <li class="request mr-t20">
<%--                                                <button class="bgColor-blue color-white" onclick="remittanceInfoModal();">가상계좌 확인하기</button>--%>
                                                <button class="bgColor-blue color-white" onclick="locationH('/user/trnscRgstr?orderNo=${buy.code}');">거래정보 확인하기</button>
                                            </li>
                                        </c:when>
                                        <c:when  test="${buy.state eq '1'}">
                                            <li class="completed mr-t20">
                                                <span class="color-145ba4 block fs12">판매자가 위탁 배송 동의를 진행합니다.</span>
                                                <button class="bgColor-f0f8ff color-145ba4 mr-t10" onclick=""><img src="${pageContext.request.contextPath}/static/assets/img/icons/complete.png">거래금액 입금 완료</button>
                                            </li>
                                        </c:when>
                                        <c:when  test="${buy.state == '2'}">
                                            <li class="completed mr-t20">
                                                <span class="color-145ba4 block fs12">판매자가 위탁 배송 동의를 진행합니다.</span>
                                                <button class="bgColor-f0f8ff color-145ba4 mr-t10" onclick=""><img src="${pageContext.request.contextPath}/static/assets/img/icons/complete.png">거래금액 입금 완료</button>
                                            </li>
                                        </c:when>
                                        <c:when  test="${buy.state == '3'}">
                                            <li class="completed mr-t20">
                                                <span class="color-145ba4 block fs12">위탁배송 기사가 배정 중 입니다.</span>
                                                <button class="bgColor-blue color-white" onclick="locationH('/user/csmUpdate?orderNo=${buy.code}');">배송금액 변경요청</button>
                                            </li>
                                        </c:when>
                                        <c:when  test="${buy.state == '4'}">
                                            <li class="completed mr-t20">
                                                <span class="color-145ba4 block fs12">차량배송이 진행중 입니다.</span>
                                                <button class="bgColor-f0f8ff color-145ba4 mr-t10" onclick=""><img src="${pageContext.request.contextPath}/static/assets/img/icons/complete.png">거래금액 입금 완료</button>
                                            </li>
                                        </c:when>
                                        <c:when  test="${buy.state eq '5'}">
                                            <li class="transaction mr-t20">
                                                <span class="color-green block fs12">차량을 수령하신 후 거래완료 버튼을 눌러 주세요.</span>
                                                <button class="bgColor-green color-white mr-t10" onclick="locationH('/user/escrowCompleteProc?orderNo=${buy.code}&productName=${buy.productName}')">거래 완료하기</button>
                                            </li>
                                        </c:when>
                                        <c:when  test="${buy.state == '6' || buy.state == '7'}">
                                            <li class="completed mr-t20">
                                                <span class="color-145ba4 block fs12">판매자에게 입금 대기 중입니다.</span>
                                                <button class="bgColor-f0f8ff color-145ba4 mr-t10" onclick=""><img src="${pageContext.request.contextPath}/static/assets/img/icons/complete.png">판매자 입금 대기중</button>
                                            </li>
                                        </c:when>
                                    </c:choose>
                                </ul>
                            </li>
                        </ul>
                    </c:forEach>

                </section>
                <!-- 거래내역 있을 경우! -->
            </c:when>



            <c:when test="${buyCount == 0 }">
                <!-- 거래내역 없을 경우! -->
                <section class="empty mr-t137">
                    <p class="notification">현재 진행중인 거래가 없어요.</p>
                </section>
            </c:when>
        </c:choose>
    </section>
    <!-- 구매 View -->


    <!-- 판매 view -->
    <section class="sale-view">

            <c:choose>
                <c:when test="${sellCount >= 1}">

                    <section class="sale-list">
                        <ul class="mr-t24">
                            <li>
                                <c:forEach var="sell" items="${sellLists}">
                                    <ul class="clear">
                                        <li>
                                            <span class="color-gray">차량명</span><!--
                                        --><span>${sell.productName}<span class="date">${sell.requestDate}</span></span>
                                        </li>
                                        <li>
                                            <span class="color-gray">판매자</span><!--
                                        --><span>${sell.userName} (${sell.userPh})</span>
                                        </li>
                                        <li class="mr-b0">
                                            <span class="color-gray">금액</span><!--
                                        --><span class="bold"><fmt:formatNumber value="${sell.amount }" pattern="#,###" /> 원</span>
                                        </li>

                                        <c:choose>
                                            <c:when test="${sell.state eq '0'}">
                                                <li class="wait mr-t20">
                                                    <span class="color-145ba4 block fs12">구매자가 거래를 생성 후 입금 대기중입니다.</span>
                                                    <button class="bgColor-f0f8ff color-145ba4 mr-t10">입금 대기중</button>
                                                </li><br><br>

                                            </c:when>
                                            <c:when test="${sell.state eq '1'}">
                                                <li class="progress-start mr-t20">
                                                    <span class="color-blue block fs12">위탁 배송 동의를 진행해 주세요.</span>
                                                    <button class="bgColor-blue color-white mr-t10" onclick="locationH('/user/csmAgree?orderNo=${sell.code}')">위탁배송 동의하기</button>
                                                </li><br><br>
                                            </c:when>
                                            <c:when test="${sell.state eq '2'}">
                                                <li class="progress-start mr-t20">
                                                    <span class="color-green block fs12">거래금액 수령 계좌를 입력해 주세요.</span>
                                                    <button class="bgColor-green color-white mr-t10" onclick="locationH('/user/remittance?orderNo=${sell.code}')">계좌 입력하기</button>
                                                </li><br><br>
                                            </c:when>
                                            <c:when test="${sell.state eq '3'}">
                                                <li class="progress-end mr-t20">
                                                    <span class="color-145ba4 block fs12">구매자가 차량 수령 후 거래가 완료됩니다.</span>
                                                    <button class="bgColor-f0f8ff color-145ba4 mr-t10"><img src="${pageContext.request.contextPath}/static/assets/img/icons/complete.png">배송기사 배정중</button>
                                                </li><br><br>
                                            </c:when>
                                            <c:when test="${sell.state eq '4'}">
                                                <li class="progress-end mr-t20">
                                                    <span class="color-145ba4 block fs12">구매자가 차량 수령 후 거래가 완료됩니다.</span>
                                                    <button class="bgColor-f0f8ff color-145ba4 mr-t10"><img src="${pageContext.request.contextPath}/static/assets/img/icons/complete.png">배송 진행중</button>
                                                </li><br><br>
                                            </c:when>
                                            <c:when test="${sell.state eq '5'}">
                                                <li class="progress-end mr-t20">
                                                    <span class="color-145ba4 block fs12">구매자가 차량 수령 후 거래가 완료됩니다.</span>
                                                    <button class="bgColor-f0f8ff color-145ba4 mr-t10"><img src="${pageContext.request.contextPath}/static/assets/img/icons/complete.png">배송 완료</button>
                                                </li><br><br>
                                            </c:when>
                                            <c:when test="${sell.state eq '6' || sell.state eq '7'}">
                                                <li class="receipt mr-t20">
                                                    <span class="color-145ba4 block fs12">모든 거래가 완료되어 입금 대기중입니다.</span>
                                                    <button class="bgColor-f0f8ff color-145ba4 mr-t10" >송금 대기중</button>
                                                </li><br><br>
                                            </c:when>
                                        </c:choose>
                                    </ul>
                                </c:forEach>
                            </li>
                        </ul>
                    </section>
                </c:when>
                <c:when test="${sellCount == 0}">
                    <section class="empty mr-t137">
                        <p class="notification">현재 진행중인 거래가 없어요.</p>
                    </section>
                </c:when>
            </c:choose>

    </section>
    <!-- 판매 view -->
</div>


<!-- 차량 배송 진행 모달 -->
<div class="modal-full completed-modal">
    <header class="header-nav">
        <img src="${pageContext.request.contextPath}/static/assets/img/icons/close.png" alt="nav" class="icon" onclick="history.back();">
        <span class="nav-title">차량배송 진행</span>
    </header>
    <section class="modal-content pd-lr25 pd-t32">
        <section class="title">
            <p class="fs22 bold lh145 mr-b12">거래금액이 입금되었으니<br>차량 배송을 진행해 주세요.</p>
            <span class="color-pink fs12 mr-b30 block">구매자가 차량을 수령한 후 거래가 완료됩니다.</span>
        </section>
        <section class="info">
            <b class="block color-gray fs13 bold">배송 차량 정보</b>
            <ul class="mr-t16">
                <li>
                    <span class="color-gray">차량번호</span>
                    <span class="bold">11하2054</span>
                </li>
                <li>
                    <span class="color-gray">구매자</span>
                    <span>차두리 (01051973312)</span>
                </li>
            </ul>
        </section>
        <section class="btn-wrap pd0">
            <p class="fs14 bold color-blue">차량 배송을 시작하신 후 버튼을 눌러주세요.</p>
            <button class="bgColor-blue fs16 color-white mr-t12 bold" onclick="$('.modal-full.completed-modal').css('opacity',0); setTimeout(()=>{$('.modal-full.completed-modal').removeClass('active')},300);">네, 배송을 시작했어요</button>
        </section>
    </section>
</div>
</body>
<div class="overlay" onclick="closeNav()"></div>
<nav class="nav-menu">
    <div class="nav-icon-box clear">
        <img src="${pageContext.request.contextPath}/static/assets/img/icons/menu.png" alt="nav-img" class="nav-icon right" onclick="closeNav()">
    </div>
    <section class="info mr-t48">
        <b class="block fs22 block">${sessionScope.user.name}</b>
        <span class="block color-gray fs13 mr-t8">${sessionScope.user.userPh}</span>
    </section>
    <ul class="list">
        <li onclick="locationH('/user/history')">거래내역</li>
        <li onclick="locationH('/user/profile')">회원정보 변경</li>
        <c:if test="${sessionScope.user.userGrade eq 'ROLE_ADMIN'}"> <li onclick="locationH('/adm/escrowList')">관리자 페이지</li></c:if>
    </ul>
    <section class="nav-btn-wrap">
        <button onclick="locationH('/logOut')">로그아웃</button>
    </section>
</nav>
</html>
