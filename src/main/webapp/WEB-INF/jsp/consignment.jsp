<%--
  Created by IntelliJ IDEA.
  User: Lee
  Date: 2021-09-24
  Time: 오후 4:06
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<%@include file="/WEB-INF/jsp/user_static/headerImport.jsp" %>
<script async src="${pageContext.request.contextPath}/static/assets/js/consignment.js"></script>

<body id="">
<!-- 공통 -->


<div id="searchAdd">
<header class="header-nav">
    <img src="${pageContext.request.contextPath}/static/assets/img/icons/back.png" alt="nav" class="icon" onclick="locationH('/user/trnscRgstr?orderNo=${param.orderNo}');">
    <span class="nav-title">탁송 요청</span>
</header>


<!-- 공통 -->
<div class="container pd-t32 pd-lr25 pd-b134">
    <section class="title">
        <b class="bold fs18 block">거래 정보</b>
        <div class="title-info-box consignmentModal">
<%--            <div style="">--%>
                    <b class="bold block mr-r12">판매자</b>
                    <span class="block fs14">${csm.sellerName} (${csm.sellerCompany}) &nbsp;&nbsp; ${csm.sellerPh}</span>
<%--            </div>--%>
        </div>

    </section>

    <form action="${pageContext.request.contextPath}/rwin/booking" method="POST" class="form mr-t20" id="consignmentForm" autocomplete="off">

        <b class="bold fs18 block mr-t40">차량 현위치</b>
        <ul class="address-select mr-t14">
            <%--거래 내역이 없다면 목록 노출 X--%>

        </ul>
        <ul class="address-add mr-t20">
            <li>
                <div class="input-title">주소</div>
                <div class="address-input-box relative">
                    <input type="text" placeholder="주소 검색" onchange="consignmentValueChk(); " onkeyup="consignmentValueChk();" value="" name="address" id="address">
                    <button class="address-search" type="button" onclick="csmPostCode();">찾기</button>
                </div>
                <div class="feedback-msg"></div>
            </li>
            <li>
                <div class="input-title">상세 주소</div>
                <input type="text" placeholder="상세주소 입력"   onchange="consignmentValueChk();" onkeyup="consignmentValueChk();" name="addressDetail" id="addressDetail">
                <div class="feedback-msg"></div>
            </li>
        </ul>

        <b class="bold fs18 block mr-t40">배송 금액</b>
        <ul class="address-select mr-t14">
            <%--거래 내역이 없다면 목록 노출 X--%>

        </ul>
        <ul class="address-add mr-t20">

            <li class="price relative">
                <div class="input-title">요청 금액</div>
                <input type="text" placeholder="숫자만 입력" onchange="consignmentValueChk();" name="price" id="price">
                <div class="feedback-msg "></div>
            </li>

            <li>
                <div class="input-title">결제수단 선택</div>
                <label class="box-radio-input"><input type="radio" name="price_TYPE" value="CARD" checked="checked"><span>카드결제</span></label>
                <label class="box-radio-input"><input type="radio" name="price_TYPE" value="CASH"><span>현금결제</span></label>
            </li>
        </ul>

        <input type="hidden" name="orderNo" id="orderNo" value="${param.orderNo}"/>

        <input type="hidden" name="rw_ID" id="rwID" value=""/>
        <input type="hidden" name="end_ADDR" id="end_ADDR" value=""/>
        <input type="hidden" name="end_LON" id="end_LON" value=""/>
        <input type="hidden" name="end_LAT" id="end_LAT" value=""/>
        <input type="hidden" name="start_ADDR" id="start_ADDR" value=""/>
        <input type="hidden" name="start_LON" id="start_LON" value=""/>
        <input type="hidden" name="start_LAT" id="start_LAT" value=""/>


        <section class="btn-wrap btn-two flex" >
            <button class="btn-white" type="button" onclick="locationH('/user/trnscRgstr?orderNo=${param.orderNo}')">뒤로</button>
            <button class="btn-blue disabled csm-btn" type="button" id="consignmentFormSubmit" onclick="btn_consignmentFormSubmit()">신청하기</button>


            <!-- 모든 정보 입력하지 않았을 경우(유효성 검사) disabled class 추가 하거나 disabled 속성 추가-->
        </section>
    </form>
</div>

<div class="modal-full consignment-request-modal">
    <header class="header-nav">
        <img src="${pageContext.request.contextPath}/static/assets/img/icons/close.png" alt="nav" class="icon" onclick="$('.modal-full').css('opacity',0); setTimeout(()=>{$('.modal-full').removeClass('active')},300);">
        <span class="nav-title">거래 정보</span>
    </header>
    <section class="modal-content pd-lr25 pd-t32">

        <section class="info">
            <b class="block color-gray fs13 bold">판매자 정보</b>
            <ul class="mr-t16" style="margin-bottom: 30px">
                <li>
                    <span class="color-gray width-15">판매자명</span>
                    <span> ${csm.sellerName} (${csm.sellerCompany})</span>
                </li>
                <li>
                    <span class="color-gray width-15">핸드폰번호</span>
                    <span>${csm.sellerPh}</span>
                </li>
            </ul>


            <b class="block color-gray fs13 bold">차량 정보</b>
            <ul class="mr-t16" style="margin-bottom: 30px">
                <li>
                    <span class="color-gray width-15">모델명</span>
                    <span> ${csm.productName1}</span>
                </li>
                <li>
                    <span class="color-gray width-15">차량번호</span>
                    <span>${csm.productName2}</span>
                </li>
            </ul>

            <b class="block color-gray fs13 bold">차량배송 주소</b>
            <ul class="mr-t16">
                <li>
                    <span class="color-gray width-15">주소</span>
                    <span>${csm.requesterAddress1}</span>
                </li>
                <li>
                    <span class="color-gray width-15">상세주소</span>
                    <span>${csm.requesterAddress2}</span>
                </li>
            </ul>

        </section>
        <section class="btn-wrap pd0">
            <p class="fs14 bold color-blue"></p>
            <button class="bgColor-blue fs16 color-white mr-t12 bold" onclick="$('.modal-full').css('opacity',0); setTimeout(()=>{$('.modal-full').removeClass('active')},300);">확인</button>
        </section>
    </section>
</div>
</div>
</body>
</html>
