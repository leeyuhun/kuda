<%--
  Writer: Lee
  Date: 2021-01-22 오후 12:21
  Explanaion : jsp file header
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
<meta name="viewport" content="width=device-width, height=device-height , initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no , viewport-fit=cover" />
<link rel="stylesheet" type="text/css" href="https://cdn.rawgit.com/moonspam/NanumSquare/master/nanumsquare.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/static/assets/css/reset.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/static/assets/css/common.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/static/assets/css/section.css">
<script  src="https://code.jquery.com/jquery-3.5.1.js" integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
<script async src="${pageContext.request.contextPath}/static/assets/js/function.js"></script>
<script async src="${pageContext.request.contextPath}/static/assets/js/section.js"></script>
<script src="http://dmaps.daum.net/map_js_init/postcode.v2.js?autoload=false"></script>
<title>Kuda</title>
