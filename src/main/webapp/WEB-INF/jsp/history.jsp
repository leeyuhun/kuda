<%--
  Writer: Lee
  Date: 2021-04-09 오후 5:17
  Explanaion : 
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>

<%@include file="/WEB-INF/jsp/user_static/headerImport.jsp" %>

<body id="history">
<!-- 공통 -->
<header class="header-nav">
    <img src="${pageContext.request.contextPath}/static/assets/img/icons/back.png" alt="nav" class="icon" onclick="locationH('/user/purchaseSale');">
    <span class="nav-title">거래내역</span>
</header>
<!-- 공통 -->
<div class="container pd-t30">
    <ul class="flex history-nav">
        <li class="active" onclick="navActive(this); displayNon('.purchase-list', '.sale-list');">구매<span class="count">${buyCount}</span></li>
        <li onclick="navActive(this); displayNon('.sale-list', '.purchase-list');">판매<span class="count">${sellCount}</span></li>
    </ul>
    <section class="purchase-list history-list">
        <ul>
            <c:forEach items="${buyLists}" var="buy">
                <li>
                    <ul class="clear pd-lr25">
                        <li>
                            <span class="color-gray">차량명</span><!--
                                    --><span>${buy.productName}<span class="date">${buy.endDate}</span></span>
                        </li>
                        <li>
                            <span class="color-gray">판매자</span><!--
                                    --><span>${buy.userName} (${buy.userPh})</span>
                        </li>
                        <li class="mr-b0">
                            <span class="color-gray">금액</span><!--
                                    --><span class="bold"><fmt:formatNumber value="${buy.amount }" pattern="#,###" /> 원</span>
                        </li>
                    </ul>
                    <hr class="line-box">
                </li>
            </c:forEach>

        </ul>
    </section>
    <section class="sale-list history-list display-none">

        <ul>
            <c:forEach items="${sellLists}" var="sell">
                <li>
                    <ul class="clear pd-lr25">
                        <li>
                            <span class="color-gray">차량명</span><!--
                                    --><span>${sell.userName}<span class="date">${sell.endDate}</span></span>
                        </li>
                        <li>
                            <span class="color-gray">구매자</span><!--
                                    --><span>${sell.userName} (${sell.userPh})</span>
                        </li>
                        <li class="mr-b0">
                            <span class="color-gray">금액</span><!--
                                    --><span class="bold"><fmt:formatNumber value="${sell.amount }" pattern="#,###" /> 원</span>
                        </li>
                    </ul>
                    <hr class="line-box">
                </li>
            </c:forEach>
        </ul>
    </section>
</div>
</body>
</html>
