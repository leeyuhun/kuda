<%--
  Created by IntelliJ IDEA.
  User: Lee
  Date: 2021-05-31
  Time: 오후 4:28
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>


<%@include file="/WEB-INF/jsp/admin/common/header.jsp" %>

<%@include file="/WEB-INF/jsp/admin/common/javascript_import.jsp" %>
<script async src="${pageContext.request.contextPath}/static/admin_assets/js/escrow_remit_list.js"></script>

<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="main-content">

    <div class="page-content">
        <div class="container-fluid">

            <!-- page title -->
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box d-flex align-items-center justify-content-between">
                        <h4 class="mb-0 font-size-24">입금하기</h4>

                        <div class="page-title-right">
                            <ol class="breadcrumb m-0">
                                <li class="breadcrumb-item"><a href="javascript: void(0);">애스크로</a></li>
                                <li class="breadcrumb-item active">입금하기</li>
                            </ol>
                        </div>

                    </div>
                </div>
            </div>
            <!-- page title -->


            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <%--<div class="row mb-2">
                                <div class="col-md-1">
                                    <div class="form-group">
                                        <select  class="form-control" id="validationCustom01" required>
                                            <option selected="">승인일</option>
                                            <option >지급예정일</option>
                                            <option >실지급일</option>
                                            <option >PG정산일</option>
                                        </select>
                                    </div>
                                </div>


                                <div class="col-md-1">
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="validationCustom02" placeholder="First name" value="날짜1" required>
                                        <div class="valid-feedback">
                                            날짜1
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-1">
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="validationCustom03" placeholder="First name" value="날짜2" required>
                                        <div class="valid-feedback">
                                            날짜2
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="validationCustom04" placeholder="First name" value="승인여부select" required>
                                        <div class="valid-feedback">
                                            승인여부select
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="validationCustom05" placeholder="First name" value="검색select" required>
                                        <div class="valid-feedback">
                                            검색select
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="validationCustom06" placeholder="First name" value="검색input" required>
                                        <div class="valid-feedback">
                                            검색input
                                        </div>
                                    </div>
                                </div>


                                <div class="col-sm-8">
                                    <div class="text-sm-right">

                                    </div>
                                </div><!-- end col-->
                            </div>--%>

                            <button type="button" class="btn-detail-view btn btn-secondary btn-sm btn-default" id="btn-deposit-comp">
                                입금처리완료
                            </button>
                            <button type="button" class="btn-detail-view btn btn-dark btn-sm btn-default" id="btn-excel">
                                엑셀다운
                            </button>


                            <br><br><br><br>

                            <div class="table-responsive">
                                <%--<table class="table table-bordered table-nowrap">--%>
                                <table id="datatable" class="table table-bordered dt-responsive nowrap table-nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead class="thead-light">
                                        <tr style="text-align: center" >
                                            <%--<th style="width: 20px;">
                                                <div class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input" id="customCheck1">
                                                    <label class="custom-control-label" for="customCheck1">&nbsp;</label>
                                                </div>
                                            </th>--%>
                                            <th><input type="checkbox" id="check-box-all"></th>
                                            <th>요청일시</th>
                                            <th>차량정보</th>
                                            <th>요청자</th>
                                            <th>판매자</th>
                                            <th>총결제금액</th>
                                            <th>입금예정금액(임시)</th>
                                            <th>은행명</th>
                                            <th>계좌번호</th>
                                            <th>예금주</th>
                                            <th>입금금액</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                            <c:forEach items="${list}" var="list">
                                                <tr id="${list.code}">
                                                    <%--<td>
                                                        <div class="custom-control custom-checkbox">
                                                            <input type="checkbox" class="custom-control-input" id="customCheck2">
                                                            <label class="custom-control-label" for="customCheck2">&nbsp;</label>
                                                        </div>
                                                    </td>--%>

                                                    <td style="text-align: center;">
                                                        <input type="checkbox" class="check-box" name="checkBoxValues" value="${list.code}">
                                                    </td>

                                                    <td style="text-align: center;">
                                                        ${list.requestDate} ${list.requestTime}
                                                    </td>
                                                    <td style="text-align: center">
                                                        ${list.product}
                                                    </td>
                                                    <td style="text-align: center">
                                                        ${list.RName}
                                                    </td>
                                                    <td style="text-align: center">
                                                        ${list.SName}
                                                    </td>
                                                    <td style="text-align: right">
                                                        <fmt:formatNumber value="${list.AAmount}" pattern="#,###" />
                                                    </td>
                                                    <td style="text-align: right">
                                                        <fmt:formatNumber value="${list.PAmount}" pattern="#,###" />
                                                    </td>
                                                    <td style="text-align: right">
                                                        ${list.bankName}
                                                    </td>
                                                    <td style="text-align: center">
                                                        ${list.bankNumber}
                                                    </td>
                                                    <td style="text-align: center">
                                                        ${list.bankHolder}
                                                    </td>
                                                    <td style="text-align: right">
                                                        <fmt:formatNumber value="${list.eriAmount}" pattern="#,###" />
                                                    </td>


                                                </tr>
                                            </c:forEach>

<%--                                            <input type="hidden" name="checkValues" id="checkValues"/>--%>

                                    </tbody>

                                    <%--                                    <tfoot style="background-color:#f8f9fa; font-weight: 600; text-align: right">--%>
                                    <%--                                        <tr>--%>
                                    <%--                                            <td colspan="8">--%>
                                    <%--                                                합계--%>
                                    <%--                                            </td>--%>
                                    <%--                                            <td>--%>
                                    <%--                                                2,173,913--%>
                                    <%--                                            </td>--%>
                                    <%--                                            <td>--%>
                                    <%--                                                2,000,000--%>
                                    <%--                                            </td>--%>
                                    <%--                                            <td>--%>
                                    <%--                                                60,000--%>
                                    <%--                                            </td>--%>

                                    <%--                                        </tr>--%>

                                    <%--                                    </tfoot>--%>
                                </table>
                            </div>
                            <form action="${pageContext.request.contextPath}/excel/eRemitBankExcel" name="remitListForm" id="remitListForm"  method="get">
                               <input type="hidden" value="" id="codeString" name="codeString"/>
                            </form>
                            <%-- <ul class="pagination pagination-rounded justify-content-end mb-2">
                                 <li class="page-item disabled">
                                     <a class="page-link" href="javascript: void(0);" aria-label="Previous">
                                         <i class="mdi mdi-chevron-left"></i>
                                     </a>
                                 </li>
                                 <li class="page-item active"><a class="page-link" href="javascript: void(0);">1</a></li>
                                 <li class="page-item"><a class="page-link" href="javascript: void(0);">2</a></li>
                                 <li class="page-item"><a class="page-link" href="javascript: void(0);">3</a></li>
                                 <li class="page-item"><a class="page-link" href="javascript: void(0);">4</a></li>
                                 <li class="page-item"><a class="page-link" href="javascript: void(0);">5</a></li>
                                 <li class="page-item">
                                     <a class="page-link" href="javascript: void(0);" aria-label="Next">
                                         <i class="mdi mdi-chevron-right"></i>
                                     </a>
                                 </li>
                             </ul>--%>
                        </div>
                    </div>
                </div>
            </div>


        </div> <!-- container-fluid -->
    </div>
    <!-- End Page-content -->

    <!-- Modal Start -->
    <div class="modal fade detailViewModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">상세정보</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p class="text-truncate font-size-16 font-weight-bold"> <span class="text-primary" id="ed-elCode"></span></p>

                    <div class="table-responsive">
                        <table class="table table-centered table-nowrap">
                            <%--                            <thead>--%>
                            <%--                            <tr>--%>
                            <%--                                <th scope="col">Productaaaaaaaaaa</th>--%>
                            <%--                                <th scope="col">Product Name</th>--%>

                            <%--                            </tr>--%>
                            <%--                            </thead>--%>
                            <tbody>
                            <tr>
                                <th scope="row">
                                    <div>
                                        차량정보
                                    </div>
                                </th>
                                <td>
                                    <div id="ed-productName">

                                    </div>
                                </td>
                            </tr>

                            <tr>
                                <th scope="row">
                                    <div>
                                        날짜정보
                                    </div>
                                </th>
                                <td>
                                    <div id="ed-date">
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">
                                    <div>
                                        구매자정보
                                    </div>
                                </th>
                                <td>
                                    <div id="ed-esr">
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">
                                    <div>
                                        판매자정보
                                    </div>
                                </th>
                                <td>
                                    <div id="ed-ess">
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">
                                    <div>
                                        거래정보
                                    </div>
                                </th>
                                <td>
                                    <div id="ed-payment">
                                        <h5 class="text-truncate font-size-14" id="ed-allAmount"></h5>
                                        <p class="text-muted mb-0" id="ed-amount"></p>
                                    </div>
                                </td>
                            </tr>

                            <tr>
                                <th scope="row">
                                    <div>
                                        주소
                                    </div>
                                </th>

                                <td >
                                    <div  id="ed-address">

                                    </div>
                                </td>
                            </tr>
                            <tr >
                                <th scope="row">
                                    <div>
                                        송금정보
                                    </div>
                                </th>
                                <td>
                                    <div>
                                        <h5 class="text-truncate font-size-14" id="ed-remittance">2,127,659</h5>
                                    </div>
                                </td>
                            </tr>

                            <%----%>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <%--    Modal End--%>


<%@include file="/WEB-INF/jsp/admin/common/footer.jsp" %>