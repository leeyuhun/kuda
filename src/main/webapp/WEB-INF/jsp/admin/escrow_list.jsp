<%--
  Created by IntelliJ IDEA.
  User: Lee
  Date: 2021-05-31
  Time: 오후 4:28
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>


<%@include file="/WEB-INF/jsp/admin/common/header.jsp" %>

<%@include file="/WEB-INF/jsp/admin/common/javascript_import.jsp" %>
<script async src="${pageContext.request.contextPath}/static/admin_assets/js/escrow_list.js"></script>

<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="main-content">

    <div class="page-content">
        <div class="container-fluid">

            <!-- page title -->
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box d-flex align-items-center justify-content-between">
                        <h4 class="mb-0 font-size-24">거래진행내역</h4>

                        <div class="page-title-right">
                            <ol class="breadcrumb m-0">
                                <li class="breadcrumb-item"><a href="javascript: void(0);">애스크로</a></li>
                                <li class="breadcrumb-item active">거래진행내역</li>
                            </ol>
                        </div>

                    </div>
                </div>
            </div>
            <!-- page title -->


            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <form class="needs-validation" id="escrow-list-search-form" method="get" action="${pageContext.request.contextPath}/adm/escrowList" novalidate>
                                <div class="row mb-2">

                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <input type="text" class="form-control" placeholder="yyyy-mm-dd" name="startDate" id="startDate"
                                                   data-date-format="yyyy-MM-dd" data-provide="datepicker"
                                                   data-date-autoclose="true">
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <input type="text" class="form-control" placeholder="yyyy-mm-dd" name="lastDate" id="lastDate"
                                                   data-date-format="yyyy-MM-dd" data-provide="datepicker"
                                                   data-date-autoclose="true">
                                        </div>
                                    </div>
                                    <div class="col-md-1">
                                        <div class="form-group">
                                            <select  class="form-control" id="state" name="state">
                                                <option value="" selected >거래상태</option>
                                                <option value="0">요청자입금대기</option>
                                                <option value="1">요청자입금완료</option>
                                                <option value="2">차량배송중</option>
                                                <option value="3">차량수령완료</option>
                                                <option value="4">정산대기</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-1">
                                        <div class="form-group">
                                            <select  class="form-control" id="searchWhere" name="searchWhere">
                                                <option value="reqName">요청자명</option>
                                                <option value="selName">판매자명</option>
                                                <option value="carInfo">차량정보</option>
                                                <option value="allAmount">입금금액</option>
                                                <option value="payAmount">지급예정금액</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="searchText" name="searchText" placeholder="search" value="">
                                        </div>
                                    </div>
                                    <div class="">
                                        <div class="form-group">
                                            <button class="form-control btn btn-primary" type="submit" id="btn-search">검색</button>
                                        </div>
                                    </div>

<%--                                    <div class="col-sm-8">--%>
<%--                                        <div class="text-sm-right">--%>

<%--                                        </div>--%>
<%--                                    </div><!-- end col-->--%>
                                </div>
                            </form>




                            <br><br><br><br>

                            <div class="table-responsive">
                                <%--<table class="table table-bordered table-nowrap">--%>
                                <table id="datatable" class="table table-bordered dt-responsive nowrap table-nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead class="thead-light">
                                    <tr style="text-align: center" >
                                        <%--<th style="width: 20px;">
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="customCheck1">
                                                <label class="custom-control-label" for="customCheck1">&nbsp;</label>
                                            </div>
                                        </th>--%>
                                        <th>요청일시</th>
                                        <th>차량정보</th>
                                        <th>요청자</th>
                                        <th>판매자</th>
                                        <th>거래상태</th>
                                        <th>거래금액</th>
                                        <th>배송기사</th>
                                        <th>배송금액</th>
                                        <th>배송상태</th>

                                        <th>상세정보</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        <c:forEach items="${escrowList}" var="list">
                                            <tr id="${list.code}">
                                                    <%--<td>
                                                        <div class="custom-control custom-checkbox">
                                                            <input type="checkbox" class="custom-control-input" id="customCheck2">
                                                            <label class="custom-control-label" for="customCheck2">&nbsp;</label>
                                                        </div>
                                                    </td>--%>
                                                <td class="align-center">
                                                    ${list.requestDate} ${list.requestTime}

                                                </td>
                                                <td>
                                                        ${list.productName}
                                                </td>
                                                <td style="text-align: center">
                                                    ${list.srSellerName} (${list.srSellerPh})
                                                </td>
                                                <td style="text-align: center">
                                                        ${list.ssSellerName} (${list.ssSellerPh})
                                                </td>
                                                <td style="text-align: center">
                                                    <c:choose>
                                                        <c:when test="${list.state eq '0'}">
                                                            <span class="badge badge-pill badge-soft-warning font-size-14">입금대기</span>
                                                        </c:when>
                                                        <c:when test="${list.state eq '1'}">
                                                            <span class="badge badge-pill badge-soft-success font-size-14">입금완료</span>
                                                        </c:when>
                                                        <c:when test="${list.state eq '2'}">
                                                            <span class="badge badge-pill badge-soft-success font-size-14">탁송미동의</span>
                                                        </c:when>
                                                        <c:when test="${list.state eq '3'}">
                                                            <span class="badge badge-pill badge-soft-success font-size-14">기사배정중</span>
                                                        </c:when>
                                                        <c:when test="${list.state eq '4'}">
                                                            <span class="badge badge-pill badge-soft-success font-size-14">차량배송중</span>
                                                        </c:when>
                                                        <c:when test="${list.state eq '5'}">
                                                            <span class="badge badge-pill badge-soft-success font-size-14">차량배송완료</span>
                                                        </c:when>
                                                        <c:when test="${list.state eq '6'}">
                                                            <span class="badge badge-pill badge-soft-success font-size-14">차량확인완료</span>
                                                        </c:when>
                                                        <c:when test="${list.state eq '7'}">
                                                            <span class="badge badge-pill badge-soft-success font-size-14">송금대기중</span>
                                                        </c:when>
                                                        <c:when test="${list.state eq 'C'}">
                                                            <span class="badge badge-pill badge-soft-danger font-size-14">거래취소</span>
                                                        </c:when>


                                                    </c:choose>


                                                </td>
                                                <td style="text-align: right">
                                                    <fmt:formatNumber value="${list.eslAmount}" pattern="#,###" />
                                                </td>
                                                <td style="text-align: center">
                                                    <c:choose>
                                                        <c:when test="${list.driverInfo eq '()'}">

                                                        </c:when>
                                                        <c:otherwise>
                                                            ${list.driverInfo}
                                                        </c:otherwise>
                                                    </c:choose>


                                                </td>
                                                <td style="text-align: right">
                                                    <fmt:formatNumber value="${list.csmAmount}" pattern="#,###" />
                                                </td>
                                                <td style="text-align: center">
                                                    <c:choose>
                                                        <c:when test="${list.csmState eq 'W'}">
                                                            <span class="badge badge-pill badge-soft-warning font-size-14">미신청</span>
                                                        </c:when>
                                                        <c:when test="${list.csmState eq 'A'}">
                                                            <span class="badge badge-pill badge-soft-success font-size-14">배정</span>
                                                        </c:when>
                                                        <c:when test="${list.csmState eq 'J'}">
                                                            <span class="badge badge-pill badge-soft-success font-size-14">출발지도착</span>
                                                        </c:when>
                                                        <c:when test="${list.csmState eq 'S'}">
                                                            <span class="badge badge-pill badge-soft-success font-size-14">출발</span>
                                                        </c:when>
                                                        <c:when test="${list.csmState eq 'E'}">
                                                            <span class="badge badge-pill badge-soft-success font-size-14">도착</span>
                                                        </c:when>
                                                        <c:when test="${list.csmState eq 'D'}">
                                                            <span class="badge badge-pill badge-soft-success font-size-14">완료</span>
                                                        </c:when>
                                                        <c:when test="${list.csmState eq 'C'}">
                                                            <span class="badge badge-pill badge-soft-danger font-size-14">취소</span>
                                                        </c:when>
                                                        <c:when test="${list.csmState eq 'R'}">
                                                            <span class="badge badge-pill badge-soft-warning font-size-14">요청</span>
                                                        </c:when>
                                                    </c:choose>

                                                </td>

                                                <td style="text-align: left">
                                                    <button type="button" class="btn-detail-view btn btn-primary btn-sm btn-rounded" data-toggle="modal" data-target=".detailViewModal" onclick="escrowDetail('${list.code}');">
                                                        상세보기
                                                    </button>

                                                    <c:if test="${list.state eq '0'}">
                                                        <button type="button" class="btn-detail-view btn btn-dark btn-sm btn-rounded" onclick="deposit('${list.code}');">
                                                            입금확인
                                                        </button>
                                                    </c:if>
<%--                                                    <c:if test="${list.state eq '4'}">--%>
<%--                                                        <button type="button" class="btn-detail-view btn btn-success btn-sm btn-rounded" onclick="escrowEnd('${list.code}');">--%>
<%--                                                            거래종료--%>
<%--                                                        </button>--%>
<%--                                                    </c:if>--%>

                                                </td>

                                            </tr>

                                        </c:forEach>
                                    </tbody>

                                    <%--                                    <tfoot style="background-color:#f8f9fa; font-weight: 600; text-align: right">--%>
                                    <%--                                        <tr>--%>
                                    <%--                                            <td colspan="8">--%>
                                    <%--                                                합계--%>
                                    <%--                                            </td>--%>
                                    <%--                                            <td>--%>
                                    <%--                                                2,173,913--%>
                                    <%--                                            </td>--%>
                                    <%--                                            <td>--%>
                                    <%--                                                2,000,000--%>
                                    <%--                                            </td>--%>
                                    <%--                                            <td>--%>
                                    <%--                                                60,000--%>
                                    <%--                                            </td>--%>

                                    <%--                                        </tr>--%>

                                    <%--                                    </tfoot>--%>
                                </table>
                            </div>
                            <%-- <ul class="pagination pagination-rounded justify-content-end mb-2">
                                 <li class="page-item disabled">
                                     <a class="page-link" href="javascript: void(0);" aria-label="Previous">
                                         <i class="mdi mdi-chevron-left"></i>
                                     </a>
                                 </li>
                                 <li class="page-item active"><a class="page-link" href="javascript: void(0);">1</a></li>
                                 <li class="page-item"><a class="page-link" href="javascript: void(0);">2</a></li>
                                 <li class="page-item"><a class="page-link" href="javascript: void(0);">3</a></li>
                                 <li class="page-item"><a class="page-link" href="javascript: void(0);">4</a></li>
                                 <li class="page-item"><a class="page-link" href="javascript: void(0);">5</a></li>
                                 <li class="page-item">
                                     <a class="page-link" href="javascript: void(0);" aria-label="Next">
                                         <i class="mdi mdi-chevron-right"></i>
                                     </a>
                                 </li>
                             </ul>--%>
                        </div>
                    </div>
                </div>
            </div>


        </div> <!-- container-fluid -->
    </div>
    <!-- End Page-content -->

    <!-- Modal Start -->
    <div class="modal fade detailViewModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">상세정보</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p class="text-truncate font-size-16 font-weight-bold"> <span class="text-primary" id="ed-elCode"></span></p>

                    <div class="table-responsive">
                        <table class="table table-centered table-nowrap">
                            <%--                            <thead>--%>
                            <%--                            <tr>--%>
                            <%--                                <th scope="col">Productaaaaaaaaaa</th>--%>
                            <%--                                <th scope="col">Product Name</th>--%>

                            <%--                            </tr>--%>
                            <%--                            </thead>--%>
                            <tbody>
                            <tr>
                                <th scope="row">
                                    <div>
                                        차량정보
                                    </div>
                                </th>
                                <td>
                                    <div id="ed-productName">

                                    </div>
                                </td>
                            </tr>

                            <tr>
                                <th scope="row">
                                    <div>
                                        날짜정보
                                    </div>
                                </th>
                                <td>
                                    <div id="ed-date">
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">
                                    <div>
                                        구매자정보
                                    </div>
                                </th>
                                <td>
                                    <div id="ed-esr">
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">
                                    <div>
                                        판매자정보
                                    </div>
                                </th>
                                <td>
                                    <div id="ed-ess">
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">
                                    <div>
                                        거래정보
                                    </div>
                                </th>
                                <td>
                                    <div id="ed-payment">
                                        <h5 class="text-truncate font-size-14" id="ed-allAmount"></h5>
                                    </div>
                                </td>
                            </tr>

                            <tr>
                                <th scope="row">
                                    <div>
                                        출발지
                                    </div>
                                </th>

                                <td >
                                    <div  id="ed-startAddress">

                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">
                                    <div>
                                        도착지
                                    </div>
                                </th>

                                <td >
                                    <div  id="ed-endAddress">

                                    </div>
                                </td>
                            </tr>
                            <tr >
                                <th scope="row">
                                    <div>
                                        송금정보
                                    </div>
                                </th>
                                <td>
                                    <div>
                                        <h5 class="text-truncate font-size-14" id="ed-remittance"></h5>
                                    </div>
                                </td>
                            </tr>


                            <%----%>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <%--    Modal End--%>


<%@include file="/WEB-INF/jsp/admin/common/footer.jsp" %>