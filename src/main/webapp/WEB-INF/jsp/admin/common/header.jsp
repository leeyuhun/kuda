<%--
  Writer: Lee
  Date: 2021-01-07 오후 5:47
  Explanaion : 
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!doctype html>
<html>

<head>

    <meta charset="utf-8" />
    <title>차토스 통합 관리</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="Premium Multipurpose Admin & Dashboard Template" name="description" />
    <meta content="Themesbrand" name="author" />
    <!-- App favicon -->
    <link rel="shortcut icon" href="${pageContext.request.contextPath}/static/admin_assets/images/logo-symbol-blue.ico">
    <%--    <link rel="shortcut icon" href="${pageContext.request.contextPath}/static/admin_assets/images/favicon.ico">--%>





    <!-- Bootstrap Css -->
    <link href="${pageContext.request.contextPath}/static/admin_assets/css/bootstrap.min.css" id="bootstrap-style" rel="stylesheet" type="text/css" />
    <!-- Icons Css -->
    <link href="${pageContext.request.contextPath}/static/admin_assets/css/icons.min.css" rel="stylesheet" type="text/css" />
    <!-- App Css-->
    <link href="${pageContext.request.contextPath}/static/admin_assets/css/app.min.css" id="app-style" rel="stylesheet" type="text/css" />

    <!-- DataTables -->
    <link href="${pageContext.request.contextPath}/static/admin_assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css" />
    <link href="${pageContext.request.contextPath}/static/admin_assets/libs/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />

    <!-- Responsive datatable examples -->
    <link href="${pageContext.request.contextPath}/static/admin_assets/libs/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />


    <!-- form -->
    <link href="${pageContext.request.contextPath}/static/admin_assets/libs/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
    <link href="${pageContext.request.contextPath}/static/admin_assets/libs/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css">
    <link href="${pageContext.request.contextPath}/static/admin_assets/libs/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css" rel="stylesheet" type="text/css">
    <link href="${pageContext.request.contextPath}/static/admin_assets/libs/bootstrap-timepicker/css/bootstrap-timepicker.min.css" rel="stylesheet" type="text/css">
    <link href="${pageContext.request.contextPath}/static/admin_assets/libs/bootstrap-touchspin/jquery.bootstrap-touchspin.min.css" rel="stylesheet" type="text/css" />
    <link href="${pageContext.request.contextPath}/static/admin_assets/libs/@chenfengyuan/datepicker/datepicker.min.css" rel="stylesheet" >

    <style>

        .right-content{
            text-align: right;
        }

        .left-content{
            text-align: left;
        }
        .center-content{
            text-align: center;
        }

    </style>
</head>


<body data-sidebar="dark">

<!-- <body data-layout="horizontal" data-topbar="dark"> -->

<!-- Begin page -->
<div id="layout-wrapper">


    <header id="page-topbar">
        <div class="navbar-header">
            <div class="d-flex">
                <!-- LOGO -->
                <div class="navbar-brand-box">
                    <a href="/" class="logo logo-dark">
                                <span class="logo-sm"> <%--symbol--%>
                                    <img src="${pageContext.request.contextPath}/static/admin_assets/images/logo-symbol.png" alt="" height="22">
                                </span>
                        <span class="logo-lg"><%--큰거--%>
                                    <img src="${pageContext.request.contextPath}/static/admin_assets/images/logo-symboltext.png" alt="" height="17">
                                </span>
                    </a>

                    <a href="/" class="logo logo-light">
                                <span class="logo-sm">
                                    <img src="${pageContext.request.contextPath}/static/admin_assets/images/logo-symbol.png" alt="" height="22">
                                </span>
                        <span class="logo-lg">
                                    <img src="${pageContext.request.contextPath}/static/admin_assets/images/logo-symboltext.png" alt="" height="19">
                                </span>
                    </a>
                </div>


                <button type="button" class="btn btn-sm px-3 font-size-16 header-item waves-effect" id="vertical-menu-btn">
                    <i class="fa fa-fw fa-bars"></i>
                </button>




            </div>

            <div class="d-flex">

                <div class="dropdown d-inline-block d-lg-none ml-2">
                    <button type="button" class="btn header-item noti-icon waves-effect" id="page-header-search-dropdown"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="mdi mdi-magnify"></i>
                    </button>
                    <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right p-0"
                         aria-labelledby="page-header-search-dropdown">

                        <form class="p-3">
                            <div class="form-group m-0">
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="Search ..." aria-label="Recipient's username">
                                    <div class="input-group-append">
                                        <button class="btn btn-primary" type="submit"><i class="mdi mdi-magnify"></i></button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

                <div class="dropdown d-inline-block">
                    <button type="button" class="btn header-item waves-effect" id="page-header-user-dropdown"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <span class="d-none d-xl-inline-block ml-1" key="login_id">${sessionScope.user.name}</span>
                        <i class="mdi mdi-chevron-down d-none d-xl-inline-block"></i>
                    </button>
                    <div class="dropdown-menu dropdown-menu-right">

                        <!-- item-->
                        <%--<a class="dropdown-item" href="#"><i class="bx bx-user font-size-16 align-middle mr-1"></i> <span key="t-profile">Profile</span></a>
                        <a class="dropdown-item" href="#"><i class="bx bx-wallet font-size-16 align-middle mr-1"></i> <span key="t-my-wallet">My Wallet</span></a>
                        <a class="dropdown-item d-block" href="#"><span class="badge badge-success float-right">11</span><i class="bx bx-wrench font-size-16 align-middle mr-1"></i> <span key="t-settings">Settings</span></a>
                        <a class="dropdown-item" href="#"><i class="bx bx-lock-open font-size-16 align-middle mr-1"></i> <span key="t-lock-screen">Lock screen</span></a>
                        <div class="dropdown-divider"></div>--%>

                        <a class="dropdown-item text-danger" href="/logOut"><i class="bx bx-power-off font-size-16 align-middle mr-1 text-danger"></i> <span key="t-logout">Logout</span></a>
                    </div>
                </div>



            </div>
        </div>
    </header>

    <!-- ========== Left Sidebar Start ========== -->
    <div class="vertical-menu">

        <div data-simplebar class="h-100">

            <!--- Sidemenu -->
            <div id="sidebar-menu">
                <!-- Left Menu Start -->
                <ul class="metismenu list-unstyled" id="side-menu">
                    <%--<li class="menu-title" key="t-menu">Menu</li>--%>
<%--                    <li class="menu-title" key="t-apps">결제관리</li>--%>
<%--                    <li>--%>
<%--                        <a href="${pageContext.request.contextPath}/static/admin_assets/paymentList_a" class="waves-effect">--%>
<%--                            <i class="bx bx-dollar-circle"></i>--%>
<%--                            <span key="bx bx-won">결제완료내역</span>--%>
<%--                        </a>--%>
<%--                    </li>--%>

<%--                    <li>--%>
<%--                        <a href="${pageContext.request.contextPath}/static/admin_assets/oPaymentList_a" class="waves-effect">--%>
<%--                            <i class="bx bx-x-circle"></i>--%>
<%--                            <span key="t-calendar">결제실패내역</span>--%>
<%--                        </a>--%>
<%--                    </li>--%>

<%--                    <li class="menu-title" key="t-apps">입출금관리</li>--%>

<%--                    <li>--%>
<%--                        <a href="javascript: void(0);" class="has-arrow waves-effect">--%>
<%--                            <i class="bx bx-log-in-circle"></i>--%>
<%--                            <span key="t-ecommerce">송금실행</span>--%>
<%--                        </a>--%>
<%--                        <ul class="sub-menu" aria-expanded="false">--%>
<%--                            <li><a href="ecommerce-products.html" key="t-products">차량예수금</a></li>--%>
<%--                            <li><a href="ecommerce-product-detail.html" key="t-product-detail">판매자 리베이트</a></li>--%>
<%--                        </ul>--%>
<%--                    </li>--%>


<%--                    <li>--%>
<%--                        <a href="javascript: void(0);" class="has-arrow waves-effect">--%>
<%--                            <i class="bx bxs-spreadsheet"></i>--%>
<%--                            <span key="t-ecommerce">송금내역</span>--%>
<%--                        </a>--%>
<%--                        <ul class="sub-menu" aria-expanded="false">--%>
<%--                            <li><a href="ecommerce-products.html" key="t-products">차량예수금</a></li>--%>
<%--                            <li><a href="ecommerce-product-detail.html" key="t-product-detail">판매자 리베이트</a></li>--%>
<%--                        </ul>--%>
<%--                    </li>--%>

<%--                    <li>--%>
<%--                        <a href="calendar.html" class="waves-effect">--%>
<%--                            <i class="bx bxs-error-circle"></i>--%>
<%--                            <span key="t-calendar">취소요청내역</span>--%>
<%--                        </a>--%>
<%--                    </li>--%>

<%--                    <li class="menu-title" key="t-apps">운영관리</li>--%>

<%--                    <li>--%>
<%--                        <a href="calendar.html" class="waves-effect">--%>
<%--                            <i class="bx bx-angry"></i>--%>
<%--                            <span key="t-calendar">민원관리</span>--%>
<%--                        </a>--%>
<%--                    </li>--%>


<%--                    <li class="menu-title" key="t-apps">통계및이력</li>--%>
<%--                    <li>--%>
<%--                        <a href="calendar.html" class="waves-effect">--%>
<%--                            <i class="bx bxs-message-square-dots"></i>--%>
<%--                            <span key="t-calendar">결제문자발신내역</span>--%>
<%--                        </a>--%>
<%--                    </li>--%>
<%--                    <li>--%>
<%--                        <a href="calendar.html" class="waves-effect">--%>
<%--                            <i class="bx bxs-store"></i>--%>
<%--                            <span key="t-calendar">매출통계</span>--%>
<%--                        </a>--%>
<%--                    </li>--%>

                        <li class="menu-title" key="t-apps">사용자버전</li>
                        <li>
                            <a href="${pageContext.request.contextPath}/user/purchaseSale" class="waves-effect">
                                <i class="bx bx-list-ol"></i>
                                <span key="">사용자페이지</span>
                            </a>
                        </li>
                        <li class="menu-title" key="t-apps">애스크로</li>
                        <li>
                            <a href="${pageContext.request.contextPath}/adm/escrowList" class="waves-effect">
                                <i class="bx bx-list-ol"></i>
                                <span key="">거래진행내역</span>
                            </a>
                        </li>
                        <li>
                            <a href="${pageContext.request.contextPath}/adm/escrowHistory" class="waves-effect">
                                <i class="bx bx-list-check"></i>
                                <span key="">거래완료내역</span>
                            </a>
                        </li>
                        <li>
                            <a href="${pageContext.request.contextPath}/adm/eRemitList" class="waves-effect">
                                <i class="bx bx-dollar-circle"></i>
                                <span key="bx bx-user">입금하기(정산실행)</span>
                            </a>
                        </li>

                        <li class="menu-title" key="t-apps">회원관리</li>
                        <li>
                            <a href="${pageContext.request.contextPath}/adm/userList" class="waves-effect">
                                <i class="bx bx-user"></i>
                                <span key="bx bx-user">가입자내역</span>
                            </a>
                        </li>

                </ul>
            </div>
            <!-- Sidebar -->
        </div>
    </div>
    <!-- Left Sidebar End -->
