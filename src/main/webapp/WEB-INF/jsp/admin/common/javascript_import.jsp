<%--
  Writer: Lee
  Date: 2021-01-22 오후 12:21
  Explanation : JavaScript 모아두는..
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<!-- JAVASCRIPT -->
<script src="${pageContext.request.contextPath}/static/admin_assets/libs/jquery/jquery.min.js"></script>
<script src="${pageContext.request.contextPath}/static/admin_assets/libs/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="${pageContext.request.contextPath}/static/admin_assets/libs/metismenu/metisMenu.min.js"></script>
<script src="${pageContext.request.contextPath}/static/admin_assets/libs/simplebar/simplebar.min.js"></script>
<script src="${pageContext.request.contextPath}/static/admin_assets/libs/node-waves/waves.min.js"></script>


<!-- Required datatable js -->
<script src="${pageContext.request.contextPath}/static/admin_assets/libs/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="${pageContext.request.contextPath}/static/admin_assets/libs/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>

<!-- Buttons examples -->
<script src="${pageContext.request.contextPath}/static/admin_assets/libs/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
<script src="${pageContext.request.contextPath}/static/admin_assets/libs/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js"></script>
<script src="${pageContext.request.contextPath}/static/admin_assets/libs/jszip/jszip.min.js"></script>
<script src="${pageContext.request.contextPath}/static/admin_assets/libs/pdfmake/build/pdfmake.min.js"></script>
<script src="${pageContext.request.contextPath}/static/admin_assets/libs/pdfmake/build/vfs_fonts.js"></script>
<script src="${pageContext.request.contextPath}/static/admin_assets/libs/datatables.net-buttons/js/buttons.html5.min.js"></script>
<script src="${pageContext.request.contextPath}/static/admin_assets/libs/datatables.net-buttons/js/buttons.print.min.js"></script>
<script src="${pageContext.request.contextPath}/static/admin_assets/libs/datatables.net-buttons/js/buttons.colVis.min.js"></script>


<script src="${pageContext.request.contextPath}/static/admin_assets/libs/select2/js/select2.min.js"></script>
<script src="${pageContext.request.contextPath}/static/admin_assets/libs/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<script src="${pageContext.request.contextPath}/static/admin_assets/libs/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>
<script src="${pageContext.request.contextPath}/static/admin_assets/libs/bootstrap-timepicker/js/bootstrap-timepicker.min.js"></script>
<script src="${pageContext.request.contextPath}/static/admin_assets/libs/bootstrap-touchspin/jquery.bootstrap-touchspin.min.js"></script>
<script src="${pageContext.request.contextPath}/static/admin_assets/libs/bootstrap-maxlength/bootstrap-maxlength.min.js"></script>
<script src="${pageContext.request.contextPath}/static/admin_assets/libs/@chenfengyuan/datepicker/datepicker.min.js"></script>

<!-- form advanced init -->
<script src="${pageContext.request.contextPath}/static/admin_assets/js/pages/form-advanced.init.js"></script>



<!-- Responsive examples -->
<script src="${pageContext.request.contextPath}/static/admin_assets/libs/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
<script src="${pageContext.request.contextPath}/static/admin_assets/libs/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js"></script>

<!-- Datatable init js -->
<script src="${pageContext.request.contextPath}/static/admin_assets/js/pages/datatables.init.js"></script>

<script src="${pageContext.request.contextPath}/static/admin_assets/js/app.js"></script>



<script>
    $(document).ready(function(){
        /*
        * 숫자만
        * */
        $(".onlyNumber").keyup(function(event){
            if (!(event.keyCode >=37 && event.keyCode<=40)) {
                var inputVal = $(this).val();
                $(this).val(inputVal.replace(/[^0-9]/gi,''));
            }
        });

        /*
        * 숫자만 + comma
        * */
        $('.numberComma').keyup(function(event){

            if (!(event.keyCode >=37 && event.keyCode<=40)) {
                var inputVal = $(this).val();

                inputVal = inputVal.replace(/[^0-9]/g, '');   // 입력값이 숫자가 아니면 공백
                inputVal = inputVal.replace(/,/g, '');          // ,값 공백처리
                $(this).val(inputVal.replace(/\B(?=(\d{3})+(?!\d))/g, ","));
            }

        });


    });



    /*
    * 숫자만 리턴(콤마제외)
    * */
    function commaRemove(val){
        val = val.replaceAll("," ,"");
        return val;
    }



    /*3자리 콤마찍기*/
    function commaReturn(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }





</script>