<%--
  Created by IntelliJ IDEA.
  User: Lee
  Date: 2021-05-31
  Time: 오후 4:28
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>


<%@include file="/WEB-INF/jsp/admin/common/header.jsp" %>

<%@include file="/WEB-INF/jsp/admin/common/javascript_import.jsp" %>

<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="main-content">

    <div class="page-content">
        <div class="container-fluid">

            <!-- page title -->
            <div class="row">
                <div class="col-12">
                    <div class="page-title-box d-flex align-items-center justify-content-between">
                        <h4 class="mb-0 font-size-24">가입자내역</h4>

                        <div class="page-title-right">
                            <ol class="breadcrumb m-0">
                                <li class="breadcrumb-item"><a href="javascript: void(0);">회원관리</a></li>
                                <li class="breadcrumb-item active">가입자내역</li>
                            </ol>
                        </div>

                    </div>
                </div>
            </div>
            <!-- page title -->


            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <%--<div class="row mb-2">
                                <div class="col-md-1">
                                    <div class="form-group">
                                        <select  class="form-control" id="validationCustom01" required>
                                            <option selected="">승인일</option>
                                            <option >지급예정일</option>
                                            <option >실지급일</option>
                                            <option >PG정산일</option>
                                        </select>
                                    </div>
                                </div>


                                <div class="col-md-1">
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="validationCustom02" placeholder="First name" value="날짜1" required>
                                        <div class="valid-feedback">
                                            날짜1
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-1">
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="validationCustom03" placeholder="First name" value="날짜2" required>
                                        <div class="valid-feedback">
                                            날짜2
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="validationCustom04" placeholder="First name" value="승인여부select" required>
                                        <div class="valid-feedback">
                                            승인여부select
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="validationCustom05" placeholder="First name" value="검색select" required>
                                        <div class="valid-feedback">
                                            검색select
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="validationCustom06" placeholder="First name" value="검색input" required>
                                        <div class="valid-feedback">
                                            검색input
                                        </div>
                                    </div>
                                </div>


                                <div class="col-sm-8">
                                    <div class="text-sm-right">

                                    </div>
                                </div><!-- end col-->
                            </div>--%>
                            <br><br><br><br>

                            <div class="table-responsive">
                                <%--<table class="table table-bordered table-nowrap">--%>
                                <table id="datatable" class="table table-bordered dt-responsive nowrap table-nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                    <thead class="thead-light">
                                    <tr style="text-align: center" >
                                        <%--<th style="width: 20px;">
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="customCheck1">
                                                <label class="custom-control-label" for="customCheck1">&nbsp;</label>
                                            </div>
                                        </th>--%>
                                        <th>가입일</th>
                                        <th>밴드키(ID)</th>
                                        <th>상사명</th>
                                        <th>이름</th>
                                        <th>핸드폰번호</th>
                                        <th>회원상태</th>
                                        <th>기능</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <c:forEach items="${userList}" var="list">
                                        <tr id="${list.code}">
                                                <%--<td>
                                                    <div class="custom-control custom-checkbox">
                                                        <input type="checkbox" class="custom-control-input" id="customCheck2">
                                                        <label class="custom-control-label" for="customCheck2">&nbsp;</label>
                                                    </div>
                                                </td>--%>
                                            <td style="text-align: center">
                                                    ${list.regDate}
                                            </td>

                                            <td style="text-align: center">
                                                    ${list.loginId}
                                            </td>
                                            <td style="text-align: center">
                                                    ${list.company}
                                            </td>
                                            <td style="text-align: center">
                                                    ${list.name}
                                            </td>
                                            <td style="text-align: center">
                                                    ${list.ph}
                                            </td>

                                            <td style="text-align: center">
                                                <c:choose>
                                                    <c:when test="${list.state eq 'Y'}">
                                                        <span class="badge badge-pill badge-soft-info font-size-14">정상</span>
                                                    </c:when>
                                                    <c:when test="${list.state eq 'N'}">
                                                        <span class="badge badge-pill badge-soft-success font-size-14">정보미기입</span>
                                                    </c:when>
                                                    <c:when test="${list.state eq 'C'}">
                                                        <span class="badge badge-pill badge-soft-warning font-size-14">애스크로미가입</span>
                                                    </c:when>

                                                </c:choose>
                                            </td>


                                            <td style="text-align: center">
<%--                                                <button type="button" class="btn-detail-view btn btn-primary btn-sm btn-rounded" data-toggle="modal" data-target=".detailViewModal">--%>
<%--                                                    상세보기--%>
<%--                                                </button>--%>
                                            </td>

                                        </tr>

                                    </c:forEach>
                                    </tbody>

                                    <%--                                    <tfoot style="background-color:#f8f9fa; font-weight: 600; text-align: right">--%>
                                    <%--                                        <tr>--%>
                                    <%--                                            <td colspan="8">--%>
                                    <%--                                                합계--%>
                                    <%--                                            </td>--%>
                                    <%--                                            <td>--%>
                                    <%--                                                2,173,913--%>
                                    <%--                                            </td>--%>
                                    <%--                                            <td>--%>
                                    <%--                                                2,000,000--%>
                                    <%--                                            </td>--%>
                                    <%--                                            <td>--%>
                                    <%--                                                60,000--%>
                                    <%--                                            </td>--%>

                                    <%--                                        </tr>--%>

                                    <%--                                    </tfoot>--%>
                                </table>
                            </div>
                            <%-- <ul class="pagination pagination-rounded justify-content-end mb-2">
                                 <li class="page-item disabled">
                                     <a class="page-link" href="javascript: void(0);" aria-label="Previous">
                                         <i class="mdi mdi-chevron-left"></i>
                                     </a>
                                 </li>
                                 <li class="page-item active"><a class="page-link" href="javascript: void(0);">1</a></li>
                                 <li class="page-item"><a class="page-link" href="javascript: void(0);">2</a></li>
                                 <li class="page-item"><a class="page-link" href="javascript: void(0);">3</a></li>
                                 <li class="page-item"><a class="page-link" href="javascript: void(0);">4</a></li>
                                 <li class="page-item"><a class="page-link" href="javascript: void(0);">5</a></li>
                                 <li class="page-item">
                                     <a class="page-link" href="javascript: void(0);" aria-label="Next">
                                         <i class="mdi mdi-chevron-right"></i>
                                     </a>
                                 </li>
                             </ul>--%>
                        </div>
                    </div>
                </div>
            </div>


        </div> <!-- container-fluid -->
    </div>
    <!-- End Page-content -->


<%@include file="/WEB-INF/jsp/admin/common/footer.jsp" %>