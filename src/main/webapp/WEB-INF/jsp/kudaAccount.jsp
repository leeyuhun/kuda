<%--
  Writer: Lee
  Date: 2021-04-09 오후 4:24
  Explanaion : kuda loginPage
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html lang="ko">

<%@include file="/WEB-INF/jsp/user_static/headerImport.jsp" %>
<script>
    if (performance.navigation.type === 1) {
        window.location.href = '/logOut';
    }
</script>

<body id="Main" >
<div class="container">
    <img src="${pageContext.request.contextPath}/static/assets/img/icons/kuda-logo.png" alt="Kuda" class="logo">
    <section class="login-impossible">
        <section class="main-text">
            <p class="fs22 bold">
                현재 회원가입이<br>
                불가능한 상태입니다.
            </p>
            <p class="description">
                안전거래 서비스는<br>
                전국중고차딜러연합 회원만 이용이 가능합니다.<br>
                <b class="bold">회원이 아니신 경우, 아래 버튼을 눌러</b><br>
                <b class="color-blue bold">전국중고차딜러연합 밴드 가입을 진행해 주세요.</b>
            </p>
        </section>
        <section class="btn-wrap">
            <p class="fs14">전국중고차딜러연합 가입을 신청해 주세요.</p>
            <button class="bgColor-blue fs16 color-white mr-t12" onclick="locationH('https://band.us/@usecar');"> <img src="${pageContext.request.contextPath}/static/assets/img/icons/band-logo.png" alt="전중연 가입하기" class="band-logo">전중연 가입하기</button>
        </section>
    </section>
</div>
</body>


</html>












