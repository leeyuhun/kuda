<%--
  Writer: Lee
  Date: 2021-04-09 오후 5:19
  Explanaion : 
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<%@include file="/WEB-INF/jsp/user_static/headerImport.jsp" %>

<script async src="${pageContext.request.contextPath}/static/assets/js/trnscRgstr.js"></script>

<body id="trnscRgstr">
<!-- 공통 -->
<header class="header-nav">
    <img src="${pageContext.request.contextPath}/static/assets/img/icons/close.png" alt="nav" class="icon" onclick="locationH('/user/purchaseSale');">
    <span class="nav-title">거래 등록 완료</span>
</header>
<!-- 공통 -->
<div class="container pd-t32 pd-lr25">
    <p class="fs22 bold lh145">
        <!--                거래금액 입금을 위한<br>-->
        <!--                가상계좌를 발급받아 주세요.-->
        거래진행을 위해<br>
        아래의 계좌로 거래금액을 입금해 주세요.
    </p>
    <!--            <span class="color-pink fs12 block mr-t12">거래금액 입금은 전중연계좌를 통해 진행됩니다.</span>-->
    <b class="block bold color-gray fs13 mr-t30">거래 정보</b>

    <ul class="transaction-list mr-t16">
        <li>
            <span class="block color-gray">판매자명</span>
            <span class="block">${list.essName} (${list.essCompany} / ${list.essPh})</span>
        </li>
        <li>
            <span class="block color-gray">차량정보</span>
            <span class="block">${list.productNum}&nbsp;${list.productName} </span>
        </li>
<%--        <li>--%>
<%--            <span class="block color-gray">차종</span>--%>
<%--            <span class="block"></span>--%>
<%--        </li>--%>
        <!--                <li>-->
        <!--                    <span class="block color-gray">거래금액</span>-->
        <!--                    <span class="block bold">8,250,000원</span>-->
        <!--                </li>-->
    </ul>
    <br>
    <b class="block bold color-gray fs13 mr-t24">계좌 정보</b>
    <ul class="transaction-list mr-t16">
        <li>
            <span class="block color-gray">은행</span>
            <span class="block">신한은행 100-035-645669</span>
        </li>
        <li>
            <span class="block color-gray">예금주</span>
            <span class="block">(주)차토스</span>
        </li>
        <li>
            <span class="block color-gray">거래금액</span>
            <span class="block bold"><fmt:formatNumber value="${list.price }" pattern="#,###" />원</span>
        </li>
    </ul>
            <br>
            <b class="block bold color-gray fs13 mr-t24">탁송 정보</b>
            <ul class="address-list mr-t16">
                <li>
                    <span class="block color-gray">출발주소</span>
                    <span class="block">${list.startAddress}&nbsp;<c:if test="${list.startAddressDetail != ''}"> (${list.startAddressDetail}) </c:if></span>
                </li>
                <li>
                    <span class="block color-gray">도착주소</span>
                    <span class="block">${list.endAddress}&nbsp;<c:if test="${list.endAddressDetail != ''}"> (${list.endAddressDetail}) </c:if></span>
                </li>
                <li>
                    <span class="block color-gray">배송기사</span>
                    <span class="block">
                        <c:choose>
                            <c:when test="${list.deliName ne ''}">
                                <span class="block">${list.deliName}&nbsp;${list.deliPh}</span>
                            </c:when>
                            <c:otherwise>
                                <span class="block">미정</span>
                            </c:otherwise>
                        </c:choose>
                    </span>
                </li>
                <li>
                    <span class="block color-gray">배송금액</span>
                    <span class="block bold"><fmt:formatNumber value="${list.deliveryCost }"/>원</span>
                </li>
            </ul>

    <a href="${pageContext.request.contextPath}/user/deliveryCancel"></a>
    <br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
<%--    <section class="btn-wrap btn-two flex">--%>
    <section class="btn-wrap pd0">
    <%--<c:choose>
        <c:when test="${list.deliveryIsuse eq 'N'}">
            <button class="btn-white" onclick="" type="button">거래 취소</button>
            <button class="bgColor-blue fs16 color-white" onclick="locationH('/user/consignment?orderNo=${param.orderNo}')" >위탁배송 신청하기</button>
        </c:when>


        <c:when test="${list.deliveryIsuse eq 'Y'}">
            <button class="btn-white" onclick="eslCancel();" type="button" >거래 취소</button>
            <button class="bgColor-blue fs16 color-white" onclick="csmCancel('${param.orderNo}');" >위탁배송 취소하기</button>
        </c:when>

    </c:choose>--%>

        <c:choose>
            <c:when test="${list.eslStatus eq '0'}">
                <button class="bgColor-blue fs16 color-white" onclick="csmCancel('${param.orderNo}');" >안전거래 취소하기</button>
            </c:when>
            <c:otherwise>
                <button class="bgColor-blue fs16 color-white" onclick="locationH('/user/csmUpdate');" >배송금액수정</button>
            </c:otherwise>

        </c:choose>

    </section>

</div>
</body>
</html>
