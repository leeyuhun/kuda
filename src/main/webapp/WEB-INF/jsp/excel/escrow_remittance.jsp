<%--
  Created by IntelliJ IDEA.
  User: Lee
  Date: 2021-07-15
  Time: 오후 6:15
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<%

    String fileName="에이전트수수료지급요청서_은행양식_" ;


    //필수 선언 부분
    //.getBytes("KSC5601"),"8859_1") 을 통한 한글파일명 깨짐 방지
    response.setHeader("Content-Disposition", "attachment; filename="+new String((fileName).getBytes("KSC5601"),"8859_1")+".xls");
    response.setHeader("Content-Description", "JSP Generated Date");



%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Insert title here</title>
</head>
<body>
<%try{ %>
<table  border="1" bordercolor="#A2AFCC" bordercolorlight="#ffffff" bordercolordark="#6C717D" cellspacing="0" cellpadding="0">

    <thead>
    <tr align="center" >
        <th scope="col" style="font-weight: bold;">입금은행</th>
        <th scope="col" style="font-weight: bold;">입금계좌번호</th>
        <th scope="col" style="font-weight: bold;">입금액</th>
        <th scope="col" style="font-weight: bold;">출금통장표시</th>
        <th scope="col" style="font-weight: bold;">메모</th>
        <th scope="col" style="font-weight: bold;">CMS코드</th>
        <th scope="col" style="font-weight: bold;">받는분 휴대폰번호</th>
    </tr>
    </thead>
    <tbody>

    <tr >
        <td style="text-align:left; mso-number-format:'\@'">1111</td>
        <td style="text-align:left; mso-number-format: '\@';"><2222</td>
        <td style="text-align:right;">444444</td>
        <td style="text-align:left;">55555</td>
        <td style="text-align:left;">주식회사 디알페이</td>
        <td style="text-align:left;">55555</td>
        <td style="text-align:left;">555555</td>
    </tr>

    </tbody>
</table>
<%}catch(Exception e){
    e.printStackTrace();
}finally{
    session.removeAttribute("agentfeeListBank");
}
%>
</body>
</html>


