<%--
  Writer: Lee
  Date: 2021-04-09 오후 4:24
  Explanaion : kuda loginPage
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html lang="ko">

<%@include file="/WEB-INF/jsp/user_static/headerImport.jsp" %>


<body id="Main" >
<div class="container">
    <img src="${pageContext.request.contextPath}/static/assets/img/icons/kuda-logo.png" alt="Kuda" class="logo">
    <section class="login-impossible">
        <section class="main-text">
            <p class="fs22 bold">
                내부 서버 오류가<br>
                발생 했습니다.
            </p>
            <p class="description">
                해당 페이지가 지속적으로 노출될 경우,<br>
                재접속 또는 고객센터로 연락 바랍니다.<br>

            </p>
        </section>
        <section class="btn-wrap">
            <p class="fs14">아래의 버튼을 클릭해 홈화면 으로 이동해주세요.</p>
            <button class="bgColor-blue fs16 color-white mr-t12" onclick="locationH('/user/purchaseSale');">홈으로</button>
        </section>
    </section>
</div>
</body>


</html>












