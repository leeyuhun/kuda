<%--
  Writer: Lee
  Date: 2021-04-09 오후 4:24
  Explanaion : kuda loginPage
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html lang="ko">

<%@include file="/WEB-INF/jsp/user_static/headerImport.jsp" %>


<body id="Main" >
<div class="container">
    <img src="${pageContext.request.contextPath}/static/assets/img/icons/kuda-logo.png" alt="Kuda" class="logo">
    <section class="login-impossible">
        <section class="main-text">
            <p class="fs22 bold">
                요청하신 페이지를<br>
                찾을수 없습니다.
            </p>
            <p class="description">
                페이지의 주소가 잘못 입력되었거나,<br>
                페이지의 주소가 변경 혹은 삭제되어 페이지를 찾을 수 없습니다.<br>

            </p>
        </section>
        <section class="btn-wrap">
            <p class="fs14">아래의 버튼을 클릭해 홈화면 으로 이동해주세요.</p>
            <button class="bgColor-blue fs16 color-white mr-t12" onclick="locationH('/user/purchaseSale');">홈으로</button>
        </section>
    </section>
</div>
</body>


</html>












