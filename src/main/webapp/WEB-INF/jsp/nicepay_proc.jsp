<%--
  Created by IntelliJ IDEA.
  User: dldbg
  Date: 2021-04-21
  Time: 오후 10:21
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>kuda</title>
    <script>



    </script>
</head>
<body onload="">
    결제모듈 로드중..


    <form name="payForm" method="post" action="nicePayForm" accept-charset="euc-kr" id="nicePayHiddenForm" class="nicePayHiddenForm">

        <input type="hidden" name="PayMethod"       id="nice_PayMethod"     value="">
        <input type="hidden" name="GoodsName"       id="nice_GoodsName"     value="${list.yr_product_name}">       <!--결제상품명-->
        <input type="hidden" name="Amt"             id="nice_Amt"           value="${list.yp_amount}">                 <!--결제상품금액-->
        <input type="hidden" name="MID"             id="nice_MID"           value="${list.ps_code}">            <!--상점아이디-->
        <input type="hidden" name="Moid"            id="nice_Moid"          value="${list.yp_code}"> <!--상품주문번호-->
        <input type="hidden" name="BuyerEmail"      id="nice_BuyerEmail"    value="">
        <input type="hidden" name="BuyerName"       id="nice_BuyerName"     value="${list.yr_buyer_name}">       <!--구매자명-->
        <input type="hidden" name="BuyerTel"        id="nice_BuyerTel"      value="${list.yr_buyer_ph}">         <!--구매자연락처-->
        <input type="hidden" name="ReturnURL"       id="nice_ReturnURL"     value="http://localhost:28088/payment?orderNo=${list.yr_code}">
        <input type="hidden" name="VbankExpDate"    id="nice_VbankExpDate"  value="">
        <input type="hidden" name="orderNo"         id="orderNo"  value="${param.orderNo}">
        <input type="hidden" name="payNo"           id="payNo"  value="${param.payNo}">



        <!-- 옵션 -->
        <input type="hidden" name="GoodsCl" value="1"/>						<!-- 상품구분(실물(1),컨텐츠(0)) -->
        <input type="hidden" name="TransType" value="0"/>					<!-- 일반(0)/에스크로(1) -->
        <input type="hidden" name="CharSet" value="utf-8"/>					<!-- 응답 파라미터 인코딩 방식 -->
        <input type="hidden" name="ReqReserved" value=""/>					<!-- 상점 예약필드 -->



        <!-- 변경 불가능 -->
        <input type="hidden" name="EdiDate" value="${list.ediDate}"/>			<!-- 전문 생성일시 -->
        <input type="hidden" name="SignData" value="${list.hashString}"/>	<!-- 해쉬값 -->


        <br>
        <br>

        <button class="btn btn-primary btn-lg btn-block waves-effect waves-light mb-1" onclick="nicepayStart()">결제하기</button>
    </form>
</body>
</html>
