<%--
  Writer: Lee
  Date: 2021-04-09 오후 5:17
  Explanaion :
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>

<%@include file="/WEB-INF/jsp/user_static/headerImport.jsp" %>
<script async src="${pageContext.request.contextPath}/static/assets/js/invite.js"></script>

<body id="invite">
<!-- 공통 -->
<header class="header-nav">
    <img src="${pageContext.request.contextPath}/static/assets/img/icons/back.png" alt="nav" class="icon" onclick="locationH('/user/search');">
    <span class="nav-title">초대하기</span>
</header>
<!-- 공통 -->
<div class="container pd-t30 pd-lr25">
    <p class="fs22 bold lh145">
        판매자 정보를 입력하시면<br>
        초대 문자메시지가 발송됩니다.
    </p>
    <span class="color-pink fs12 block mr-t12">판매자의 휴대전화번호를 정확히 입력해 주세요.</span>
    <form action="${pageContext.request.contextPath}/user/inviteMessage" method="POST" class="form mr-t30" autocomplete="off">
        <ul>
            <li>
                <div class="input-title">이름</div>
                <input type="text" placeholder="한글 성명 입력" onchange="chkKo(this, 'name'); inviteValueChk(); " onkeyup="chkKo(this, 'name'); inviteValueChk();" name="name" value="">
                <div class="feedback-msg name"></div>
            </li>
            <li>
                <div class="input-title">휴대전화번호</div>
                <input type="number" pattern="\d*" placeholder="숫자만 입력" onchange="chkNum(this, 'phone'); inviteValueChk();" onkeyup="chkNum(this, 'phone'); inviteValueChk();" name="phone" class="inputPhone">
                <div class="feedback-msg phone"></div>
            </li>
        </ul>

        <section class="btn-wrap btn-two flex">
            <button class="btn-white" type="button" onclick="locationH('/user/search');">취소</button>
            <button class="btn-blue type disabled invite-btn" type="submit"  disabled>초대하기</button>
        </section>
    </form>
</div>
</body>
</html>
