<%--
  Writer: Lee
  Date: 2021-04-09 오후 5:19
  Explanaion : 
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<%@include file="/WEB-INF/jsp/user_static/headerImport.jsp" %>
<script async src="${pageContext.request.contextPath}/static/assets/js/searchAdd.js"></script>
<%--<script>
    function  addressListBtn(){

        $('input:radio[name="myAddress"]').eq(0) .attr("checked" , "checked");

        addressSetting('<c:out value="${myAddress.address}"/>' , '<c:out value="${myAddress.addressDetail}"/>');

    }

</script>--%>


<body id="searchAdd">
<!-- 공통 -->
<header class="header-nav">
    <img src="${pageContext.request.contextPath}/static/assets/img/icons/back.png" alt="nav" class="icon" onclick="locationH('/user/search');">
    <span class="nav-title">거래정보 등록</span>
</header>
<!-- 공통 -->
<%--<div class="container pd-t32 pd-lr25 pd-b134">--%>
<div class="container pd-t32 pd-lr25">
    <section class="title">
        <b class="bold fs18 block">거래 정보</b>
        <div class="title-info-box">
            <b class="bold block mr-r15">${sellerInfo.seller_name}</b>
            <span class="block fs14">${sellerInfo.seller_company} / ${sellerInfo.seller_ph}</span>
        </div>
    </section>
    <form action="${pageContext.request.contextPath}/user/escrowInsert" method="POST" class="form mr-t20" id="searchAddForm" autocomplete="off">
        <ul>
            <li>
                <div class="input-title">차량번호</div>
                <input type="text" placeholder="차량번호 입력"  onchange="searchAddValueChk();" onkeyup="searchAddValueChk();" name="productNum">
                <div class="feedback-msg"></div>
            </li>

            <li>
                <div class="input-title">차종</div>
                <input type="text" placeholder="차종 입력" onchange="searchAddValueChk();" onkeyup="searchAddValueChk();" name="productName">
                <div class="feedback-msg"></div>
            </li>
<%--            <li>--%>
<%--                <div class="input-title">색상</div>--%>
<%--                <input type="text" placeholder="차량색상 입력" onchange="searchAddValueChk();" onkeyup="searchAddValueChk();" name="productColor">--%>
<%--                <div class="feedback-msg"></div>--%>
<%--            </li>--%>
            <li class="relative">
                <div class="input-title">입금자명(상사 입금시 상사명기입)</div>
                <input type="text" placeholder="입금자명(상사 입금시 상사명기입)"  onchange="searchAddValueChk();" onkeyup="searchAddValueChk();" name="bankHolder" id="bankHolder" value="${myInfo.myHolder}">
                <div class="feedback-msg"></div>
            </li>

            <li class="price relative">
                <div class="input-title">거래금액</div>

                <input class="price-input" type="text" pattern="\d*" placeholder="숫자만 입력"  onchange="chkNum(this, 'allAmountStr'); searchAddValueChk(); " onkeyup="chkNum(this, 'allAmountStr'); searchAddValueChk(); koView(this , 'allAmountStr'); " name="allAmountStr" id="allAmountStr">
                <div class="feedback-msg view-korean-msg allAmountStr "></div>

            </li>
        </ul>
        <b class="bold fs18 block mr-t40">배송 정보</b>
        <ul class="address-select mr-t14">
            <%--거래 내역이 없다면 목록 노출 X--%>

<%--            <c:if test="${addressListCount > '0'}">--%>
<%--                <li class="mr-r10 list " onclick="addressSelect(this); addressListBtn();">최근 주소</li>--%>
<%--            </c:if>--%>
<%--            <li class="active" onclick="addressSelect(this); directInputInit();" >직접 입력</li>--%>
        </ul>
        <ul class="address-add mr-t20">
            <li>
                <div class="input-title">출발지 주소</div>
                <div class="address-input-box relative">
                    <input type="text" placeholder="주소 검색" readonly  id="address1" onchange="searchAddValueChk();" onkeyup="searchAddValueChk();" value="${sellerInfo.seller_address}" name="address1" onclick="execPostCodeAdd1();">
                </div>
                <div class="feedback-msg"></div>
            </li>
            <li>
                <div class="input-title">출발지 상세주소</div>
                <div class="address-input-box relative">
                    <input type="text" placeholder="배송출발지 상세주소(필요시입력)"  id="addressDetail1" value="${sellerInfo.seller_address_detail}" name="addressDetail1" >
                </div>
                <div class="feedback-msg"></div>
            </li>
            <li>
                <div class="input-title">도착지 주소</div>
                <div class="address-input-box relative">
                    <input type="text" placeholder="주소 검색" readonly  id="address2" onchange="searchAddValueChk();" onkeyup="searchAddValueChk();" value="${myInfo.myAddress}" name="address2" onclick="execPostCodeAdd2();">

                </div>
                <div class="feedback-msg"></div>
            </li>
            <li>
                <div class="input-title">도착지 상세주소</div>
                <div class="address-input-box relative">
                    <input type="text" placeholder="배송도착지 상세주소(필요시입력)" id="addressDetail2" value="${myInfo.myAddressDetail}" name="addressDetail2" >
                </div>
            </li>
            <li>
                <div class="input-title"></div>
                <div class="address-input-box relative">
                    <button class="address-search" type="button" id="priceCheck">배송금액조회</button>
                </div>

                <div class="feedback-msg"></div>
            </li>

            <li class="price relative">
                <div class="input-title">위탁배송금액</div>

                <input class="price-input price-input-1000"  type="text" pattern="\d*" placeholder="숫자만 입력"  onchange="chkNum(this, 'deliveryCost'); searchAddValueChk(); " onkeyup="chkNum(this, 'deliveryCost'); searchAddValueChk(); priceComma(this);" name="deliveryCost" id="deliveryCost" value="">
                <div class="feedback-msg deliveryCost"></div>

            </li>

        </ul>

        <input type="hidden" name="essUserCode" value="${param.code}"/>
        <input type="hidden" name="userName" id="userName" value="${sessionScope.user.name}"/>
        <input type="hidden" name="rwID" id="rwID">
        <input type="hidden" name="end_LON" id="end_LON">
        <input type="hidden" name="end_LAT" id="end_LAT">
        <input type="hidden" name="start_LON" id="start_LON">
        <input type="hidden" name="start_LAT" id="start_LAT">
        <input type="hidden" name="price_TYPE" id="CASH" value="CASH">


        <section class="btn-wrap btn-one flex" style="margin-top: 10%; margin-bottom: 20px; position: static; width: 100%; padding: 0px;">
<%--            <button class="btn-white" type="button" onclick="locationH('/user/search')">취소</button>--%>
<%--            <button class="btn-blue type disabled pricechk-btn" type="button" id="priceCheck">배송금액조회</button>--%>
            <button class="btn-blue type disabled add-btn" type="button" id="searchFormSubmit" onclick="this.disabled=true">거래 시작</button>
            <!-- 모든 정보 입력하지 않았을 경우(유효성 검사) disabled class 추가 하거나 disabled 속성 추가-->
        </section>
    </form>
</div>


</body>
</html>
