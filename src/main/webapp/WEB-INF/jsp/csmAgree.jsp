<%--
  Writer: Lee
  Date: 2021-04-09 오후 5:19
  Explanaion : 
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<%@include file="/WEB-INF/jsp/user_static/headerImport.jsp" %>

<script async src="${pageContext.request.contextPath}/static/assets/js/csmAgree.js"></script>

<body id="trnscRgstr">
<!-- 공통 -->
<header class="header-nav">
    <img src="${pageContext.request.contextPath}/static/assets/img/icons/close.png" alt="nav" class="icon" onclick="locationH('/user/purchaseSale');">
    <span class="nav-title">위탁배송 동의</span>
</header>
<!-- 공통 -->
<div class="container pd-t32 pd-lr25">
    <p class="fs22 bold lh145">
        거래진행을 위해<br>
        위탁배송 동의를 해주세요.
    </p>

    <b class="block bold color-gray fs13 mr-t24">거래 정보</b>
    <ul class="transaction-list mr-t16">
        <li>
            <span class="block color-gray">차량 정보</span>
            <span class="block ">${data.productName}</span>
        </li>
        <li>
            <span class="block color-gray">구매자</span>
            <span class="block ">${data.esrName}(${data.esrCompany}) / ${data.esrPh} </span>
        </li>
        <li>
            <span class="block color-gray">거래금액</span>
            <span class="block bold"><fmt:formatNumber value="${data.price }" pattern="#,###" />원</span>
        </li>
    </ul>
            <br>
            <b class="block bold color-gray fs13 mr-t24">탁송 정보</b>
            <ul class="address-list mr-t16">
                <li>
                    <span class="block color-gray">출발주소</span>
                    <span class="block">${data.startAddress}&nbsp;<c:if test="${data.startAddressDetail != ''}"> (${data.startAddressDetail}) </c:if></span>
                </li>
                <li>
                    <span class="block color-gray">도착주소</span>
                    <span class="block">${data.endAddress}&nbsp;<c:if test="${data.endAddressDetail != ''}"> (${data.endAddressDetail}) </c:if></span>
                </li>
            </ul>


    <form id="csmForm" name="csmForm" method="get" action="#">
        <input type="hidden" name="orderNo" id="orderNo" value="${param.orderNo}">
    </form>

    <a href="${pageContext.request.contextPath}/user/deliveryCancel"></a>
    <br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
    <section class="btn-wrap pd0">
        <p class="fs14 bold color-blue">위의 정보로 위탁배송 요청합니다.<br> 동의하시면 아래의 버튼을 눌러주세요.</p>
        <button class="bgColor-blue fs16 color-white" id="csmAgreeBtn">네, 동의합니다.</button>
    </section>

</div>
</body>
</html>
