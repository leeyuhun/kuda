/*
* 입금확인 처리 ajax
* */
function deposit(orderNo){


    $.ajax({
        url:'/adm/rComplete/?orderNo=' +orderNo,
        type:'get',
        // dataType : 'json',
        contentType : 'application/json; charset=UTF-8',
        success : function(data){
            if(data ==='ok'){
                alert("입금처리를 완료했습니다. \n 페이지를 새로고침 합니다.");
                location.reload(true);
            }else{
                alert('잘못됨');
            }
        },
        error:function(request,status,error){
            alert("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
        }
    });

}


/*
* 애스크로 거래종료 ajax
* */
function escrowEnd(orderNo){

    $.ajax({
        url:'/adm/escrowEnd/?orderNo=' +orderNo,
        type:'get',
        // dataType : 'json',
        contentType : 'application/json; charset=UTF-8',
        success : function(data){
            if(data ==='ok'){
                alert("거래종료 처리를 완료했습니다 \n 페이지를 새로고침 합니다.");
                location.reload(true);
            }else{
                alert('잘못됨');
            }
        },
        error:function(request,status,error){
            alert("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
        }
    });

}



/*
* DetailView
* */
function escrowDetail(code){


    $.ajax({
        url:'/adm/escrowListDetail?orderNo='+code,
        type:'get',
        dataType : 'json',
        contentType : 'application/json; charset=UTF-8',
        success : function(data){
            let json = data.detail;

            $('#ed-elCode').html(json.elCode);
            $('#ed-productName').html('' + json.productName);

            $('#ed-date').html(json.requestDate +'&nbsp;'+json.requestTime);


            $('#ed-esr').html(json.srName +'('+ json.srCompany+')&nbsp'+ json.srPh);
            $('#ed-ess').html(json.ssName +'('+ json.ssCompany+')&nbsp'+ json.ssPh);

            $('#ed-allAmount').html(json.allAmount +'원');
            $('#ed-startAddress').html(json.startAddress + ' (' + json.startAddressDetail + ')');
            $('#ed-endAddress').html(json.endAddress + ' (' + json.endAddressDetail + ')');
            if(json.bankInfo ==='N'){
                $('#ed-remittance').html('미기입상태');
            }else{
                $('#ed-remittance').html(json.bankName +') '+ json.bankNumber +'&nbsp;'+json.accountHolder);
            }

        },
        error:function(request,status,error){
            alert("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
        }
    });

}
