

$(function () {

    /*
    * 체크박스 전체 체크 && 해제
    * */
    $("#check-box-all").click(function () {
        if ($("#check-box-all").prop("checked")) {
            $("input[type=checkbox]").prop("checked", true);
        } else {
            $("input[type=checkbox]").prop("checked", false);
        }
    })


    /*
    * 폼생성 // checkBox 체크값 // excel //처리
    */



    $('#btn-excel').click(function (){

        let chkValue = [];

        $("input:checkbox[name=checkBoxValues]:checked").each(function(i) {
            chkValue.push($(this).val());
        });



        let form = $('#remitListForm');
        let remitFormData = form.serialize();

        if(chkValue.length === 0 ){
            alert('1개 이상의 컬럼을 선택해주세요.');
        }else{

            /*
            * 배열의 형식을 str => split '/'
            * */
            let codeArr = "";

            for(let i=0 ; i <chkValue.length ; i++){
                codeArr = codeArr + "'"+chkValue[i] +"',";
            }

            $('#codeString').val(codeArr.substring(0 , codeArr.length -1));


            // $.ajax({
            //     url:'/excel/eRemitBankExcel',
            //     type:'GET',
            //     data: $('#remitListForm').serialize(),
            //     // dataType : 'json',
            //     contentType : 'application/json; charset=UTF-8',
            //     success : function(data){
            //         if(data ==='ok'){
            //             // alert('ok? No');
            //
            //         }else{
            //             // alert('잘못됨');
            //         }
            //     },
            //     error:function(request,status,error){
            //         alert("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
            //     }
            // });



            $('#remitListForm').attr("method" , "get");
            $('#remitListForm').attr("action" , "/excel/eRemitBankExcel").submit();
            // form.submit();


        }


    });


    $('#btn-deposit-comp').click(function (){

        let chkValue = [];

        $("input:checkbox[name=checkBoxValues]:checked").each(function(i) {
            chkValue.push($(this).val());
        });



        let form = $('#remitListForm');
        let remitFormData = form.serialize();

        if(chkValue.length === 0 ){
            alert('1개 이상의 컬럼을 선택해주세요.');
        }else{

            /*
            * 배열의 형식을 str => split '/'
            * */
            let codeArr = "";

            for(let i=0 ; i <chkValue.length ; i++){
                codeArr = codeArr + "'"+chkValue[i] +"',";
            }


            let codeList = codeArr.substring(0 , codeArr.length -1);

            let data = JSON.stringify ({
                codeList : codeList,
            });

            $.ajax({
                type: "POST",
                url: "/adm/depositCompete",
                data: codeList,
                dataType : 'json',
                contentType: "application/json; charset=utf-8",
                success: function (data) {
                    console.log(data);

                    if(data.msg ==="ok"){
                        alert('정상 처리되었습니다.');
                    }else{
                        alert('처리 실패');
                    }

                    location.reload();

                },
                error: function (e) {

                    alert("서버 에러");
                }
            });


        }


    });






})

