let btnOK="확인",btnCancel="취소";


function objectifyForm(formArray) {//serializeArray data function
    var returnArray = {};
    for (var i = 0; i < formArray.length; i++) {
        returnArray[formArray[i]['name']] = formArray[i]['value'];
    } return returnArray;
}


// 링크 이동
function locationH(href){
    location.href= href;
}


// 우편번호 defualt
function execPostCode(){
    daum.postcode.load(function(){
        new daum.Postcode({
            oncomplete: function(data) {
                // 팝업에서 검색결과 항목을 클릭했을때 실행할 코드를 작성하는 부분.

                // 각 주소의 노출 규칙에 따라 주소를 조합한다.
                // 내려오는 변수가 값이 없을땐 공백('')값을 가지므로, 이를 참고하여 분기한다.
                var addr = ''; // 주소 변수

                //사용자가 선택한 주소 타입에 따라 해당 주소 값을 가져온다.
                if (data.userSelectedType === 'R') { // 사용자가 도로명 주소를 선택했을 경우
                    addr = data.roadAddress;
                } else { // 사용자가 지번 주소를 선택했을 경우(J)
                    addr = data.jibunAddress;
                }

                // 우편번호와 주소 정보를 해당 필드에 넣는다.
                // document.getElementById('zipCode').value = data.zonecode;
                document.getElementById("address").value = addr;


                // 커서를 상세주소 필드로 이동한다.
                document.getElementById("addressDetail").focus();
            }
        }).open();
    });
}




// jQuery

// 은행 선택 control
function selectboxClick(ele) {
    let selectList = $(ele).next('.select-list-box');
    if(selectList.css('display')=='none')selectList.slideDown();
    else selectList.slideUp();
}

// 임시 작성
function stateGo(href){
    // alert('a');
    locationH(href);
}

// 구매/판매 View control
function purchaseSaleView(viewName){
    if(viewName=='sale'){
        $('.purchase-view').fadeOut(50);
        setTimeout(() => {$('.sale-view').fadeIn();},100);
    }else{
        $('.sale-view').fadeOut(50);
        setTimeout(() => {$('.purchase-view').fadeIn();},100);
    }
}

// tab menu control
function navActive(ele){
    $(ele).parent().children().removeClass('active');
    $(ele).addClass('active');
}

//fadeIn control
function fadeIn(fadeInEle, fadeOutEle){
    $(fadeOutEle).fadeOut(50);
    setTimeout(() => {$(fadeInEle).fadeIn();},100);
}

// nav menu open
function openNav(){
    $('html, body').addClass('not_scroll');
    $(".nav-menu").css('left', '0');
    $('.overlay').fadeIn(500);
}

//nav menu close
function closeNav(){
    $('html, body').removeClass('not_scroll');
    $(".nav-menu").css('left', '-278px');
    $('.overlay').fadeOut(500);
}

//display control
function displayNon(inEle, outEle){
    $(inEle).removeClass('display-none');
    $(outEle).addClass('display-none');
}

//유효성 검사
const check_num = /[0-9]/;  //숫자
const check_eng = /[a-zA-Z]/;   //영문
const check_spc = /[~!@#$%^&*()_+|<>?:{}]/; //특수문자
const check_kor = /[ㄱ-ㅎ|ㅏ-ㅣ|가-힣]/; //한글
const check_not_spc = /^[ㄱ-ㅎ|가-힣|a-z|A-Z|0-9|]+$/;

//Btn disabled 처리하기 위한 Flag
let inviteFlag = false; //초대하기 Btn Flag
let chkNumFlag = false; //영문 체크 Flag
let chkKoFlag = false; //한글 체크 Flag
let bankCodeFlag = false;
let phoneFlag = false; //핸드폰번호 11자리

/*
* 금액콤마찍기
* */
function numberComma(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}



//숫자 검사
function chkNum(ele, feedbackEle){
    const str = ele.value;

    if(str!==''){
        if(!check_kor.test(str) && check_num.test(str) && !check_eng.test(str) && !check_spc.test(str)) {
            feedbackMsg(feedbackEle,'');
            chkNumFlag = true;
            return true;

        }else{
            feedbackMsg(feedbackEle,'숫자만 입력 가능합니다.');
            chkNumFlag = false;
            return false;
        }
    }else{
        chkNumFlag = false;
        feedbackMsg(feedbackEle,'');
    }
}

//금액콤마
// function priceToString(ele, feedbackEle) {
//     return price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
// }



function chkPhoneLength(ele){
    let phone = ele.value;
    if(phone.length>=11){
        phoneFlag = true;

        return true;
    } else{
        phoneFlag = false;
        return false;
    }
}

//한글 검사
function chkKo(ele, feedbackEle){
    const str = ele.value;


    if(str!==''){
        if( check_kor.test(str) && !check_num.test(str) && !check_eng.test(str) && !check_spc.test(str)) {
            feedbackMsg(feedbackEle,'');
            chkKoFlag = true;
            return true;
        }else{
            chkKoFlag = false;
            feedbackMsg(feedbackEle,'한글만 입력 가능합니다.');

            return false;
        }
    }else{
        chkKoFlag = false;
        feedbackMsg(feedbackEle,'');
    }
}

function checkSpecial(str) {
    const regExp = /[~!@#$%^&*()_+-|<>?:;`,{}\]\[/\'\"\\\']/gi;
    if (regExp.test(str)) {
        return true;
    } else {
        return false;
    }
}

function checkKor(str) {
    const regExp = /^[ㄱ-ㅎ|ㅏ-ㅣ|가-힣]*$/;
    if (regExp.test(str)) {
        return true;
    } else {
        return false;
    }
}

function checkNum(str) {
    const regExp = /^[0-9]*$/;
    if (regExp.test(str)) {
        return true;
    } else {
        return false;
    }
}

function checkBlank(str){
    if(str.length >=1){
        return true;
    }else{
        return false;
    }
}

function checkEng(str) {
    const regExp = /^[a-zA-Z]*$/;
    if (regExp.test(str)) {
        return true;
    } else {
        return false;
    }
}

// 영문+숫자만 입력 체크
function checkEngNum(str) {
    const regExp = /^[a-zA-Z0-9]*$/;
    if (regExp.test(str)) {
        return true;
    } else {
        return false;
    }
}
// 공백(스페이스 바) 체크
function checkSpace(str) {
    if (str.search(/\s/) !== -1) {
        return true; // 스페이스가 있는 경우 }else{ return false; // 스페이스 없는 경우 } }
    }
}



//피드백 메세지
function feedbackMsg(name, content){
    let msg = document.querySelector(`.feedback-msg.${name}`);
    msg.innerText = content;

    if(content!='') msg.classList += ' active';
    else removeClass(msg, 'active');
}



function koView(ele , name){
    let value = ele.value.replace(/,/gi, "");
    if(!check_kor.test(value) && check_num.test(value) && !check_eng.test(value) && !check_spc.test(value)) {
        let returnText = viewKorean(value);

        koViewMsg(name , returnText);
    }
    ele.value = value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}


function priceComma(ele){
    let value = ele.value.replace(/,/gi, "");
    ele.value = value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

//한글 입력
function koViewMsg(name, content){
    let msg = document.querySelector(`.view-korean-msg.${name}`);
    msg.innerText = content;

    if(content!='') msg.classList += ' active';
    else removeClass(msg, 'active');


}

//removeClass
function removeClass(element, className) {
    let check = new RegExp("(\\s|^)" + className + "(\\s|$)");
    element.className = element.className.replace(check, " ").trim();
}

//주소 클릭(목록/직접 입력)
function addressSelect(ele){
    navActive(ele);
    searchAddValueChk();
    if(ele.classList.contains('list')){
        $('.address-add').fadeOut(100);
        setTimeout(function() {
            $('.address-list').fadeIn();
        }, 100);
    }else{
        $('.address-list').fadeOut(100);
        setTimeout(function() {
            $('.address-add').fadeIn();
        }, 100);
    }
}


// Confirm Modal


function alertMobile(msg,callback, isScroll) {
    if(typeof(toastr)!="undefined") toastr.remove();
    $('#modalAlertMobile .modal-footer button:first-child').hide();
    confirmMobile(msg,callback,null,null,true, isScroll);
}

function confirmMobile(msg,callback,callback_cancel,arrBTN,isAlert, isScroll) {
    if (!isScroll) scrollDisable();
    ajaxLoading(true);
    if(isAlert) {
        $('#modalAlertMobile .modal-footer button:first-child').hide();
        if (!$('#modalAlertMobile').hasClass('modal-type-alert')) $('#modalAlertMobile').addClass('modal-type-alert');
        if ($('#modalAlertMobile .modal-footer').hasClass("confirm")) $('#modalAlertMobile .modal-footer').removeClass("confirm");
    }
    else {
        $('#modalAlertMobile .modal-footer button:first-child').show();
        if ($('#modalAlertMobile').hasClass('modal-type-alert')) $('#modalAlertMobile').removeClass('modal-type-alert');
        if (!$('#modalAlertMobile .modal-footer').hasClass("confirm")) $('#modalAlertMobile .modal-footer').addClass("confirm");
    }
    $('#modalAlertMobile').modal({ keyboard: true});





    $('#modalAlertMobile .modal-title').html(msg);


    // var chkCR = msg.match(/\n\n/g);

    $('#modalAlertMobile .modal-footer .btn-outline-theme').unbind("click");
    $('#modalAlertMobile .modal-footer .btn-theme').unbind("click");
    $('#modalAlertMobile .modal-backdrop-cancel').remove();





    if(typeof(arrBTN)!="undefined" && arrBTN) {
        btnOK=arrBTN[0];
        btnCancel=arrBTN[1];
    }



    $('#modalAlertMobile .modal-footer .btn-theme ').html(btnOK);
    $('#modalAlertMobile .modal-footer .btn-outline-theme ').html(btnCancel);

    if(isAlert) {
        $('#modalAlertMobile').on('shown.bs.modal', function (e) {
            $('#modalAlertMobile').on( 'keypress', function( e ) {
                if( e.keyCode == 13 ) {
                    e.preventDefault();
                    $(this).find(".modal-footer .btn-theme").trigger("click");
                }
            } );
        });
    }

    $('#modalAlertMobile').on('hidden.bs.modal', function (e) {
        $(e.currentTarget).unbind();
        scrollAble();
    });



    if(typeof callback === 'function') {

        $('#modalAlertMobile .modal-footer .btn-theme').on('click', function(e, active) {
            if(typeof callback === 'function') {
                $('#modalAlertMobile').on('hidden.bs.modal', function (e) {
                    callback();
                });
            }
        });
    }

    if(callback_cancel) {
        $('#modalAlertMobile .modal-footer .btn-outline-theme').on('click', function(e, active) {
            if(typeof callback_cancel === 'function') {
                $('#modalAlertMobile').on('hidden.bs.modal', function (e) {
                    callback_cancel();
                });
            }
        });
        $('#modalAlertMobile').append('<div class="modal-backdrop-cancel"></div>');
        $('#modalAlertMobile .modal-backdrop-cancel').on('click', function(e, active) {
            if(typeof callback_cancel === 'function') {
                callback_cancel();
            }
        });
    }
}

function scrollDisable(){
    $('html, body').addClass('not_scroll');
}
function scrollAble(){
    $('html, body').removeClass('not_scroll');
}
function ajaxLoading(isHide,txtLoading) {

    if(typeof(txtLoading)!="undefined" && txtLoading!="") {
        $("#ajax_loader .loading-text").html(txtLoading);
        $("#ajax_loader .loading-text").show();
    }
    else {
        $("#ajax_loader .loading-text").html("");
        $("#ajax_loader .loading-text").hide();
    }

    if(isHide) {
        clearTimeout(loaderTimeout);
        $("#ajax_loader").hide();
    }
    else if($("#ajax_loader").css("display")=="none") {
        $("#ajax_loader").show();
        loaderTimeout = setTimeout(function() {
            ajaxLoading(true);
            ajaxError("","서버에서 응답이 없습니다. 잠시 후 다시 시도 해 주세요.");
        },30000);
    }
}
//////////////////////////////////////////////////////////////////////////////////////////


/*차량배송*/
function deliveryModal(){
    $('.modal-full.completed-modal').addClass('active'); $('.modal-full.completed-modal').css('opacity',1);
}

function remittanceInfoModal(){
    $('.modal-full.request-modal').css('opacity',1); setTimeout(()=>{$('.modal-full.request-modal').addClass('active')},300);
}

function searchAddSubmit(){
    $('.modal-full').css('opacity',1); setTimeout(()=>{$('.modal-full').addClass('active')},300);
}


/*
* page : searchAdd
* comment : 직접입력 클릭시 value 초기화
* */
function directInputInit(){
    $('#address').val('');
    $('#addressDetail').val('');
    document.querySelector('.add-btn').disabled = true;
}

/*
* page : search.jsp
* comment : 초성추출
* */
function korDiv(str) {
    var cho = ["ㄱ","ㄲ","ㄴ","ㄷ","ㄸ","ㄹ","ㅁ","ㅂ","ㅃ","ㅅ","ㅆ","ㅇ","ㅈ","ㅉ","ㅊ","ㅋ","ㅌ","ㅍ","ㅎ"];
    var korDivResult = "";
    for(i=0;i<str.length;i++) {
        code = str.charCodeAt(i)-44032;
        if(code>-1 && code<11172) korDivResult += cho[Math.floor(code/588)];
    }
    return korDivResult;
}


function stateUpdate(orderNo , state){


    /*상태값 변경.*/
    $.ajax({
        url:'/user/stateUpdate?state='+state+'&orderNo='+orderNo,
        type:'get',
        dataType : 'json',
        contentType : 'application/json; charset=UTF-8',
        success : function(data){
            alert('ok : ' + data);
        },
        error : function(e){
            alert('연결도중 error :' + e );
        }
    });


}

//파타미터 가져오기
function getParameter(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}


//parameter check 1개 이상인지
function getParameterCheck(){

    var url = location.href;

    if(url.indexOf('?') !== -1){
        return true ;
    }else{
        return false;
    }
}


$(function() {


    /*
    * page : search.jsp
    * comment : 검색 초성?자음?숫자? 체크
    * */
    var timer;

    $('#userSearch').keyup(function(){
        if (!timer) {
            timer = setTimeout(function() {
                timer = null;
                // 실행할 코드 내용
                let searchDiv = "";

                let keyword = $("#userSearch").val().toUpperCase();
                let korDivValue = korDiv($("#userSearch").val());

                if( isNaN( keyword ) === true){

                    if (keyword!=="" && korDivValue===""){
                        searchDiv = 'cho';
                    }else{
                        searchDiv = 'ja';
                    }
                }else{
                    searchDiv = 'num'
                }

                // if(checkKor(keyword) || checkNum(keyword)){
                $.ajax({
                    url:'/user/searchSellerList?searchName='+keyword+'&searchDiv='+searchDiv,
                    type:'get',
                    dataType : 'json',
                    contentType : 'application/json; charset=UTF-8',
                    success : function(data){
                        $('.search-container').empty();

                        if(data.searchList.length >= 1) {


                            for (let i = 0; i < data.searchList.length; i++) {

                                $('.search-container').append(
                                    '<li onClick="locationH(\'/user/searchAdd?code='+data.searchList[i].usercode+'\')">' +
                                    '<b class="block">' + data.searchList[i].name + '</b>' +
                                    '<span>' + data.searchList[i].company + '/' + data.searchList[i].ph + '</span>' +
                                    '</li>'
                                );

                            }

                        }else{
                            $('.search-container').append(
                                '<div class="impossible">'+
                                '<div class="impossible-box">'+
                                '<p class="impossible-text">' +
                                '판매자가 검색 되지 않는 경우<br>' +
                                '초대하기를 눌러 주세요.' +
                                '</p>'+
                                '<button class="impossible-btn" onclick="locationH(\'./invite\')">초대하기</button>'+
                                '</div>'+
                                '</div>'

                            )
                        }

                    },
                    error:function(request,status,error){
                        alert("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
                    }
                });
                // }else{
                //     alertMobile('한글 또는 숫자만 입력해주세요.');
                //     $('#userSearch').val('');
                //     $('#userSearch').focus();
                // }

                /*
                * page : search.jsp
                * comment : 검색결과 가져오기
                * */
            }, 1000);
        }



    });

    /*
    * comment : 선택된 은행코드 form=> hiddenInput = .bankCode 에적용
    * */
    $('.select-list-li').click(function (e) {

        bankCodeFlag = true;

        let value = $(this).data('value');

        $(".bankCode").val(value);


    });

    //숫자만 입력가능
    $(".numberOnly").on("keyup", function() {
        $(this).val($(this).val().replace(/[^0-9]/g,""));
    });

    /*
    * 숫자만 + comma
    * */
    $('.numberComma').keyup(function(event){

        if (!(event.keyCode >=37 && event.keyCode<=40)) {
            let inputVal = $(this).val();

            inputVal = inputVal.replace(/[^0-9]/g, '');   // 입력값이 숫자가 아니면 공백
            inputVal = inputVal.replace(/,/g, '');          // ,값 공백처리
            $(this).val(inputVal.replace(/\B(?=(\d{3})+(?!\d))/g, ","));
        }

    });


    /*
    * 핸드폰 11자리
    * input focusing out
    * */
    $('.inputPhone').keyup(function(){

        let phNum = $('.inputPhone').val();


        if(phNum.length === 11){

            $('.inputPhone').blur();


        }
    });

    $('.price-input-1000').click(function (){

        let priceInput = $('.price-input-1000').val();
        let priceInputLength = priceInput.length;
        let res = 0;
        if(priceInput.indexOf(',') === -1){
            res = 0;
            console.log("if");
        }else {
            res = priceInputLength-4;
            console.log("else");
        }
        console.log("res : "  +  res);
        move_cursor(res);

        if(priceInputLength <=2){
            alertMobile('천원단위로만 변경 가능합니다.' ,
                function (){$('.price-input-1000').val('000')});
        }

    });


    $('.price-input-1000').keyup(function (){

        let priceInput = $('.price-input-1000').val();
        let priceInputLength = priceInput.length;
        let res = 0;
        if(priceInput.indexOf(',') === -1){
            res = 0;
            console.log("if");
        }else {
            res = priceInputLength-4;
            console.log("else");
        }
        console.log("res : "  +  res);
        move_cursor(res);

        if(priceInputLength <=2){
        alertMobile('천원단위로만 변경 가능합니다.' ,
            function (){$('.price-input-1000').val('000')});
        }
    });



})

var loaderTimeout = null;
$('body').append('<div class="modal fade" id="modalAlertMobile" tabindex="-1" role="dialog" aria-hidden="true" style="z-index:99999;"><div class="modal-dialog"><div class="modal-content"><div class="modal-header"><h4 class="modal-title" style="font-weight:normal !important;"></h4></div><div class="modal-footer"><button type="button" class="btn btn-outline-theme" data-dismiss="modal">취소</button> <button type="button" class="btn btn-theme " id="alert-ok" data-dismiss="modal"> 확인</button></div></div></div></div>');

function move_cursor(num) {
    var pos = num;
    var obj = document.getElementById("deliveryCost");
    if (obj.setSelectionRange) {
        obj.focus();
        obj.setSelectionRange(pos, pos);
    } else if (obj.createTextRange) {
        var c = obj.createTextRange();
        c.move("character", pos);
        c.select();
    }
}

function viewKorean(num) {
    num = parseInt((num + '').replace(/[^0-9]/g, ''), 10) + '';
    var hanA = new Array("","일","이","삼","사","오","육","칠","팔","구","십");
    var danA = new Array("","십","백","천","","십","백","천","","십","백","천","","십","백","천");
    var result = "";
    for(i=0; i<num.length; i++) {
        str = "";
        han = hanA[num.charAt(num.length-(i+1))];
        if(han != "") str += han+danA[i];
        if(i == 4) str += "만";
        if(i == 8) str += "억";
        if(i == 12) str += "조";
        result = str + result;
    }
    if(num != 0)
        result = result + "원";
    return result ;
}



//$function end


