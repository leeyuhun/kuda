if(getParameterCheck()){


    let deliveryProcMsg= getParameter("deliveryProcMsg");

    if(deliveryProcMsg ==='ok'){
        alertMobile('차량 배송 처리되었습니다.\n' +
            '구매자에게 알림이 발송되며\n' +
            '차량 수령 후 거래금액이 입금됩니다.');
    }else if(deliveryProcMsg ==='not') {
        alertMobile('요청이 정상 처리되지 않았습니다.');
    }



    let escrowCompleteProcMsg = getParameter("escrowCompleteProcMsg");
    let productNameMsg = getParameter("productNameMsg");

    if(escrowCompleteProcMsg ==='ok'){
        alertMobile(productNameMsg + '\n' +
            '차량 거래가 완료되었습니다.');
    } else if(deliveryProcMsg ==='not') {
        alertMobile('요청이 정상 처리되지 않았습니다.');
    }


    let remitMsg = getParameter("remitMsg");

    if(remitMsg ==='n'){
        alertMobile('해당거래에 접근할수 없습니다.');
    }else if(remitMsg ==='n2'){
        alertMobile('해당거래건은 이미 송금등록이 완료 되었습니다.');
    }


    let eRemitInsertMsg = getParameter("eRemitInsertMsg");
    let eHolder = getParameter("eHolder");
    let eNum = getParameter("eNum");
    let eName = getParameter("eName");
    let pAmount = getParameter("pAmount");
    let stateUpdateResultMsg = getParameter("stateUpdateResultMsg");

    if(eRemitInsertMsg ==='ok' && stateUpdateResultMsg ==='ok'){

        alertMobile(eHolder +'\n' +
            eName + '/' + eNum +'\n'+
            pAmount+'\n\n'+
            '해당 계좌로 거래금액이 입금됩니다.'
        );

    }else if(eRemitInsertMsg ==='not' || stateUpdateResultMsg ==='not'){
        alertMobile('요청이 정상 처리되지 않았습니다.');
    }


}



if (performance.navigation.type === 1) {
    window.location.href = '/user/purchaseSale';
}

