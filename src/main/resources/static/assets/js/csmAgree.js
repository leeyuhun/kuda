$(document).ready(function (){
    let chk = true;
    $('#csmAgreeBtn').click(function (){

        $('#csmAgreeBtn').attr('disabled' , true)
        btnOK = "예, 동의합니다.";
        btnCancel = "거래정보를 다시 확인할게요.";
        confirmMobile("<br>위탁배송 진행을 동의 하시겠습니까?<br><br>");
    })


    $(document).on("click","#alert-ok",function(){
        btnOK = "확인";
        btnCancel = "취소"
        // let data = $('#remittanceForm').serializeArray();
        // let formSubmit = JSON.stringify(objectifyForm(data));
        const orderNo = $('#orderNo').val();


        if(chk === false) return false;
        $.ajax({
            type: "GET",
            url: "/user/api/csmAgreeY?orderNo=" + orderNo,
            dataType: 'json',
            contentType: "application/json; charset=utf-8",
            complete: function (data) {

                if (data.responseJSON.RESULT_CODE === '0000') {

                    alertMobile("위탁 배송이 정상적으로 요청 되었어요.<br>확인버튼을 누르시면 계좌입력 페이지로 전환됩니다.", function () {
                        location.href = "/user/remittance?orderNo=" + orderNo;
                    });
                } else {
                    alertMobile(data.responseJSON.RESULT_MSG);
                }
                chk = false;
            },
            error: function (e) {

                alert('서버통신에러');
                chk = false;
            },

        });

    })

})












