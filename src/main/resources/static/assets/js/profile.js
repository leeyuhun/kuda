let pfFlag = false; //btn flag

function profileValueChk(){

    const companyVal_pf = document.querySelector('input[name=company]').value;
    const empNumVal_pf = document.querySelector('input[name=empNum]').value;
    const phVal_pf = document.querySelector('input[name=ph]').value;
    // const bankCodeVal_pf = document.querySelector('.select-list .select-list-li').value;
    const bankNum_pf = document.querySelector('input[name=bankNum]').value;
    const bankHolder_pf = document.querySelector('input[name=bankHolder]').value;
    // const bankCode_pf = document.querySelector('input[name=bankCode]').value;

    let flag = true;

    let bankCodeHidden_pf = $('#bankCode').val();

    if( bankCodeHidden_pf === '' ){
        flag =false;
    }

    let empNumFlag = checkNum($('input[name=empNum]').val());
    let phFlag = checkNum($('input[name=ph]').val());
    let bankNumFlag = checkNum($('input[name=bankNum]').val());
    let bankHolderFlag = checkKor($('input[name=bankHolder]').val());
    let addressFlag = checkBlank($('input[name=address]').val());
    let addressDetailFlag = checkBlank($('input[name=addressDetail]').val());

    let profileFlag = empNumFlag && phFlag && bankNumFlag && bankHolderFlag && addressFlag && addressDetailFlag;

    pfFlag = profileFlag && (companyVal_pf!=='') && (empNumVal_pf!=='') && (phVal_pf!=='')
                && (bankNum_pf!=='') && (bankHolder_pf!=='')  && flag;

    let pfBtn = document.querySelector('.pf-btn');


    if(pfFlag){
        removeClass(pfBtn, 'disabled');
        pfBtn.disabled = false;
    }else{
        if(!(pfBtn.classList.contains('disabled'))){
            pfBtn.classList += ' disabled';
        }

        pfBtn.disabled = true;
    }

}



