let regFlag = false; //btn flag

function regValueChk(){

    const companyVal_reg = document.querySelector('input[name=departName]').value;
    const empNumVal_reg = document.querySelector('input[name=departNum]').value;
    const phVal_reg = document.querySelector('input[name=phoneNum]').value;
    // const bankCodeVal_reg = document.querySelector('.select-list .select-list-li');
    const bankNum_reg = document.querySelector('input[name=bankNumber]').value;
    const bankHolder_reg = document.querySelector('input[name=bankHolder]').value;
    // const bankCode_reg = document.querySelector('input[name=bankCode]').value;
    // const sellerName_reg = document.querySelector('input[name=sellerName]').value;

    let flag = true;

    let bankCodeHidden_reg = $('#bankCode').val();

    if( bankCodeHidden_reg === '' ){
        flag = false;
    }

    let empNumFlag = checkNum($('input[name=departNum]').val());
    let sellerNameFlag = checkBlank($('input[name=sellerName]').val());
    let phFlag = checkNum($('input[name=phoneNum]').val());
    let bankNumFlag = checkNum($('input[name=bankNumber]').val());
    let bankHolderFlag = checkBlank($('input[name=bankHolder]').val());
    let addressFlag = checkBlank($('input[name=address]').val());
    let addressDetailFlag = checkBlank($('input[name=addressDetail]').val());

    let profileFlag = sellerNameFlag && bankCodeFlag &&  empNumFlag && phFlag && bankNumFlag && bankHolderFlag && addressFlag && addressDetailFlag;

    regFlag = profileFlag && (companyVal_reg!=='') && (empNumVal_reg!=='') && (phVal_reg!=='')
        && (bankNum_reg!=='') && (bankHolder_reg!=='')  && flag;

    let regBtn = document.querySelector('.reg-btn');
    let btnWrap = document.querySelector('.btn-wrap');

    if(regFlag){
        removeClass(regBtn, 'disabled');
        regBtn.disabled = false;
        $('.btn-wrap').css('position','static');

    }else{
        if(!(regBtn.classList.contains('disabled'))){
            regBtn.classList += ' disabled';
        }

        regBtn.disabled = true;
    }

}



$(document).ready(function (){
    $('#reg-btn').click(function (){
        $('#registrationForm').submit();
    })

})



