let addFlag = false;

//거래정보 등록 페이지 Value chk
function searchAddValueChk(){
    const carNumVal = document.querySelector('input[name=productNum]').value;
    const markInVal = document.querySelector('input[name=productName]').value;
    const bankHolderVal = document.querySelector('input[name=bankHolder]').value;
    const address1 = document.querySelector('input[name=address1]').value;
    const address2 = document.querySelector('input[name=address2]').value;
    const deliveryCost = document.querySelector('input[name=deliveryCost]').value;
    // const colorVal = document.querySelector('input[name=productColor]').value;
    // const addressVal = document.querySelector('input[name=address]').value;
    // const addressDetailVal = document.querySelector('input[name=addressDetail]').value;

    // const addressList = document.querySelector('.address-select .list');

    let flag = true;


    // if(!(addressList.classList.contains('active'))){
    //     flag = addressVal!=='' && addressDetailVal!=='';
    // }
    // else if((addressList.classList.contains('active'))){
    //     flag = addressVal!=='' && addressDetailVal!=='';
    // }



    addFlag = chkNumFlag && (carNumVal!=='') && (markInVal!=='') && (bankHolderVal!=='')&& (address1!=='')&& (address2!=='')&& (deliveryCost!=='')&&  flag;

    let addBtn = document.querySelector('.add-btn');

    if(addFlag){
        removeClass(addBtn, 'disabled');
        addBtn.disabled = false;
    }else{
        if(!(addBtn.classList.contains('disabled'))){
            addBtn.classList += ' disabled';
        }

        addBtn.disabled = true;
    }
}



/*
* 주소 최근이용
* */
function addressSetting(address , addressDetail){

    $('#address').val(address);
    $('#addressDetail').val(addressDetail);

}






$(document).ready(function (){

    /*
    * 예금주 & 총입금금액이 같은 거래건 생성 금지
    * */
    $('#searchFormSubmit').click(function (){

        $.ajax({
            url:'/api/existsEscrowList?amount='+$('#allAmountStr').val() + '&bankHolder='+$('#bankHolder').val(),
            type:'get',
            contentType : 'application/json; charset=UTF-8',
            success : function(data){

                if(data ==='1'){
                    alertMobile('해당 금액과 예금주 명의 거래 건이 이미 존재합니다.'+ '<br> 유선 문의 : 1660-4840');
                    $('#searchFormSubmit').attr('disabled',false);
                }else if(data ==='0'){
                    $('#searchAddForm').submit();
                }else{
                    alertMobile('잘못된 접근입니다.<br>유선 문의 : 1660-4840');
                }
            },
            error:function(request,status,error){
                alert("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
            }
        });


    });


    $('#priceCheck').click(function (){

        let data = $('#searchAddForm').serializeArray();
        let formSubmit = JSON.stringify(objectifyForm(data));

        $.ajax({
            url : '/rwin/priceCheck',
            type: 'POST' ,
            data : formSubmit,
            dataType : 'json',
            contentType : 'application/json; charset=UTF-8',
            success : function (data){


                alertMobile("위탁배송 예상금액은 " + numberComma(data.PRICE) +"원 이며, <br>요청 금액 입력란에서 변경 가능합니다.")
                $('#deliveryCost').val( numberComma(data.PRICE)); //가격
                $('#deliveryCost').focus();
                $('#rwID').val( data.RW_ID); //로드윈측 ID


                $('#end_LON').val(data.priceCheckVo.end_LON);
                $('#end_LAT').val(data.priceCheckVo.end_LAT);
                $('#end_ADDR').val(data.priceCheckVo.end_ADDR);
                $('#start_LON').val(data.priceCheckVo.start_LON);
                $('#start_LAT').val(data.priceCheckVo.start_LAT);
                $('#start_ADDR').val(data.priceCheckVo.start_ADDR);


                $('#addressDetail').focus();

                searchAddValueChk();
            },error:function(request,status,error){
                alert("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
            }

        })


    })


})




function execPostCodeAdd1(){
    daum.postcode.load(function(){
        new daum.Postcode({
            oncomplete: function(data) {
                // 팝업에서 검색결과 항목을 클릭했을때 실행할 코드를 작성하는 부분.

                // 각 주소의 노출 규칙에 따라 주소를 조합한다.
                // 내려오는 변수가 값이 없을땐 공백('')값을 가지므로, 이를 참고하여 분기한다.
                var addr = ''; // 주소 변수

                //사용자가 선택한 주소 타입에 따라 해당 주소 값을 가져온다.
                if (data.userSelectedType === 'R') { // 사용자가 도로명 주소를 선택했을 경우
                    addr = data.roadAddress;
                } else { // 사용자가 지번 주소를 선택했을 경우(J)
                    addr = data.jibunAddress;
                }

                // 우편번호와 주소 정보를 해당 필드에 넣는다.
                // document.getElementById('zipCode').value = data.zonecode;
                document.getElementById("address1").value = addr;
                document.getElementById("addressDetail1").value ='';
                document.getElementById("addressDetail1").focus();
                // 커서를 상세주소 필드로 이동한다.
            }
        }).open();
    });
}

function execPostCodeAdd2(){
    daum.postcode.load(function(){
        new daum.Postcode({
            oncomplete: function(data) {
                // 팝업에서 검색결과 항목을 클릭했을때 실행할 코드를 작성하는 부분.

                // 각 주소의 노출 규칙에 따라 주소를 조합한다.
                // 내려오는 변수가 값이 없을땐 공백('')값을 가지므로, 이를 참고하여 분기한다.
                var addr = ''; // 주소 변수

                //사용자가 선택한 주소 타입에 따라 해당 주소 값을 가져온다.
                if (data.userSelectedType === 'R') { // 사용자가 도로명 주소를 선택했을 경우
                    addr = data.roadAddress;
                } else { // 사용자가 지번 주소를 선택했을 경우(J)
                    addr = data.jibunAddress;
                }

                // 우편번호와 주소 정보를 해당 필드에 넣는다.
                // document.getElementById('zipCode').value = data.zonecode;
                document.getElementById("address2").value = addr;
                document.getElementById("addressDetail2").value ='';
                document.getElementById("addressDetail2").focus();

                // 커서를 상세주소 필드로 이동한다.
            }
        }).open();
    });


}


