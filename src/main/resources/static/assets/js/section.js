// 은행 선택 control
$("body").on("click", ".select-list-box li", function () {
    var text = $(this).html();

    $(".selectbox span").html(text).css({
        'color':'#34373c',
        'font-weight':'bold'
    });

    $(".select-list-box").slideUp();
})
