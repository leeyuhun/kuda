


// 계좌셋팅하기
function remittanceSetting(remitInfo){

    let split = remitInfo.split('#$');

    $('#bankCode').val(split[0]);
     $('#aHolder').val(split[1]);
     $('#aNumber').val(split[2]);

}

//계좌 초기화
function remittanceInit(){
    $('#bankCode').val('');
    $('#aHolder').val('');
    $('#aNumber').val('');
    $('#carOwnerPh').val('');

    let remittanceBtn = document.querySelector('.remittanceBtn');
    if(!(remittanceBtn.classList.contains('disabled'))){
        remittanceBtn.classList += ' disabled';
    }
}

function setUserID(myValue) {
    $('#userid').val(myValue)
        .trigger('change');
}

//거래정보 등록 페이지 Value chk
function remittanceValueChk(){

    const aNum = document.querySelector('input[name=eri_account_number]').value;
    const aHolder = document.querySelector('input[name=eri_account_holder]').value;
    // const bankCode = document.querySelector('input[name=bank_code]').value;
    const remitList = document.querySelector('.remittanceSelect-list .list');


    let carOwnerPh = checkBlank($('input[name=car_owner_ph]').val());

    let flag = true;

    /*
    * 직접입력일때
    * */
    if(!(remitList.classList.contains('active'))){
        flag =  chkNumFlag && phoneFlag && (aNum !== '') && (aHolder !== '') && carOwnerPh && bankCodeFlag;
    }


    let remittanceFlag = flag;

    let remittanceBtn = document.querySelector('.remittanceBtn');

    if(remittanceFlag){
        removeClass(remittanceBtn, 'disabled');
        remittanceBtn.disabled = false;
    }else{
        if(!(remittanceBtn.classList.contains('disabled'))){
            remittanceBtn.classList += ' disabled';
        }

        remittanceBtn.disabled = true;
    }

}


$("document").ready(function (){

    $('.remittanceBtn').click(function (){
        btnOK = "예, 동의합니다.";
        btnCancel = "계좌 정보를 다시 확인할게요."
        confirmMobile("\n" +
            "계좌 정보를 반드시 확인해 주세요.<br>" +
            "오 입력으로 인한 금융거래 사고는 차토스에서 책임지지 않습니다.");
    });


})


$(document).on("click","#alert-ok",function(){
    btnOK = "확인";
    btnCancel = "취소"
    let data = $('#remittanceForm').serializeArray();
    let formSubmit = JSON.stringify(objectifyForm(data));

    $.ajax({
        type: "POST",
        url: "/user/api/eRemitUpdate",
        data: formSubmit,
        dataType : 'json',
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            if(data.msg ==='OK'){
                alertMobile("계좌등록이 완료 되었습니다.<br> 3초후 자동으로 거래내역 페이지로 전환됩니다.");

                setTimeout(function(){
                    location.href = "/user/purchaseSale";
                }, 3000);
            }
        },
        error: function (e) {
            $('.remittanceBtn').attr('disabled' , false);
            alert('서버통신에러');
        }
    });
})




