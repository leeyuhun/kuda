let csmFlag = false; //btn flag


// 우편번호 차량배송
function csmPostCode(address , orderNo){
    daum.postcode.load(function(){
        new daum.Postcode({
            oncomplete: function(data) {

                var addr = ''; // 주소 변수

                if (data.userSelectedType === 'R') {
                    addr = data.roadAddress;
                } else {
                    addr = data.jibunAddress;
                }

                document.getElementById("address").value = addr;

                let sendData = JSON.stringify({
                    "address":$('#address').val(),
                    "orderNo":$('#orderNo').val()
                });

                $.ajax({
                    url : '/rwin/priceCheck',
                    type: 'POST' ,
                    data : sendData,
                    dataType : 'json',
                    contentType : 'application/json; charset=UTF-8',
                    success : function (data){


                        alertMobile("위탁배송 예상금액은 " + numberComma(data.PRICE) +"원 이며, <br>요청 금액 입력란에서 변경 가능합니다.")
                        // $('#price').val( numberComma(data.PRICE)); //가격
                        // $('#rwID').val( data.RW_ID); //로드윈측 ID
                        //
                        //
                        // $('#end_LON').val(data.priceCheckVo.end_LON);
                        // $('#end_LAT').val(data.priceCheckVo.end_LAT);
                        // $('#end_ADDR').val(data.priceCheckVo.end_ADDR);
                        // $('#start_LON').val(data.priceCheckVo.start_LON);
                        // $('#start_LAT').val(data.priceCheckVo.start_LAT);
                        // $('#start_ADDR').val(data.priceCheckVo.start_ADDR);


                        // $('#addressDetail').focus();
                    },error:function(){
                        alert("서버에러");
                    }

                })

                // 커서를 상세주소 필드로 이동한다.
                document.getElementById("addressDetail").focus();
            }
        }).open();
    });
}



function btn_consignmentFormSubmit(){


    let bookingData = JSON.stringify({
        "rw_ID": $('#rwID').val(),
        "end_ADDR": $('#end_ADDR').val(),
        "end_LON": $('#end_LON').val(),
        "end_LAT": $('#end_LAT').val(),
        "start_ADDR": $('#start_ADDR').val(),
        "start_LON": $('#start_LON').val(),
        "start_LAT": $('#start_LAT').val(),
        "price_TYPE": $("input[name='price_TYPE']:checked").val(),
        "price": $('#price').val().replace(/,/g, "") ,
        "orderNo": $('#orderNo').val(),
        "addressDetail": $('#addressDetail').val(),
    });

    console.log("bookingData : " + bookingData)


    $.ajax({
        url : '/rwin/booking',
        type: 'POST' ,
        data : bookingData,
        dataType : 'json',
        contentType : 'application/json; charset=UTF-8',
        success : function (data){

            if(data.msg === 'ok'){

                alertMobile("위탁 배송이 정상적으로 요청 되었어요.<br>배송기사가 배정되면 문자로 알려드릴게요." , function csmBookingOk(){
                    locationH('/user/trnscRgstr?orderNo='+$('#orderNo').val());

                });

            }


        },error:function(request,status,error){
            alert("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
        }

    })

}





function consignmentValueChk(){

    let addressFlag = checkBlank($('#address').val());
    let addressDetailFlag = checkBlank($('#addressDetail').val());
    let csmAmountFlag = checkBlank($('#price').val());

    let csmFlag = addressFlag && addressDetailFlag && csmAmountFlag

    let csmBtn = document.querySelector('.csm-btn');

    if(csmFlag){
        removeClass(csmBtn, 'disabled');
        csmBtn.disabled = false;
    }else{
        if(!(csmBtn.classList.contains('disabled'))){
            csmBtn.classList += ' disabled';
        }


        csmBtn.disabled = true;
    }

}





$(function(){

    $('.consignmentModal').click(function (){

        $('.modal-full.consignment-request-modal').css('opacity',1); setTimeout(()=>{$('.modal-full.consignment-request-modal').addClass('active')},300);

    });

})

