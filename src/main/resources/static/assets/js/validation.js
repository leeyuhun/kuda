/*상사명*/
function inputCompany(){

    let val = $('.input-company').val();
    $('.input-company-fm').html('');

    if(val === "" || val.length < 2 ){
        $('.input-company-fm').html('상사명을 2자이상 입력해주세요.');
        $('.input-company').focus();
        return false;
    }

    return true;

}


/*종사원번호*/
function inputEmpNum(){
    let val = $('.input-empNum').val();
    $('.input-empNum-fm').html('');

    if(val === "" || val.length !== 10 ){
        $('.input-empNum-fm').html('종사원 번호 10자리를 \'-\' 없이 입력해주세요.');
        $('.input-empNum').focus();
        return false;
    }
    return true;
}


/*휴대전화번호*/
function inputPh(){
    let val = $('.input-ph').val();
    $('.input-ph-fm').html('');

    if(val === "" || (val.length < 10 || val.length > 11) ){
        $('.input-ph-fm').html('휴대전화번호 10자리 또는 11자리를 \'-\' 없이 입력해주세요.');
        $('.input-ph').focus();
        return false;
    }
    return true;
}


/*계좌번호*/
function inputBankNum(){
    let val = $('.input-bankNum').val();
    $('.input-bankNum-fm').html('');

    if(val === ""  ){
        $('.input-bankNum-fm').html('계좌번호를 입력해주세요.');
        $('.input-bankNum').focus();
        return false;
    }
    return true;
}


/*예금주*/
function inputBankHolder(){
    let val = $('.input-bankHolder').val();
    $('.input-bankHolder-fm').html('');

    if(val === ""  || val.length < 2){
        $('.input-bankHolder-fm').html('예금주명 을 2자이상 입력해주세요');
        $('.input-bankHolder').focus();
        return false;
    }
    return true;
}

