if(getParameterCheck()) {

    let inviteMsg = getParameter("inviteMsg");

    let inviteMessage = "";


    if (inviteMsg === 'exist') {
        inviteMessage = '이미 가입된 핸드폰번호 입니다.\n' +
            '핸드폰번호를 확인해주세요.';
    } else if (inviteMsg === 'ok') {
        inviteMessage = '초대 메시지가 발송 되었습니다.\n상대가 회원가입을 완료하면\n회원님께 거래 진행 링크를 전송해 드립니다.';
    }


    if (inviteMsg != null && inviteMessage !== '') {
        alertMobile(inviteMessage);
    }
}

if (performance.navigation.type === 1) {
    window.location.href = '/user/search';
}

