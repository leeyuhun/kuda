let escrowProcessMsg = getParameter('escrowProcessMsg');
// let orderNo = getParameter('orderNo');

if(escrowProcessMsg ==='ok'){
    alertMobile('거래가 추가 되었습니다. \n' +
        ' 안전 거래 계좌정보를 확인해 주세요');

}else if(escrowProcessMsg ==='not'){
    alertMobile('요청이 정상 처리되지 않았습니다.');
}




// if (performance.navigation.type === 1) {
//     window.location.href = '/purchaseSale';
// }


// confirmMobile("탁송을 진행하시겠어요?");



let orderNo = getParameter('orderNo');



function csmRequest(){
    $.ajax({
        url : '/user/getStatus?orderNo='+orderNo,
        type: 'GET' ,
        // data : csmCancelData,
        dataType : 'json',
        contentType : 'application/json; charset=UTF-8',
        success : function (data){

            if(data.status === '0'){

                alertMobile("거래대금 입금 완료후 위탁배송 신청이 가능해요.<br>입금진행후 다시 신청해주세요." );

            }else{
                locationH('/user/consignment?orderNo='+orderNo);
            }


        },error:function(request,status,error){
            alert("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
        }

    })
}

function csmCancel(orderNo){

    // let csmCancelData = JSON.stringify({
    //     "orderNo": orderNo
    // });


    $.ajax({
        url : '/user/api/orderCancel?orderNo='+orderNo,
        type: 'GET' ,
        // data : csmCancelData,
        dataType : 'json',
        contentType : 'application/json; charset=UTF-8',
        success : function (data){

            if(data.resultCd === '0000'){

                alertMobile("배송취소가 정상적으로 처리 되었어요." , function(){
                    locationH('/user/purchaseSale');
                });

            }else{
                alertMobile(data.resultMsg, function(){
                    locationH('/user/purchaseSale');
                });

            }


        },error:function(request,status,error){
            alert("code:"+request.status+"\n"+"message:"+request.responseText+"\n"+"error:"+error);
        }

    })
}



$(document).ready(function(){

    $('.csmBtn').click(function (){

            locationH('/user/consignment?orderNo='+orderNo);

    })



})