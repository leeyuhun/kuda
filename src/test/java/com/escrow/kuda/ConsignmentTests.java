package com.escrow.kuda;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit4.SpringRunner;

@Slf4j
@SpringBootTest
@ExtendWith(SpringExtension.class)
@RunWith(SpringRunner.class)
//@ActiveProfiles({"${spring.config.activate.on-profile}"})
@ActiveProfiles("local")
class ConsignmentTests {


}