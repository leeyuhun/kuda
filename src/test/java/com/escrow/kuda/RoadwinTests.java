package com.escrow.kuda;

import com.escrow.kuda.config.SecurityConfig;
import com.escrow.kuda.controller.RoadwinRestController;
import com.escrow.kuda.model.Seller;
import com.escrow.kuda.model.oauth.SessionUser;
import com.escrow.kuda.service.RoadwinServiceImpl;
import com.escrow.kuda.util.SecurityKey;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.json.simple.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import org.springframework.hateoas.MediaTypes;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.time.LocalTime;
import java.util.Locale;
import java.util.concurrent.ConcurrentHashMap;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest
        ( controllers = RoadwinRestController.class
        , excludeFilters = { @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, classes = SecurityConfig.class) })
@Slf4j
@ExtendWith(SpringExtension.class)
@RunWith(SpringRunner.class)
@ActiveProfiles("local")
public class RoadwinTests {

    @Autowired
    MockMvc mockMvc;

    @Autowired
    ObjectMapper objectMapper;

    @MockBean
    private RoadwinServiceImpl roadwinService;

    @Autowired
    private WebApplicationContext webApplicationContext;



    @Before()
    public void setup()
    {
        //Init MockMvc Object and build
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();

        Seller seller = new Seller();

        seller.setSeller_name("이유훈");
        seller.setSeller_email("dldbgns1@naver.com");
        seller.setSeller_picture("");
        seller.setSeller_code(10013);
        seller.setSeller_state("Y");
        seller.setSeller_ph("01022051229");
        seller.setLogin_grade("ADMIN");


        SessionUser sessionUser = new SessionUser(seller);

    }


    @SneakyThrows
    @DisplayName("로드윈 상태변경")
    @Test
    public void roadwinStatus(){

        mockMvc.perform(post("http://localhost/rwin/statusChange")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaTypes.HAL_JSON)
                .header("Authorization" , "apiKey 54afbee2-126d-44c2-a64d-43fa7dbfb76b")
                .content("{\"data\":\"0qw1nMqkX5q8P4krfL0FQsRXVgRCs57uGBKfTfv8UIjKByuwYHuLq7Dcv4SCMbklNEdgAvyN3iqeStfr30eG8w==\"}")
        )
                .andDo(print())
                .andExpect(status().isOk());

    }


    @SneakyThrows
    @DisplayName("로드윈")
    @Test
    public void roadwinDecode(){
        log.info( SecurityKey.decodeKey("0qw1nMqkX5q8P4krfL0FQsRXVgRCs57uGBKfTfv8UIjKByuwYHuLq7Dcv4SCMbklNEdgAvyN3iqeStfr30eG8w=="));

    }

    @SneakyThrows
    @DisplayName("로드윈")
    @Test
    public void roadwinEncode(){
        log.info( SecurityKey.encodeKey("{\"RW_ID\" : \"RW20210930142841_B19\",\"STATUS_CODE\" : \"S\"}"));
    }



    @SneakyThrows
    @Test
    @DisplayName("테스트")
//    @WithMockUser(roles="SELLER")
    public void createEvent(){

        String param = "{\"data\":\"0qw1nMqkX5q8P4krfL0FQsRXVgRCs57uGBKfTfv8UIjKByuwYHuLq7Dcv4SCMbklNEdgAvyN3iqeStfr30eG8w==\"}";

//        ConcurrentHashMap<String ,String> map = objectMapper.readValue(SecurityKey.decodeKey(param.get("data").toString()).toUpperCase(Locale.ROOT) , ConcurrentHashMap.class);

        mockMvc.perform(post("http://kuda.chatoss.com/rwin/statusChange")
//        mockMvc.perform(post("http://localhost/rwin/statusChange")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaTypes.HAL_JSON)
                .header("Authorization" , "apiKey 54afbee2-126d-44c2-a64d-43fa7dbfb76b")
                .characterEncoding("utf-8")
                .content(param)
        )
                .andDo(print())
                .andExpect(status().isOk());

    }




    @Test
    public void mapNullTest(){

        ConcurrentHashMap map = new ConcurrentHashMap();

        String aa = (String) map.get("LINK_VIEW");

        if(aa ==null){
            System.out.println("null");
        }else{
            System.out.println("not null");
        }

    }


    @SneakyThrows
    @Test
    public void jsonTest(){

        JSONObject object = new JSONObject();

        object.put("data" , "0qw1nMqkX5q8P4krfL0FQsRXVgRCs57uGBKfTfv8UIjKByuwYHuLq7Dcv4SCMbklNEdgAvyN3iqeStfr30eG8w==");

        ObjectMapper mapper = new ObjectMapper();


        ConcurrentHashMap<String ,String> map = mapper.readValue(SecurityKey.decodeKey(object.get("data").toString()).toUpperCase(Locale.ROOT) , ConcurrentHashMap.class);


        System.out.println(map);
    }
    
    
    
    @Test
    public void timeTests(){
        LocalTime lt = LocalTime.now();


        System.out.println("lt.isBefore(LocalTime.of(17,00) = " + lt.isBefore(LocalTime.of(17,00)));
        System.out.println("lt = " + lt);
    }




}
