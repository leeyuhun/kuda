package com.escrow.kuda.service;


import lombok.extern.slf4j.Slf4j;
import org.json.simple.JSONObject;
import org.junit.Test;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Slf4j
@ExtendWith(SpringExtension.class)
@RunWith(SpringRunner.class)
//@ActiveProfiles({"${spring.config.activate.on-profile}"})
@SpringBootTest(properties = "spring.profiles.active:local")
public class HyphenApiServiceImplTest {



    @Test
    @DisplayName("하이픈 API 통신")
    public void  hyphenApiAccessTests(){


        String baseURL = "https://cmsapitest.ksnet.co.kr/ksnet/";
//        String baseURL = "https://cmsapi.ksnet.co.kr/ksnet/";
        HashMap<String , Object> map = new HashMap<>();

        HttpHeaders headers = new HttpHeaders();
        HttpEntity<?> entity = new HttpEntity<>(headers);

        headers.set("Content-Type" , "application/x-www-form-urlencoded;charset=utf-8;");


        HashMap<String , Object> reqData = new HashMap<>();
        map.put("ekey" , "D563E76BB6A8A60C62DA5F86320F9A32");
        map.put("msalt" , "MA01");
        map.put("kscode" , "6633");

        reqData.put("seqNo" , "000001");
        reqData.put("accountNo" , "110356378174");
        reqData.put("compCode" , "CHATOSS1");
        reqData.put("bankCode" , "088");

        List<HashMap<String,Object>> list = new ArrayList<>();

        list.add(reqData);
        map.put("reqdata",list);

        

        JSONObject object = new JSONObject(map);


        HashMap<String, Object> jsonObject = new HashMap<>();
        jsonObject.put("JSONData" , object);

        System.out.println(object);

        System.out.println("jsonObject = " + jsonObject);


        String url =baseURL +"rfb/retail/inquiry/balance";

        System.out.println(url);

//        ResponseEntity<String> res1 = new RestTemplate().getForEntity(url, String.class, "JSONData="+object);
        ResponseEntity<String> res = new RestTemplate().postForEntity(url, "JSONData="+object, String.class);
        System.out.println(res.getBody());
        System.out.println(res.getStatusCodeValue());

    }

}
