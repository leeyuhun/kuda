package com.escrow.kuda.service;

import com.escrow.kuda.model.admin.SMSAutoSubmit;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit4.SpringRunner;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;


@Slf4j
@ExtendWith(SpringExtension.class)
@RunWith(SpringRunner.class)
//@ActiveProfiles({"${spring.config.activate.on-profile}"})
@SpringBootTest(properties = "spring.profiles.active:local")
public class ApiServiceImplTest {

    @Autowired
    ApiServiceImpl apiService;

    @Test
    @DisplayName("문자자동전달 API")
    public void escrowSmsAutoSubmit() {

        SMSAutoSubmit sm = new SMSAutoSubmit();

        sm.setData(null);
        sm.setFrom("15778000");
        sm.setTo("01022051229");
        sm.setMessage("[Web발신]\n" +
                "신한07/29 10:23\n" +
                "100-035-645669\n" +
                "입금          1\n" +
                "잔액      1,789\n" +
                " 이유훈");
        
        String[] aa = sm.getMessage().split("\n");

        for (String s : aa) {
            System.out.println("s = " + s);
        }

        System.out.println("aa[3].substring(0,2) = " + aa[3].substring(0,2));
        apiService.escrowSmsAutoSubmit(sm);


    }


    @Test
    @DisplayName("시간계산")
    public void timeTests(){

        LocalDateTime ndt = LocalDateTime.now();

        LocalDateTime dateTime = ndt.plusDays(1);

        System.out.println("dateTime = " + dateTime);
        
        LocalDate nd = LocalDate.now();

        System.out.println("nd.plusDays(1).format(DateTimeFormatter.ISO_DATE) = " + nd.plusDays(1).format(DateTimeFormatter.ISO_DATE));
    }


    @Test
    public void tettt(){
        String aa = "1234";


        System.out.println("driver = " + aa.substring(3,4));
    }
}