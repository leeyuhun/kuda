package com.escrow.kuda;


import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;

@Slf4j
@SpringBootTest
@ExtendWith(SpringExtension.class)
@RunWith(SpringRunner.class)
//@ActiveProfiles({"${spring.config.activate.on-profile}"})
@ActiveProfiles("local")
public class CrawlingTests {


    private WebDriver driver;
    private WebElement element;


    @Test
    @DisplayName("크롤링 테스트")
    public void crawling() throws IOException {

//        String url ="http://www.ssg.com/item/itemView.ssg?ckwhere=ssg_criteo&itemId=1000061865929";
        String url ="https://www.amazon.com/s?k=%EC%95%84%EB%A7%88%EC%A1%B4&language=ko_KR&adgrpid=78907565662&gclid=Cj0KCQjwvO2IBhCzARIsALw3ASrM92n2BkXVGVIxAxZaLnHd4yPioM96T0xxX7-qLvTPHs-KZu11a_MaAh28EALw_wcB&hvadid=393471688716&hvdev=c&hvlocphy=1009875&hvnetw=g&hvqmt=e&hvrand=17345622672469257246&hvtargid=kwd-11635163316&hydadcr=10782_10985474&tag=hydglogoo-20&ref=pd_sl_e3fpf5myf_e";

//        Document doc = Jsoup.connect(url).get();

//        Elements contents = doc.select("div#search_result");

//        log.info(doc.toString());

        String webDriverId = "webdriver.chrome.driver";
        String webDriverPath = "C:/chromedriver.exe";

        System.setProperty(webDriverId , webDriverPath);

        ChromeOptions options = new ChromeOptions();
        options.setCapability("ignoreProtectedModeSettings", true);
        driver = new ChromeDriver(options);



            try {
                driver.get("https://www.naver.com");

                //로그인 버튼 클릭
                element = driver.findElement(By.xpath("//*[@id=account]/a"));
                element.click();

                //아이디 입력
                element = driver.findElement(By.id("id"));
                Thread.sleep(500);
                element.sendKeys("dldbgns1");

                //비밀번호 입력
                element = driver.findElement(By.id("pw"));
                Thread.sleep(500);
                element.sendKeys("lyh123!!");

                //전송
                element = driver.findElement(By.className("btn_global"));
                element.submit();

                Thread.sleep(10000);

            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                driver.close();
            }


    }






}

