package com.escrow.kuda;

import com.escrow.kuda.model.kakao.address.GetLonLat;
import com.escrow.kuda.model.kakao.address.KaKaoMap;
import com.escrow.kuda.model.roadwin.PriceCheckReq;
import com.escrow.kuda.model.roadwin.RoadwinCancelReq;
import com.escrow.kuda.service.SellerServiceImpl;
import com.escrow.kuda.util.DateReturn;
import com.escrow.kuda.util.SecurityKey;
import com.escrow.kuda.util.infobank.MessageSubmit;
import com.escrow.kuda.util.kakaoaddress.KakaoAddress;
import com.escrow.kuda.util.message.MessageContent;
import com.escrow.kuda.util.roadwin.Roadwin;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.*;


@Slf4j
@SpringBootTest(properties = "spring.profiles.active:local")
@ExtendWith(SpringExtension.class)
@RunWith(SpringRunner.class)
//@ActiveProfiles({"${spring.config.activate.on-profile}"})
@ActiveProfiles("local")
//@AutoConfigureMockMvc
class EscrowApplicationTests {



//    @Autowired
//    protected MockMvc mockMvc;


//    @Autowired
//    protected ObjectMapper objectMapper;



    @Autowired
    MessageSubmit messageSubmit;


    @Autowired
    SellerServiceImpl sellerService;

    @Autowired
    MessageContent messageContent;
//
//
//    @Test
//    void contextLoads() {
//    }
//
//
//    @Test
//    void escrowSchedule(){
//
//        DateReturn dr = new DateReturn();
//        System.out.println(dr.escrowCalDatetime());
//    }
//
//
//
//    /*
//     * sellerList 검색
//     * */
//    @Test
//    public void searchTest(){
//
//        SearchWhere searchWhere = new SearchWhere();
//
//        searchWhere.setSearchDiv("ja");
//        searchWhere.setSearchName("마마");
////        searchHash.put("userCode" , "10013");
//
//        sellerService.searchSellerList(searchWhere).forEach(
//                (x) -> System.out.println("listItem"+x)
//
//        );
//
//    }









    /*
    *판매자 나머지 정보 입력
    * */
//    public void updateSellerRemainInfo(){
//
//        UpRemainInfo upRemainInfo = new UpRemainInfo();
//
//        String aa = sellerService.updateSellerRemainInfo(upRemainInfo);
//
//    }


    /*
     *거래 진행
     *생성테이블3개 ESCROW_LIST,ESCROW_REQUESTER , ESCROW_SELLER
     * */
//    public void escrowProcess(){

//        EscrowProcess ep = new EscrowProcess();
//        String a =sellerService.escrowProcess(ep);

//    }


    /*
    *거래 리스트
    * */
//    public void escrowList(){
//
//        int userCode = 0;
//
//        List<BuyList> buyLists = sellerService.buyLists(userCode);
//        List<SellList> sellLists = sellerService.sellLists(userCode);



//    @Test
//    public void httpTemplateTests(){
//
//        HttpComponentsClientHttpRequestFactory factory = new HttpComponentsClientHttpRequestFactory();
//
////        factory.setConnectTimeout(5);
////        factory.setReadTimeout(5);
//
//        HttpHeaders headers = new HttpHeaders();
//
//
//        HttpEntity<?> entity = new HttpEntity<>(headers);
//
//        RestTemplate restTemplate = new RestTemplate( );
//
//        headers.set("X-IB-Client-Id" , "cidauto_rest");
////        headers.set("X-IB-Client-Id" , "ID");
//        headers.set("X-IB-Client-Passwd" , "49401L10TM756104YCE0");
////        headers.set("X-IB-Client-Passwd" , "Password");
//        headers.set("Accept" , "application/json");
//
//
//        ResponseEntity<Map> responseEntity = restTemplate.exchange(
//                "https://auth.supersms.co:7000/auth/v3/token",
//                HttpMethod.POST,
//                entity,
//                Map.class
//        );
//
//        HashMap linkedHashMap = (HashMap) responseEntity.getBody();
//
//
//
//
//    }


//    @Test
//    public void infoBankTests(){
//
//        TokenIssuance ti = new TokenIssuance();
//
//        HttpComponentsClientHttpRequestFactory factory = new HttpComponentsClientHttpRequestFactory();
//
//        factory.setConnectTimeout(5);
//        factory.setReadTimeout(5);
//
//        HttpHeaders headers = new HttpHeaders();
//
//
//        JSONObject jsonObject = new JSONObject();
//        JSONObject destinationsJO = new JSONObject(); //destinations jsonArray 담을 object
//
//
//        JSONArray jsonArray = new JSONArray();
//
//
////        jsonArray.put()
//
//        try {
//            jsonObject.put("title" , "테스트입니다");
//            jsonObject.put("from" , "16708055");
//            jsonObject.put("text" , "테스트내용");
//            jsonObject.put("ttl" , 86400);
//
//            jsonArray.put(destinationsJO.put("to","821022051229"));
//            jsonObject.put("destinations" , jsonArray);
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//
//
//        String jsonString = jsonObject.toString();
//
//        HttpEntity<?> entity = new HttpEntity<>(jsonString , headers);
//
//        RestTemplate restTemplate = new RestTemplate( );
//
//        headers.set("Authorization" ,ti.getToken().getSchema() + " " + ti.getToken().getAccessToken() );
//        headers.set("Accept" , "application/json;");
//        headers.set("Content-Type" , "application/json;");
//
//
//        ResponseEntity<HashMap> responseEntity = restTemplate.exchange(
//                "https://sms.supersms.co:7020/sms/v3/multiple-destinations",
//                HttpMethod.POST,
//                entity,
//                HashMap.class
//        );
//
//
//        HashMap responseHash = (HashMap) responseEntity.getBody();
//
//        JSONObject resJO = new JSONObject(responseHash);
//
//        System.out.println( responseEntity.getBody() );
//
//        System.out.println(resJO);
//
//        MessageSubmitResult messageSubmitResult =new MessageSubmitResult();
//
//
//
//
//    }


/*    @Test
    public void parsingTest(){
        JSONObject jsonObject = new JSONObject();


        HashMap<String, Object> hashMap = new HashMap<>();

        JSONArray jsonArray = new JSONArray();
        jsonArray.put("{\"messageId\"=\"20210427120958674A0262959974-0\", \"to\"=\"821022051229\", \"status=R000\", \"errorText\"=\"\"}");

        hashMap.put("ref" , null);
        hashMap.put("toCount" , "1");
        hashMap.put("groupId" , "20210427120958674A0262959974");
        hashMap.put("destinations" , jsonArray);




        JSONObject jsonObject1 = new JSONObject( hashMap);

        try {
            System.out.println(   jsonObject1.get("ref") );
            System.out.println(   jsonObject1.get("toCount") );
            System.out.println(   jsonObject1.get("groupId") );
            System.out.println(   jsonObject1.get("destinations") );
            System.out.println(   jsonObject1.toString() );

            System.out.println(jsonArray.get(0));
            jsonArray = (JSONArray) (jsonObject1.get("destinations"));



//            jsonObject = (JSONObject) jsonArray.get(0);

//            System.out.println("messageId : " + jsonObject.get("messageId"));;
//            System.out.println("messageId : " + jsonObject.get("to"));;



        } catch (JSONException e) {
            e.printStackTrace();
        }






        ObjectMapper objectMapper = new ObjectMapper();

        MessageSubmitResult messageSubmitResult = objectMapper.readValue(jsonObject1 , MessageSubmitResult.class);

        System.out.println(messageSubmitResult.getToCount());






    }*/


/*    @Test
    public void returnAAA(){

        messageSubmit.submitMSG("01022051229" ,messageContent.messageContent1_seller("이유훈") );

    }


    @Test
    public void returnMessage(){

        String returnStr = sellerService.inviteMessage("01022051229", 10013);

        System.out.println( returnStr );

        if(returnStr == null){
            System.out.println("null");
        }else if(returnStr.equals("")){
            System.out.println(" not null");
        }


    }

}*/



    @Test
    @DisplayName("카카오 주소로 위도경도 불러오기")
    public void xyReturn_T(){

        KakaoAddress kakaoAddress = new KakaoAddress();
//        kakaoAddress.xyReturn("경기도 성남시 수정구 수정남로 129-1 205호");

        System.out.println( kakaoAddress.xyReturn("경기도 성남시 수정구 수정남로 129-1 205호").getBody() );


        ResponseEntity<KaKaoMap> documents = kakaoAddress.xyReturn("경기도 성남시 수정구 수정남로 129-1 205호");

        List<GetLonLat> list = Objects.requireNonNull(documents.getBody()).getDocuments();



        System.out.println(list.get(0).getX());
        System.out.println(list.get(0).getY());


    }



    @Test
    @DisplayName("로드윈 api 접근테스트")
    public void approachTest() throws InvalidAlgorithmParameterException, UnsupportedEncodingException, NoSuchPaddingException, IllegalBlockSizeException, NoSuchAlgorithmException, BadPaddingException, InvalidKeyException, ParseException, JsonProcessingException {


        JSONObject jsonObject = new JSONObject();
        JSONObject jsonObject1 = new JSONObject();




        jsonObject.put("START_ADDR" , "서울특별시 마포구 토정로31길 31, 1층");
        jsonObject.put("START_LAT" , 37.54233333);
        jsonObject.put("START_LON" , 126.94296944);
        jsonObject.put("END_ADDR" , "경기도 하남시 미사대로 550,현대지식산업센터 1차지하 1층 ");
        jsonObject.put("END_LAT" , 37.55649110);
        jsonObject.put("END_LON" , 127.20459160);

        jsonObject1.put("data" , SecurityKey.encodeKey( jsonObject.toString() ) );

        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        HttpEntity<?> entity = new HttpEntity<>(jsonObject1 , headers );

        String url  = "https://api.roadwin.shop/vancar/service/priceCheck";
        String apiKey = "54afbee2-126d-44c2-a64d-43fa7dbfb76b";

        headers.set("apiKey",apiKey);
        headers.set("Content-Type","application/json");
        headers.set("Accept","application/json;charset=UTF-8");

        ResponseEntity<Map> responseEntity = restTemplate.exchange(
                url,
                HttpMethod.POST,
                entity,
                Map.class
        );


        HashMap hashMap = (HashMap) responseEntity.getBody();

        System.out.println( SecurityKey.decodeKey((String) hashMap.get("data")));

        Map<String, Object> map  = new ObjectMapper().readValue( SecurityKey.decodeKey((String) hashMap.get("data")), Map.class);

        System.out.println(map.get("PRICE"));

    }

    @Test
    @DisplayName("로드윈 전문 디코드 테스트")
    public void roadwinDecodeTests() throws InvalidAlgorithmParameterException, UnsupportedEncodingException, NoSuchPaddingException, IllegalBlockSizeException, NoSuchAlgorithmException, BadPaddingException, InvalidKeyException {

        System.out.println( SecurityKey.decodeKey("mAXCGEa6gP37gAK6ewxbNNbgRCdSrC2aI2Cuhw6rCrdn+OFzRZRELjN9XOQ+met9DCZ1wUtn/kJskD92ph9A1A=="));

    }


    @Test
    @DisplayName("로드윈 전문 인코딩 테스트") //기사배정 assign
    public void roadwinEncodeTests1() throws InvalidAlgorithmParameterException, UnsupportedEncodingException, NoSuchPaddingException, IllegalBlockSizeException, NoSuchAlgorithmException, BadPaddingException, InvalidKeyException {
        System.out.println( SecurityKey.encodeKey("{\"TRANS_ID\" : \"20220920043631663-651705\",\n" +
                "\"MEMO\" : \"빠른배차 부탁드립니다\"\n}"));
    }

    @Test
    @DisplayName("로드윈 전문 인코딩 테스트") //기사출발 statusChange =S
    public void roadwinEncodeTests2() throws InvalidAlgorithmParameterException, UnsupportedEncodingException, NoSuchPaddingException, IllegalBlockSizeException, NoSuchAlgorithmException, BadPaddingException, InvalidKeyException {

        System.out.println( SecurityKey.encodeKey("{\n" +
                "\"RW_ID\" : \"RW20220823143518_D3F\",\n" +
                "\"STATUS_CODE\" : \"S\",\n" +
                "\"SERVICE_VIEW_LINK\" : \"https://roadwin.shop/pic?RW20210928102030_ABC\"\n" +
                "}\n"));
    }


    @Test
    @DisplayName("로드윈 전문 인코딩 테스트") //전달완료 statusChange =D
    public void roadwinEncodeTests3() throws InvalidAlgorithmParameterException, UnsupportedEncodingException, NoSuchPaddingException, IllegalBlockSizeException, NoSuchAlgorithmException, BadPaddingException, InvalidKeyException {

        System.out.println( SecurityKey.encodeKey("{\n" +
                "\"RW_ID\" : \"RW20220823143518_D3F\",\n" +
                "\"STATUS_CODE\" : \"D\"\n" +
                "}\n"));
    }


    @Test
    @DisplayName("JSON to Map")
    public void JSONtoMapTest() throws ParseException, JsonProcessingException {

        String jsonStr = "{\"DISTANCE\":34,\"RESULT_CODE\":\"0000\",\"RESULT_MSG\":\"SUCCESS\",\"PRICE\":32000,\"RW_ID\":\"RW20211025143212_504\"}";
        JSONParser parser = new JSONParser();
        Object obj = parser.parse( jsonStr );



        JSONObject jsonObj = (JSONObject) obj;

        System.out.println(jsonObj);


        Map<String, Object> map = null;


        map = new ObjectMapper().readValue(jsonStr, Map.class);

        System.out.println(map);


    }


    @Test
    @DisplayName("priceCheckTest")
    public void priceCheckTest() throws InvalidAlgorithmParameterException, UnsupportedEncodingException, NoSuchPaddingException, IllegalBlockSizeException, NoSuchAlgorithmException, BadPaddingException, InvalidKeyException, JsonProcessingException {

        PriceCheckReq priceCheckReq = new PriceCheckReq();


        priceCheckReq.setStart_ADDR("서울특별시 마포구 토정로31길 31, 1층");
        priceCheckReq.setStart_LAT(37.54233333);
        priceCheckReq.setStart_LON(126.94296944);
        priceCheckReq.setEnd_ADDR("경기도 하남시 미사대로 550,현대지식산업센터 1차지하 1층");
        priceCheckReq.setEnd_LAT(37.55649110);
        priceCheckReq.setEnd_LON(127.20459160);

        System.out.println(Roadwin.priceCheck(priceCheckReq));

//        ObjectMapper mapper = new ObjectMapper();
//        String jsonString = mapper.writeValueAsString( priceCheckReq );
//
//        System.out.println(jsonString.toUpperCase(Locale.ROOT));
    }


    @Test
    @DisplayName("로드윈 취소")
    public void roadwinCancel() throws InvalidAlgorithmParameterException, UnsupportedEncodingException, NoSuchPaddingException, IllegalBlockSizeException, NoSuchAlgorithmException, BadPaddingException, InvalidKeyException, JsonProcessingException {

        RoadwinCancelReq roadwinCancelReq = new RoadwinCancelReq();

        roadwinCancelReq.setRw_ID("RW20211119155504_67D");
        roadwinCancelReq.setCancel_REASON("고객취소");

        log.info( Roadwin.cancel(roadwinCancelReq).toString() );

    }


//    @SneakyThrows

    @DisplayName("로드윈 상태변경")
    @Test
    public void roadwinStatusChange(){

    }

    @DisplayName("문자 테스트")
    @Test
    public void sendMessage(){
        messageSubmit.submitMSG("01022051229" , "테스트입니다");

    }


    @DisplayName("TTT")
    @Test
    @SneakyThrows
    public void calDateTime(){
        System.out.println(new DateReturn().escrowCalDatetime());

        String url = "HTTPS://ROADWIN.SHOP/VIEWPIC?COMP=0&ID=RW20220819150316_827";

        String SERVICE_VIEW_LINK = url.toLowerCase(Locale.ROOT).replaceAll("rw","RW");

        System.out.println("SERVICE_VIEW_LINK = " + SERVICE_VIEW_LINK);
    }




}