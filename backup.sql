-- MySQL dump 10.13  Distrib 8.0.22, for Win64 (x86_64)
--
-- Host: localhost    Database: chatoss
-- ------------------------------------------------------
-- Server version	8.0.22

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `admin`
--

DROP TABLE IF EXISTS `admin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `admin` (
  `admin_code` int NOT NULL,
  `login_id` varchar(50) NOT NULL,
  PRIMARY KEY (`admin_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin`
--

LOCK TABLES `admin` WRITE;
/*!40000 ALTER TABLE `admin` DISABLE KEYS */;
INSERT INTO `admin` VALUES (1,'admin');
/*!40000 ALTER TABLE `admin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `area`
--

DROP TABLE IF EXISTS `area`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `area` (
  `area_code` varchar(5) NOT NULL COMMENT '지약전화번호 ?',
  `area_name` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`area_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `area`
--

LOCK TABLES `area` WRITE;
/*!40000 ALTER TABLE `area` DISABLE KEYS */;
/*!40000 ALTER TABLE `area` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bank`
--

DROP TABLE IF EXISTS `bank`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `bank` (
  `bank_code` varchar(4) NOT NULL,
  `bank_name` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`bank_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bank`
--

LOCK TABLES `bank` WRITE;
/*!40000 ALTER TABLE `bank` DISABLE KEYS */;
INSERT INTO `bank` VALUES ('002','산업은행'),('003','기업은행'),('004','국민은행'),('005','외환은행'),('007','수협'),('008','수출입은행'),('011','농협중앙회'),('012','단위농협(축협)'),('020','우리은행'),('023','SC제일은행'),('027','씨티은행'),('031','대구은행'),('032','부산은행'),('034','광주은행'),('035','제주은행'),('037','전북은행'),('039','경남은행'),('045','새마을금고'),('048','신협'),('050','상호저축은행'),('065','대화은행'),('071','우체국'),('081','하나은행'),('088','신한은행'),('089','케이뱅크'),('090','카카오뱅크');
/*!40000 ALTER TABLE `bank` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `binnumer`
--

DROP TABLE IF EXISTS `binnumer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `binnumer` (
  `card_binnumber` varchar(6) NOT NULL,
  `card_name` varchar(4) DEFAULT NULL,
  PRIMARY KEY (`card_binnumber`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `binnumer`
--

LOCK TABLES `binnumer` WRITE;
/*!40000 ALTER TABLE `binnumer` DISABLE KEYS */;
/*!40000 ALTER TABLE `binnumer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `capital_push_history`
--

DROP TABLE IF EXISTS `capital_push_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `capital_push_history` (
  `cph_code` bigint NOT NULL,
  `cph_ph` varchar(11) DEFAULT NULL,
  `seller_code` int NOT NULL,
  PRIMARY KEY (`cph_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `capital_push_history`
--

LOCK TABLES `capital_push_history` WRITE;
/*!40000 ALTER TABLE `capital_push_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `capital_push_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `complain`
--

DROP TABLE IF EXISTS `complain`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `complain` (
  `complain_code` bigint NOT NULL,
  `complain_customer_name` varchar(50) DEFAULT NULL,
  `complain_customer_ph` varchar(11) DEFAULT NULL,
  `complain_reason` varchar(200) DEFAULT NULL,
  `complain_creation_date` varchar(10) DEFAULT NULL,
  `complain_creation_time` varchar(8) DEFAULT NULL,
  `complain_completion_date` varchar(10) DEFAULT NULL,
  `complain_completion_time` varchar(8) DEFAULT NULL,
  `complain_state` varchar(1) DEFAULT NULL,
  `yr_code` varchar(25) NOT NULL,
  PRIMARY KEY (`complain_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `complain`
--

LOCK TABLES `complain` WRITE;
/*!40000 ALTER TABLE `complain` DISABLE KEYS */;
/*!40000 ALTER TABLE `complain` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `corporate`
--

DROP TABLE IF EXISTS `corporate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `corporate` (
  `corporate_code` int NOT NULL,
  `corporate_name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`corporate_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `corporate`
--

LOCK TABLES `corporate` WRITE;
/*!40000 ALTER TABLE `corporate` DISABLE KEYS */;
INSERT INTO `corporate` VALUES (1,'notName');
/*!40000 ALTER TABLE `corporate` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `e_remittance_info`
--

DROP TABLE IF EXISTS `e_remittance_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `e_remittance_info` (
  `eri_code` bigint NOT NULL,
  `eri_account_number` varchar(30) DEFAULT NULL,
  `eri_account_holder` varchar(50) DEFAULT NULL,
  `eri_amount` bigint DEFAULT NULL,
  `rr_status` varchar(1) DEFAULT NULL,
  `bank_code` varchar(4) NOT NULL,
  `esl_code` varchar(25) NOT NULL,
  `seller_code` int NOT NULL,
  PRIMARY KEY (`eri_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `e_remittance_info`
--

LOCK TABLES `e_remittance_info` WRITE;
/*!40000 ALTER TABLE `e_remittance_info` DISABLE KEYS */;
/*!40000 ALTER TABLE `e_remittance_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `escrow_list`
--

DROP TABLE IF EXISTS `escrow_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `escrow_list` (
  `esl_code` varchar(25) NOT NULL,
  `esl_payment_amount` bigint DEFAULT NULL,
  `esl_all_amount` bigint DEFAULT NULL,
  `esl_fee_amount` bigint DEFAULT NULL,
  `esr_code` varchar(25) NOT NULL,
  `ess_code` varchar(25) NOT NULL,
  `esl_request_date` varchar(10) DEFAULT NULL,
  `esl_request_time` varchar(8) DEFAULT NULL,
  `esl_end_date` varchar(10) DEFAULT NULL,
  `esl_end_time` varchar(8) DEFAULT NULL,
  `esl_product_name` varchar(100) DEFAULT NULL,
  `esl_state` varchar(1) DEFAULT '0',
  `esl_bank_code` varchar(3) DEFAULT NULL,
  `esl_bank_name` varchar(25) DEFAULT NULL,
  `esl_bank_number` varchar(30) DEFAULT NULL,
  `esl_exp_date` varchar(10) DEFAULT NULL,
  `esl_exp_time` varchar(8) DEFAULT NULL,
  `esl_auth_date` varchar(10) DEFAULT NULL,
  `esl_auth_time` varchar(8) DEFAULT NULL,
  `esl_address` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`esl_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `escrow_list`
--

LOCK TABLES `escrow_list` WRITE;
/*!40000 ALTER TABLE `escrow_list` DISABLE KEYS */;
INSERT INTO `escrow_list` VALUES ('20210413062032764-731257',0,100000,0,'10013','10011','2021-04-13','18:20:32',NULL,NULL,'싼타페-11가1111','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('20210413064039161-601552',0,11111111,0,'10013','10011','2021-04-13','18:40:39',NULL,NULL,'11111-111111','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('20210414034130067-063757',13000000,13000000,0,'20210414034130066-597769','20210414034130067-017047','2021-04-14','15:41:30',NULL,NULL,'테슬라-12다4567','4',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('20210414112148110-482178',1000000,1000000,0,'10013','10011','2021-04-14','11:21:48',NULL,NULL,'카니발-11가1111','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('20210414123831847-330665',100000,100000,0,'10013','10012','2021-04-14','12:38:31',NULL,NULL,'K5-11가1111','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('20210414125631513-725255',100000,100000,0,'20210414125631512-331286','20210414125631513-167507','2021-04-14','12:56:31',NULL,NULL,'그랜저-33다3333','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('20210420033644818-633049',1111111,1111111,0,'20210420033644817-824274','20210420033644818-091872','2021-04-20','15:36:44',NULL,NULL,'null-null','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('20210420033656908-523649',1111111,1111111,0,'20210420033656908-502726','20210420033656908-046266','2021-04-20','15:36:56',NULL,NULL,'null-null','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('20210420034020450-990769',150000,150000,0,'20210420034020450-888538','20210420034020450-518552','2021-04-20','15:40:20',NULL,NULL,'구형쏘나타-11가1111','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('20210420035604513-105483',15000000,15000000,0,'20210420035604513-640408','20210420035604513-955652','2021-04-20','15:56:04',NULL,NULL,'싼타페-11가1111','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('20210420035843192-933684',150000,150000,0,'20210420035843191-268035','20210420035843191-991480','2021-04-20','15:58:43',NULL,NULL,'싼타페-11가1111','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('20210420045351315-047792',3333333333,3333333333,0,'20210420045351315-509067','20210420045351315-556091','2021-04-20','16:53:51',NULL,NULL,'33333-13333','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('20210420045650403-678399',3333333333,3333333333,0,'20210420045650402-950580','20210420045650403-341904','2021-04-20','16:56:50',NULL,NULL,'33333-133333','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('20210420045715511-477522',24124124,24124124,0,'20210420045715511-297628','20210420045715511-219142','2021-04-20','16:57:15',NULL,NULL,'2141241-124214124','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('20210420045851670-405709',12333,12333,0,'20210420045851670-380333','20210420045851670-740270','2021-04-20','16:58:51',NULL,NULL,'123123-123213','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('20210420045915889-685519',231233,231233,0,'20210420045915889-471018','20210420045915889-860842','2021-04-20','16:59:15',NULL,NULL,'232131-1231231','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('20210420045958302-497185',1244,1244,0,'20210420045958302-774203','20210420045958302-741677','2021-04-20','16:59:58',NULL,NULL,'24124124-1231241','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('20210420050009429-543194',23,23,0,'20210420050009429-053385','20210420050009429-208373','2021-04-20','17:00:09',NULL,NULL,'23211-1231231','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('20210420050115753-796980',124124,124124,0,'20210420050115753-656473','20210420050115753-595480','2021-04-20','17:01:15',NULL,NULL,'124124-124124','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('20210420050211897-171506',124124,124124,0,'20210420050211896-458612','20210420050211896-872544','2021-04-20','17:02:11',NULL,NULL,'124124-124124','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('20210420050533551-340789',1500000,1500000,0,'20210420050533550-942319','20210420050533551-196708','2021-04-20','17:05:33',NULL,NULL,'싼타페-11가1111','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('20210421054551998-030543',10000,10000,0,'20210421054551997-447615','20210421054551998-488838','2021-04-21','17:45:51',NULL,NULL,'231231-1232131','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('20210421054850730-936711',10000,10000,0,'20210421054850730-259026','20210421054850730-257582','2021-04-21','17:48:50',NULL,NULL,'12311-123123','0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('20210423022203914-733999',0,1000000,0,'20210423022217259-021301','20210423022217260-356713','2021-04-23','14:22:17',NULL,NULL,'차종-차량번호','0','081','하나은행','15791879013537','20210503','235959','21-04-23','14:22:17',NULL),('20210423022938361-086097',1500000,1500000,0,'20210423022959444-806984','20210423022959444-055069','2021-04-23','14:29:59',NULL,NULL,'차종-차량번호','0','081','하나은행','15791586872537','20210503','235959','21-04-23','14:29:59',NULL),('20210423024009224-984305',10000,10000,0,'20210423024027017-876377','20210423024027017-464320','2021-04-23','14:40:27',NULL,NULL,'구형쏘나타-11가1111','0','020','우리은행','62134937118586','20210503','235959','21-04-23','14:40:27',NULL),('20210423034104174-940785',1111,1111,0,'20210423034159243-762550','20210423034159243-088339','2021-04-23','15:41:59',NULL,NULL,'1111-1111','0','031','대구은행','9600257848049','20210503','235959','21-04-23','15:41:59',NULL),('20210423035528141-781426',100000,100000,0,'20210423035548680-957277','20210423035548680-648248','2021-04-23','15:55:48',NULL,NULL,'K5-01가1067','0','004','국민은행','40119013034889','20210503','235959','21-04-23','15:55:49',NULL),('20210423113709989-526396',0,1000000,0,'20210423113728833-694595','20210423113728833-203854','2021-04-23','11:37:28',NULL,NULL,'차종-차량명','0','089','케이뱅크','70102004663142','20210503','235959','21-04-23','11:37:29',NULL);
/*!40000 ALTER TABLE `escrow_list` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `escrow_requester`
--

DROP TABLE IF EXISTS `escrow_requester`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `escrow_requester` (
  `esr_code` varchar(45) NOT NULL,
  `seller_code` int DEFAULT NULL,
  PRIMARY KEY (`esr_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `escrow_requester`
--

LOCK TABLES `escrow_requester` WRITE;
/*!40000 ALTER TABLE `escrow_requester` DISABLE KEYS */;
INSERT INTO `escrow_requester` VALUES ('1',NULL),('10013',NULL),('2',NULL),('20210414034130066-597769',10013),('20210414123831874-749155',10013),('20210414125631512-331286',10013),('20210420033644817-824274',10013),('20210420033656908-502726',10013),('20210420034020450-888538',10013),('20210420035604513-640408',10013),('20210420035843191-268035',10013),('20210420045351315-509067',10013),('20210420045650402-950580',10013),('20210420045715511-297628',10013),('20210420045851670-380333',10013),('20210420045915889-471018',10013),('20210420045958302-774203',10013),('20210420050009429-053385',10013),('20210420050115753-656473',10013),('20210420050211896-458612',10013),('20210420050533550-942319',10013),('20210421054551997-447615',10013),('20210421054850730-259026',10013),('20210422082350170-480325',0),('20210423022217259-021301',0),('20210423022959444-806984',0),('20210423024027017-876377',10013),('20210423034159243-762550',10013),('20210423035548680-957277',10013),('20210423113728833-694595',0);
/*!40000 ALTER TABLE `escrow_requester` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `escrow_seller`
--

DROP TABLE IF EXISTS `escrow_seller`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `escrow_seller` (
  `ess_code` varchar(45) NOT NULL,
  `seller_code` int DEFAULT NULL,
  PRIMARY KEY (`ess_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `escrow_seller`
--

LOCK TABLES `escrow_seller` WRITE;
/*!40000 ALTER TABLE `escrow_seller` DISABLE KEYS */;
INSERT INTO `escrow_seller` VALUES ('1',NULL),('10011',NULL),('2',NULL),('20210414034130067-017047',10013),('20210414123831872-904414',10012),('20210414125631513-167507',10012),('20210420033644818-091872',0),('20210420033656908-046266',0),('20210420034020450-518552',0),('20210420035604513-955652',10011),('20210420035843191-991480',10012),('20210420045351315-556091',10011),('20210420045650403-341904',10011),('20210420045715511-219142',10011),('20210420045851670-740270',10011),('20210420045915889-860842',10011),('20210420045958302-741677',10011),('20210420050009429-208373',10011),('20210420050115753-595480',10011),('20210420050211896-872544',10011),('20210420050533551-196708',10011),('20210421054551998-488838',10011),('20210421054850730-257582',10011),('20210422082350171-265535',0),('20210423022217260-356713',0),('20210423022959444-055069',0),('20210423024027017-464320',10011),('20210423034159243-088339',10011),('20210423035548680-648248',10012),('20210423113728833-203854',0);
/*!40000 ALTER TABLE `escrow_seller` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `holidate`
--

DROP TABLE IF EXISTS `holidate`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `holidate` (
  `holi_date` varchar(10) NOT NULL,
  `holi_name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`holi_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `holidate`
--

LOCK TABLES `holidate` WRITE;
/*!40000 ALTER TABLE `holidate` DISABLE KEYS */;
/*!40000 ALTER TABLE `holidate` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `loginid`
--

DROP TABLE IF EXISTS `loginid`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `loginid` (
  `login_id` varchar(50) NOT NULL,
  `login_grade` varchar(20) DEFAULT NULL COMMENT '0: admin\\r\\n1: seller',
  `login_pass` varchar(50) DEFAULT NULL,
  `login_regdate` char(10) DEFAULT NULL,
  PRIMARY KEY (`login_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `loginid`
--

LOCK TABLES `loginid` WRITE;
/*!40000 ALTER TABLE `loginid` DISABLE KEYS */;
INSERT INTO `loginid` VALUES ('AACWuUYd8Y60dKF2-sTso9GX','ADMIN','2BW61NKQRD','2021-03-12'),('admin','ADMIN','admin0000','2021-03-17'),('dldbgns1@gmail.com','SELLER','DK27356A5V','2021-02-26'),('dldbgns1@naver.com','SELLER','72RK8M3T0E','2021-03-02');
/*!40000 ALTER TABLE `loginid` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `n_payment`
--

DROP TABLE IF EXISTS `n_payment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `n_payment` (
  `np_code` varchar(25) NOT NULL,
  `np_result_message` varchar(100) DEFAULT NULL,
  `np_result_code` varchar(1) DEFAULT NULL,
  `np_pg_trade_number` varchar(30) DEFAULT NULL,
  `np_agree_number` varchar(20) DEFAULT NULL,
  `nr_code` varchar(25) NOT NULL,
  PRIMARY KEY (`np_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `n_payment`
--

LOCK TABLES `n_payment` WRITE;
/*!40000 ALTER TABLE `n_payment` DISABLE KEYS */;
/*!40000 ALTER TABLE `n_payment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `n_receipt`
--

DROP TABLE IF EXISTS `n_receipt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `n_receipt` (
  `nr_code` varchar(25) NOT NULL,
  `nr_calcul_date` varchar(10) DEFAULT NULL,
  `nr_fee_date` varchar(10) DEFAULT NULL,
  `nr_pg_calcul_date` varchar(10) DEFAULT NULL,
  `nr_deal_state` varchar(1) DEFAULT NULL,
  `nr_creation_date` varchar(10) DEFAULT NULL,
  `nr_creation_time` varchar(8) DEFAULT NULL,
  `nr_amount` bigint DEFAULT NULL,
  `yr_code` varchar(25) NOT NULL,
  PRIMARY KEY (`nr_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `n_receipt`
--

LOCK TABLES `n_receipt` WRITE;
/*!40000 ALTER TABLE `n_receipt` DISABLE KEYS */;
/*!40000 ALTER TABLE `n_receipt` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `n_receipt_fee`
--

DROP TABLE IF EXISTS `n_receipt_fee`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `n_receipt_fee` (
  `nrf_code` bigint NOT NULL,
  `nrf_default_per` varchar(5) DEFAULT NULL,
  `nrf_seller_per` varchar(5) DEFAULT NULL,
  `nrf_pg_per` varchar(5) DEFAULT NULL,
  `nrf_default_fee` bigint DEFAULT NULL,
  `nrf_seller_fee` bigint DEFAULT NULL,
  `nrf_pg_fee` bigint DEFAULT NULL,
  `nr_code` varchar(25) NOT NULL,
  PRIMARY KEY (`nrf_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `n_receipt_fee`
--

LOCK TABLES `n_receipt_fee` WRITE;
/*!40000 ALTER TABLE `n_receipt_fee` DISABLE KEYS */;
/*!40000 ALTER TABLE `n_receipt_fee` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `payment_history`
--

DROP TABLE IF EXISTS `payment_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `payment_history` (
  `ph_code` bigint NOT NULL AUTO_INCREMENT,
  `ph_card_name` varchar(20) DEFAULT NULL,
  `ph_card_number` varchar(20) DEFAULT NULL,
  `ph_pg_trade_number` varchar(30) DEFAULT NULL,
  `ph_result_message` varchar(100) DEFAULT NULL,
  `ph_result_code` varchar(2) DEFAULT NULL,
  `ph_agree_number` varchar(20) DEFAULT NULL,
  `ph_instalment` varchar(2) DEFAULT NULL,
  `ph_amount` bigint DEFAULT NULL,
  `yr_code` varchar(25) NOT NULL,
  `ps_code` varchar(25) NOT NULL,
  `ph_creation_date` varchar(10) DEFAULT NULL,
  `ph_creation_time` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`ph_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `payment_history`
--

LOCK TABLES `payment_history` WRITE;
/*!40000 ALTER TABLE `payment_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `payment_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pg`
--

DROP TABLE IF EXISTS `pg`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `pg` (
  `pg_code` int NOT NULL COMMENT 'PG가맹점 정보',
  `pg_name` varchar(50) DEFAULT NULL,
  `Field` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`pg_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pg`
--

LOCK TABLES `pg` WRITE;
/*!40000 ALTER TABLE `pg` DISABLE KEYS */;
INSERT INTO `pg` VALUES (1,'나이스페이테스트','not');
/*!40000 ALTER TABLE `pg` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pg_store`
--

DROP TABLE IF EXISTS `pg_store`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `pg_store` (
  `ps_code` varchar(25) NOT NULL,
  `ps_name` varchar(50) DEFAULT NULL,
  `ps_fee_per` varchar(5) DEFAULT NULL,
  `ps_crypto_key` varchar(50) DEFAULT NULL,
  `pg_code` int NOT NULL COMMENT 'PG가맹점 정보',
  `ps_calcul_cycle` varchar(10) NOT NULL,
  PRIMARY KEY (`ps_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pg_store`
--

LOCK TABLES `pg_store` WRITE;
/*!40000 ALTER TABLE `pg_store` DISABLE KEYS */;
INSERT INTO `pg_store` VALUES ('nicepay00m','NICEPAYTEST','3.3','asdf15a3153wr153wer',1,'1');
/*!40000 ALTER TABLE `pg_store` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `r_remittance_info`
--

DROP TABLE IF EXISTS `r_remittance_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `r_remittance_info` (
  `rr_code` bigint NOT NULL AUTO_INCREMENT,
  `rr_account_number` varchar(30) DEFAULT NULL,
  `rr_account_holder` varchar(50) DEFAULT NULL,
  `rr_amount` bigint DEFAULT NULL,
  `rr_status` varchar(1) DEFAULT '0',
  `yr_code` varchar(25) NOT NULL,
  `bank_code` varchar(4) NOT NULL,
  `seller_code` int DEFAULT NULL,
  `rr_creation_date` varchar(10) DEFAULT NULL,
  `rr_creation_time` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`rr_code`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `r_remittance_info`
--

LOCK TABLES `r_remittance_info` WRITE;
/*!40000 ALTER TABLE `r_remittance_info` DISABLE KEYS */;
INSERT INTO `r_remittance_info` VALUES (1,'110-311-703202','이유훈',10000,'9','','001',10001,NULL,NULL),(2,'110-311-703202','이유훈',100000,'9','','001',10001,NULL,NULL),(3,'110-311-703202','이유훈',10000,'9','','001',10001,NULL,NULL),(4,'110-311-703202','이유훈',100000,'9','','001',10001,NULL,NULL),(5,'110-311-703202','이유훈',100000,'9','20210217121905936-294226','001',10001,NULL,NULL),(6,'110-311-703202','이유훈',100,'9','20210217121905936-294226','001',10001,NULL,NULL),(7,'110-311-703204','릐유훈',1000000,'9','20210217121905936-294226','035',10001,NULL,NULL),(8,'110-311-703202','이유훈',1000,'9','20210217121905936-294226','001',10001,NULL,NULL),(9,'153153153','예금주',1000,'9','20210217121905936-294226','090',10001,NULL,NULL),(10,'예금주','1000',10000,'9','20210217121905936-294226','050',10001,NULL,NULL),(11,'100013','예금주',100,'9','20210217121905936-294226','071',10001,NULL,NULL),(12,'11013153155212','예금주',10000,'9','20210217121905936-294226','089',NULL,'2021-02-19','11:29:42'),(13,'예금주','1000',100000,'0','20210217121905936-294226','050',NULL,'2021-02-19','11:40:09'),(14,'110-311-703204','릐유훈',100000,'0','20210217121905936-294226','035',NULL,'2021-02-19','11:40:29'),(15,'이유훈','110-311-703202',1500,'0','20210217121905936-294226','088',NULL,'2021-02-19','11:49:33'),(16,'이유훈','110-311-703202',100,'0','20210217121905936-294226','088',NULL,'2021-02-19','11:50:03'),(17,'100013','예금주',100,'0','20210217121905936-294226','071',NULL,'2021-02-19','11:50:24'),(18,'이유훈','110-311-703202',1500,'0','20210217121905936-294226','088',NULL,'2021-02-19','11:50:44'),(19,'110-311-703204','릐유훈',1500,'0','20210217121905936-294226','035',NULL,'2021-02-19','11:50:51'),(20,'110-311-703202','이유훈',100,'0','20210217121905936-294226','088',NULL,'2021-02-19','12:03:10'),(21,'','',100000000000000,'0','20210217121905936-294226','023',NULL,'2021-02-19','13:53:47'),(22,'110311703202','예금주',10000,'0','20210304061524687-805235','039',NULL,'2021-03-17','10:47:39');
/*!40000 ALTER TABLE `r_remittance_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `request_cancel`
--

DROP TABLE IF EXISTS `request_cancel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `request_cancel` (
  `rc_code` bigint NOT NULL AUTO_INCREMENT,
  `rc_creation_date` varchar(10) DEFAULT NULL,
  `rc_creation_time` varchar(8) DEFAULT NULL,
  `rc_completion_date` varchar(10) DEFAULT '',
  `rc_completion_time` varchar(8) DEFAULT '',
  `rc_state` varchar(1) DEFAULT '0',
  `rc_account_number` varchar(50) DEFAULT '',
  `rc_name` varchar(50) DEFAULT '',
  `rc_amount` bigint DEFAULT NULL,
  `yr_code` varchar(25) NOT NULL,
  PRIMARY KEY (`rc_code`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `request_cancel`
--

LOCK TABLES `request_cancel` WRITE;
/*!40000 ALTER TABLE `request_cancel` DISABLE KEYS */;
INSERT INTO `request_cancel` VALUES (1,'2021-03-23','15:17:19','','','0','','',NULL,'0'),(2,'2021-03-23','15:35:02','','','1','','',11580,'20210304061524687-805235'),(3,'2021-03-23','16:58:51','','','1','','',11580,'20210304061524687-805235'),(4,'2021-03-23','17:00:04','','','1','','',11580,'20210304061524687-805235'),(5,'2021-03-23','17:43:43','','','1','','',11580,'20210304061524687-805235'),(6,'2021-03-23','17:48:48','','','1','','',11580,'20210304061524687-805235'),(7,'2021-03-24','18:34:07','','','1','','',10527,'20210324054526641-170307'),(8,'2021-03-25','18:20:51','','','0','','',11580,'20210304061524687-805235');
/*!40000 ALTER TABLE `request_cancel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `s_remittance_info`
--

DROP TABLE IF EXISTS `s_remittance_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `s_remittance_info` (
  `ri_code` bigint NOT NULL AUTO_INCREMENT,
  `ri_account_number` varchar(30) DEFAULT NULL,
  `ri_account_holder` varchar(50) DEFAULT NULL,
  `ri_choice_isuse` varchar(1) DEFAULT NULL,
  `seller_code` int NOT NULL,
  `bank_code` varchar(4) NOT NULL,
  PRIMARY KEY (`ri_code`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `s_remittance_info`
--

LOCK TABLES `s_remittance_info` WRITE;
/*!40000 ALTER TABLE `s_remittance_info` DISABLE KEYS */;
INSERT INTO `s_remittance_info` VALUES (1,'이유훈','110-311-703202','Y',10001,'088'),(2,'110311703202','한글성명','Y',10013,'034');
/*!40000 ALTER TABLE `s_remittance_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `seller`
--

DROP TABLE IF EXISTS `seller`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `seller` (
  `seller_code` bigint NOT NULL AUTO_INCREMENT,
  `seller_default_fee` varchar(5) DEFAULT '5',
  `login_id` varchar(50) NOT NULL,
  `area_code` varchar(5) NOT NULL COMMENT '지약전화번호 ?',
  `seller_name` varchar(50) DEFAULT '',
  `seller_email` varchar(100) DEFAULT '',
  `seller_picture` varchar(200) DEFAULT '',
  `seller_oauth_key` varchar(300) DEFAULT '',
  `seller_ph` varchar(11) DEFAULT '',
  `seller_company` varchar(100) DEFAULT '',
  `seller_emp_num` varchar(45) DEFAULT '',
  `seller_state` varchar(1) DEFAULT 'N',
  PRIMARY KEY (`seller_code`),
  UNIQUE KEY `login_id_UNIQUE` (`login_id`),
  UNIQUE KEY `seller_email_UNIQUE` (`seller_email`)
) ENGINE=InnoDB AUTO_INCREMENT=10016 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `seller`
--

LOCK TABLES `seller` WRITE;
/*!40000 ALTER TABLE `seller` DISABLE KEYS */;
INSERT INTO `seller` VALUES (10001,'5.0','seller1','001',NULL,NULL,NULL,NULL,'01022051229',NULL,NULL,'Y'),(10011,'5.0','dldbgns1@gmail.com','0','이유훈','dldbgns1@gmail.com','https://lh4.googleusercontent.com/-38zPUDT7L04/AAAAAAAAAAI/AAAAAAAAAAA/AMZuucnVqHnWmiIssbudbbl8vtWV7edC5w/s96-c/photo.jpg','sub','01022051229',NULL,NULL,'Y'),(10012,'5.0','dldbgns1@naver.com','0','이유훈','dldbgns1@naver.com','https://ssl.pstatic.net/static/pwe/address/img_profile.png','id','01022051229',NULL,NULL,'Y'),(10013,'5.0','AACWuUYd8Y60dKF2-sTso9GX','0','마마마마마','AACWuUYd8Y60dKF2-sTso9GX','','id','01022051229','상사명입력','NC-11053','Y'),(10015,'0','admin','0','관리자','admin','','','','','','Y');
/*!40000 ALTER TABLE `seller` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `spring_session`
--

DROP TABLE IF EXISTS `spring_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `spring_session` (
  `PRIMARY_ID` char(36) NOT NULL,
  `SESSION_ID` char(36) NOT NULL,
  `CREATION_TIME` bigint NOT NULL,
  `LAST_ACCESS_TIME` bigint NOT NULL,
  `MAX_INACTIVE_INTERVAL` int NOT NULL,
  `EXPIRY_TIME` bigint NOT NULL,
  `PRINCIPAL_NAME` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`PRIMARY_ID`),
  UNIQUE KEY `SPRING_SESSION_IX1` (`SESSION_ID`),
  KEY `SPRING_SESSION_IX2` (`EXPIRY_TIME`),
  KEY `SPRING_SESSION_IX3` (`PRINCIPAL_NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `spring_session`
--

LOCK TABLES `spring_session` WRITE;
/*!40000 ALTER TABLE `spring_session` DISABLE KEYS */;
INSERT INTO `spring_session` VALUES ('06cbc6f8-00ab-4c35-b8a7-2c0739174181','ed20a8f3-f334-4530-98be-c552f6801a4e',1619164109949,1619168980230,1440,1619170420230,'AACWuUYd8Y60dKF2-sTso9GX');
/*!40000 ALTER TABLE `spring_session` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `spring_session_attributes`
--

DROP TABLE IF EXISTS `spring_session_attributes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `spring_session_attributes` (
  `SESSION_PRIMARY_ID` char(36) NOT NULL,
  `ATTRIBUTE_NAME` varchar(200) NOT NULL,
  `ATTRIBUTE_BYTES` blob NOT NULL,
  PRIMARY KEY (`SESSION_PRIMARY_ID`,`ATTRIBUTE_NAME`),
  CONSTRAINT `SPRING_SESSION_ATTRIBUTES_FK` FOREIGN KEY (`SESSION_PRIMARY_ID`) REFERENCES `spring_session` (`PRIMARY_ID`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `spring_session_attributes`
--

LOCK TABLES `spring_session_attributes` WRITE;
/*!40000 ALTER TABLE `spring_session_attributes` DISABLE KEYS */;
INSERT INTO `spring_session_attributes` VALUES ('06cbc6f8-00ab-4c35-b8a7-2c0739174181','javax.servlet.jsp.jstl.fmt.request.charset',_binary '�\�\0t\0UTF-8'),('06cbc6f8-00ab-4c35-b8a7-2c0739174181','SPRING_SECURITY_CONTEXT',_binary '�\�\0sr\0=org.springframework.security.core.context.SecurityContextImpl\0\0\0\0\0\0\0L\0authenticationt\02Lorg/springframework/security/core/Authentication;xpsr\0Sorg.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken\0\0\0\0\0\0\0L\0authorizedClientRegistrationIdt\0Ljava/lang/String;L\0	principalt\0:Lorg/springframework/security/oauth2/core/user/OAuth2User;xr\0Gorg.springframework.security.authentication.AbstractAuthenticationTokenӪ(~nGd\0Z\0\rauthenticatedL\0authoritiest\0Ljava/util/Collection;L\0detailst\0Ljava/lang/Object;xpsr\0&java.util.Collections$UnmodifiableList�%1�\�\0L\0listt\0Ljava/util/List;xr\0,java.util.Collections$UnmodifiableCollectionB\0�\�^�\0L\0cq\0~\0xpsr\0java.util.ArrayListx�\��\�a�\0I\0sizexp\0\0\0w\0\0\0sr\0Borg.springframework.security.core.authority.SimpleGrantedAuthority\0\0\0\0\0\0\0L\0roleq\0~\0xpt\0SELLERxq\0~\0sr\0Horg.springframework.security.web.authentication.WebAuthenticationDetails\0\0\0\0\0\0\0L\0\rremoteAddressq\0~\0L\0	sessionIdq\0~\0xpt\00:0:0:0:0:0:0:1t\0$67d8806b-d9c8-4fcd-aa8b-9f93863c37e8t\0bandsr\0?org.springframework.security.oauth2.core.user.DefaultOAuth2User\0\0\0\0\0\0\0L\0\nattributest\0Ljava/util/Map;L\0authoritiest\0Ljava/util/Set;L\0nameAttributeKeyq\0~\0xpsr\0%java.util.Collections$UnmodifiableMap��t�B\0L\0mq\0~\0xpsr\0java.util.LinkedHashMap4�N\\l��\0Z\0accessOrderxr\0java.util.HashMap\��\�`\�\0F\0\nloadFactorI\0	thresholdxp?@\0\0\0\0\0w\0\0\0\0\0\0t\0namet\0마마마마마t\0profile_image_urlt\0\0t\0user_keyt\0AACWuUYd8Y60dKF2-sTso9GXt\0\ris_app_membersr\0java.lang.Boolean\� r�՜�\�\0Z\0valuexpt\0message_allowedq\0~\0)x\0sr\0%java.util.Collections$UnmodifiableSet��я��U\0\0xq\0~\0sr\0java.util.LinkedHashSet\�l\�Z�\�*\0\0xr\0java.util.HashSet�D�����4\0\0xpw\0\0\0?@\0\0\0\0\0q\0~\0xq\0~\0%'),('06cbc6f8-00ab-4c35-b8a7-2c0739174181','user',_binary '�\�\0sr\0\'com.escrow.kuda.model.oauth.SessionUser�y�nY|G\0I\0userCodeL\0emailt\0Ljava/lang/String;L\0nameq\0~\0L\0pictureq\0~\0L\0userPhq\0~\0L\0	userStateq\0~\0xp\0\0\'t\0AACWuUYd8Y60dKF2-sTso9GXt\0마마마마마t\0\0t\001022051229t\0Y');
/*!40000 ALTER TABLE `spring_session_attributes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `y_payment`
--

DROP TABLE IF EXISTS `y_payment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `y_payment` (
  `yp_code` varchar(25) NOT NULL,
  `yp_card_name` varchar(20) DEFAULT NULL,
  `yp_card_number` varchar(20) DEFAULT NULL,
  `yp_pg_trade_number` varchar(30) DEFAULT NULL,
  `yp_result_message` varchar(100) DEFAULT NULL,
  `yp_result_code` varchar(10) DEFAULT NULL,
  `yp_agree_number` varchar(20) DEFAULT NULL,
  `yp_instalment` varchar(2) DEFAULT NULL,
  `yp_amount` bigint DEFAULT NULL,
  `yr_code` varchar(25) NOT NULL,
  `ps_code` varchar(25) NOT NULL,
  `yp_card_type` varchar(45) DEFAULT NULL,
  `yp_cancel_part_type` varchar(45) DEFAULT NULL,
  `yp_pay_type` varchar(45) DEFAULT NULL,
  `yp_point_amount` varchar(45) DEFAULT NULL,
  `yp_pg_calcul_date` varchar(45) DEFAULT NULL,
  `yp_buyer_name` varchar(50) DEFAULT NULL,
  `yp_buyer_ph` varchar(50) DEFAULT NULL,
  `yp_payment_result_code` varchar(45) DEFAULT 'X',
  `yp_auth_date` varchar(10) DEFAULT NULL,
  `yp_auth_time` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`yp_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `y_payment`
--

LOCK TABLES `y_payment` WRITE;
/*!40000 ALTER TABLE `y_payment` DISABLE KEYS */;
INSERT INTO `y_payment` VALUES ('20210203122721249-413930',NULL,NULL,NULL,NULL,NULL,NULL,NULL,10000,'20210203122721249-677628','nicepay00m',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('20210203122721260-124165',NULL,NULL,NULL,NULL,NULL,NULL,NULL,100000,'20210203122721249-677628','nicepay00m',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('20210204115959464-788844',NULL,NULL,NULL,NULL,NULL,NULL,NULL,107527,'20210204115959463-360677','nicepay00m',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('20210216022720768-706390',NULL,NULL,NULL,NULL,NULL,NULL,NULL,130657,'20210216022720767-646905','nicepay00m',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('20210217121905937-350201',NULL,NULL,NULL,NULL,NULL,NULL,NULL,10753,'20210217121905936-294226','nicepay00m',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('20210217121905989-047235',NULL,NULL,NULL,NULL,NULL,NULL,NULL,107527,'20210217121905936-294226','nicepay00m',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('20210217121905994-205399',NULL,NULL,NULL,NULL,NULL,NULL,NULL,1075269,'20210217121905936-294226','nicepay00m',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('20210222023953242-952179',NULL,NULL,NULL,NULL,NULL,NULL,NULL,1053,'20210222023953242-301923','nicepay00m',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('20210222024132541-160151',NULL,NULL,NULL,NULL,NULL,NULL,NULL,1053,'20210222024132541-832705','nicepay00m',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('20210222122048066-230285',NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'20210222122048066-835912','nicepay00m',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('20210302031107310-063694',NULL,NULL,NULL,NULL,NULL,NULL,NULL,1053,'20210302031107310-139604','nicepay00m',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('20210302031107356-347518',NULL,NULL,NULL,NULL,NULL,NULL,NULL,10527,'20210302031107310-139604','nicepay00m',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('20210302031611707-334034',NULL,NULL,NULL,NULL,NULL,NULL,NULL,15790,'20210302031611707-886641','nicepay00m',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('20210302031918801-975799',NULL,NULL,NULL,NULL,NULL,NULL,NULL,15790,'20210302031918800-410287','nicepay00m',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('20210303062802037-854619',NULL,NULL,NULL,NULL,NULL,NULL,NULL,1053,'20210303062802037-363485','nicepay00m',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('20210303063417654-668277',NULL,NULL,NULL,NULL,NULL,NULL,NULL,1053,'20210303063417653-769095','nicepay00m',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('20210303073436212-899723',NULL,NULL,NULL,NULL,NULL,NULL,NULL,1053,'20210303073436212-814352','nicepay00m',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),('20210303073436218-330727','삼성','53614890****2341','nicepay00m01012103031940352378','카드 결제 성공','3001','43427913','00',1158,'20210303073436212-814352','nicepay00m','0','1','','000000000000',NULL,NULL,NULL,NULL,NULL,NULL),('20210304043816323-090797','삼성','53614890****2341','nicepay00m01012103041747415681','카드 결제 성공','3001','09145612','00',1053,'20210304043816323-043918','nicepay00m','0','1','','000000000000','2021-03-06',NULL,NULL,NULL,NULL,NULL),('20210304043816370-034651','삼성','53614890****2341','nicepay00m01012103041751205318','카드 결제 성공','3001','43602207','00',2106,'20210304043816323-043918','nicepay00m','0','1','','000000000000','2021-03-06',NULL,NULL,NULL,NULL,NULL),('20210304061524687-631175','삼성','53614890****2341','nicepay00m01012103041816023409','카드 결제 성공','3001','89491443','00',1053,'20210304061524687-805235','nicepay00m','0','1','','000000000000','2021-03-05','이유훈','01022051229','O',NULL,NULL),('20210304061524703-121842','삼성','53614890****2341','nicepay00m01012103041817023963','카드 결제 성공','3001','73131378','00',10527,'20210304061524687-805235','nicepay00m','0','1','','000000000000','2021-03-05','이유훈','01022051229','O',NULL,NULL),('20210305040047583-243430','삼성','53614890****2341','nicepay00m01012103051827413894','카드 결제 성공','3001','77275755','00',1053,'20210305040047582-578522','nicepay00m','0','1','','000000000000','2021-03-06','이유훈','01022051229','O','21-03-05','18:27:41'),('20210305040047631-180666','삼성','53614890****2341','nicepay00m01012103051830273768','카드 결제 성공','3001','28500352','00',10527,'20210305040047582-578522','nicepay00m','0','1','','000000000000','2021-03-06','이유훈','01022051229','O','21-03-05','18:30:27'),('20210324051208988-925834',NULL,NULL,NULL,NULL,NULL,NULL,NULL,10527,'20210324051208988-241693','nicepay00m',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'X',NULL,NULL),('20210324051209002-625962',NULL,NULL,NULL,NULL,NULL,NULL,NULL,1053,'20210324051208988-241693','nicepay00m',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'X',NULL,NULL),('20210324054526641-775539','삼성','53614890****2341','nicepay00m01012103241746072282','카드 결제 성공','3001','38182622','00',10527,'20210324054526641-170307','nicepay00m','0','1','','000000000000','2021-03-25','고객명','01022051229','O','21-03-24','17:46:08'),('20210324060657061-286609',NULL,NULL,NULL,NULL,NULL,NULL,NULL,130657,'20210324060657061-926883','nicepay00m',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'X',NULL,NULL),('20210324063810760-522036',NULL,NULL,NULL,NULL,NULL,NULL,NULL,10527,'20210324063810760-566800','nicepay00m',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'X',NULL,NULL),('20210324111559145-042395','삼성','53614890****2341','nicepay00m01012103241118073070','카드 결제 성공','3001','45145134','00',10527,'20210324111559144-381493','nicepay00m','0','1','','000000000000','2021-03-25','고객명','01022051229','O','21-03-24','11:18:07');
/*!40000 ALTER TABLE `y_payment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `y_payment_calcul`
--

DROP TABLE IF EXISTS `y_payment_calcul`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `y_payment_calcul` (
  `ypc_code` bigint NOT NULL AUTO_INCREMENT,
  `ypc_default_per` varchar(5) DEFAULT NULL,
  `ypc_seller_per` varchar(5) DEFAULT NULL,
  `ypc_pg_per` varchar(5) DEFAULT NULL,
  `ypc_seller_fee_money` bigint DEFAULT NULL,
  `ypc_pg_fee_money` bigint DEFAULT NULL,
  `ypc_payment_money` bigint DEFAULT NULL,
  `ypc_fee_profit_money` bigint DEFAULT NULL,
  `ypc_profit_money` bigint DEFAULT NULL,
  `yp_code` varchar(25) NOT NULL,
  PRIMARY KEY (`ypc_code`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `y_payment_calcul`
--

LOCK TABLES `y_payment_calcul` WRITE;
/*!40000 ALTER TABLE `y_payment_calcul` DISABLE KEYS */;
INSERT INTO `y_payment_calcul` VALUES (1,'5.0','2.0',NULL,2000,0,100000,7527,0,'20210204115959464-788844'),(2,'5.0','0.0',NULL,0,NULL,124124,6533,NULL,'20210216022720768-706390'),(5,'5.0','2.0',NULL,200,NULL,10000,753,NULL,'20210217121905937-350201'),(6,'5.0','2.0',NULL,2000,NULL,100000,7527,NULL,'20210217121905989-047235'),(7,'5.0','2.0',NULL,20000,NULL,1000000,75269,NULL,'20210217121905994-205399'),(8,'5.0','0.0',NULL,0,NULL,0,0,NULL,'20210222122048066-230285'),(9,'5.0','0.0',NULL,0,NULL,1000,53,NULL,'20210222023953242-952179'),(10,'5.0','0.0',NULL,0,NULL,1000,53,NULL,'20210222024132541-160151'),(11,'5.0','0.0',NULL,0,NULL,1000,53,NULL,'20210302031107310-063694'),(12,'5.0','0.0',NULL,0,NULL,10000,527,NULL,'20210302031107356-347518'),(13,'5.0','0.0',NULL,0,NULL,15000,790,NULL,'20210302031611707-334034'),(14,'5.0','0.0',NULL,0,NULL,15000,790,NULL,'20210302031918801-975799'),(15,'5.0','0.0',NULL,0,NULL,1000,53,NULL,'20210303062802037-854619'),(16,'5.0','0.0',NULL,0,NULL,1000,53,NULL,'20210303063417654-668277'),(17,'5.0','0.0',NULL,0,NULL,1000,53,NULL,'20210303073436212-899723'),(18,'5.0','0.0',NULL,0,NULL,1100,58,NULL,'20210303073436218-330727'),(19,'5.0','0.0','3.3',0,35,1000,53,NULL,'20210304043816323-090797'),(20,'5.0','0.0','3.3',0,70,2000,106,NULL,'20210304043816370-034651'),(21,'5.0','0.0','3.3',0,35,1000,53,NULL,'20210304061524687-631175'),(22,'5.0','0.0','3.3',0,348,10000,527,NULL,'20210304061524703-121842'),(23,'5.0','0.0','3.3',0,35,1000,53,NULL,'20210305040047583-243430'),(24,'5.0','0.0','3.3',0,348,10000,527,NULL,'20210305040047631-180666'),(25,'5.0','0.0','3.3',0,348,10000,527,NULL,'20210324111559145-042395'),(26,'5.0','0.0',NULL,0,NULL,10000,527,NULL,'20210324051208988-925834'),(27,'5.0','0.0',NULL,0,NULL,1000,53,NULL,'20210324051209002-625962'),(28,'5.0','0.0','3.3',0,348,10000,527,NULL,'20210324054526641-775539'),(29,'5.0','0.0',NULL,0,NULL,124124,6533,NULL,'20210324060657061-286609'),(30,'5.0','0.0',NULL,0,NULL,10000,527,NULL,'20210324063810760-522036');
/*!40000 ALTER TABLE `y_payment_calcul` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `y_receipt`
--

DROP TABLE IF EXISTS `y_receipt`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `y_receipt` (
  `yr_code` varchar(25) NOT NULL,
  `yr_product_name` varchar(100) DEFAULT NULL,
  `yr_calcul_date` varchar(10) DEFAULT NULL,
  `yr_schedule_date` varchar(10) DEFAULT NULL,
  `yr_fee_date` varchar(10) DEFAULT NULL,
  `yr_pg_calcul_date` varchar(10) DEFAULT NULL,
  `yr_deal_state` varchar(1) DEFAULT '0',
  `yr_creation_date` varchar(10) DEFAULT NULL,
  `yr_creation_time` varchar(8) DEFAULT NULL,
  `yr_buyer_name` varchar(50) DEFAULT NULL,
  `yr_buyer_ph` varchar(11) DEFAULT NULL,
  `seller_code` int NOT NULL,
  `yr_all_amount` bigint DEFAULT '0',
  `yr_accumulate_amount` bigint DEFAULT '0',
  `yr_payment_amount` bigint DEFAULT '0',
  `yr_fee_amount` bigint DEFAULT '0',
  PRIMARY KEY (`yr_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `y_receipt`
--

LOCK TABLES `y_receipt` WRITE;
/*!40000 ALTER TABLE `y_receipt` DISABLE KEYS */;
INSERT INTO `y_receipt` VALUES ('1533115333','11가1111-산타페(2019)','','','','','','2021-01-21','19:11:11','테스터','01012345678',1,NULL,NULL,0,0),('20210121023930099-201112','11가1111-산타페(2019)',NULL,NULL,NULL,NULL,NULL,'2021-01-21','14:39:30','테스터','01012345678',1,NULL,NULL,0,0),('20210121124734180-866364','11가1111-산타페(2019)',NULL,NULL,NULL,NULL,NULL,'2021-01-21','19:11:11','테스터','01012345678',1,NULL,NULL,0,0),('20210121125055059-112966','11가1111-산타페(2019)',NULL,NULL,NULL,NULL,NULL,'2021-01-21','12:50:55','테스터','01012345678',1,NULL,NULL,0,0),('20210122033919649-440591','11가1111-산타페(2019)',NULL,NULL,NULL,NULL,NULL,'2021-01-22','15:39:19','테스터','01012345678',1,0,0,0,0),('20210122041303336-621260','11가1111-산타페(2019)',NULL,NULL,NULL,NULL,NULL,'2021-01-22','16:13:03','테스터','01012345678',1,0,0,0,0),('20210122041322330-426585','11가1111-산타페(2019)',NULL,NULL,NULL,NULL,NULL,'2021-01-22','16:13:22','테스터','01012345678',1,0,0,0,0),('20210122041347423-341444','11가1111-산타페(2019)',NULL,NULL,NULL,NULL,NULL,'2021-01-22','16:13:47','테스터','01012345678',1,0,0,0,0),('20210122041355989-277976','11가1111-산타페(2019)',NULL,NULL,NULL,NULL,NULL,'2021-01-22','16:13:55','테스터','01012345678',1,0,0,0,0),('20210122041419893-551382','11가1111-산타페(2019)',NULL,NULL,NULL,NULL,NULL,'2021-01-22','16:14:19','테스터','01012345678',1,0,0,0,0),('20210122061304683-341664','11차1234',NULL,NULL,NULL,NULL,NULL,'2021-01-22','18:13:04','111111','01012345678',0,0,0,0,0),('20210125042814406-131634','11가1111',NULL,NULL,NULL,NULL,'0','2021-01-25','16:28:14','고객명','01012345678',0,20000,0,0,0),('20210125042900566-395062','11가1111',NULL,NULL,NULL,NULL,'0','2021-01-25','16:29:00','고객명2','01012345678',0,6000,0,0,0),('20210125052503518-751015','11가1111',NULL,NULL,NULL,NULL,'0','2021-01-25','17:25:03','홍길동123','01012345678',0,11000,0,0,0),('20210125125223592-161184','11가1111',NULL,NULL,NULL,NULL,NULL,'2021-01-25','12:52:23','홍길동','0102345678',0,113333,0,0,0),('20210202044145551-451859','11가1111',NULL,NULL,NULL,NULL,'0','2021-02-02','16:41:45','고객명','01012345678',0,100000,0,0,0),('20210203122721249-677628','차량번호',NULL,NULL,NULL,NULL,'0','2021-02-03','12:27:21','고객명','수신자번호',10001,110000,0,0,0),('20210204115959463-360677','111가1111',NULL,NULL,NULL,NULL,'0','2021-02-04','11:59:59','테스트고객명','01012345678',10001,107527,0,0,0),('20210216022720767-646905','124124',NULL,NULL,NULL,NULL,'0','2021-02-16','14:27:20','124124','124124',10001,130657,0,0,0),('20210217121905936-294226','315로1173',NULL,NULL,NULL,NULL,'0','2021-02-17','12:19:05','고객명','01022051229',10001,1193549,0,1110000,0),('20210222023953242-301923','11가1111',NULL,NULL,NULL,NULL,'0','2021-02-22','14:39:53','고객명','01022051229',10001,1053,0,1000,0),('20210222024132541-832705','22가2222',NULL,NULL,NULL,NULL,'0','2021-02-22','14:41:32','고객명','01022051229',10001,1053,0,1000,0),('20210222122048066-835912','',NULL,NULL,NULL,NULL,'0','2021-02-22','12:20:48','','',10001,0,0,0,0),('20210302031107310-139604','11가1111',NULL,NULL,NULL,NULL,'0','2021-03-02','15:11:07','고객명','01022051229',10001,11580,0,11000,0),('20210302031611707-886641','11가1111',NULL,NULL,NULL,NULL,'0','2021-03-02','15:16:11','고객명','01022051229',10001,15790,0,15000,0),('20210302031918800-410287','11가2321',NULL,NULL,NULL,NULL,'0','2021-03-02','15:19:18','고객명','01022051229',10012,15790,0,15000,0),('20210303062802037-363485','11가1111',NULL,NULL,NULL,NULL,'0','2021-03-03','18:28:02','고객명','01022051229',10012,1053,0,1000,0),('20210303063417653-769095','13고1131',NULL,NULL,NULL,NULL,'0','2021-03-03','18:34:17','고객명','01022051229',10012,1053,0,1000,0),('20210303073436212-814352','113가1151',NULL,NULL,NULL,NULL,'0','2021-03-03','19:34:36','고객명','01022051229',10012,2211,0,2100,0),('20210304043816323-043918','11가1111',NULL,'2021-03-06',NULL,NULL,'9','2021-03-04','16:38:16','고객명','01022051229',10012,3159,3159,3000,0),('20210304061524687-805235','39하1301',NULL,'2021-03-05',NULL,NULL,'2','2021-03-04','18:15:24','이유훈','01022051229',10013,11580,11580,11000,0),('20210304113332589-297301','11가1111-산타페(2019)',NULL,NULL,NULL,NULL,'0','2021-03-04','11:33:32','테스터','01012345678',1,0,0,0,0),('20210305040047582-578522','11가1111',NULL,'2021-03-06',NULL,NULL,'9','2021-03-05','16:00:47','이유훈','01022051229',10013,11580,11580,11000,0),('20210324051208988-241693','111가3048',NULL,NULL,NULL,NULL,'0','2021-03-24','17:12:09','테스트차량고객명','01012345678',10013,11580,0,0,0),('20210324054526641-170307','차량번호111',NULL,'2021-03-24',NULL,NULL,'9','2021-03-24','17:45:26','고객명','01022051229',10013,10527,10527,0,0),('20210324060657061-926883','124124124',NULL,NULL,NULL,NULL,'0','2021-03-24','18:06:57','12412412','01022051229',10013,130657,0,0,0),('20210324063810760-566800','차량번호111',NULL,NULL,NULL,NULL,'0','2021-03-24','18:38:10','이유훈','01022051229',10013,10527,0,10000,0),('20210324111559144-381493','111가1111',NULL,'2021-03-24',NULL,NULL,'9','2021-03-24','11:15:59','고객명','01022051229',10013,10527,10527,0,0);
/*!40000 ALTER TABLE `y_receipt` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `y_receipt_fee`
--

DROP TABLE IF EXISTS `y_receipt_fee`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `y_receipt_fee` (
  `yrf_code` int NOT NULL AUTO_INCREMENT,
  `yrf_default_per` varchar(5) DEFAULT NULL,
  `yrf_seller_per` varchar(5) DEFAULT NULL,
  `yrf_pg_per` varchar(5) DEFAULT NULL,
  `yrf_default_fee` bigint DEFAULT NULL,
  `yrf_seller_fee` bigint DEFAULT NULL,
  `yrf_pg_fee` bigint DEFAULT NULL,
  `yr_code` varchar(25) NOT NULL,
  PRIMARY KEY (`yrf_code`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `y_receipt_fee`
--

LOCK TABLES `y_receipt_fee` WRITE;
/*!40000 ALTER TABLE `y_receipt_fee` DISABLE KEYS */;
INSERT INTO `y_receipt_fee` VALUES (1,'5.0','3.0',NULL,NULL,NULL,NULL,'20210125052503518-751015'),(2,'5.0','2.0',NULL,NULL,NULL,NULL,'20210202044145551-451859'),(3,'5.0','1.0',NULL,NULL,NULL,NULL,'20210203122721249-677628');
/*!40000 ALTER TABLE `y_receipt_fee` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-04-23 18:12:29
